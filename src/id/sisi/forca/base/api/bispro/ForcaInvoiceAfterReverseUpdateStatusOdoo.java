/*----------------------------------------------------------------------------
Product        : Forca ERP Foundation                                
Author         : Budi Darmanto                                                                                
Email          : (1) budiskom93@gmail.com                                 
Subject        : Set status Odoo via API
Issue          :                               
---------------------------------------------------------------------------*/
package id.sisi.forca.base.api.bispro;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInvoice;
import org.compiere.model.MSysConfig;
import org.compiere.model.PO;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import id.sisi.forca.base.api.interfacez.ForcaPerformExecution;
import id.sisi.forca.base.api.util.WsUtil;

public class ForcaInvoiceAfterReverseUpdateStatusOdoo implements ForcaPerformExecution {
    protected CLogger log = CLogger.getCLogger(getClass());
    private PO po = null;
    Properties ctx = null;

    public ForcaInvoiceAfterReverseUpdateStatusOdoo(Properties ctx, PO po, String trxName) {
        this.po = po;
        this.ctx = ctx;
    }

    @Override
    public void execute() {
        MInvoice i = (MInvoice) po;
        WsUtil wu = new WsUtil();
        String HR_HOST = MSysConfig.getValue("HR_HOST");
        log.warning("Call API Odoo");

        if (HR_HOST != null) {
            try {
                Map<String, String> params = new LinkedHashMap<>();
                String url = HR_HOST + "/api/ws/authentication/login";
                // MUser u =
                 ForcaObject.getObject(ctx, MUser.Table_Name, Env.getAD_User_ID(ctx), null);
                params.put("username", "admin");
                params.put("password", "admin");
                Map<String, String> mapHeader = new LinkedHashMap<>();
                mapHeader.put("Content-Type", "application/x-www-form-urlencoded");
                String sObject = wu.postHTML(url, params, mapHeader);
                JSONObject jObject = new JSONObject(sObject);
                JSONArray jaResult = jObject.getJSONArray("resultdata");
                String token = "";
                for (int a = 0; a < jaResult.length(); a++) {
                    token = jaResult.getJSONObject(a).getString("access_token");
                }

                url = HR_HOST + "/api/ws/transaction/ReversePayslipBatch";
                params.clear();
                params.put("documentno", i.getDocumentNo());
                params.put("docstatus", DocAction.STATUS_Reversed);
                params.put("access_token", token);
                log.warning("Call HR API to Update Invoice Document Status");
                String jUpdate = wu.postHTML(url, params, mapHeader);
                log.warning("Success : " + jUpdate);
            } catch (JSONException e) {
                log.warning(e.getMessage());
            } catch (IOException e) {
                log.warning(e.getMessage());
            } catch (Exception e) {
                log.warning(e.getMessage());
                throw new AdempiereException("Error");
            }
        }
    }

}
