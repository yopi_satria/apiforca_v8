package id.sisi.forca.base.api.component;
/*----------------------------------------------------------------------------
Product        : Forca ERP Foundation                                      
Author         : Budi Darmanto                                   
Email          : (1) budiskom93@gmail.com                                    
Subject        :                                     
---------------------------------------------------------------------------*/

import org.adempiere.base.event.AbstractEventHandler;
import org.adempiere.base.event.IEventTopics;
import org.compiere.model.I_C_Invoice;
import org.compiere.model.PO;
import org.osgi.service.event.Event;
import id.sisi.forca.base.api.event.doc.MInvoiceDocEvent;

public class EventFactory extends AbstractEventHandler {

 @Override
 protected void initialize() {
     /*-- I_C_Invoice --*/
     registerTableEvent(IEventTopics.DOC_AFTER_REVERSEACCRUAL, I_C_Invoice.Table_Name);
     registerTableEvent(IEventTopics.DOC_AFTER_REVERSECORRECT, I_C_Invoice.Table_Name);
 }

 @Override
 protected void doHandleEvent(Event event) {
     if (event.getTopic().contains(IEventTopics.DOC_EVENT_PREFIX))
         doHandleDocEvent(event);
     else if (event.getTopic().contains(IEventTopics.MODEL_EVENT_PREFIX))
         doHandleModelEvent(event);
 }

 private void doHandleModelEvent(Event event) {
     PO po = getPO(event);

//     switch (po.get_TableName()) {
//         case I_C_Invoice.Table_Name:
//             MInvoiceDocEvent.validate(event, po);
//
//             break;
//         
//         default:
//
//             break;
//     }
 }

 
 private void doHandleDocEvent(Event event) {
     PO po = getPO(event);

     switch (po.get_TableName()) {
         case I_C_Invoice.Table_Name:
             MInvoiceDocEvent.validate(event, po);

             break;
         
         default:

             break;
     }
 }
}
