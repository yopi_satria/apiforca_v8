/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Novan Dwi Nugroho                                                                                
   Email          : novan.nugroho@sisi.id                                                                  
   Subject        :                                  
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.component;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import id.sisi.forca.base.api.process.GeneratePaymentProcess;
import id.sisi.forca.base.api.process.GenerateBusinessPartner;
import id.sisi.forca.base.api.process.GenerateSalesOrder;

public class ProcessFactory implements IProcessFactory {

    @Override
    public ProcessCall newProcessInstance(String className) {
        ProcessCall process = null;
        Class<?> processClass = null;
        
        
        if (className.equals("id.sisi.forca.base.api.process.PosGenerateBusinessPartner")) {
            processClass = GenerateBusinessPartner.class;
        } else if (className.equals("id.sisi.forca.base.api.process.PosGenerateSalesOrder")) {
            processClass = GenerateSalesOrder.class;
        } else if (className.equals("id.sisi.forca.base.api.process.PosGeneratePayment")) {
            processClass = GeneratePaymentProcess.class;
        }
        
        if (processClass != null) {
            try {
                process = (ProcessCall) processClass.newInstance();
                return process;
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

}
