/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Novan Dwi Nugroho                                                                                
   Email          : novan.nugroho@sisi.id                                                                  
   Subject        :                                  
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.component;

import java.sql.ResultSet;
import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;
import id.sisi.forca.base.api.model.MFORCAUserMobile;
import id.sisi.forca.base.api.model.MFORCAWsToken;
import id.sisi.forca.base.api.model.MFORCAAttendance;
import id.sisi.forca.base.api.model.MFORCABranch;
import id.sisi.forca.base.api.model.MFORCACustomerPOS;
import id.sisi.forca.base.api.model.MFORCALeave;
import id.sisi.forca.base.api.model.MFORCAPOSOrderRequest;
import id.sisi.forca.base.api.model.MFORCAPOSPayment;

public class ModelFactory implements IModelFactory {

    @Override
    public Class<?> getClass(String tableName) {
        if (tableName.equalsIgnoreCase(MFORCAWsToken.Table_Name)) {
            return MFORCAWsToken.class;
        } 
        else if (tableName.equalsIgnoreCase(MFORCAPOSPayment.Table_Name)){
            return MFORCAPOSPayment.class;
        }
        else if (tableName.equalsIgnoreCase(MFORCACustomerPOS.Table_Name)){
            return MFORCACustomerPOS.class;
        }
        else if (tableName.equalsIgnoreCase(MFORCAPOSOrderRequest.Table_Name)){
            return MFORCAPOSOrderRequest.class;
        }
        else if (tableName.equalsIgnoreCase(MFORCAUserMobile.Table_Name)) {
            return MFORCAUserMobile.class;
        }
        else if (tableName.equalsIgnoreCase(MFORCAAttendance.Table_Name)) {
            return MFORCAAttendance.class;
        }
        else if (tableName.equalsIgnoreCase(MFORCABranch.Table_Name)) {
            return MFORCABranch.class;
        }
        else if (tableName.equalsIgnoreCase(MFORCALeave.Table_Name)) {
            return MFORCALeave.class;
        }
        return null;
    }

    @Override
    public PO getPO(String tableName, int Record_ID, String trxName) {
        if (tableName.equalsIgnoreCase(MFORCAWsToken.Table_Name)) {
            return new MFORCAWsToken(Env.getCtx(), Record_ID, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCACustomerPOS.Table_Name)){
            return new MFORCACustomerPOS(Env.getCtx(), Record_ID, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCAPOSOrderRequest.Table_Name)){
            return new MFORCAPOSOrderRequest(Env.getCtx(), Record_ID, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCAPOSPayment.Table_Name)){
            return new MFORCAPOSPayment(Env.getCtx(), Record_ID, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCAUserMobile.Table_Name)) {
            return new MFORCAUserMobile(Env.getCtx(), Record_ID, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCAAttendance.Table_Name)) {
            return new MFORCAAttendance(Env.getCtx(), Record_ID, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCABranch.Table_Name)) {
            return new MFORCABranch(Env.getCtx(), Record_ID, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCALeave.Table_Name)) {
            return new MFORCALeave(Env.getCtx(), Record_ID, trxName);
        }
        return null;
    }

    @Override
    public PO getPO(String tableName, ResultSet rs, String trxName) {
        if (tableName.equalsIgnoreCase(MFORCAWsToken.Table_Name)) {
            return new MFORCAWsToken(Env.getCtx(), rs, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCACustomerPOS.Table_Name)){
            return new MFORCACustomerPOS(Env.getCtx(), rs, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCAPOSOrderRequest.Table_Name)){
            return new MFORCAPOSOrderRequest(Env.getCtx(), rs, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCAPOSPayment.Table_Name)){
            return new MFORCAPOSPayment(Env.getCtx(), rs, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCAUserMobile.Table_Name)) {
            return new MFORCAUserMobile(Env.getCtx(), rs, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCAAttendance.Table_Name)) {
            return new MFORCAAttendance(Env.getCtx(), rs, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCABranch.Table_Name)) {
            return new MFORCABranch(Env.getCtx(), rs, trxName);
        }
        else if (tableName.equalsIgnoreCase(MFORCALeave.Table_Name)) {
            return new MFORCALeave(Env.getCtx(), rs, trxName);
        }
        return null;
    }

}
