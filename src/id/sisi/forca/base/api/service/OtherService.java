/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                               
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                  
   Subject        : Forca Intgration Pos                                 
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.service;

import org.compiere.util.Trx;
import id.sisi.forca.base.api.impl.OtherImpl;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.Constants;

public class OtherService implements ModelService<ResponseData, ParamRequest, String> {
    private OtherImpl otherImpl = new OtherImpl();

    @Override
    public ResponseData get(ParamRequest param, String str) {
        return null;
    }

    @Override
    public ResponseData set(ParamRequest param, String tsr) {
        return null;
    }

    @Override
    public ResponseData insert(ParamRequest param, String str) {
        ResponseData result = new ResponseData();
        String trxLocalName = Trx.createTrxName("get" + str);
        Trx trxLocal = Trx.get(trxLocalName, true);
//        try {
//            switch (str) {
//                case "TempBPartner":
//                    result = otherImpl.insertTempBPartner(param, trxLocal);
//                    break;
//                case "TempOrder":
//                    result = otherImpl.insertTempOrder(param, trxLocal);
//                    break;
//                case "TempPayment":
//                    result = otherImpl.insertTempPayment(param, trxLocal);
//                    break;
//                default:
//                    result = ResponseData.errorResponse(Constants.ERROR_UnknownAPI);
//                    break;
//            }
//        } catch (Exception e) {
//            result = ResponseData.errorResponse(e.getMessage());
//            trxLocal.rollback();
//        } finally {
//            if (trxLocal.isActive()) {
//                trxLocal.commit();
//            }
//            trxLocal.close();
//        }
        return result;
    }
}
