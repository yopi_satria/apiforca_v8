/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                               
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                  
   Subject        : Forca Intgration Pos                                 
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.service;

import org.compiere.util.Trx;
import id.sisi.forca.base.api.impl.TransactionImpl;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.Constants;

public class TransactionService implements ModelService<ResponseData, ParamRequest, String> {
    private TransactionImpl transImpl = null;

    @Override
    public ResponseData get(ParamRequest param, String str) {
        ResponseData result = new ResponseData();
        String trxLocalName = Trx.createTrxName("get" + str);
        Trx trxLocal = Trx.get(trxLocalName, true);
        transImpl = new TransactionImpl();
//        try {
//            switch (str) {
//                case "Order":
//                    result = transImpl.getOrder(param, trxLocal);
//                    break;
//                case "OrderList":
//                    result = transImpl.getOrderList(param, trxLocal);
//                    break;
//                case "OrderLine":
//                    result = transImpl.getOrderLine(param, trxLocal);
//                    break;
//                case "InOut" :
//                    result = transImpl.getInOut(param, trxLocal);
//                    break;
//                case "InOutList" :
//                    result = transImpl.getInOutList(param, trxLocal);
//                    break;
//                case "InOutLine" :
//                    result = transImpl.getInOutLine(param, trxLocal);
//                    break;
//                case "Invoice" :
//                    result = transImpl.getInvoice(param, trxLocal);
//                    break;
//                case "InvoiceMulti" :
//                    result = transImpl.getInvoiceMulti(param, trxLocal);
//                    break;
//                case "InvoiceList" :
//                    result = transImpl.getInvoiceList(param, trxLocal);
//                    break;
//                case "InvoiceLine" :
//                    result = transImpl.getInvoiceLine(param, trxLocal);
//                    break;
//                case "DocumentApprovedHeader":
//                    result = transImpl.getDocumentApproved(param, "Header", trxLocal);
//                    break;
//                case "DocumentApprovedDetail":
//                    result = transImpl.getDocumentApproved(param, "Detail", trxLocal);
//                    break;
//                case "UserInfo":
//                    result = transImpl.getUserInfo(param, trxLocal);
//                    break;
//                case "Location":
//                    result = transImpl.getLocation(param, trxLocal);
//                    break;
//                case "LeaveType":
//                    result = transImpl.getLeaveType(param, trxLocal);
//                    break;
//                case "Leave":
//                    result = transImpl.getLeave(param, trxLocal);
//                    break;
//                case "WorkflowAll":
//                    result = transImpl.getWorkflow(param, "All", trxLocal);
//                    break;
//                case "WorkflowGroup":
//                    result = transImpl.getWorkflow(param, "Group", trxLocal);
//                    break;
//                case "WorkflowDetail":
//                    result = transImpl.getWorkflow(param, "Detail", trxLocal);
//                    break;
//                case "WorkflowList":
//                    result = transImpl.getWorkflow(param, "List", trxLocal);
//                    break;
//                case "InventoryMove":
//                    result = transImpl.getInventoryMove(param, trxLocal);
//                    break;
//                case "InventoryMoveList":
//                    result = transImpl.getInventoryMoveList(param, trxLocal);
//                    break;
//                case "InventoryMoveLine":
//                    result = transImpl.getInventoryMoveLine(param, trxLocal);
//                    break;
//                case "InvoiceCustomerSAAS":
//                    result = transImpl.getInvoiceCustomerSAAS(param, trxLocal);
//                    break;
//                case "getAttendanceHistory":
//                    result = transImpl.getAttendanceHistory(param, trxLocal);
//                    break;
//                default:
//                    result = ResponseData.errorResponse(Constants.ERROR_UnknownAPI);
//                    break;
//            }
//        } catch (Exception e) {
//            result = ResponseData.errorResponse(e.getMessage());
//            trxLocal.rollback();
//        } finally {
//            if (trxLocal.isActive()) {
//                trxLocal.commit();
//            }
//            trxLocal.close();
//        }
        return result;
    }


    @Override
    public ResponseData set(ParamRequest param, String str) {
        ResponseData result = new ResponseData();
        String trxLocalName = Trx.createTrxName("set" + str);
        Trx trxLocal = Trx.get(trxLocalName, true);
        transImpl = new TransactionImpl();
//        try {
//            switch (str) {
//                case "Attendance":
//                    result = transImpl.insertAttendance(param, trxLocal);
//                    break;
//                case "Leave":
//                    result = transImpl.insertLeave(param, trxLocal);
//                    break;
//                case "OrderComplete":
//                    result = transImpl.setOrderDoc(param, "CO", trxLocal);
//                    break;
//                case "OrderVoid":
//                    result = transImpl.setOrderDoc(param, "VO", trxLocal);
//                    break;
//                case "OrderLine":
//                    result = transImpl.setOrderLine(param, trxLocal);
//                    break;
//                case "InventoryMoveComplete":
//                    result = transImpl.setInventoryMoveDoc(param, "CO", trxLocal);
//                    break;
//                case "InventoryMoveVoid":
//                    result = transImpl.setInventoryMoveDoc(param, "VO", trxLocal);
//                    break;
//                case "InOutComplete":
//                    result = transImpl.setInOutDoc(param, "CO", trxLocal);
//                    break;
//                case "InOutVoid":
//                    result = transImpl.setInOutDoc(param, "VO", trxLocal);
//                    break;
//                case "InvoiceComplete":
//                    result = transImpl.setInvoiceDoc(param, "CO", trxLocal);
//                    break;
//                case "InvoiceVoid":
//                    result = transImpl.setInvoiceDoc(param, "VO", trxLocal);
//                    break;
//                case "MultiDocumentApproved":
//                    result = transImpl.setMultiDocumentApproved(param, trxLocal);
//                    break;
//                case "InventoryMoveLine":
//                    result = transImpl.setInventoryMoveLine(param, trxLocal);
//                    break;
//                case "InOutLine":
//                    result = transImpl.setInOutLine(param, trxLocal);
//                    break;
//                case "InvoiceLine":
//                    result = transImpl.setInvoiceLine(param, trxLocal);
//                    break;
//                case "Payment":
//                    result = transImpl.setPayment(param, trxLocal);
//                    break;
//                case "PaymentAllocate":
//                    result = transImpl.setPaymentAllocate(param, trxLocal);
//                    break;
//                case "PaymentComplete":
//                    result = transImpl.setPaymentDoc(param, "CO", trxLocal);
//                    break;
//                case "PaymentVoid":
//                    result = transImpl.setPaymentDoc(param, "VO", trxLocal);
//                    break;
//                case "PersonID":
//                    result = transImpl.insertPersonID(param, trxLocal);
//                    break;
//                case "DeviceID":
//                    result = transImpl.insertDeviceID(param, trxLocal);
//                    break;
//                case "IAInventoryMove":
//                    result = transImpl.inactiveInventoryMove(param, trxLocal);
//                    break;
//                case "IAInOut":
//                    result = transImpl.inactiveInOut(param, trxLocal);
//                    break;
//                case "IAOrder":
//                    result = transImpl.inactiveOrder(param, trxLocal);
//                    break;
//                case "delInventoryMove":
//                    result = transImpl.deleteInventoryMove(param, trxLocal);
//                    break;
//                case "delInventoryMoveLine":
//                    result = transImpl.deleteInventoryMoveLine(param, trxLocal);
//                    break;
//                case "delInOut":
//                    result = transImpl.deleteInOut(param, trxLocal);
//                    break;
//                case "delInOutLine":
//                    result = transImpl.deleteInOutLine(param, trxLocal);
//                    break;
//                case "delOrder":
//                    result = transImpl.deleteOrder(param, trxLocal);
//                    break;
//                case "delOrderLine":
//                    result = transImpl.deleteOrderLine(param, trxLocal);
//                    break;
//                case "calculateUOMConversion":
//                    result = transImpl.calculateUOMConversion(param, trxLocal);
//                    break;
//                default:
//                    result = ResponseData.errorResponse(Constants.ERROR_UnknownAPI);
//                    break;
//            }
//        } catch (Exception e) {
//            result = ResponseData.errorResponse(e.getMessage());
//            trxLocal.rollback();
//        } finally {
//            if (trxLocal.isActive()) {
//                trxLocal.commit();
//            }
//            trxLocal.close();
//        }
        return result;
    }


    @Override
    public ResponseData insert(ParamRequest param, String str) {
        return null;
    }


}
