/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Fitriani Rohmah Hardianti                                                                                
   Email          : fitrianirohmahhardianti@gmail.com                                                                 
   Subject        : Master Service                              
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.service;

import org.compiere.util.Trx;
import id.sisi.forca.base.api.impl.MasterImpl;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.Constants;

public class MasterService implements ModelService<ResponseData, ParamRequest, String>{
    private MasterImpl masterImpl = null;
    
    @Override
    public ResponseData get(ParamRequest param, String str) {
        return null;
    }

    @Override
    public ResponseData set(ParamRequest param, String str) {
        ResponseData result = new ResponseData();
        String trxLocalName = Trx.createTrxName("set" + str);
        Trx trxLocal = Trx.get(trxLocalName, true);
        masterImpl = new MasterImpl();
//        try {
//            switch (str) {
//                case "BPartner":
//                    result = masterImpl.setBPartner(param, trxLocal);
//                    break;
//                case "BPartnerLocation":
//                    result = masterImpl.setBPartnerLocation(param, trxLocal);
//                    break;
//                case "User":
//                    result = masterImpl.setUser(param, trxLocal);
//                    break;
//                case "Uom":
//                    result = masterImpl.setUom(param, trxLocal);
//                    break;
//                case "Org":
//                    result = masterImpl.setOrg(param, trxLocal);
//                    break;
//                case "Product":
//                    result = masterImpl.setProduct(param, trxLocal);
//                    break;
//                case "ProductCategory":
//                    result = masterImpl.setProductCategory(param, trxLocal);
//                    break;
//                case "TaxCategory":
//                    result = masterImpl.setTaxCategory(param, trxLocal);
//                    break;
//                case "Tax":
//                    result = masterImpl.setTax(param, trxLocal);
//                    break;
//                case "Channel":
//                    result = masterImpl.setChannel(param, trxLocal);
//                    break;
//                case "Campaign":
//                    result = masterImpl.setCampaign(param, trxLocal);
//                    break;
//                case "UserMobile":
//                    result = masterImpl.deleteUserMobile(param, trxLocal);
//                    break;
//                default:
//                    result = ResponseData.errorResponse(Constants.ERROR_UnknownAPI);
//                    break;
//            }
//        } catch (Exception e) {
//            trxLocal.rollback();
//        } finally {
//            if (trxLocal.isActive()) {
//                trxLocal.commit();
//            }
//            trxLocal.close();
//        }
        return result;
    }

    @Override
    public ResponseData insert(ParamRequest param, String str) {
        ResponseData result = new ResponseData();
        String trxLocalName = Trx.createTrxName("insert" + str);
        Trx trxLocal = Trx.get(trxLocalName, true);
        masterImpl = new MasterImpl();
//        try {
//            switch (str) {
//                case "UserMobile":
//                    result = masterImpl.insertUserMobile(param, trxLocal);
//                    break;
//                default:
//                    result = ResponseData.errorResponse(Constants.ERROR_UnknownAPI);
//                    break;
//            }
//        } catch (Exception e) {
//            trxLocal.rollback();
//        } finally {
//            if (trxLocal.isActive()) {
//                trxLocal.commit();
//            }
//            trxLocal.close();
//        }
        return result;
    }
    

}
