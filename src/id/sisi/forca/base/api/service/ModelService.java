/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Novan Dwi Nugroho                                                                                
   Email          : novan.nugroho@sisi.id                                                                  
   Subject        :                                  
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.service;

public interface ModelService <T,U,V>{
    
    public T get(U objectData, V String);
    public T set(U objectData, V String);
    public T insert(U objectData, V String);

}
