/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : Authentication Api                                
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.filter;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import javax.ws.rs.core.Response.Status;
import org.compiere.model.MSession;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;
import id.sisi.forca.base.api.model.MFORCAWsToken;
import id.sisi.forca.base.api.model.X_FORCA_WS_Token;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.Constants;

public class AuthenticationApi {

    List<String> urlprefix = Arrays.asList(
            "/test/",
            "/authentication/",
            "/api-docs",
            "/swaggerui"
            );

    public static Properties wsctx = new Properties();
    Status status = Status.OK;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public AuthenticationApi() {
        super();
    }

    public Boolean nonHeaderAuth(String path) {
        Boolean valid = false;

        for (String url : urlprefix) {
            if (path.contains(url)) {
                valid = true;
            }
        }

        return valid;
    }

    public static Properties getDefaultCtx() {
        wsctx.setProperty(Constants.CTX_LANGUAGE, "en_US");
        wsctx.setProperty(Constants.CTX_CLIENT, "0");
        return wsctx;
    }

    public static void setContext(Integer user_id, Integer client_id, Integer role_id,
            Integer org_id, Integer warehouse_id) {

        wsctx.setProperty(Constants.CTX_LANGUAGE, "en_US");
        wsctx.setProperty(Constants.CTX_USER, user_id.toString());
        wsctx.setProperty(Constants.CTX_CLIENT, client_id.toString());
        wsctx.setProperty(Constants.CTX_ROLE, role_id.toString());
        wsctx.setProperty(Constants.CTX_ORG, org_id.toString());
        if (warehouse_id > 0) {
            wsctx.setProperty(Constants.CTX_WAREHOUSE, warehouse_id.toString());
        }
        Env.setCtx(wsctx);
    }


    public static void setContextByToken(String token) {
        StringBuilder sql = new StringBuilder();
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            sql.append(""
                    + "SELECT "
                    + "  * "
                    + "FROM "
                    + "  forca_ws_token "
                    + "WHERE "
                    + "  forca_ws_token_uu ilike ? "
                    + "FETCH FIRST 1 ROWS ONLY");
            statement = DB.prepareStatement(sql.toString(), null);
            statement.setString(1, token);
            rs = statement.executeQuery();
            while (rs.next()) {
                wsctx.setProperty(Constants.CTX_CLIENT, rs.getString("ad_client_id"));
                wsctx.setProperty(Constants.CTX_ORG, rs.getString("ad_org_id"));
                wsctx.setProperty(Constants.CTX_ROLE, rs.getString("ad_role_id"));
                wsctx.setProperty(Constants.CTX_USER, rs.getString("ad_user_id"));
                if (!Util.isEmpty(rs.getString("m_warehouse_id"))) {
                    wsctx.setProperty(Constants.CTX_WAREHOUSE, rs.getString("m_warehouse_id"));
                }
                wsctx.setProperty(Constants.CTX_TOKEN, rs.getString("forca_ws_token_uu"));
                Env.setCtx(wsctx);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DB.close(rs, statement);
            rs = null;
            statement = null;
        }
    }

    public static void logoutSession() {
        MSession msesion = MSession.get(Env.getCtx(), false);
        msesion.logout();
    }

    public Boolean validasiToken(String token) {
        Boolean valid = false;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Env.setCtx(getDefaultCtx());
        try {

            String sql= ""
                    + "SELECT "
                    + "  * "
                    + "FROM "
                    + "  forca_ws_token "
                    + "WHERE "
                    + "  FORCA_WS_Token_UU = ? "
                    + "  and isActive = 'Y' "
                    + "FETCH FIRST 1 ROWS ONLY";
            stmt = DB.prepareStatement(sql, null);
            stmt.setString(1, token);
            rs = stmt.executeQuery();
            if (rs.next()) {
                MFORCAWsToken MFORCAWSToken = new MFORCAWsToken(Env.getCtx(), rs, null);

                if (MFORCAWSToken.getFORCA_WS_Token_ID() > 0) {
                    valid = true;
                } else {
                    valid = false;
                }
            } else {
                valid = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            valid = false;
        } finally {
            DB.close(rs, stmt);
            rs = null;
            stmt = null;
        }
        return valid;
    }

    public static void countHitToken(String token) {
        try {
            String prmtr = "FORCA_WS_Token_UU = ?";
            Query q = new Query(Env.getCtx(), X_FORCA_WS_Token.Table_Name, prmtr, null);
            q.setParameters(token);
            q.setOnlyActiveRecords(true);
            MFORCAWsToken MFORCAWSToken = q.first();

            if (MFORCAWSToken != null) {
                Integer valhit = MFORCAWSToken.getHit().intValue();
                valhit++;
                MFORCAWSToken.setHit(new BigDecimal(String.valueOf(valhit)));
                MFORCAWSToken.saveEx();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void countErrorSukses(ResponseData respon) {
        if (respon.getCodestatus().equals("S")) {
            setErrorSuksesToken(true);
        } else {
            setErrorSuksesToken(false);
        }
    }

    public static void setErrorSuksesToken(Boolean sukses) {
        String xtoken = Env.getContext(Env.getCtx(), Constants.CTX_TOKEN);
        try {
            String prmtr = "FORCA_WS_Token_UU = ?";
            Query q = new Query(Env.getCtx(), MFORCAWsToken.Table_Name, prmtr, null);
            q.setParameters(xtoken);
            q.setOnlyActiveRecords(true);
            MFORCAWsToken MFORCAWSToken = q.first();

            if (MFORCAWSToken != null) {
                if (sukses) {
                    Integer valsukses = MFORCAWSToken.getSuccess().intValue();
                    valsukses++;
                    MFORCAWSToken.setSuccess(new BigDecimal(String.valueOf(valsukses)));
                    MFORCAWSToken.saveEx();
                } else {
                    Integer valerror = MFORCAWSToken.getErrorRequest().intValue();
                    valerror++;
                    MFORCAWSToken.setErrorRequest(new BigDecimal(String.valueOf(valerror)));
                    MFORCAWSToken.saveEx();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
