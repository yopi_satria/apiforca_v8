/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Novan Dwi Nugroho                                                                                
   Email          : novan.nugroho@sisi.id                                                                  
   Subject        :                                  
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.request;

public class ParamSaasServiceOther {

    private int ad_client_id;
    private String action;
//    private int m_product_id;
//    private String duedate;
    
    
    
    /**
     * @param ad_client_id
     * @param action
     * @param m_product_id
     * @param duedate
     */
    public ParamSaasServiceOther(int ad_client_id, String action) {
        super();
        this.ad_client_id = ad_client_id;
        this.action = action;
//        this.m_product_id = m_product_id;
//        this.duedate = duedate;
    }
    public int getAd_client_id() {
        return ad_client_id;
    }
    public void setAd_client_id(int ad_client_id) {
        this.ad_client_id = ad_client_id;
    }
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
   
        
}
