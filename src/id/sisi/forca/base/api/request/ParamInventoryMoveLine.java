package id.sisi.forca.base.api.request;

import java.math.BigDecimal;

public class ParamInventoryMoveLine {
    Integer m_locator_id;
    Integer m_locatorto_id;
    Integer m_product_id;
    BigDecimal movementqty;
    BigDecimal forca_qtyentered;
    Integer forca_c_uom_id;
    
    public Integer getM_locator_id() {
        return m_locator_id;
    }
    public void setM_locator_id(Integer m_locator_id) {
        this.m_locator_id = m_locator_id;
    }
    public Integer getM_locatorto_id() {
        return m_locatorto_id;
    }
    public void setM_locatorto_id(Integer m_locatorto_id) {
        this.m_locatorto_id = m_locatorto_id;
    }
    public Integer getM_product_id() {
        return m_product_id;
    }
    public void setM_product_id(Integer m_product_id) {
        this.m_product_id = m_product_id;
    }
    public BigDecimal getMovementqty() {
        return movementqty;
    }
    public void setMovementqty(BigDecimal movementqty) {
        this.movementqty = movementqty;
    }
    public BigDecimal getForca_qtyentered() {
        return forca_qtyentered;
    }
    public void setForca_qtyentered(BigDecimal forca_qtyentered) {
        this.forca_qtyentered = forca_qtyentered;
    }
    public Integer getForca_c_uom_id() {
        return forca_c_uom_id;
    }
    public void setForca_c_uom_id(Integer forca_c_uom_id) {
        this.forca_c_uom_id = forca_c_uom_id;
    }
    
}
