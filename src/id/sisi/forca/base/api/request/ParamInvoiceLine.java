package id.sisi.forca.base.api.request;

public class ParamInvoiceLine {

    String c_charge_id;
    String price_entered;
    String description_line;
    String c_tax_id;

    public String getDescription_line() {
        return description_line;
    }

    public void setDescription_line(String description_line) {
        this.description_line = description_line;
    }

    public String getC_charge_id() {
        return c_charge_id;
    }

    public void setC_charge_id(String c_charge_id) {
        this.c_charge_id = c_charge_id;
    }

    public String getPrice_entered() {
        return price_entered;
    }

    public void setPrice_entered(String price_entered) {
        this.price_entered = price_entered;
    }

    public String getC_tax_id() {
        return c_tax_id;
    }

    public void setC_tax_id(String c_tax_id) {
        this.c_tax_id = c_tax_id;
    }
    
    
}
