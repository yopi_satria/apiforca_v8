package id.sisi.forca.base.api.request;

import java.math.BigDecimal;

public class ParamInvoicesLine {
    Integer c_charge_id = 0;
    Integer m_inoutline_id = 0;
    Integer c_orderline_id = 0;
    Integer m_product_id = 0;
    Integer c_uom_id = 0;
    Integer c_tax_id = 0;
    Integer cost_center = 0;
    
    
    BigDecimal qty;
    BigDecimal priceentered;
    
    String description_line;
    
    
    public Integer getCost_center() {
        return cost_center;
    }
    public void setCost_center(Integer cost_center) {
        this.cost_center = cost_center;
    }
    public Integer getC_charge_id() {
        return c_charge_id;
    }
    public void setC_charge_id(Integer c_charge_id) {
        this.c_charge_id = c_charge_id;
    }
    public Integer getM_inoutline_id() {
        return m_inoutline_id;
    }
    public void setM_inoutline_id(Integer m_inoutline_id) {
        this.m_inoutline_id = m_inoutline_id;
    }
    public Integer getM_product_id() {
        return m_product_id;
    }
    public void setM_product_id(Integer m_product_id) {
        this.m_product_id = m_product_id;
    }
    public Integer getC_uom_id() {
        return c_uom_id;
    }
    public void setC_uom_id(Integer c_uom_id) {
        this.c_uom_id = c_uom_id;
    }
    public BigDecimal getQty() {
        return qty;
    }
    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }
    public BigDecimal getPriceentered() {
        return priceentered;
    }
    public void setPriceentered(BigDecimal priceentered) {
        this.priceentered = priceentered;
    }
    public String getDescription_line() {
        return description_line;
    }
    public void setDescription_line(String description_line) {
        this.description_line = description_line;
    }
    public Integer getC_orderline_id() {
        return c_orderline_id;
    }
    public void setC_orderline_id(Integer c_orderline_id) {
        this.c_orderline_id = c_orderline_id;
    }
    public Integer getC_tax_id() {
        return c_tax_id;
    }
    public void setC_tax_id(Integer c_tax_id) {
        this.c_tax_id = c_tax_id;
    }
    
    
}
