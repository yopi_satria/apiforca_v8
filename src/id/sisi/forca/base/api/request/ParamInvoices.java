package id.sisi.forca.base.api.request;

import java.util.List;

public class ParamInvoices {
    String description_header;
    String dateordered;
    String paymentrule;
    String poreference;
    String dateacct;
    String dateinvoiced;
    String issotrx;
    
    Integer c_bpartner_id = 0;
    Integer c_bpartner_location_id = 0;
    Integer ad_user_id = 0;
    Integer ad_org_id = 0;
    Integer m_pricelist_id = 0;
    Integer salesrep_id = 0;
    Integer c_order_id = 0;
    Integer c_doctypetarget_id = 0;
    Integer c_invoice_id = 0;
    Integer c_paymentterm_id = 0;
    Integer c_currency_id = 0;
    
    List<ParamInvoicesLine> line_list;

    public Integer getC_currency_id() {
        return c_currency_id;
    }

    public void setC_currency_id(Integer c_currency_id) {
        this.c_currency_id = c_currency_id;
    }

    public String getDescription_header() {
        return description_header;
    }

    public void setDescription_header(String description_header) {
        this.description_header = description_header;
    }

    public String getDateordered() {
        return dateordered;
    }

    public void setDateordered(String dateordered) {
        this.dateordered = dateordered;
    }

    public String getPaymentrule() {
        return paymentrule;
    }

    public void setPaymentrule(String paymentrule) {
        this.paymentrule = paymentrule;
    }

    public String getPoreference() {
        return poreference;
    }

    public void setPoreference(String poreference) {
        this.poreference = poreference;
    }

    public String getDateacct() {
        return dateacct;
    }

    public void setDateacct(String dateacct) {
        this.dateacct = dateacct;
    }

    public String getDateinvoiced() {
        return dateinvoiced;
    }

    public void setDateinvoiced(String dateinvoiced) {
        this.dateinvoiced = dateinvoiced;
    }

    public Integer getC_bpartner_id() {
        return c_bpartner_id;
    }

    public void setC_bpartner_id(Integer c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }

    public Integer getC_bpartner_location_id() {
        return c_bpartner_location_id;
    }

    public void setC_bpartner_location_id(Integer c_bpartner_location_id) {
        this.c_bpartner_location_id = c_bpartner_location_id;
    }

    public Integer getAd_user_id() {
        return ad_user_id;
    }

    public void setAd_user_id(Integer ad_user_id) {
        this.ad_user_id = ad_user_id;
    }

    public Integer getAd_org_id() {
        return ad_org_id;
    }

    public void setAd_org_id(Integer ad_org_id) {
        this.ad_org_id = ad_org_id;
    }

    public Integer getM_pricelist_id() {
        return m_pricelist_id;
    }

    public void setM_pricelist_id(Integer m_pricelist_id) {
        this.m_pricelist_id = m_pricelist_id;
    }

    public Integer getSalesrep_id() {
        return salesrep_id;
    }

    public void setSalesrep_id(Integer salesrep_id) {
        this.salesrep_id = salesrep_id;
    }

    public Integer getC_order_id() {
        return c_order_id;
    }

    public void setC_order_id(Integer c_order_id) {
        this.c_order_id = c_order_id;
    }

    public Integer getC_doctypetarget_id() {
        return c_doctypetarget_id;
    }

    public void setC_doctypetarget_id(Integer c_doctypetarget_id) {
        this.c_doctypetarget_id = c_doctypetarget_id;
    }

    public List<ParamInvoicesLine> getLine_list() {
        return line_list;
    }

    public void setLine_list(List<ParamInvoicesLine> line_list) {
        this.line_list = line_list;
    }

    public Integer getC_invoice_id() {
        return c_invoice_id;
    }

    public void setC_invoice_id(Integer c_invoice_id) {
        this.c_invoice_id = c_invoice_id;
    }

    public String getIssotrx() {
        return issotrx;
    }

    public void setIssotrx(String issotrx) {
        this.issotrx = issotrx;
    }

    public Integer getC_paymentterm_id() {
        return c_paymentterm_id;
    }

    public void setC_paymentterm_id(Integer c_paymentterm_id) {
        this.c_paymentterm_id = c_paymentterm_id;
    }
    
}
