package id.sisi.forca.base.api.request;

import java.math.BigDecimal;

public class ParamOrderLineOther {
    Integer m_product_id;
    Integer qty_entered;
    Integer c_tax_id;
    Integer c_uom_id;
    Integer line_number;
    BigDecimal price_entered;
    BigDecimal discount;
    
    public Integer getM_product_id() {
        return m_product_id;
    }
    public void setM_product_id(Integer m_product_id) {
        this.m_product_id = m_product_id;
    }
    public Integer getQty_entered() {
        return qty_entered;
    }
    public void setQty_entered(Integer qty_entered) {
        this.qty_entered = qty_entered;
    }
    public Integer getC_tax_id() {
        return c_tax_id;
    }
    public void setC_tax_id(Integer c_tax_id) {
        this.c_tax_id = c_tax_id;
    }
    public Integer getC_uom_id() {
        return c_uom_id;
    }
    public void setC_uom_id(Integer c_uom_id) {
        this.c_uom_id = c_uom_id;
    }
    public Integer getLine_number() {
        return line_number;
    }
    public void setLine_number(Integer line_number) {
        this.line_number = line_number;
    }
    public BigDecimal getPrice_entered() {
        return price_entered;
    }
    public void setPrice_entered(BigDecimal price_entered) {
        this.price_entered = price_entered;
    }
    public BigDecimal getDiscount() {
        return discount;
    }
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
}
