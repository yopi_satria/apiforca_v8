package id.sisi.forca.base.api.request;

import java.util.List;

public class ParamInvoice {

    String description_header;
    String c_bpartner_id;
    List<ParamInvoiceLine> line_list;
    String date_invoiced;
    String ad_org_id;
    String m_pricelist_id;
    String paymentrule;
    String salesrep_id;
    String c_tax_id;

    public String getSalesrep_id() {
        return salesrep_id;
    }

    public void setSalesrep_id(String salesrep_id) {
        this.salesrep_id = salesrep_id;
    }

    public String getPaymentrule() {
        return paymentrule;
    }

    public void setPaymentrule(String paymentrule) {
        this.paymentrule = paymentrule;
    }

    public String getAd_org_id() {
        return ad_org_id;
    }

    public void setAd_org_id(String ad_org_id) {
        this.ad_org_id = ad_org_id;
    }

    public String getDate_invoiced() {
        return date_invoiced;
    }

    public void setDate_invoiced(String date_invoiced) {
        this.date_invoiced = date_invoiced;
    }

    public String getDescription_header() {
        return description_header;
    }

    public void setDescription_header(String description_header) {
        this.description_header = description_header;
    }

    public String getC_bpartner_id() {
        return c_bpartner_id;
    }

    public void setC_bpartner_id(String c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }

    public List<ParamInvoiceLine> getLine_list() {
        return line_list;
    }

    public void setLine_list(List<ParamInvoiceLine> line_list) {
        this.line_list = line_list;
    }

    public String getM_pricelist_id() {
        return m_pricelist_id;
    }

    public void setM_pricelist_id(String m_pricelist_id) {
        this.m_pricelist_id = m_pricelist_id;
    }
    
    
}
