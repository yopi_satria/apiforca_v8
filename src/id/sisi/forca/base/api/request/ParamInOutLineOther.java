package id.sisi.forca.base.api.request;

import java.math.BigDecimal;

public class ParamInOutLineOther {
    Integer m_product_id;
    BigDecimal qty;
    Integer c_orderline_id;
    Integer c_uom_id;
    String description;
    Integer m_inout_id;
    Integer m_inoutline_id;
    Integer m_locator_id;
    Integer line_number;
    public Integer getM_product_id() {
        return m_product_id;
    }
    public void setM_product_id(Integer m_product_id) {
        this.m_product_id = m_product_id;
    }
    public BigDecimal getQty() {
        return qty;
    }
    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }
    public Integer getC_orderline_id() {
        return c_orderline_id;
    }
    public void setC_orderline_id(Integer c_orderline_id) {
        this.c_orderline_id = c_orderline_id;
    }
    public Integer getC_uom_id() {
        return c_uom_id;
    }
    public void setC_uom_id(Integer c_uom_id) {
        this.c_uom_id = c_uom_id;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Integer getM_inout_id() {
        return m_inout_id;
    }
    public void setM_inout_id(Integer m_inout_id) {
        this.m_inout_id = m_inout_id;
    }
    public Integer getM_inoutline_id() {
        return m_inoutline_id;
    }
    public void setM_inoutline_id(Integer m_inoutline_id) {
        this.m_inoutline_id = m_inoutline_id;
    }
    public Integer getM_locator_id() {
        return m_locator_id;
    }
    public void setM_locator_id(Integer m_locator_id) {
        this.m_locator_id = m_locator_id;
    }
    public Integer getLine_number() {
        return line_number;
    }
    public void setLine_number(Integer line_number) {
        this.line_number = line_number;
    }
}
