package id.sisi.forca.base.api.request;

import java.util.List;

public class ParamInventoryMove {
    String salerep_name;
    List<ParamInventoryMoveLine> list_line;
    Integer c_bpartner_id;
    Integer m_movement_id;
    
    //diambil dari forca crm impl
    // mandatory
    Integer crm_id; 
    Integer ad_org_id;
    Integer ad_orgtrx_id;
    Integer c_bpartner_location_id;
    String movementdate;
    String description;
    String isintransit;
    
    // bukan mandatory
    Integer c_campaign_id;
    Integer ad_user_id;
    
    public String getSalerep_name() {
        return salerep_name;
    }
    public void setSalerep_name(String salerep_name) {
        this.salerep_name = salerep_name;
    }
    public List<ParamInventoryMoveLine> getList_line() {
        return list_line;
    }
    public void setList_line(List<ParamInventoryMoveLine> list_line) {
        this.list_line = list_line;
    }
    public Integer getC_bpartner_id() {
        return c_bpartner_id;
    }
    public void setC_bpartner_id(Integer c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }
    public Integer getM_movement_id() {
        return m_movement_id;
    }
    public void setM_movement_id(Integer m_movement_id) {
        this.m_movement_id = m_movement_id;
    }
    public Integer getCrm_id() {
        return crm_id;
    }
    public void setCrm_id(Integer crm_id) {
        this.crm_id = crm_id;
    }
    public Integer getAd_org_id() {
        return ad_org_id;
    }
    public void setAd_org_id(Integer ad_org_id) {
        this.ad_org_id = ad_org_id;
    }
    public Integer getAd_orgtrx_id() {
        return ad_orgtrx_id;
    }
    public void setAd_orgtrx_id(Integer ad_orgtrx_id) {
        this.ad_orgtrx_id = ad_orgtrx_id;
    }
    public Integer getC_bpartner_location_id() {
        return c_bpartner_location_id;
    }
    public void setC_bpartner_location_id(Integer c_bpartner_location_id) {
        this.c_bpartner_location_id = c_bpartner_location_id;
    }
    public String getMovementdate() {
        return movementdate;
    }
    public void setMovementdate(String movementdate) {
        this.movementdate = movementdate;
    }
    public Integer getC_campaign_id() {
        return c_campaign_id;
    }
    public void setC_campaign_id(Integer c_campaign_id) {
        this.c_campaign_id = c_campaign_id;
    }
    public Integer getAd_user_id() {
        return ad_user_id;
    }
    public void setAd_user_id(Integer ad_user_id) {
        this.ad_user_id = ad_user_id;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getIsintransit() {
        return isintransit;
    }
    public void setIsintransit(String isintransit) {
        this.isintransit = isintransit;
    }
    
}
