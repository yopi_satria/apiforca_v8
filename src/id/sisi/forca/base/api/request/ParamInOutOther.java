package id.sisi.forca.base.api.request;

import java.util.List;

public class ParamInOutOther {
    Integer m_warehouse_id;
    String movementdate;
    List<ParamInOutLineOther> list_line;
    Integer c_bpartner_id;
    Integer c_order_id;
    Integer m_inout_id;
    
    //diambil dari forca crm impl
    // mandatory
    Integer crm_id; 
    Integer ad_org_id;
    Integer ad_orgtrx_id;
    Integer c_bpartner_location_id;
    String description;
    // bukan mandatory
    Integer ad_user_id;
    Integer c_project_id;
    Integer user2_id;
    String documentno;
    String docstatus;
    
    String issotrx;

    public Integer getM_warehouse_id() {
        return m_warehouse_id;
    }

    public void setM_warehouse_id(Integer m_warehouse_id) {
        this.m_warehouse_id = m_warehouse_id;
    }

    public String getMovementdate() {
        return movementdate;
    }

    public void setMovementdate(String movementdate) {
        this.movementdate = movementdate;
    }

    public List<ParamInOutLineOther> getList_line() {
        return list_line;
    }

    public void setList_line(List<ParamInOutLineOther> list_line) {
        this.list_line = list_line;
    }

    public Integer getC_bpartner_id() {
        return c_bpartner_id;
    }

    public void setC_bpartner_id(Integer c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }

    public Integer getC_order_id() {
        return c_order_id;
    }

    public void setC_order_id(Integer c_order_id) {
        this.c_order_id = c_order_id;
    }

    public Integer getM_inout_id() {
        return m_inout_id;
    }

    public void setM_inout_id(Integer m_inout_id) {
        this.m_inout_id = m_inout_id;
    }

    public Integer getCrm_id() {
        return crm_id;
    }

    public void setCrm_id(Integer crm_id) {
        this.crm_id = crm_id;
    }

    public Integer getAd_org_id() {
        return ad_org_id;
    }

    public void setAd_org_id(Integer ad_org_id) {
        this.ad_org_id = ad_org_id;
    }

    public Integer getAd_orgtrx_id() {
        return ad_orgtrx_id;
    }

    public void setAd_orgtrx_id(Integer ad_orgtrx_id) {
        this.ad_orgtrx_id = ad_orgtrx_id;
    }

    public Integer getC_bpartner_location_id() {
        return c_bpartner_location_id;
    }

    public void setC_bpartner_location_id(Integer c_bpartner_location_id) {
        this.c_bpartner_location_id = c_bpartner_location_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAd_user_id() {
        return ad_user_id;
    }

    public void setAd_user_id(Integer ad_user_id) {
        this.ad_user_id = ad_user_id;
    }

    public Integer getC_project_id() {
        return c_project_id;
    }

    public void setC_project_id(Integer c_project_id) {
        this.c_project_id = c_project_id;
    }

    public Integer getUser2_id() {
        return user2_id;
    }

    public void setUser2_id(Integer user2_id) {
        this.user2_id = user2_id;
    }

    public String getDocumentno() {
        return documentno;
    }

    public void setDocumentno(String documentno) {
        this.documentno = documentno;
    }

    public String getDocstatus() {
        return docstatus;
    }

    public void setDocstatus(String docstatus) {
        this.docstatus = docstatus;
    }

    public String getIssotrx() {
        return issotrx;
    }

    public void setIssotrx(String issotrx) {
        this.issotrx = issotrx;
    }
    
    
}
