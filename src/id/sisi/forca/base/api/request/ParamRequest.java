/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : Param Request                                
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.request;

import java.math.BigDecimal;

public class ParamRequest {
    String username;
    String password;
    String token;
    String datefrom;
    String dateto;
    String status;
    String process_name;
    String name;
    String message;
    String yesno;
    String is_active;
    String documentno;
    String ad_wf_name;
    String created;
    String accountdate;
    String movementdate;
    String dateordered;
    String issotrx;
    String searchkey;
    String bpkey;
    String productkey;
    String ispaid;
    String issold;
    String showpricelist;
    String periode;
    String date_log;
    String referenceno;
    String so_credit_limit;
    String name2;
    String address1;
    String address2;
    String address3;
    String address4;
    String country_name;
    String region_name;
    String city_name;
    String email;
    String phone;
    String phone2;
    String fax;
    String title;
    String description;
    String uomsymbol;
    String value;
    String forca_model;
    String forca_platform;
    String forca_device_id;
    String forca_manufaktur;
    String forca_version;
    String forca_versionappname;
    String forca_versionappcode;
    String sap_code;
    String id_customer;
    String ref_customer_id;
    String address_customer;
    String isproductcrm;
    String isapproved;
    String sopotype;
    String distributorcode;
    String retail_id;
    String ref_order_id;
    String ref_payment_id;
    String multiple_m_product_id;
    String multiple_qty;
    String multiple_ad_wf_activity_id;
    String product_name;
    String uuid;
    String issopricelist;
    String docbasetype;
    String docsubtypeso;
    String issalesrep;
    String datetrx;
    String dateacct;
    String datedoc;
    String nik;
    String imageurl;
    String forca_person_id;
    String forca_deviceid;
    String forca_devicemodel;
    //#20588
    String forca_location;

    Integer ad_client_id;
    Integer ad_user_id;
    Integer ad_role_id;
    Integer ad_org_id;
    Integer m_warehouse_id;
    Integer c_bpartner_id;
    Integer c_bpartner_location_id;
    Integer c_order_id;
    Integer c_bp_group_id;
    Integer c_city_id;
    Integer c_country_id;
    Integer c_region_id;
    Integer crm_id;
    Integer ad_orgtrx_id;
    Integer m_product_id;
    Integer c_orderline_id;
    Integer c_uom_id;
    Integer c_project_id;
    Integer c_taxcategory_id;
    Integer m_product_category_id;
    Integer c_payment_id;
    Integer c_opportunity_id;
    Integer c_campaign_id;
    Integer ad_wf_activity_id;
    Integer ad_wf_node_id;
    Integer page;
    Integer perpage;
    Integer forca_user_mobile_id;
    Integer forca_crm_bpartner_id;
    Integer c_location_id;
    Integer forca_crm_contact_id;
    Integer c_channel_id;
    Integer m_pricelist_id;
    Integer m_movement_id;
    Integer m_movementline_id;
    Integer m_locator_id;
    Integer m_locatorto_id;
    Integer movementqty;
    Integer forca_c_uom_id;
    Integer c_uom_to_id;
    Integer m_inout_id;
    Integer m_inoutline_id;
    Integer c_tax_id;
    Integer stdprecision;
    Integer costingprecision;
    Integer c_uom_conversion_id;
    Integer m_productprice_id;
    Integer m_pricelist_version_id;
    Integer pricelist;
    Integer pricestd;
    Integer pricelimit;
    Integer user2_id;
    Integer c_invoice_id;
    Integer c_invoiceline_id;
    Integer c_uom_from_id;
    Integer c_doctype_id;
    Integer c_charge_id;
    Integer c_bankaccount_id;
    Integer c_paymentallocate_id;
    Integer forca_branch_id;
    Integer forca_leavetype_id;

    BigDecimal qty;
    BigDecimal priceactual;
    BigDecimal pay_amt;
    BigDecimal forca_qtyentered;
    BigDecimal multiplyrate;
    BigDecimal dividerate;
    BigDecimal priceentered;
    BigDecimal latitude;
    BigDecimal longitude;
    BigDecimal forca_distance;
    BigDecimal forca_totalday;

    byte[] inputStream;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDatefrom() {
        return datefrom;
    }

    public void setDatefrom(String datefrom) {
        this.datefrom = datefrom;
    }

    public String getDateto() {
        return dateto;
    }

    public void setDateto(String dateto) {
        this.dateto = dateto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProcess_name() {
        return process_name;
    }

    public void setProcess_name(String process_name) {
        this.process_name = process_name;
    }

    public Integer getAd_wf_activity_id() {
        return ad_wf_activity_id;
    }

    public void setAd_wf_activity_id(Integer ad_wf_activity_id) {
        this.ad_wf_activity_id = ad_wf_activity_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getYesno() {
        return yesno;
    }

    public void setYesno(String yesno) {
        this.yesno = yesno;
    }

    public String getRef_order_id() {
        return ref_order_id;
    }

    public void setRef_order_id(String ref_order_id) {
        this.ref_order_id = ref_order_id;
    }

    public String getRef_payment_id() {
        return ref_payment_id;
    }

    public void setRef_payment_id(String ref_payment_id) {
        this.ref_payment_id = ref_payment_id;
    }

    public String getDocumentno() {
        return documentno;
    }

    public String getAd_wf_name() {
        return ad_wf_name;
    }

    public void setAd_wf_name(String ad_wf_name) {
        this.ad_wf_name = ad_wf_name;
    }

    public Integer getAd_wf_node_id() {
        return ad_wf_node_id;
    }

    public void setAd_wf_node_id(Integer ad_wf_node_id) {
        this.ad_wf_node_id = ad_wf_node_id;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPerpage() {
        return perpage;
    }

    public void setPerpage(Integer perpage) {
        this.perpage = perpage;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getAccountdate() {
        return accountdate;
    }

    public void setAccountdate(String accountdate) {
        this.accountdate = accountdate;
    }

    public String getMovementdate() {
        return movementdate;
    }

    public void setMovementdate(String movementdate) {
        this.movementdate = movementdate;
    }

    public String getDistributorcode() {
        return distributorcode;
    }

    public void setDistributorcode(String distributorcode) {
        this.distributorcode = distributorcode;
    }

    public String getDateordered() {
        return dateordered;
    }

    public void setDateordered(String dateordered) {
        this.dateordered = dateordered;
    }

    public String getIssotrx() {
        return issotrx;
    }

    public void setIssotrx(String issotrx) {
        this.issotrx = issotrx;
    }

    public String getSearchkey() {
        return searchkey;
    }

    public void setSearchkey(String searchkey) {
        this.searchkey = searchkey;
    }

    public String getMultiple_m_product_id() {
        return multiple_m_product_id;
    }

    public void setMultiple_m_product_id(String multiple_m_product_id) {
        this.multiple_m_product_id = multiple_m_product_id;
    }

    public String getMultiple_qty() {
        return multiple_qty;
    }

    public void setMultiple_qty(String multiple_qty) {
        this.multiple_qty = multiple_qty;
    }

    public String getMultiple_ad_wf_activity_id() {
        return multiple_ad_wf_activity_id;
    }

    public void setMultiple_ad_wf_activity_id(String multiple_ad_wf_activity_id) {
        this.multiple_ad_wf_activity_id = multiple_ad_wf_activity_id;
    }

    public String getRetail_id() {
        return retail_id;
    }

    public void setRetail_id(String retail_id) {
        this.retail_id = retail_id;
    }

    public void setDocumentno(String documentno) {
        this.documentno = documentno;
    }

    public Integer getAd_client_id() {
        return ad_client_id;
    }

    public void setAd_client_id(Integer ad_client_id) {
        this.ad_client_id = ad_client_id;
    }

    public Integer getAd_user_id() {
        return ad_user_id;
    }

    public void setAd_user_id(Integer ad_user_id) {
        this.ad_user_id = ad_user_id;
    }

    public Integer getAd_role_id() {
        return ad_role_id;
    }

    public void setAd_role_id(Integer ad_role_id) {
        this.ad_role_id = ad_role_id;
    }

    public Integer getAd_org_id() {
        return ad_org_id;
    }

    public void setAd_org_id(Integer ad_org_id) {
        this.ad_org_id = ad_org_id;
    }

    public Integer getM_warehouse_id() {
        return m_warehouse_id;
    }

    public void setM_warehouse_id(Integer m_warehouse_id) {
        this.m_warehouse_id = m_warehouse_id;
    }

    public Integer getC_bpartner_id() {
        return c_bpartner_id;
    }

    public void setC_bpartner_id(Integer c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }

    public Integer getC_bpartner_location_id() {
        return c_bpartner_location_id;
    }

    public void setC_bpartner_location_id(Integer c_bpartner_location_id) {
        this.c_bpartner_location_id = c_bpartner_location_id;
    }

    public Integer getC_order_id() {
        return c_order_id;
    }

    public void setC_order_id(Integer c_order_id) {
        this.c_order_id = c_order_id;
    }

    public Integer getC_bp_group_id() {
        return c_bp_group_id;
    }

    public void setC_bp_group_id(Integer c_bp_group_id) {
        this.c_bp_group_id = c_bp_group_id;
    }

    public Integer getC_city_id() {
        return c_city_id;
    }

    public void setC_city_id(Integer c_city_id) {
        this.c_city_id = c_city_id;
    }

    public Integer getC_country_id() {
        return c_country_id;
    }

    public void setC_country_id(Integer c_country_id) {
        this.c_country_id = c_country_id;
    }

    public Integer getC_region_id() {
        return c_region_id;
    }

    public void setC_region_id(Integer c_region_id) {
        this.c_region_id = c_region_id;
    }

    public Integer getCrm_id() {
        return crm_id;
    }

    public void setCrm_id(Integer crm_id) {
        this.crm_id = crm_id;
    }

    public Integer getAd_orgtrx_id() {
        return ad_orgtrx_id;
    }

    public void setAd_orgtrx_id(Integer ad_orgtrx_id) {
        this.ad_orgtrx_id = ad_orgtrx_id;
    }

    public Integer getM_product_id() {
        return m_product_id;
    }

    public void setM_product_id(Integer m_product_id) {
        this.m_product_id = m_product_id;
    }

    public Integer getC_orderline_id() {
        return c_orderline_id;
    }

    public void setC_orderline_id(Integer c_orderline_id) {
        this.c_orderline_id = c_orderline_id;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getPriceactual() {
        return priceactual;
    }

    public void setPriceactual(BigDecimal priceactual) {
        this.priceactual = priceactual;
    }

    public BigDecimal getPay_amt() {
        return pay_amt;
    }

    public void setPay_amt(BigDecimal pay_amt) {
        this.pay_amt = pay_amt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getC_project_id() {
        return c_project_id;
    }

    public void setC_project_id(Integer c_project_id) {
        this.c_project_id = c_project_id;
    }

    public Integer getC_taxcategory_id() {
        return c_taxcategory_id;
    }

    public void setC_taxcategory_id(Integer c_taxcategory_id) {
        this.c_taxcategory_id = c_taxcategory_id;
    }

    public Integer getC_uom_id() {
        return c_uom_id;
    }

    public void setC_uom_id(Integer c_uom_id) {
        this.c_uom_id = c_uom_id;
    }

    public Integer getC_payment_id() {
        return c_payment_id;
    }

    public void setC_payment_id(Integer c_payment_id) {
        this.c_payment_id = c_payment_id;
    }

    public Integer getC_opportunity_id() {
        return c_opportunity_id;
    }

    public void setC_opportunity_id(Integer c_opportunity_id) {
        this.c_opportunity_id = c_opportunity_id;
    }

    public Integer getC_campaign_id() {
        return c_campaign_id;
    }

    public void setC_campaign_id(Integer c_campaign_id) {
        this.c_campaign_id = c_campaign_id;
    }

    public Integer getM_product_category_id() {
        return m_product_category_id;
    }

    public void setM_product_category_id(Integer m_product_category_id) {
        this.m_product_category_id = m_product_category_id;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getIssold() {
        return issold;
    }

    public void setIssold(String issold) {
        this.issold = issold;
    }

    public String getShowpricelist() {
        return showpricelist;
    }

    public void setShowpricelist(String showpricelist) {
        this.showpricelist = showpricelist;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getDate_log() {
        return date_log;
    }

    public void setDate_log(String date_log) {
        this.date_log = date_log;
    }

    public String getReferenceno() {
        return referenceno;
    }

    public void setReferenceno(String referenceno) {
        this.referenceno = referenceno;
    }

    public String getSo_credit_limit() {
        return so_credit_limit;
    }

    public void setSo_credit_limit(String so_credit_limit) {
        this.so_credit_limit = so_credit_limit;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public Integer getForca_crm_bpartner_id() {
        return forca_crm_bpartner_id;
    }

    public void setForca_crm_bpartner_id(Integer forca_crm_bpartner_id) {
        this.forca_crm_bpartner_id = forca_crm_bpartner_id;
    }

    public Integer getC_location_id() {
        return c_location_id;
    }

    public void setC_location_id(Integer c_location_id) {
        this.c_location_id = c_location_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getForca_crm_contact_id() {
        return forca_crm_contact_id;
    }

    public void setForca_crm_contact_id(Integer forca_crm_contact_id) {
        this.forca_crm_contact_id = forca_crm_contact_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUomsymbol() {
        return uomsymbol;
    }

    public void setUomsymbol(String uomsymbol) {
        this.uomsymbol = uomsymbol;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getC_channel_id() {
        return c_channel_id;
    }

    public void setC_channel_id(Integer c_channel_id) {
        this.c_channel_id = c_channel_id;
    }

    public String getForca_model() {
        return forca_model;
    }

    public void setForca_model(String forca_model) {
        this.forca_model = forca_model;
    }

    public String getForca_platform() {
        return forca_platform;
    }

    public void setForca_platform(String forca_platform) {
        this.forca_platform = forca_platform;
    }

    public String getForca_device_id() {
        return forca_device_id;
    }

    public void setForca_device_id(String forca_device_id) {
        this.forca_device_id = forca_device_id;
    }

    public String getForca_manufaktur() {
        return forca_manufaktur;
    }

    public void setForca_manufaktur(String forca_manufaktur) {
        this.forca_manufaktur = forca_manufaktur;
    }

    public String getForca_version() {
        return forca_version;
    }

    public void setForca_version(String forca_version) {
        this.forca_version = forca_version;
    }

    public String getForca_versionappname() {
        return forca_versionappname;
    }

    public void setForca_versionappname(String forca_versionappname) {
        this.forca_versionappname = forca_versionappname;
    }

    public String getForca_versionappcode() {
        return forca_versionappcode;
    }

    public void setForca_versionappcode(String forca_versionappcode) {
        this.forca_versionappcode = forca_versionappcode;
    }

    public String getSap_code() {
        return sap_code;
    }

    public void setSap_code(String sap_code) {
        this.sap_code = sap_code;
    }

    public String getId_customer() {
        return id_customer;
    }

    public void setId_customer(String id_customer) {
        this.id_customer = id_customer;
    }

    public String getRef_customer_id() {
        return ref_customer_id;
    }

    public void setRef_customer_id(String ref_customer_id) {
        this.ref_customer_id = ref_customer_id;
    }

    public String getAddress_customer() {
        return address_customer;
    }

    public void setAddress_customer(String address_customer) {
        this.address_customer = address_customer;
    }

    public String getIsproductcrm() {
        return isproductcrm;
    }

    public void setIsproductcrm(String isproductcrm) {
        this.isproductcrm = isproductcrm;
    }

    public Integer getM_pricelist_id() {
        return m_pricelist_id;
    }

    public void setM_pricelist_id(Integer m_pricelist_id) {
        this.m_pricelist_id = m_pricelist_id;
    }

    public Integer getM_movement_id() {
        return m_movement_id;
    }

    public void setM_movement_id(Integer m_movement_id) {
        this.m_movement_id = m_movement_id;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public Integer getM_movementline_id() {
        return m_movementline_id;
    }

    public void setM_movementline_id(Integer m_movementline_id) {
        this.m_movementline_id = m_movementline_id;
    }

    public Integer getM_locator_id() {
        return m_locator_id;
    }

    public void setM_locator_id(Integer m_locator_id) {
        this.m_locator_id = m_locator_id;
    }

    public Integer getM_locatorto_id() {
        return m_locatorto_id;
    }

    public void setM_locatorto_id(Integer m_locatorto_id) {
        this.m_locatorto_id = m_locatorto_id;
    }

    public Integer getMovementqty() {
        return movementqty;
    }

    public void setMovementqty(Integer movementqty) {
        this.movementqty = movementqty;
    }


    public Integer getForca_c_uom_id() {
        return forca_c_uom_id;
    }

    public void setForca_c_uom_id(Integer forca_c_uom_id) {
        this.forca_c_uom_id = forca_c_uom_id;
    }

    public BigDecimal getForca_qtyentered() {
        return forca_qtyentered;
    }

    public void setForca_qtyentered(BigDecimal forca_qtyentered) {
        this.forca_qtyentered = forca_qtyentered;
    }

    public Integer getC_uom_to_id() {
        return c_uom_to_id;
    }

    public void setC_uom_to_id(Integer c_uom_to_id) {
        this.c_uom_to_id = c_uom_to_id;
    }

    public Integer getM_inout_id() {
        return m_inout_id;
    }

    public void setM_inout_id(Integer m_inout_id) {
        this.m_inout_id = m_inout_id;
    }

    public Integer getM_inoutline_id() {
        return m_inoutline_id;
    }

    public void setM_inoutline_id(Integer m_inoutline_id) {
        this.m_inoutline_id = m_inoutline_id;
    }

    public String getSopotype() {
        return sopotype;
    }

    public void setSopotype(String sopotype) {
        this.sopotype = sopotype;
    }

    public Integer getC_tax_id() {
        return c_tax_id;
    }

    public void setC_tax_id(Integer c_tax_id) {
        this.c_tax_id = c_tax_id;
    }

    public Integer getStdprecision() {
        return stdprecision;
    }

    public void setStdprecision(Integer stdprecision) {
        this.stdprecision = stdprecision;
    }

    public Integer getCostingprecision() {
        return costingprecision;
    }

    public void setCostingprecision(Integer costingprecision) {
        this.costingprecision = costingprecision;
    }

    public Integer getC_uom_conversion_id() {
        return c_uom_conversion_id;
    }

    public void setC_uom_conversion_id(Integer c_uom_conversion_id) {
        this.c_uom_conversion_id = c_uom_conversion_id;
    }

    public BigDecimal getMultiplyrate() {
        return multiplyrate;
    }

    public void setMultiplyrate(BigDecimal multiplyrate) {
        this.multiplyrate = multiplyrate;
    }

    public BigDecimal getDividerate() {
        return dividerate;
    }

    public void setDividerate(BigDecimal dividerate) {
        this.dividerate = dividerate;
    }

    public Integer getM_productprice_id() {
        return m_productprice_id;
    }

    public void setM_productprice_id(Integer m_productprice_id) {
        this.m_productprice_id = m_productprice_id;
    }

    public Integer getM_pricelist_version_id() {
        return m_pricelist_version_id;
    }

    public void setM_pricelist_version_id(Integer m_pricelist_version_id) {
        this.m_pricelist_version_id = m_pricelist_version_id;
    }

    public Integer getPricelist() {
        return pricelist;
    }

    public void setPricelist(Integer pricelist) {
        this.pricelist = pricelist;
    }

    public Integer getPricestd() {
        return pricestd;
    }

    public void setPricestd(Integer pricestd) {
        this.pricestd = pricestd;
    }

    public Integer getPricelimit() {
        return pricelimit;
    }

    public void setPricelimit(Integer pricelimit) {
        this.pricelimit = pricelimit;
    }

    public BigDecimal getPriceentered() {
        return priceentered;
    }

    public void setPriceentered(BigDecimal priceentered) {
        this.priceentered = priceentered;
    }

    public Integer getUser2_id() {
        return user2_id;
    }

    public void setUser2_id(Integer user2_id) {
        this.user2_id = user2_id;
    }

    public Integer getC_invoice_id() {
        return c_invoice_id;
    }

    public void setC_invoice_id(Integer c_invoice_id) {
        this.c_invoice_id = c_invoice_id;
    }

    public Integer getC_invoiceline_id() {
        return c_invoiceline_id;
    }

    public void setC_invoiceline_id(Integer c_invoiceline_id) {
        this.c_invoiceline_id = c_invoiceline_id;
    }

    public String getBpkey() {
        return bpkey;
    }

    public void setBpkey(String bpkey) {
        this.bpkey = bpkey;
    }

    public String getProductkey() {
        return productkey;
    }

    public void setProductkey(String productkey) {
        this.productkey = productkey;
    }

    public String getIspaid() {
        return ispaid;
    }

    public void setIspaid(String ispaid) {
        this.ispaid = ispaid;
    }

    public Integer getForca_user_mobile_id() {
        return forca_user_mobile_id;
    }

    public void setForca_user_mobile_id(Integer forca_user_mobile_id) {
        this.forca_user_mobile_id = forca_user_mobile_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public byte[] getInputStream() {
        return inputStream;
    }

    public void setInputStream(byte[] inputStream) {
        this.inputStream = inputStream;
    }

    public Integer getC_uom_from_id() {
        return c_uom_from_id;
    }

    public void setC_uom_from_id(Integer c_uom_from_id) {
        this.c_uom_from_id = c_uom_from_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIssopricelist() {
        return issopricelist;
    }

    public void setIssopricelist(String issopricelist) {
        this.issopricelist = issopricelist;
    }

    public String getDocbasetype() {
        return docbasetype;
    }

    public void setDocbasetype(String docbasetype) {
        this.docbasetype = docbasetype;
    }

    public Integer getC_doctype_id() {
        return c_doctype_id;
    }

    public void setC_doctype_id(Integer c_doctype_id) {
        this.c_doctype_id = c_doctype_id;
    }

    public String getDocsubtypeso() {
        return docsubtypeso;
    }

    public void setDocsubtypeso(String docsubtypeso) {
        this.docsubtypeso = docsubtypeso;
    }

    public String getIssalesrep() {
        return issalesrep;
    }

    public void setIssalesrep(String issalesrep) {
        this.issalesrep = issalesrep;
    }

    public Integer getC_charge_id() {
        return c_charge_id;
    }

    public void setC_charge_id(Integer c_charge_id) {
        this.c_charge_id = c_charge_id;
    }

    public String getDatetrx() {
        return datetrx;
    }

    public void setDatetrx(String datetrx) {
        this.datetrx = datetrx;
    }

    public Integer getC_bankaccount_id() {
        return c_bankaccount_id;
    }

    public void setC_bankaccount_id(Integer c_bankaccount_id) {
        this.c_bankaccount_id = c_bankaccount_id;
    }

    public String getDateacct() {
        return dateacct;
    }

    public void setDateacct(String dateacct) {
        this.dateacct = dateacct;
    }

    public String getDatedoc() {
        return datedoc;
    }

    public void setDatedoc(String datedoc) {
        this.datedoc = datedoc;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getForca_person_id() {
        return forca_person_id;
    }

    public void setForca_person_id(String forca_person_id) {
        this.forca_person_id = forca_person_id;
    }
    
    public String getForca_deviceid() {
        return forca_deviceid;
    }

    public void setForca_deviceid(String forca_deviceid) {
        this.forca_deviceid = forca_deviceid;
    }

    public String getForca_devicemodel() {
        return forca_devicemodel;
    }

    public void setForca_devicemodel(String forca_devicemodel) {
        this.forca_devicemodel = forca_devicemodel;
    }

    public Integer getForca_branch_id() {
        return forca_branch_id;
    }

    public void setForca_branch_id(Integer forca_branch_id) {
        this.forca_branch_id = forca_branch_id;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
    
    public BigDecimal getForca_distance() {
        return forca_distance;
    }

    public void setForca_distance(BigDecimal forca_distance) {
        this.forca_distance = forca_distance;
    }

    public Integer getC_paymentallocate_id() {
        return c_paymentallocate_id;
    }

    public void setC_paymentallocate_id(Integer c_paymentallocate_id) {
        this.c_paymentallocate_id = c_paymentallocate_id;
    }
    
    public String getForca_location() {
        return forca_location;
    }
    
    public void setForca_location(String forca_location) {
        this.forca_location = forca_location;
    }

    public Integer getForca_leavetype_id() {
        return forca_leavetype_id;
    }

    public void setForca_leavetype_id(Integer forca_leavetype_id) {
        this.forca_leavetype_id = forca_leavetype_id;
    }

    public BigDecimal getForca_totalday() {
        return forca_totalday;
    }

    public void setForca_totalday(BigDecimal forca_totalday) {
        this.forca_totalday = forca_totalday;
    }

}
