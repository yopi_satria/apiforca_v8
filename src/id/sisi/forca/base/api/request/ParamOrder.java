package id.sisi.forca.base.api.request;

import java.util.List;

public class ParamOrder {
    Integer m_warehouse_id;
    Integer m_pricelist_id;
    String salerep_name;
    String payment_rule;
    List<ParamOrderLine> list_line;
    Integer c_bpartner_id;
    Integer c_order_id;
    
    //diambil dari forca crm impl
    // mandatory
    Integer crm_id; 
    Integer ad_org_id;
    Integer ad_orgtrx_id;
    Integer c_bpartner_location_id;
    Integer dropship_location_id;
    Integer dropship_bpartner_id;
    String description;
    String dateordered;
    // bukan mandatory
    Integer c_opportunity_id;
    Integer c_campaign_id;
    Integer ad_user_id;
    Integer c_project_id;
    Integer user2_id;
    Integer c_paymentterm_id;
    String documentno;
    String poreference;
    String docstatus;
    
    String issotrx;
    
    /*
     * dobel,
     * mandatory :
     * //    Integer c_bpartner_id;
     * bukan mandatory :
     * //    Integer c_order_id;
     * //    Integer m_warehouse_id;
     */
    
    
    
    public Integer getC_order_id() {
        return c_order_id;
    }

    public void setC_order_id(Integer c_order_id) {
        this.c_order_id = c_order_id;
    }

    public String getSalerep_name() {
        return salerep_name;
    }

    public void setSalerep_name(String salerep_name) {
        this.salerep_name = salerep_name;
    }

    public Integer getC_bpartner_id() {
        return c_bpartner_id;
    }

    public void setC_bpartner_id(Integer c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }

    public Integer getM_warehouse_id() {
        return m_warehouse_id;
    }

    public void setM_warehouse_id(Integer m_warehouse_id) {
        this.m_warehouse_id = m_warehouse_id;
    }

    public Integer getM_pricelist_id() {
        return m_pricelist_id;
    }

    public void setM_pricelist_id(Integer m_pricelist_id) {
        this.m_pricelist_id = m_pricelist_id;
    }

    public String getPayment_rule() {
        return payment_rule;
    }

    public void setPayment_rule(String payment_rule) {
        this.payment_rule = payment_rule;
    }

    public List<ParamOrderLine> getList_line() {
        return list_line;
    }

    public void setList_line(List<ParamOrderLine> list_line) {
        this.list_line = list_line;
    }

    public Integer getCrm_id() {
        return crm_id;
    }

    public void setCrm_id(Integer crm_id) {
        this.crm_id = crm_id;
    }

    public Integer getAd_org_id() {
        return ad_org_id;
    }

    public void setAd_org_id(Integer ad_org_id) {
        this.ad_org_id = ad_org_id;
    }

    public Integer getAd_orgtrx_id() {
        return ad_orgtrx_id;
    }

    public void setAd_orgtrx_id(Integer ad_orgtrx_id) {
        this.ad_orgtrx_id = ad_orgtrx_id;
    }

    public Integer getC_bpartner_location_id() {
        return c_bpartner_location_id;
    }

    public void setC_bpartner_location_id(Integer c_bpartner_location_id) {
        this.c_bpartner_location_id = c_bpartner_location_id;
    }

    public String getDateordered() {
        return dateordered;
    }

    public void setDateordered(String dateordered) {
        this.dateordered = dateordered;
    }

    public Integer getC_opportunity_id() {
        return c_opportunity_id;
    }

    public void setC_opportunity_id(Integer c_opportunity_id) {
        this.c_opportunity_id = c_opportunity_id;
    }

    public Integer getC_campaign_id() {
        return c_campaign_id;
    }

    public void setC_campaign_id(Integer c_campaign_id) {
        this.c_campaign_id = c_campaign_id;
    }

    public Integer getAd_user_id() {
        return ad_user_id;
    }

    public void setAd_user_id(Integer ad_user_id) {
        this.ad_user_id = ad_user_id;
    }

    public String getDocumentno() {
        return documentno;
    }

    public void setDocumentno(String documentno) {
        this.documentno = documentno;
    }

    public String getIssotrx() {
        return issotrx;
    }

    public void setIssotrx(String issotrx) {
        this.issotrx = issotrx;
    }

    public Integer getDropship_location_id() {
        return dropship_location_id;
    }

    public void setDropship_location_id(Integer dropship_location_id) {
        this.dropship_location_id = dropship_location_id;
    }

    public Integer getDropship_bpartner_id() {
        return dropship_bpartner_id;
    }

    public void setDropship_bpartner_id(Integer dropship_bpartner_id) {
        this.dropship_bpartner_id = dropship_bpartner_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getC_project_id() {
        return c_project_id;
    }

    public void setC_project_id(Integer c_project_id) {
        this.c_project_id = c_project_id;
    }

    public Integer getUser2_id() {
        return user2_id;
    }

    public void setUser2_id(Integer user2_id) {
        this.user2_id = user2_id;
    }

    public Integer getC_paymentterm_id() {
        return c_paymentterm_id;
    }

    public void setC_paymentterm_id(Integer c_paymentterm_id) {
        this.c_paymentterm_id = c_paymentterm_id;
    }

    public String getPoreference() {
        return poreference;
    }

    public void setPoreference(String poreference) {
        this.poreference = poreference;
    }

    public String getDocstatus() {
        return docstatus;
    }

    public void setDocstatus(String docstatus) {
        this.docstatus = docstatus;
    }

    
}
