/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Fitriani Rohmah Hardianti                                                                                
   Email          : fitrianirohmahhardianti@gmail.com                                                                 
   Subject        : Authentication Form Data v1                   
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.path;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.stereotype.Component;
import id.sisi.forca.base.api.filter.AuthenticationApi;
import id.sisi.forca.base.api.impl.AuthenticationImpl;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.response.ResponseData;

@Component
@Path("/authentication/json")
public class Authentication_Json_v1 {
    AuthenticationApi auth;
    ResponseData result = new ResponseData();

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(ParamRequest param) {

        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
        result = apiLoginImpl.login(param.getUsername(), param.getPassword());
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/getClients")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClients(ParamRequest param) {

        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
        result = apiLoginImpl.getClients(param);
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/getRoles")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRoles(ParamRequest param) {

        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
        result = apiLoginImpl.getRoles(param);
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/getOrgs")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("application/json")
    public Response getOrgs(ParamRequest param) {

        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
        result = apiLoginImpl.getOrgs(param);
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/getWarehouses")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("application/json")
    public Response getWarehouses(ParamRequest param) {

        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
        result = apiLoginImpl.getWarehouses(param);
        return ResponseData.finalResponse(result);

    }

    @POST
    @Path("/getToken")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getToken(ParamRequest param) {

        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
        result = apiLoginImpl.getToken(param);
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/refreshToken")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response refreshToken(ParamRequest param) {

        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
//        result = apiLoginImpl.refreshToken(param);
        result = null;
        return ResponseData.finalResponse(result);

    }

    @POST
    @Path("/infoToken")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response infoToken(ParamRequest param) {
        
        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
        result = apiLoginImpl.infoToken(param);
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/logout")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout(ParamRequest param) {

        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
        result = apiLoginImpl.logout(param);
        return ResponseData.finalResponse(result);
    }
}
