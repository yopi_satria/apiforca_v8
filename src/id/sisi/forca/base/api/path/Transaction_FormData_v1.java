/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : Transaction FormData v1                               
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.path;

import java.math.BigDecimal;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.compiere.util.Trx;
import org.springframework.stereotype.Component;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.Authorization;
import id.sisi.forca.base.api.filter.AuthenticationApi;
import id.sisi.forca.base.api.filter.RestFilter;
import id.sisi.forca.base.api.impl.TransactionImpl;
import id.sisi.forca.base.api.request.ParamInOut;
import id.sisi.forca.base.api.request.ParamInventoryMove;
import id.sisi.forca.base.api.request.ParamInvoice;
import id.sisi.forca.base.api.request.ParamInvoices;
import id.sisi.forca.base.api.request.ParamOrder;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.service.TransactionService;
import id.sisi.forca.base.api.util.Constants;

@Component
@Path("/transaction/v1")
@Api(position = 3, value = "Transaction", description = "Transaction Data in Forca")
public class Transaction_FormData_v1 {

    @POST
    @Path("/getOrder")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 1, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve specific order data.")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getOrder(@ApiParam(required = true,
            value = "Unique code for identifier order data.") @FormParam("c_order_id") int c_order_id) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {
            ParamRequest param = new ParamRequest();
            param.setC_order_id(Integer.valueOf(c_order_id));

            AuthenticationApi.setContextByToken(token);
            TransactionService srv = new TransactionService();
            result = srv.get(param, "Order");
        }
        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/getOrderList")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 2, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve list of order data.")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getOrderList(@ApiParam(
            value = "Indication of sales transaction data. Sales Order : <b>Y</b>. Purchase Order : <b>N</b>. <b>Format : Y/N</b>",
            required = false) @FormParam("issotrx") String issotrx,
    //@formatter:off
            @ApiParam(value = "This column indicates the status of a document. <b>Format : "
                    + "Waiting Confirmation/ " 
                    + "Approved/ " 
                    + "Completed/ " 
                    + "Drafted/ " 
                    + "Invalid/ " 
                    + "Reversed/ " 
                    + "Voided/ " 
                    + "In Progress/ " 
                    + "Waiting Payment/ " 
                    + "Unknown/ " 
                    + "Closed/ " 
                    + "Not Approved</b>",
            required = false) @FormParam("status") String status,
            //@formatter:on
            @ApiParam(value = "Amount of data showed per page",
                    required = false) @FormParam("perpage") Integer perpage,
            @ApiParam(value = "Page that you wanted to show",
                    required = false) @FormParam("page") Integer page,
            @ApiParam(value = "Start date. <b>Format : YYYY-MM-DD</b>",
                    required = false) @FormParam("datefrom") String datefrom,
            @ApiParam(value = "End date. <b>Format : YYYY-MM-DD</b>",
                    required = false) @FormParam("dateto") String dateto,
            @ApiParam(value = "Unique code to identify warehouse",
                    required = false) @FormParam("m_warehouse_id") Integer m_warehouse_id,
            @ApiParam(value = "Unique code to identify business partner",
                    required = false) @FormParam("c_bpartner_id") Integer c_bpartner_id,
            @ApiParam(value = "Number of document",
                    required = false) @FormParam("documentno") String documentno) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {
            ParamRequest param = new ParamRequest();
            param.setIssotrx(issotrx);
            param.setStatus(status);
            param.setPage(page);
            param.setPerpage(perpage);
            param.setDatefrom(datefrom);
            param.setDateto(dateto);
            param.setM_warehouse_id(m_warehouse_id);
            param.setC_bpartner_id(c_bpartner_id);
            param.setDocumentno(documentno);

            AuthenticationApi.setContextByToken(token);
            TransactionService srv = new TransactionService();
            result = srv.get(param, "OrderList");
        }
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/getOrderLine")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 3, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve specific orderline data.")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getOrderLine(@ApiParam(required = false,
            value = "Unique code for identifier order data.") @FormParam("c_order_id") int c_order_id,
            @ApiParam(required = false,
                    value = "Unique code for identifier orderline data.") @FormParam("c_orderline_id") int c_orderline_id,
            @ApiParam(required = false,
                    value = "Unique code for identifier product data.") @FormParam("m_product_id") int m_product_id,
            @ApiParam(required = false,
                    value = "Name of product.") @FormParam("product_name") String product_name,
            @ApiParam(value = "Amount of data showed per page",
                    required = false) @FormParam("perpage") Integer perpage,
            @ApiParam(value = "Page that you wanted to show",
                    required = false) @FormParam("page") Integer page) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {
            ParamRequest param = new ParamRequest();
            param.setC_order_id(c_order_id);
            param.setC_orderline_id(c_orderline_id);
            param.setM_product_id(m_product_id);
            param.setProduct_name(product_name);
            param.setPage(page);
            param.setPerpage(perpage);

            AuthenticationApi.setContextByToken(token);
            TransactionService srv = new TransactionService();
            result = srv.get(param, "OrderLine");
        }
        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/getInOut")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 4, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve one or many in out data. At least 1 parameter must be filled.",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getInOut(@ApiParam(value = "Unique code to identifier material receipt data.",
            required = true) @FormParam("m_inout_id") Integer m_inout_id) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {
            ParamRequest param = new ParamRequest();
            param.setM_inout_id(m_inout_id);

            AuthenticationApi.setContextByToken(token);
            TransactionService srv = new TransactionService();
            result = srv.get(param, "InOut");
        }
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/getInOutList")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 5, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve list of in out data.")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getInOutList(
            @ApiParam(
                    value = "This column indicates the status of a document. <b>Format : "
                            + "Waiting Confirmation/ " + "Approved/ " + "Completed/ " + "Drafted/ "
                            + "Invalid/ " + "Reversed/ " + "Voided/ " + "In Progress/ "
                            + "Waiting Payment/ " + "Unknown/ " + "Closed/ " + "Not Approved</b>",
                    required = false) @FormParam("status") String status,
            //@formatter:on
            @ApiParam(
                    value = "Indication of sales transaction data. Shipment : <b>Y</b>. Material Receipt : <b>N</b>. <b>Format : Y/N</b>",
                    required = false) @FormParam("issotrx") String issotrx,
            @ApiParam(value = "Amount of data showed per page",
                    required = false) @FormParam("perpage") Integer perpage,
            @ApiParam(value = "Page that you wanted to show",
                    required = false) @FormParam("page") Integer page,
            @ApiParam(value = "Start date. <b>Format : YYYY-MM-DD</b>",
                    required = false) @FormParam("datefrom") String datefrom,
            @ApiParam(value = "End date. <b>Format : YYYY-MM-DD</b>",
                    required = false) @FormParam("dateto") String dateto,
            @ApiParam(value = "Number of document",
                    required = false) @FormParam("documentno") String documentno) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {
            ParamRequest param = new ParamRequest();
            param.setIssotrx(issotrx);
            param.setStatus(status);
            param.setPage(page);
            param.setPerpage(perpage);
            param.setDatefrom(datefrom);
            param.setDateto(dateto);
            param.setDocumentno(documentno);

            AuthenticationApi.setContextByToken(token);
            TransactionService srv = new TransactionService();
            result = srv.get(param, "InOutList");
        }
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/getInOutLine")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 6, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve specific in out line data. At least 1 parameter must be filled.",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getInOutLine(@ApiParam(value = "Unique code to identifier in out line data.",
            required = true) @FormParam("m_inoutline_id") Integer m_inoutline_id) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {
            ParamRequest param = new ParamRequest();
            param.setM_inoutline_id(m_inoutline_id);

            AuthenticationApi.setContextByToken(token);
            TransactionService srv = new TransactionService();
            result = srv.get(param, "InOutLine");
        }
        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/getInvoice")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 7, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve one or many invoice data. At least 1 parameter must be filled.",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getInvoice(@ApiParam(value = "Unique code to identifier invoice data.",
            required = true) @FormParam("c_invoice_id") Integer c_invoice_id) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {
            ParamRequest param = new ParamRequest();
            param.setC_invoice_id(c_invoice_id);

            AuthenticationApi.setContextByToken(token);
            TransactionService srv = new TransactionService();
            result = srv.get(param, "Invoice");
        }
        return ResponseData.finalResponse(result);
    }
    
    @POST
    @Path("/getInvoiceMulti")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 9, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve specific invoice data. At least 1 parameter must be filled.",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getInvoiceMulti(@ApiParam(value = "c_invoice_id multi with ';' as delimeter",
            required = true) @FormParam("c_invoice_id") String value) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {
            ParamRequest param = new ParamRequest();
            param.setValue(value);

            AuthenticationApi.setContextByToken(token);
            TransactionService srv = new TransactionService();
            result = srv.get(param, "InvoiceMulti");
        }
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/getInvoiceList")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 8, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve list of invoice data.")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getInvoiceList(
            @ApiParam(
                    value = "This column indicates the status of a document. <b>Format : "
                            + "Waiting Confirmation/ " + "Approved/ " + "Completed/ " + "Drafted/ "
                            + "Invalid/ " + "Reversed/ " + "Voided/ " + "In Progress/ "
                            + "Waiting Payment/ " + "Unknown/ " + "Closed/ " + "Not Approved</b>",
                    required = false) @FormParam("status") String status,
            //@formatter:on
            @ApiParam(
                    value = "Indication of sales transaction data. Shipment : <b>Y</b>. Material Receipt : <b>N</b>. <b>Format : Y/N</b>",
                    required = false) @FormParam("issotrx") String issotrx,
            @ApiParam(value = "Amount of data showed per page",
                    required = false) @FormParam("perpage") Integer perpage,
            @ApiParam(value = "Page that you wanted to show",
                    required = false) @FormParam("page") Integer page,
            @ApiParam(value = "Start date. <b>Format : YYYY-MM-DD</b>",
                    required = false) @FormParam("datefrom") String datefrom,
            @ApiParam(value = "End date. <b>Format : YYYY-MM-DD</b>",
                    required = false) @FormParam("dateto") String dateto) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {
            ParamRequest param = new ParamRequest();
            param.setIssotrx(issotrx);
            param.setStatus(status);
            param.setPage(page);
            param.setPerpage(perpage);
            param.setDatefrom(datefrom);
            param.setDateto(dateto);

            AuthenticationApi.setContextByToken(token);
            TransactionService srv = new TransactionService();
            result = srv.get(param, "InvoiceList");
        }
        return ResponseData.finalResponse(result);
    }

    @POST
    @Path("/getInvoiceLine")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 9, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve specific invoice data. At least 1 parameter must be filled.",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getInvoiceLine(@ApiParam(value = "Unique code to identifier in out line data.",
            required = true) @FormParam("c_invoiceline_id") Integer c_invoiceline_id) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {
            ParamRequest param = new ParamRequest();
            param.setC_invoiceline_id(c_invoiceline_id);

            AuthenticationApi.setContextByToken(token);
            TransactionService srv = new TransactionService();
            result = srv.get(param, "InvoiceLine");
        }
        return ResponseData.finalResponse(result);
    }

//    @POST
//    @Path("/getPayment")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 10, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many payment data. At least 1 parameter must be filled.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getPayment(
//            @ApiParam(value = "Unique code for identifier payment data.",
//                    required = false) @FormParam("c_payment_id") Integer c_payment_id,
//            @ApiParam(value = "Document number of the order data.",
//                    required = false) @FormParam("documentno") String documentno) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setDocumentno(documentno);
//            param.setC_payment_id(c_payment_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionImpl impl = new TransactionImpl();
//            result = impl.getPayment(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/getDocumentApprovedHeader")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 11, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve document approval header data.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getDocumentApprovedHeader(
//            @ApiParam(value = "Start date. <b>Format : YYYY-MM-DD</b>",
//                    required = true) @FormParam("datefrom") String datefrom,
//            @ApiParam(value = "End date. <b>Format : YYYY-MM-DD</b>",
//                    required = true) @FormParam("dateto") String dateto,
//            @ApiParam(value = "<b>Format : Accept/Reject</b>",
//                    required = true) @FormParam("status") String status,
//            @ApiParam(value = "Process Name.",
//                    required = false) @FormParam("process_name") String process_name) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//            param.setDatefrom(datefrom);
//            param.setDateto(dateto);
//            param.setStatus(status);
//            param.setProcess_name(process_name);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "DocumentApprovedHeader");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/getDocumentApprovedDetail")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 12, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve document approval detail data.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getDocumentApprovedDetail(
//            @ApiParam(value = "Unique code for identifier workflow activity.",
//                    required = true) @FormParam("ad_wf_activity_id") Integer ad_wf_activity_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//            param.setAd_wf_activity_id(ad_wf_activity_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "DocumentApprovedDetail");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/getCementReceived")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 13, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve cement received data.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getCementReceived(
//            @ApiParam(value = "accountdate",
//                    required = true) @FormParam("accountdate") String accountdate,
//            @ApiParam(value = "distributorcode",
//                    required = true) @FormParam("distributorcode") String distributorcode,
//            @ApiParam(value = "Start date. <b>Format : YYYY-MM-DD</b>",
//                    required = true) @FormParam("datefrom") String datefrom,
//            @ApiParam(value = "End date. <b>Format : YYYY-MM-DD</b>",
//                    required = true) @FormParam("dateto") String dateto) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setAccountdate(accountdate);
//            param.setDistributorcode(distributorcode);
//            param.setDatefrom(datefrom);
//            param.setDateto(dateto);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionImpl impl = new TransactionImpl();
//            result = impl.getCementReceived(param, false);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/getCementReceivedBSA")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 14, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve cement received BSA data.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getCementReceivedBSA(
//            @ApiParam(value = "Start date. <b>Format : YYYY-MM-DD</b>",
//                    required = true) @FormParam("datefrom") String datefrom,
//            @ApiParam(value = "End date. <b>Format : YYYY-MM-DD</b>",
//                    required = true) @FormParam("dateto") String dateto) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setDatefrom(datefrom);
//            param.setDateto(dateto);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionImpl impl = new TransactionImpl();
//            result = impl.getCementReceived(param, true);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/getMaterialStock")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 15, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve stock material data.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getMaterialStock(
//            @ApiParam(value = "distributorcode",
//                    required = true) @FormParam("distributorcode") String distributorcode,
//            @ApiParam(value = "movementdate",
//                    required = true) @FormParam("movementdate") String movementdate) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setDistributorcode(distributorcode);;
//            param.setMovementdate(movementdate);;
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionImpl impl = new TransactionImpl();
//            result = impl.getMaterialStock(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getUserInfo")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 16, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve list of User Info",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getUserInfo(
//            @ApiParam(required = true) @FormParam("ad_user_id") int ad_user_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//            param.setAd_user_id(ad_user_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "UserInfo");
//        }
//        return ResponseData.finalResponse(result);
//    }
//    
//    @POST
//    @Path("/getAttendanceHistory")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 16, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve list of User Info",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getAttendanceHistory(
//            @ApiParam(required = false) @FormParam("NIK") String nik,
//            @ApiParam(required = false) @FormParam("Name") String name,
//            @ApiParam(required = false) @FormParam("Branch Name") int forca_branch_id,
//            @ApiParam(value = "ex : 2019-01-20", required = false) @FormParam("Date From") String date_from,
//            @ApiParam(value = "ex : 2019-01-20", required = false) @FormParam("Date To") String date_to,
//            @ApiParam(value = "in / out", required = false) @FormParam("Status") String status,
//            @ApiParam(value = "Amount of data showed per page", required = false) @FormParam("perpage") Integer perpage,
//            @ApiParam(value = "The page that you want to display",required = false) @FormParam("page") Integer page
//    		) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//            param.setNik(nik);
//            param.setName(name);
//            param.setDatefrom(date_from);
//            param.setDateto(date_to);
//            param.setStatus(status);
//            param.setPage(page);
//            param.setPerpage(perpage);
//            param.setForca_branch_id(forca_branch_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "getAttendanceHistory");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getLocation")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 17, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve list of Location",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getLocation(
//            @ApiParam(required = true) @FormParam("ad_user_id") int ad_user_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//            param.setAd_user_id(ad_user_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "Location");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getWorkflow")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 18, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve List All Pending Approval",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getWorkflow() {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "WorkflowAll");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/getWorkflowGrup")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 19, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this methof to retrieve workflow of document group approval",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getWorkflowGrup() {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "WorkflowGroup");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/getWorkflowList")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 20, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve workflow list data",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getWorkflowList(
//            @ApiParam(value = "Workflow name",
//                    required = true) @FormParam("ad_wf_name") String ad_wf_name,
//            @ApiParam(value = "Unique code for identifier workflow node",
//                    required = true) @FormParam("ad_wf_node_id") Integer ad_wf_node_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//            param.setAd_wf_name(ad_wf_name);
//            param.setAd_wf_node_id(ad_wf_node_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "WorkflowList");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/getWorkflowDetail")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 21, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve workflow detail data",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getWorkflowDetail(
//            @ApiParam(value = "Workflow name",
//                    required = true) @FormParam("ad_wf_name") String ad_wf_name,
//            @ApiParam(value = "Unique code for identifier workflow activity",
//                    required = true) @FormParam("ad_wf_activity_id") Integer ad_wf_activity_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//            param.setAd_wf_name(ad_wf_name);
//            param.setAd_wf_activity_id(ad_wf_activity_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "WorkflowDetail");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getInventoryMove")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 23, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve specific inventory move data.")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response getInventoryMove(@ApiParam(required = true,
//            value = "Unique code for identifier inventory move data.") @FormParam("m_movement_id") Integer m_movement_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_movement_id(m_movement_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "InventoryMove");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getInventoryMoveList")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 24, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve list of inventory move data.")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response getInventoryMoveList(@ApiParam(
//            value = "This form indicates that the document is approved or not. <b>Format : Y/N</b>",
//            required = false) @FormParam("isapproved") String isapproved,
//    //@formatter:off
//            @ApiParam(value = "This column indicates the status of a document. <b>Format : "
//                    + "Waiting Confirmation/ " 
//                    + "Approved/ " 
//                    + "Completed/ " 
//                    + "Drafted/ " 
//                    + "Invalid/ " 
//                    + "Reversed/ " 
//                    + "Voided/ " 
//                    + "In Progress/ " 
//                    + "Waiting Payment/ " 
//                    + "Unknown/ " 
//                    + "Closed/ " 
//                    + "Not Approved</b>",
//            required = false) @FormParam("status") String status,
//            //@formatter:on
//            @ApiParam(value = "Amount of data showed per page",
//                    required = false) @FormParam("perpage") Integer perpage,
//            @ApiParam(value = "Page that you wanted to show",
//                    required = false) @FormParam("page") Integer page,
//            @ApiParam(value = "Start date. <b>Format : YYYY-MM-DD</b>",
//                    required = false) @FormParam("datefrom") String datefrom,
//            @ApiParam(value = "End date. <b>Format : YYYY-MM-DD</b>",
//                    required = false) @FormParam("dateto") String dateto,
//            @ApiParam(value = "Number of document",
//                    required = false) @FormParam("documentno") String documentno) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setIsapproved(isapproved);
//            param.setStatus(status);
//            param.setPage(page);
//            param.setPerpage(perpage);
//            param.setDatefrom(datefrom);
//            param.setDateto(dateto);
//            param.setDocumentno(documentno);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "InventoryMoveList");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getInventoryMoveLine")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 25, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve specific inventory move line data.")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response getInventoryMoveLine(@ApiParam(required = true,
//            value = "Unique code for identifier inventory move line data.") @FormParam("m_movementline_id") Integer m_movementline_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_movementline_id(m_movementline_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "InventoryMoveLine");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getInvoiceCustomerSAAS")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 26, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve specific inventory invoice customer SAAS data.")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response getInvoiceCustomerData(@ApiParam(required = false,
//            value = "Unique code for identifier invoice data.") @FormParam("c_invoice_id") Integer c_invoice_id,
//            @ApiParam(required = false,
//                    value = "Unique code for search key of business partner.") @FormParam("bpkey") String bpkey,
//            @ApiParam(required = false,
//                    value = "Unique code for search key of product.") @FormParam("productkey") String productkey,
//            @ApiParam(required = false,
//                    value = "The document is paid or no. <b>Format : Y/N</b>") @FormParam("ispaid") String ispaid) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_invoice_id(c_invoice_id);
//            param.setBpkey(bpkey);
//            param.setProductkey(productkey);
//            param.setIspaid(ispaid);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "InvoiceCustomerSAAS");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setOrderComplete")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 27, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to doing action complete of order")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setOrderComplete(@ApiParam(value = "Unique code for identifier order data",
//            required = true) @FormParam("c_order_id") int c_order_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_order_id(c_order_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "OrderComplete");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/setOrderVoid")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 28, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to doing action void of order")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setOrderVoid(@ApiParam(value = "Unique code for identifier order data",
//            required = true) @FormParam("c_order_id") int c_order_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_order_id(Integer.valueOf(c_order_id));
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "OrderVoid");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/setOrder")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 29, httpMethod = "POST", value = "application/json",
//            notes = "Use this method to set or insert order. Fill c_order_id for set purpose.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response setOrder(ParamOrder param) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            String trxLocalName = Trx.createTrxName("setOrder");
//            Trx trxLocal = Trx.get(trxLocalName, true);
//            try {
//                AuthenticationApi.setContextByToken(token);
//                TransactionImpl impl = new TransactionImpl();
//                result = impl.setOrder(param, trxLocal);
//            } catch (Exception e) {
//                result = ResponseData.errorResponse(e.getMessage());
//                trxLocal.rollback();
//            } finally {
//                if (trxLocal.isActive()) {
//                    trxLocal.commit();
//                }
//                trxLocal.close();
//            }
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/setOrderLine")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 30, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to set or insert orderline. Fill c_orderline_id for set purpose. ")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setOrderLine(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
//            @ApiParam(required = true) @FormParam("c_order_id") int c_order_id,
//            @ApiParam(required = true) @FormParam("ad_orgtrx_id") int ad_orgtrx_id,
//            @ApiParam(required = false) @FormParam("c_orderline_id") int c_orderline_id,
//            @ApiParam(required = true) @FormParam("qty") BigDecimal qty,
//            @ApiParam(required = false) @FormParam("priceentered") BigDecimal priceentered,
//            @ApiParam(required = false) @FormParam("priceactual") BigDecimal priceactual,
//            @ApiParam(required = true) @FormParam("m_product_id") int m_product_id,
//            @ApiParam(required = true) @FormParam("c_uom_id") int c_uom_id,
//            @ApiParam(required = false) @FormParam("c_tax_id") int c_tax_id,
//            @ApiParam(required = false) @FormParam("description") String description) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setCrm_id(Integer.valueOf(crm_id));
//            param.setC_order_id(Integer.valueOf(c_order_id));
//            param.setAd_orgtrx_id(Integer.valueOf(ad_orgtrx_id));
//            param.setM_product_id(Integer.valueOf(m_product_id));
//            param.setC_orderline_id(Integer.valueOf(c_orderline_id));
//            param.setQty(qty);
//            param.setPriceentered(priceentered);
//            param.setPriceactual(priceactual);
//            param.setC_uom_id(Integer.valueOf(c_uom_id));
//            param.setC_tax_id(c_tax_id);
//            param.setDescription(description);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "OrderLine");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    @POST
//    @Path("/setMultiDocumentApproved")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 31, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to execute multi approval",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response setMultiDocumentApproved(@ApiParam(
//            value = "Unique code for identifier workflow activity data",
//            required = true) @FormParam("multiple_ad_wf_activity_id") String multiple_ad_wf_activity_id,
//            @ApiParam(value = "Message of the document",
//                    required = true) @FormParam("message") String message,
//            @ApiParam(
//                    value = "This form indicates that the document is approved or not. Format : <b>Y/N</b>",
//                    required = true) @FormParam("yesno") String yesno) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//            param.setMultiple_ad_wf_activity_id(multiple_ad_wf_activity_id);
//            param.setMessage(message);
//            param.setYesno(yesno);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "MultiDocumentApproved");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInventoryMove")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 32, httpMethod = "POST", value = "application/json",
//            notes = "Use this method to set or insert inventory move. Fill m_movement_id for set purpose.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response setInventoryMove(ParamInventoryMove param) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            String trxLocalName = Trx.createTrxName("setInventoryMove");
//            Trx trxLocal = Trx.get(trxLocalName, true);
//            try {
//                AuthenticationApi.setContextByToken(token);
//                TransactionImpl impl = new TransactionImpl();
//                result = impl.setInventoryMove(param, trxLocal);
//            } catch (Exception e) {
//                result = ResponseData.errorResponse(e.getMessage());
//                trxLocal.rollback();
//            } finally {
//                if (trxLocal.isActive()) {
//                    trxLocal.commit();
//                }
//                trxLocal.close();
//            }
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInvetoryMoveLine")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 33, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to set or insert inventory move line. Fill m_movementline_id for set purpose. ")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setInventoryMoveLine(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
//            @ApiParam(required = true) @FormParam("m_movement_id") int m_movement_id,
//            @ApiParam(required = false) @FormParam("ad_orgtrx_id") int ad_orgtrx_id,
//            @ApiParam(required = false) @FormParam("m_movementline_id") int m_movementline_id,
//            @ApiParam(required = true) @FormParam("m_locator_id") int m_locator_id,
//            @ApiParam(required = true) @FormParam("m_locatorto_id") int m_locatorto_id,
//            @ApiParam(required = true) @FormParam("forca_qtyentered") BigDecimal forca_qtyentered,
//            @ApiParam(required = true) @FormParam("m_product_id") int m_product_id,
//            @ApiParam(required = true) @FormParam("forca_c_uom_id") int forca_c_uom_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setCrm_id(Integer.valueOf(crm_id));
//            param.setM_movement_id(m_movement_id);
//            param.setAd_orgtrx_id(Integer.valueOf(ad_orgtrx_id));
//            param.setM_movementline_id(m_movementline_id);
//            param.setM_locator_id(m_locator_id);
//            param.setM_locatorto_id(m_locatorto_id);
//            param.setForca_qtyentered(forca_qtyentered);
//            param.setM_product_id(Integer.valueOf(m_product_id));
//            param.setForca_c_uom_id(forca_c_uom_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "InventoryMoveLine");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInventoryMoveComplete")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 34, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to doing action inventory move")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setInventoryMoveComplete(
//            @ApiParam(value = "Unique code for identifier inventory move data",
//                    required = true) @FormParam("m_movement_id") int m_movement_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_movement_id(m_movement_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "InventoryMoveComplete");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInventoryMoveVoid")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 35, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to doing action void of inventory move")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setInventoryMoveVoid(
//            @ApiParam(value = "Unique code for identifier inventory move data",
//                    required = true) @FormParam("m_movement_id") int m_movement_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_movement_id(m_movement_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "InventoryMoveVoid");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInOut")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 36, httpMethod = "POST", value = "application/json",
//            notes = "Use this method to set or insert in out data. Fill m_inout_id for set purpose.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response setInOut(ParamInOut param) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            String trxLocalName = Trx.createTrxName("setInOut");
//            Trx trxLocal = Trx.get(trxLocalName, true);
//            try {
//                AuthenticationApi.setContextByToken(token);
//                TransactionImpl impl = new TransactionImpl();
//                result = impl.setInOut(param, trxLocal);
//            } catch (Exception e) {
//                result = ResponseData.errorResponse(e.getMessage());
//                trxLocal.rollback();
//            } finally {
//                if (trxLocal.isActive()) {
//                    trxLocal.commit();
//                }
//                trxLocal.close();
//            }
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInOutLine")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 37, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to set or insert in out line. Fill m_inoutline_id for set purpose. ")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setInOutLine(@ApiParam(required = true) @FormParam("m_inout_id") int m_inout_id,
//            @ApiParam(required = false) @FormParam("m_inoutline_id") int m_inoutline_id,
//            @ApiParam(required = true) @FormParam("m_locator_id") int m_locator_id,
//            @ApiParam(required = true) @FormParam("qty") BigDecimal qty,
//            @ApiParam(required = true) @FormParam("m_product_id") Integer m_product_id,
//            @ApiParam(required = true) @FormParam("c_uom_id") Integer c_uom_id,
//            @ApiParam(required = true) @FormParam("c_orderline") Integer c_orderline_id,
//            @ApiParam(required = false) @FormParam("c_project_id") Integer c_project_id,
//            @ApiParam(required = false) @FormParam("user2_id") Integer user2_id,
//            @ApiParam(required = false) @FormParam("description") String description) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_inout_id(m_inout_id);
//            param.setM_inoutline_id(m_inoutline_id);
//            param.setM_locator_id(m_locator_id);
//            param.setQty(qty);
//            param.setM_product_id(m_product_id);
//            param.setC_uom_id(c_uom_id);
//            param.setC_orderline_id(c_orderline_id);
//            param.setDescription(description);
//            param.setC_project_id(c_project_id);
//            param.setUser2_id(user2_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "InOutLine");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInOutComplete")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 38, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to doing action in out")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setInOutComplete(@ApiParam(value = "Unique code for identifier in out data",
//            required = true) @FormParam("m_inout_id") int m_inout_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_inout_id(m_inout_id);;
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "InOutComplete");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInOutVoid")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 39, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to doing action void of in out")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setInOutVoid(@ApiParam(value = "Unique code for identifier inventory move data",
//            required = true) @FormParam("m_inout_id") int m_inout_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_inout_id(m_inout_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "InOutVoid");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInvoice")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 40, httpMethod = "POST", value = "application/json",
//            notes = "Use this method to set or insert invoice data. Fill c_invoice_id for set purpose.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response setInvoice(ParamInvoices param) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            String trxLocalName = Trx.createTrxName("setInvoice");
//            Trx trxLocal = Trx.get(trxLocalName, true);
//            try {
//                AuthenticationApi.setContextByToken(token);
//                TransactionImpl impl = new TransactionImpl();
//                result = impl.setInvoice(param, trxLocal);
//            } catch (Exception e) {
//                result = ResponseData.errorResponse(e.getMessage());
//                trxLocal.rollback();
//            } finally {
//                if (trxLocal.isActive()) {
//                    trxLocal.commit();
//                }
//                trxLocal.close();
//            }
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInvoiceLine")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 41, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to set or insert invoice line. Fill c_invoiceline_id for set purpose. ")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setInvoiceLine(
//            @ApiParam(required = true) @FormParam("c_invoice_id") int c_invoice_id,
//            @ApiParam(required = false) @FormParam("c_invoiceline_id") int c_invoiceline_id,
//            @ApiParam(required = false) @FormParam("qty") BigDecimal qty,
//            @ApiParam(required = false) @FormParam("priceentered") BigDecimal priceentered,
//            @ApiParam(required = false) @FormParam("c_charge_id") int c_charge_id,
//            @ApiParam(required = false) @FormParam("m_product_id") Integer m_product_id,
//            @ApiParam(required = false) @FormParam("c_uom_id") Integer c_uom_id,
//            @ApiParam(required = false) @FormParam("c_orderline_id") Integer c_orderline_id,
//            @ApiParam(required = false) @FormParam("m_inoutline_id") Integer m_inoutline_id,
//            @ApiParam(required = false) @FormParam("description") String description) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_invoice_id(c_invoice_id);
//            param.setC_invoiceline_id(c_invoiceline_id);
//            param.setPriceentered(priceentered);
//            param.setC_charge_id(c_charge_id);
//            param.setM_inoutline_id(m_inoutline_id);
//            param.setQty(qty);
//            param.setM_product_id(m_product_id);
//            param.setC_uom_id(c_uom_id);
//            param.setC_orderline_id(c_orderline_id);
//            param.setDescription(description);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "InvoiceLine");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInvoiceComplete")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 42, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to doing action invoice")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setInvoiceComplete(@ApiParam(value = "Unique code for identifier invoice data",
//            required = true) @FormParam("c_invoice_id") int c_invoice_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_invoice_id(c_invoice_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "InvoiceComplete");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setInvoiceVoid")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 43, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to doing action void of invoice")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setInvoiceVoid(@ApiParam(value = "Unique code for identifier invoice data",
//            required = true) @FormParam("c_invoice_id") int c_invoice_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_invoice_id(c_invoice_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "InvoiceVoid");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setPayment")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 44, httpMethod = "POST", value = "application/json",
//            notes = "Use this method to set or insert payment data. Fill c_payment_id for set purpose.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response setPayment(
//            @ApiParam(required = false) @FormParam("c_payment_id") int c_payment_id,
//            @ApiParam(required = false) @FormParam("c_bankaccount_id") int c_bankaccount_id,
//            @ApiParam(required = false) @FormParam("c_doctype_id") int c_doctype_id,
//            @ApiParam(required = false) @FormParam("datetrx") String datetrx,
//            @ApiParam(required = false) @FormParam("dateacct") String dateacct,
//            @ApiParam(required = false) @FormParam("description") String description,
//            @ApiParam(required = false) @FormParam("c_bpartner_id") Integer c_bpartner_id,
//            @ApiParam(required = false) @FormParam("c_invoice_id") Integer c_invoice_id,
//            @ApiParam(required = false) @FormParam("ad_org_id") Integer ad_org_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_payment_id(c_payment_id);
//            param.setC_bankaccount_id(c_bankaccount_id);
//            param.setC_doctype_id(c_doctype_id);
//            param.setDatetrx(datetrx);
//            param.setDateacct(dateacct);
//            param.setC_bpartner_id(c_bpartner_id);
//            param.setC_invoice_id(c_invoice_id);
//            param.setDescription(description);
//            param.setAd_org_id(ad_org_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "Payment");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setPaymentAllocate")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 45, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to set or insert payment allocate. Fill c_paymentallocate_id for set purpose. ")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setPaymentAllocate(
//            @ApiParam(required = false) @FormParam("c_paymentallocate_id") int c_paymentallocate_id,
//            @ApiParam(required = false) @FormParam("c_payment_id") int c_payment_id,
//            @ApiParam(required = false) @FormParam("ad_org_id") Integer ad_org_id,
//            @ApiParam(required = false) @FormParam("c_invoice_id") Integer c_invoice_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_paymentallocate_id(c_paymentallocate_id);
//            param.setAd_org_id(ad_org_id);
//            param.setC_invoice_id(c_invoice_id);
//            param.setC_payment_id(c_payment_id);
//
//            param.setC_invoice_id(c_invoice_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "PaymentAllocate");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setPaymentComplete")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 46, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to doing action payment")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setPaymentComplete(@ApiParam(value = "Unique code for identifier payment data",
//            required = true) @FormParam("c_payment_id") int c_payment_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_payment_id(c_payment_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "PaymentComplete");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setPaymentVoid")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 47, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to doing action void of payment")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setPaymentVoid(@ApiParam(value = "Unique code for identifier payment data",
//            required = true) @FormParam("c_payment_id") int c_payment_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_payment_id(c_payment_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "PaymentVoid");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/insertAttendance")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 48, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to insert Attendance data.")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response insertAttendance(@ApiParam(required = false) @FormParam("nik") String nik,
//            @ApiParam(required = false) @FormParam("name") String name,
//            @ApiParam(required = false) @FormParam("status") String status,
//            @ApiParam(required = false) @FormParam("imageurl") String imageurl,
//            @ApiParam(required = false) @FormParam("latitude") BigDecimal latitude,
//            @ApiParam(required = false) @FormParam("longitude") BigDecimal longitude,
//            @ApiParam(required = false) @FormParam("forca_distance") BigDecimal forca_distance,
//            @ApiParam(required = false) @FormParam("forca_branch_id") Integer forca_branch_id,
//            @ApiParam(required = false) @FormParam("forca_deviceid") String forca_deviceid,
//            @ApiParam(required = false) @FormParam("forca_devicemodel") String forca_devicemodel,
//            //20588
//            @ApiParam(required = false) @FormParam("description") String description,
//            @ApiParam(required = false) @FormParam("forca_location") String forca_location)
//{
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setNik(nik);
//            param.setName(name);
//            param.setStatus(status);
//            param.setImageurl(imageurl);
//            param.setLatitude(latitude);
//            param.setLongitude(longitude);
//            param.setForca_distance(forca_distance);
//            param.setForca_branch_id(forca_branch_id);
//            param.setForca_deviceid(forca_deviceid);
//            param.setForca_devicemodel(forca_devicemodel);
//            param.setForca_location(forca_location);
//            param.setDescription(description);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "Attendance");
//        }
//        return ResponseData.finalResponse(result);
//    }
//    
//    @POST
//    @Path("/insertPersonID")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 48, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to insert Person ID data.")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response insertPersonID(@ApiParam(required = true) @FormParam("ad_user_id") int ad_user_id, 
//            @ApiParam(required = true) @FormParam("forca_person_id") String forca_person_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setAd_user_id(ad_user_id);
//            param.setForca_person_id(forca_person_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "PersonID");
//        }
//        return ResponseData.finalResponse(result);
//    }
//    
//    @POST
//    @Path("/insertDeviceID")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 48, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to insert Person ID data.")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response insertDeviceID(@ApiParam(required = true) @FormParam("ad_user_id") int ad_user_id, 
//            @ApiParam(required = true) @FormParam("forca_deviceid") String forca_deviceid) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setAd_user_id(ad_user_id);
//            param.setForca_deviceid(forca_deviceid);
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "DeviceID");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/insertInvoiceVendor")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 49, httpMethod = "POST", value = "application/json",
//            notes = "Use this method to create invoice vendor data",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response insertInvoiceVendor(ParamInvoice param) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            String trxLocalName = Trx.createTrxName("insertInvoiceVendor");
//            Trx trxLocal = Trx.get(trxLocalName, true);
//            try {
//                AuthenticationApi.setContextByToken(token);
//                TransactionImpl transImpl = new TransactionImpl();
//                result = transImpl.insertInvoiceVendor(param, token, trxLocal);
//            } catch (Exception e) {
//                trxLocal.rollback();
//            } finally {
//                if (trxLocal.isActive()) {
//                    trxLocal.commit();
//                }
//                trxLocal.close();
//            }
//        }
//        return Response.status(Status.OK).entity(result).build();
//    }
//
//    @POST
//    @Path("/inactiveInventoryMove")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 50, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to inactive inventory move data")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response inactiveInventoryMove(
//            @ApiParam(required = true) @FormParam("m_movement_id") int m_movement_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_movement_id(m_movement_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "IAInventoryMove");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/inactiveInOut")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 51, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to inactive in out data")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response inactiveInOut(
//            @ApiParam(required = true) @FormParam("m_inout_id") int m_inout_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_inout_id(m_inout_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "IAInOut");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/inactiveOrder")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 52, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to inactive order data")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response inactiveOrder(
//            @ApiParam(required = true) @FormParam("c_order_id") int c_order_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_order_id(c_order_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "IAOrder");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/deleteInventoryMove")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 53, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to delete inventory move data")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response deleteInventoryMove(
//            @ApiParam(required = true) @FormParam("m_movement_id") int m_movement_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_movement_id(m_movement_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "delInventoryMove");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/deleteInventoryMoveLine")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 54, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to delete inventory move line data")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response deleteInventoryMoveLine(
//            @ApiParam(required = true) @FormParam("m_movementline_id") int m_movementline_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_movementline_id(m_movementline_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "delInventoryMoveLine");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/deleteInOut")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 55, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to delete in out data")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response deleteInOut(
//            @ApiParam(required = true) @FormParam("m_inout_id") int m_inout_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_inout_id(m_inout_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "delInOut");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/deleteInOutLine")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 56, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to delete in out line data")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response deleteInOutLine(
//            @ApiParam(required = true) @FormParam("m_inoutline_id") int m_inoutline_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_inoutline_id(m_inoutline_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "delInOutLine");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/deleteOrder")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 57, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to delete order data")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response deleteOrder(
//            @ApiParam(required = true) @FormParam("c_order_id") int c_order_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_order_id(c_order_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "delOrder");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/deleteOrderLine")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 58, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to delete order line data")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response deleteOrderLine(
//            @ApiParam(required = true) @FormParam("c_orderline_id") int c_orderline_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_orderline_id(c_orderline_id);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "delOrderLine");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/calculateUOMConversion")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 59, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to calculate quantity of UOM Conversion")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response calculateUOMConversion(@ApiParam(required = true,
//            value = "Unique code to identifier product data") @FormParam("m_product_id") int m_product_id,
//            @ApiParam(required = true,
//                    value = "Unique code to identifier uom from") @FormParam("c_uom_from_id") int c_uom_from_id,
//            @ApiParam(required = true,
//                    value = "Unique code to identifier uom to") @FormParam("c_uom_to_id") int c_uom_to_id,
//            @ApiParam(required = true, value = "Quantity") @FormParam("qty") BigDecimal qty) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_product_id(m_product_id);
//            param.setC_uom_from_id(c_uom_from_id);
//            param.setC_uom_to_id(c_uom_to_id);
//            param.setQty(qty);
//
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "calculateUOMConversion");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getLeaveType")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 60, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve list of Leave Type",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getLeaveType() {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "LeaveType");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getLeave")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 60, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to retrieve list of Leave",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getLeave(
//            @ApiParam(required = true) @FormParam("ad_user_id") int ad_user_id,
//            @ApiParam(value = "Amount of data showed per page", required = false) @FormParam("perpage") Integer perpage,
//            @ApiParam(value = "The page that you want to display",required = false) @FormParam("page") Integer page) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setToken(token);
//            param.setAd_user_id(ad_user_id);
//            param.setPage(page);
//            param.setPerpage(perpage);
//            AuthenticationApi.setContextByToken(token);
//            TransactionService srv = new TransactionService();
//            result = srv.get(param, "Leave");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/insertLeave")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 48, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to insert Leave data.")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response insertLeave(
//            @ApiParam(required = true) @FormParam("ad_user_id") int ad_user_id,
//            @ApiParam(required = true) @FormParam("forca_leavetype_id") int forca_leavetype_id,
//            @ApiParam(value = "ex : 2019-01-20", required = true) @FormParam("Date From") String datefrom,
//            @ApiParam(value = "ex : 2019-01-20", required = true) @FormParam("Date To") String dateto,
//            @ApiParam(required = true) @FormParam("forca_totalday") BigDecimal forca_totalday,
//            @ApiParam(required = false) @FormParam("description") String description
//            ) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setAd_user_id(ad_user_id);
//            param.setForca_leavetype_id(forca_leavetype_id);
//            param.setDatefrom(datefrom);
//            param.setDateto(dateto);
//            param.setForca_totalday(forca_totalday);
//            param.setDescription(description);
//            AuthenticationApi.setContextByToken(token);
//            TransactionService impl = new TransactionService();
//            result = impl.set(param, "Leave");
//        }
//        return ResponseData.finalResponse(result);
//    }
}
