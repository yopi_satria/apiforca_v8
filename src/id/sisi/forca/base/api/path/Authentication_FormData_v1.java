/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Fitriani Rohmah Hardianti                                                                                
   Email          : fitrianirohmahhardianti@gmail.com                                                                 
   Subject        : Authentication Form Data v1                   
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.path;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.compiere.util.Util;
import org.springframework.stereotype.Component;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.Authorization;
import id.sisi.forca.base.api.filter.AuthenticationApi;
import id.sisi.forca.base.api.filter.RestFilter;
import id.sisi.forca.base.api.impl.AuthenticationImpl;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.Constants;
import id.sisi.forca.base.api.util.WebserviceUtil;


@Component
@Path("/authentication/v1")
@Api(position = 1, value = "Authentication", description = "Login Validation to get Forca-Token")
public class Authentication_FormData_v1 {
    WebserviceUtil util = new WebserviceUtil();
    AuthenticationApi authAPI = new AuthenticationApi();


    @GET
    @Path("/test")
    public Response test() {
        //ResponseData result = new ResponseData();
            //result = ResponseData.errorResponse("Please input parameter 'username' ");
        System.out.println("api ws token");

        return Response.status(200).build();
    }
    
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 1, httpMethod = "POST", value = "application/x-www-form-urlencoded",
            notes = "Use this method to login and get forca token")
    public Response login(
            @ApiParam(value = "Username Forca ERP",
                    required = true) @FormParam("username") String username,
            @ApiParam(value = "Password user Forca ERP",
                    required = true) @FormParam("password") String password) {
        ResponseData result = new ResponseData();
        if (Util.isEmpty(username)) {
            result = ResponseData.errorResponse("Please input parameter 'username' ");
        } else if (Util.isEmpty(password)) {
            result = ResponseData.errorResponse("Please input parameter 'password' ");
        } else {
            AuthenticationImpl auth = new AuthenticationImpl();
            result = auth.login(username, password);
        }

        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/getClients")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 2, httpMethod = "POST", value = "application/x-www-form-urlencoded",
            notes = "Use this method to retrieve client data",
            authorizations = {@Authorization(value = "Forca-Token")})
    public Response getClients(
            @ApiParam(value = "Username Forca ERP",
                    required = true) @FormParam("username") String username,
            @ApiParam(value = "Password user Forca ERP",
                    required = true) @FormParam("password") String password) {
        ParamRequest param = new ParamRequest();
        ResponseData result = new ResponseData();
        param.setUsername(username);
        param.setPassword(password);

        AuthenticationImpl auth = new AuthenticationImpl();
        result = auth.getClients(param);

        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/getRoles")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 3, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve role user data")
    public Response getRoles(
            @ApiParam(value = "Username Forca ERP",
                    required = true) @FormParam("username") String username,
            @ApiParam(value = "Unique code for identifier client",
                    required = true) @FormParam("ad_client_id") Integer client_id) {
        ParamRequest param = new ParamRequest();
        ResponseData result = new ResponseData();
        param.setUsername(username);
        param.setAd_client_id(client_id);

        AuthenticationImpl auth = new AuthenticationImpl();
        result = auth.getRoles(param);

        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/getOrgs")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 4, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve organization data")
    public Response getOrgs(
            @ApiParam(value = "Username Forca ERP",
                    required = true) @FormParam("username") String username,
            @ApiParam(value = "Unique code for identifier client",
                    required = true) @FormParam("ad_client_id") Integer client_id,
            @ApiParam(value = "Unique code for identifier role",
                    required = true) @FormParam("ad_role_id") Integer role_id) {
        ParamRequest param = new ParamRequest();
        ResponseData result = new ResponseData();

        param.setUsername(username);
        param.setAd_client_id(client_id);
        param.setAd_role_id(role_id);

        AuthenticationImpl auth = new AuthenticationImpl();
        result = auth.getOrgs(param);

        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/getWarehouses")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 5, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to retrieve warehouse data")
    public Response getWarehouses(
            @ApiParam(value = "Username Forca ERP",
                    required = true) @FormParam("username") String username,
            @ApiParam(value = "Unique code for identifier client",
                    required = true) @FormParam("ad_client_id") Integer client_id,
            @ApiParam(value = "Unique code for identifier organization",
                    required = true) @FormParam("ad_org_id") Integer org_id) {
        ParamRequest param = new ParamRequest();
        ResponseData result = new ResponseData();

        param.setUsername(username);
        param.setAd_client_id(client_id);
        param.setAd_org_id(org_id);

        AuthenticationImpl auth = new AuthenticationImpl();
        result = auth.getWarehouses(param);

        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/getToken")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 6, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to get token for request API Forca")
    public Response getToken(
            @ApiParam(value = "Username Forca ERP",
                    required = true) @FormParam("username") String username,
            @ApiParam(value = "Password user Forca ERP",
                    required = true) @FormParam("password") String password,
            @ApiParam(value = "Unique code for identifier client",
                    required = true) @FormParam("ad_client_id") Integer client_id,
            @ApiParam(value = "Unique code for identifier role",
                    required = true) @FormParam("ad_role_id") Integer role_id,
            @ApiParam(value = "Unique code for identifier organization",
                    required = true) @FormParam("ad_org_id") Integer org_id,
            @ApiParam(value = "Unique code for identifier warehouse",
                    required = true) @FormParam("m_warehouse_id") Integer warehouse_id,
            @ApiParam(required = false,
                    value = "Unique code for identifier busineess partner (*) Only POS Apps/Customer") @FormParam("c_bpartner_id") Integer bpartner_id) {

        ParamRequest param = new ParamRequest();
        ResponseData result = new ResponseData();

        param.setUsername(username);
        param.setPassword(password);
        param.setAd_client_id(client_id);
        param.setAd_role_id(role_id);
        param.setAd_org_id(org_id);
        param.setM_warehouse_id(warehouse_id);
        param.setC_bpartner_id(bpartner_id);

        AuthenticationImpl auth = new AuthenticationImpl();
        result = auth.getToken(param);

        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/refreshToken")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 7, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to refresh token when Forca-Token has expired")
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response refreshToken() {
        ParamRequest param = new ParamRequest();
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        
        param.setToken(token);
        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
//        result = apiLoginImpl.refreshToken(param);
        result = null;

        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/infoToken")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 8, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to get info of token")
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response infoToken() {
        ParamRequest param = new ParamRequest();
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);

        param.setToken(token);
        AuthenticationImpl apiLoginImpl = new AuthenticationImpl();
        result = apiLoginImpl.infoToken(param);

        return ResponseData.finalResponse(result);
    }


    @POST
    @Path("/logout")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 9, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Use this method to logout")
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response logout() {
        ParamRequest param = new ParamRequest();
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);

        param.setToken(token);
        AuthenticationImpl auth = new AuthenticationImpl();
        result = auth.logout(param);

        return ResponseData.finalResponse(result);
    }
}
