/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Fitriani Rohmah Hardiyanti                                                                                
   Email          : fitrianirohmahhardiyanti@gmail.com                                                                 
   Subject        : Master FormData v1                              
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.path;

import java.math.BigDecimal;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.stereotype.Component;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.Authorization;
import id.sisi.forca.base.api.filter.AuthenticationApi;
import id.sisi.forca.base.api.filter.RestFilter;
import id.sisi.forca.base.api.impl.MasterImpl;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.service.MasterService;
import id.sisi.forca.base.api.util.Constants;
import id.sisi.forca.base.api.util.WebserviceUtil;;

@Component
@Path("/master/v1")
@Api(position = 2, value = "Master", description = "Master Data in Forca")
public class Master_FormData_v1 {
    WebserviceUtil util = new WebserviceUtil();
    AuthenticationApi authAPI = new AuthenticationApi();

    // ANI
//    @POST
//    @Path("/getBPGroup")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 1, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many business partner group data")
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getBPGroup(
//            @ApiParam(value = "Unique code for identifier business partner group data",
//                    required = false) @FormParam("c_bp_group_id") Integer bp_group_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_bp_group_id(bp_group_id);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getBPGroup(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getCity")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 2, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many city data.")
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getCity(
//            @ApiParam(value = "Unique code for identifier client",
//                    required = false) @FormParam("ad_client_id") Integer ad_client_id,
//            @ApiParam(value = "Unique code for identifier city data",
//                    required = false) @FormParam("c_city_id") Integer city_id,
//            @ApiParam(value = "Unique code for identifier country",
//                    required = true) @FormParam("c_country_id") Integer country_id,
//            @ApiParam(value = "Unique code for identifier region",
//                    required = true) @FormParam("c_region_id") Integer region_id,
//            @ApiParam(value = "Name of the city",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_city_id(city_id);
//            param.setAd_client_id(ad_client_id);
//            param.setC_country_id(country_id);
//            param.setC_region_id(region_id);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getCity(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//
//    // ANI
//    @POST
//    @Path("/getCountry")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 3, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many country data.")
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getCountry(
//            @ApiParam(value = "Unique code for identifier client",
//                    required = false) @FormParam("ad_client_id") Integer ad_client_id,
//            @ApiParam(value = "Unique code for identifier country data",
//                    required = false) @FormParam("c_country_id") Integer country_id,
//            @ApiParam(value = "Name of the country",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_country_id(country_id);
//            param.setAd_client_id(ad_client_id);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getCountry(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getProject")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 4, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many project data.")
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getProject(@ApiParam(value = "Unique code for identifier project data",
//            required = false) @FormParam("c_project_id") Integer c_project_id) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_project_id(c_project_id);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getProject(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getRegion")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 5, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many region data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getRegion(
//            @ApiParam(value = "Unique code for identifier country",
//                    required = true) @FormParam("c_country_id") Integer c_country_id,
//            @ApiParam(value = "Unique code for identifier client",
//                    required = false) @FormParam("ad_client_id") Integer ad_client_id,
//            @ApiParam(value = "Unique code for identifier region data",
//                    required = false) @FormParam("c_region_id") Integer c_region_id,
//            @ApiParam(value = "Name of the region",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_region_id(c_region_id);
//            param.setAd_client_id(ad_client_id);
//            param.setName(name);
//            param.setC_country_id(c_country_id);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getRegion(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getTax")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 6, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many tax data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getTax(
//            @ApiParam(value = "Unique code for identifier client",
//                    required = false) @FormParam("ad_client_id") Integer ad_client_id,
//            @ApiParam(value = "Unique code for identifier organization",
//                    required = false) @FormParam("ad_org_id") Integer ad_org_id,
//            @ApiParam(value = "Unique code for identifier tax category",
//                    required = false) @FormParam("c_taxcategory_id") Integer c_taxcategory_id,
//            @ApiParam(value = "Name of the tax", required = false) @FormParam("name") String name,
//            @ApiParam(value = "Amount of data showed per page",
//                    required = false) @FormParam("perpage") Integer perpage,
//            @ApiParam(value = "The page that you want to display",
//                    required = false) @FormParam("page") Integer page) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setAd_client_id(ad_client_id);
//            param.setAd_org_id(ad_org_id);
//            param.setC_taxcategory_id(c_taxcategory_id);
//            param.setName(name);
//            param.setPage(page);
//            param.setPerpage(perpage);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getTax(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getTaxCategory")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 7, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many tax category data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getTaxCategory(@ApiParam(value = "Unique code for identifier tax category data",
//            required = false) @FormParam("c_taxcategory_id") Integer c_taxcategory_id) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_taxcategory_id(c_taxcategory_id);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getTaxCategory(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getUom")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 8, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many unit of measure data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getUom(
//            @ApiParam(value = "Unique code for identifier unit of measure data",
//                    required = false) @FormParam("c_uom_id") Integer c_uom_id,
//            @ApiParam(value = "Name of the unit of measure",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_uom_id(c_uom_id);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getUom(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getProductCategory")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 9, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many product category data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getProductCategory(@ApiParam(
//            value = "Unique code for identifier product category data",
//            required = false) @FormParam("m_product_category_id") Integer m_product_category_id) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_product_category_id(m_product_category_id);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getProductCategory(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getUser")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 10, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many user data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getUser(
//            @ApiParam(value = "Unique code for identifier user data",
//                    required = false) @FormParam("ad_user_id") Integer ad_user_id,
//            @ApiParam(value = "Unique code for identifier client",
//                    required = false) @FormParam("ad_client_id") Integer ad_client_id,
//            @ApiParam(value = "The record is active or not in the system. Format : Y/N",
//                    required = false) @FormParam("is_active") String is_active,
//            @ApiParam(value = "Name of user", required = false) @FormParam("name") String name,
//            @ApiParam(value = "Identify the user is sales representative or no",
//                    required = false) @FormParam("issalesrep") String issalesrep,
//            @ApiParam(value = "Amount of data showed per page",
//                    required = false) @FormParam("perpage") Integer perpage,
//            @ApiParam(value = "The page that you want to display",
//                    required = false) @FormParam("page") Integer page) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setAd_user_id(ad_user_id);
//            param.setAd_client_id(ad_client_id);
//            param.setIs_active(is_active);
//            param.setName(name);
//            param.setPage(page);
//            param.setPerpage(perpage);
//            param.setIssalesrep(issalesrep);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getUser(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getWarehouse")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 11, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many warehouse data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getWarehouse(
//            @ApiParam(value = "Unique code for identifier warehouse data",
//                    required = false) @FormParam("m_warehouse_id") Integer m_warehouse_id,
//            @ApiParam(value = "Name of the warehouse",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_warehouse_id(m_warehouse_id);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getWarehouse(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getBPartner")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 12, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many business partner data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getBPartner(
//            @ApiParam(value = "Unique code for identifier business partner data",
//                    required = false) @FormParam("c_bpartner_id") Integer c_bpartner_id,
//            @ApiParam(value = "Unique code for identifier client",
//                    required = false) @FormParam("ad_client_id") Integer ad_client_id,
//            @ApiParam(value = "Name of the business partner",
//                    required = false) @FormParam("name") String name,
//            @ApiParam(value = "Unique code for identifier business partner group data",
//                    required = false) @FormParam("c_bp_group_id") Integer c_bp_group_id,
//            @ApiParam(value = "distributorcode",
//                    required = false) @FormParam("distributorcode") String distributorcode,
//            @ApiParam(value = "Amount of data showed per page",
//                    required = false) @FormParam("perpage") Integer perpage,
//            @ApiParam(value = "The page that you want to display",
//                    required = false) @FormParam("page") Integer page) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_bpartner_id(c_bpartner_id);
//            param.setAd_client_id(ad_client_id);
//            param.setName(name);
//            param.setC_bp_group_id(c_bp_group_id);
//            param.setDistributorcode(distributorcode);
//            param.setPage(page);
//            param.setPerpage(perpage);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getBPartner(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getOrg")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 13, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many organization data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getOrg(@ApiParam(value = "Unique code for identifier organization data",
//            required = false) @FormParam("ad_org_id") Integer ad_org_id) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setAd_org_id(ad_org_id);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getOrg(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getClient")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 14, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many client data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getClient(
//            @ApiParam(value = "Unique code for identifier client data",
//                    required = false) @FormParam("ad_client_id") Integer ad_client_id,
//            @ApiParam(value = "Name of the client",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setAd_client_id(ad_client_id);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getClient(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getProduct")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 15, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many product data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getProduct(
//            @ApiParam(value = "Unique code for identifier product data",
//                    required = false) @FormParam("m_product_id") Integer m_product_id,
//            @ApiParam(value = "Name of the product",
//                    required = false) @FormParam("name") String name,
//            @ApiParam(value = "Unique code for identifier pricelist data",
//                    required = false) @FormParam("m_pricelist_id") Integer m_pricelist_id,
//            @ApiParam(
//                    value = "The sold column indicates if this product is sold by this organization or not. Format : Y/N",
//                    required = false) @FormParam("issold") String issold,
//            @ApiParam(value = "This column indicates to show pricelist or not. Format : Y/N",
//                    required = false) @FormParam("showpricelist") String showpricelist,
//            @ApiParam(value = "Start date. <b>Format : YYYY-MM-DD</b>",
//                    required = false) @FormParam("datefrom") String datefrom,
//            @ApiParam(value = "Unique code for identifier unit of measure",
//                    required = false) @FormParam("c_uom_to_id") Integer c_uom_to_id,
//            @ApiParam(value = "Amount of data showed per page",
//                    required = false) @FormParam("perpage") Integer perpage,
//            @ApiParam(value = "The page that you want to display",
//                    required = false) @FormParam("page") Integer page) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_product_id(m_product_id);
//            param.setName(name);
//            param.setIssold(issold);
//            param.setM_pricelist_id(m_pricelist_id);
//            param.setShowpricelist(showpricelist);
//            param.setC_uom_to_id(c_uom_to_id);
//            param.setPage(page);
//            param.setPerpage(perpage);
//            param.setDatefrom(datefrom);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getProduct(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getPricelist")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 16, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many pricelist data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getPricelist(
//            @ApiParam(value = "Unique code for identifier pricelist data",
//                    required = false) @FormParam("m_pricelist_id") Integer m_pricelist_id,
//            @ApiParam(value = "This is a Sales Price List or No. <b>Format : Y/N</b>",
//                    required = false) @FormParam("issopricelist") String issopricelist,
//            @ApiParam(value = "Amount of data showed per page",
//                    required = false) @FormParam("perpage") Integer perpage,
//            @ApiParam(value = "Page that you wanted to show",
//                    required = false) @FormParam("page") Integer page,
//            @ApiParam(value = "Name of the pricelist",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_pricelist_id(m_pricelist_id);
//            param.setIssopricelist(issopricelist);
//            param.setName(name);
//            param.setPage(page);
//            param.setPerpage(perpage);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getPricelist(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getPaymentRule")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 17, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many payment rule data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getPaymentRule(
//            @ApiParam(value = "Value of payment rule data",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Name of the payment rule",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setValue(value);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getPaymentRule(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getLastLogin")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 18, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or last login data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getLastLogin(
//            @ApiParam(value = "Unique code for identifier client",
//                    required = false) @FormParam("ad_client_id") Integer ad_client_id,
//            @ApiParam(
//                    value = "This column indicates the period of time that you want to retrieve last login. Format : YYYYMM",
//                    required = true) @FormParam("periode") String periode,
//            @ApiParam(value = "The record is active or not in the system. Format : Y/N",
//                    required = false) @FormParam("is_active") String is_active) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setAd_client_id(ad_client_id);
//            param.setPeriode(periode);
//            param.setIs_active(is_active);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getLastLogin(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getMaxLogin")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 19, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or max login data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getMaxLogin(@ApiParam(
//            value = "This column indicates the date log that you want to retrieve max login. Format : YYYYMM",
//            required = true) @FormParam("date_log") String date_log,
//            @ApiParam(value = "Unique code for identifier client",
//                    required = false) @FormParam("ad_client_id") Integer ad_client_id) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setDate_log(date_log);
//            param.setAd_client_id(ad_client_id);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getMaxLogin(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getLocator")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 20, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many locator data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getLocator(
//            @ApiParam(value = "Unique code for identifier locator data",
//                    required = false) @FormParam("m_locator_id") Integer m_locator_id,
//            @ApiParam(value = "Unique code for identifier warehouse data",
//                    required = false) @FormParam("m_warehouse_id") Integer m_warehouse_id) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setM_locator_id(m_locator_id);
//            param.setM_warehouse_id(m_warehouse_id);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getLocator(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getProfitCenter")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 21, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many profit center data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getProfitCenter(
//            @ApiParam(value = "Value of profit center data",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Name of profit center",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setValue(value);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getProfitCenter(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getCostCenter")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 22, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many profit center data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getCostCenter(
//            @ApiParam(value = "Value of profit center data",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Name of profit center",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setValue(value);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getCostCenter(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getPaymentTerm")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 23, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many payment term data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getPaymentTerm(
//            @ApiParam(value = "Value of payment term data",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Name of payment term",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setValue(value);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getPaymentTerm(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getPriorityRule")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 24, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many priority rule data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getPriorityRule(
//            @ApiParam(value = "Value of priority rule data",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Name of the priority rule",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setValue(value);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getPriorityRule(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getInvoiceRule")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 25, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many invoice rule data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getInvoiceRule(
//            @ApiParam(value = "Value of invoice rule data",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Name of the invoice rule",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setValue(value);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getInvoiceRule(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getDeliveryRule")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 26, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many delivery rule data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getDeliveryRule(
//            @ApiParam(value = "Value of delivery rule data",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Name of the delivery rule",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setValue(value);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getDeliveryRule(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getDeliveryViaRule")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 27, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many delivery via rule data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getDeliveryViaRule(
//            @ApiParam(value = "Value of delivery via rule data",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Name of the delivery via rule",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setValue(value);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getDeliveryViaRule(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getCategory")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 28, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many sell category data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getCategory(
//            @ApiParam(value = "Value of sell category data",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Name of sell category rule",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setValue(value);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getCategory(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getDocBaseType")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 29, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many document base type data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getDocBaseType(
//            @ApiParam(value = "Value of document base type data",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Name of document base type",
//                    required = false) @FormParam("name") String name) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setValue(value);
//            param.setName(name);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getDocBaseType(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getDocType")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 30, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many document type data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getDocType(
//            @ApiParam(value = "Unique code for identifier document type data",
//                    required = false) @FormParam("c_doctype_id") Integer c_doctype_id,
//            @ApiParam(value = "Name of document type",
//                    required = false) @FormParam("name") String name,
//            @ApiParam(value = "Value of document base type",
//                    required = false) @FormParam("docbasetype") String docbasetype,
//            @ApiParam(value = "Indication of sales transaction data",
//                    required = false) @FormParam("issotrx") String issotrx,
//            @ApiParam(value = "Document sub type SO is Return Material or No. <b>Format : Y/N</b>",
//                    required = false) @FormParam("docsubtypeso") String docsubtypeso) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_doctype_id(c_doctype_id);
//            param.setName(name);
//            param.setDocbasetype(docbasetype);
//            param.setIssotrx(issotrx);
//            param.setDocsubtypeso(docsubtypeso);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getDocType(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setBPartner")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 31, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to update a business partner")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setBPartner(
//            @ApiParam(value = "Unique code for identifier business partner data",
//                    required = false) @FormParam("c_bpartner_id") Integer c_bpartner_id,
//            @ApiParam(value = "Unique code for identifier crm business partner",
//                    required = false) @FormParam("forca_crm_bpartner_id") Integer forca_crm_bpartner_id,
//            @ApiParam(value = "The first name of business partner",
//                    required = true) @FormParam("name") String name,
//            @ApiParam(value = "The second name of business partner",
//                    required = false) @FormParam("name2") String name2,
//            @ApiParam(value = "Your customer or vendor number at the business partner's site",
//                    required = false) @FormParam("referenceno") String referenceno,
//            @ApiParam(
//                    value = "Total outstanding invoice amounts allowed \"on account\" in primary accounting currency",
//                    required = false) @FormParam("so_credit_limit") String so_credit_limit,
//            @ApiParam(value = "Code from sap that will be sent to create business partner",
//                    required = false) @FormParam("sap_code") String sap_code,
//            @ApiParam(value = "Unique code for identifier customer",
//                    required = false) @FormParam("id_customer") String id_customer,
//            @ApiParam(value = "distributorcode",
//                    required = false) @FormParam("distributorcode") String distributorcode,
//            @ApiParam(required = false) @FormParam("address1") String address1,
//            @ApiParam(required = false) @FormParam("address2") String address2,
//            @ApiParam(required = false) @FormParam("address3") String address3,
//            @ApiParam(required = false) @FormParam("address4") String address4,
//            @ApiParam(value = "Name of country",
//                    required = false) @FormParam("country_name") String country_name,
//            @ApiParam(value = "Name of region",
//                    required = false) @FormParam("region_name") String region_name,
//            @ApiParam(value = "Name of city",
//                    required = false) @FormParam("city_name") String city_name,
//            @ApiParam(
//                    value = "A search key allows you a fast method of finding business partner data.",
//                    required = false) @FormParam("searchKey") String searchkey) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setForca_crm_bpartner_id(forca_crm_bpartner_id);
//            param.setC_bpartner_id(c_bpartner_id);
//            param.setName(name);
//            param.setName2(name2);
//            param.setReferenceno(referenceno);
//            param.setSo_credit_limit(so_credit_limit);
//
//            param.setSap_code(sap_code);
//            param.setId_customer(id_customer);
//            param.setDistributorcode(distributorcode);
//            param.setSearchkey(searchkey);
//
//            param.setCountry_name(country_name);
//            param.setRegion_name(region_name);
//            param.setCity_name(city_name);
//
//            param.setAddress1(address1);
//            param.setAddress2(address2);
//            param.setAddress3(address3);
//            param.setAddress4(address4);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "BPartner");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setBPartnerLocation")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 32, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to insert/update a business partner location")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setBPartnerLocation(
//            @ApiParam(value = "Unique code for identifier business partner data",
//                    required = true) @FormParam("c_bpartner_id") Integer c_bpartner_id,
//            @ApiParam(value = "Unique code for identifier business partner location data",
//                    required = false) @FormParam("c_bpartner_location_id") Integer c_bpartner_location_id,
//            @ApiParam(required = false) @FormParam("phone") String phone,
//            @ApiParam(required = false) @FormParam("address1") String address1,
//            @ApiParam(required = false) @FormParam("address2") String address2,
//            @ApiParam(required = false) @FormParam("address3") String address3,
//            @ApiParam(required = false) @FormParam("address4") String address4,
//            @ApiParam(value = "Name of country",
//                    required = false) @FormParam("country_name") String country_name,
//            @ApiParam(value = "Name of region",
//                    required = false) @FormParam("region_name") String region_name,
//            @ApiParam(value = "Name of city",
//                    required = false) @FormParam("city_name") String city_name) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_bpartner_location_id(c_bpartner_location_id);
//            param.setC_bpartner_id(c_bpartner_id);
//            param.setPhone(phone);
//
//            param.setCountry_name(country_name);
//            param.setRegion_name(region_name);
//            param.setCity_name(city_name);
//
//            param.setAddress1(address1);
//            param.setAddress2(address2);
//            param.setAddress3(address3);
//            param.setAddress4(address4);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "BPartnerLocation");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setUser")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 33, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to update user/contact")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setUser(
//            @ApiParam(value = "Unique code for identifier business partner",
//                    required = false) @FormParam("c_bpartner_id") Integer c_bpartner_id,
//            @ApiParam(value = "Unique code for identifier user data",
//                    required = true) @FormParam("ad_user_id") Integer ad_user_id,
//            @ApiParam(value = "Unique code for identifier crm contact",
//                    required = false) @FormParam("forca_crm_contact_id") Integer forca_crm_contact_id,
//            @ApiParam(value = "The name of user/contact",
//                    required = false) @FormParam("name") String name,
//            @ApiParam(value = "Short description of user/contact",
//                    required = false) @FormParam("description") String description,
//            @ApiParam(value = "Email of user/contact",
//                    required = false) @FormParam("email") String email,
//            @ApiParam(value = "The first phone number of user/contact",
//                    required = false) @FormParam("phone") String phone,
//            @ApiParam(value = "The second phone number of user/contact",
//                    required = false) @FormParam("phone2") String phone2,
//            @ApiParam(value = "The fax number of user/contact",
//                    required = false) @FormParam("fax") String fax,
//            @ApiParam(value = "The title of user/contact",
//                    required = false) @FormParam("title") String title) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setForca_crm_contact_id(forca_crm_contact_id);
//            param.setAd_user_id(ad_user_id);
//            param.setC_bpartner_id(c_bpartner_id);
//            param.setName(name);
//            param.setDescription(description);
//            param.setEmail(email);
//            param.setPhone(phone);
//            param.setPhone2(phone2);
//            param.setFax(fax);
//            param.setTitle(title);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "User");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setUom")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 34, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to update unit of measure")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setUom(
//            @ApiParam(value = "Unique code for identifier unit of measure data",
//                    required = false) @FormParam("c_uom_id") Integer c_uom_id,
//            @ApiParam(value = "Unique code for identifier crm",
//                    required = false) @FormParam("crm_id") Integer crm_id,
//            @ApiParam(value = "Name of unit of measure",
//                    required = true) @FormParam("name") String name,
//            @ApiParam(value = "Short description of user/contact",
//                    required = false) @FormParam("description") String description,
//            @ApiParam(value = "Symbol for a unit of measure",
//                    required = false) @FormParam("uomsymbol") String uomsymbol,
//            @ApiParam(value = "Rule for rounding calculated amounts",
//                    required = false) @FormParam("stdprecision") Integer stdprecision,
//            @ApiParam(value = "Rounding used costing calculation",
//                    required = false) @FormParam("costingprecision") Integer costingprecision,
//            @ApiParam(value = "Unique code to identifier uom conversion data",
//                    required = false) @FormParam("c_uom_conversion_id") Integer c_uom_conversion_id,
//            @ApiParam(value = "Target or destination unit of measure",
//                    required = false) @FormParam("c_uom_to_id") Integer c_uom_to_id,
//            @ApiParam(value = "Unique code to identifier product data",
//                    required = false) @FormParam("m_product_id") Integer m_product_id,
//            @ApiParam(value = "Rate to multiple the source by to calculate the target",
//                    required = false) @FormParam("multiplyrate") BigDecimal multiplyrate,
//            @ApiParam(value = "To convert source number to target number, the source is divided",
//                    required = false) @FormParam("dividerate") BigDecimal dividerate) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setCrm_id(crm_id);
//            param.setC_uom_id(c_uom_id);
//            param.setName(name);
//            param.setDescription(description);
//            param.setUomsymbol(uomsymbol);
//            param.setStdprecision(stdprecision);
//            param.setCostingprecision(costingprecision);
//            param.setC_uom_conversion_id(c_uom_conversion_id);
//            param.setC_uom_to_id(c_uom_to_id);
//            param.setM_product_id(m_product_id);
//            param.setMultiplyrate(multiplyrate);
//            param.setDividerate(dividerate);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "Uom");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setOrg")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 35, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to update organization")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setOrg(
//            @ApiParam(value = "Unique code for identifier organization data",
//                    required = true) @FormParam("ad_org_id") Integer ad_org_id,
//            @ApiParam(value = "Unique code for identifier crm",
//                    required = false) @FormParam("crm_id") Integer crm_id,
//            @ApiParam(value = "Name of organization",
//                    required = false) @FormParam("name") String name,
//            @ApiParam(value = "Short description of organization",
//                    required = false) @FormParam("description") String description,
//            @ApiParam(
//                    value = "Search key for the organization in the format required - must be unique",
//                    required = false) @FormParam("value") String value) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setCrm_id(crm_id);
//            param.setAd_org_id(ad_org_id);
//            param.setName(name);
//            param.setDescription(description);
//            param.setValue(value);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "Org");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setProduct")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 36, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to update product")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setProduct(
//            @ApiParam(value = "Unique code for identifier product data",
//                    required = false) @FormParam("m_product_id") Integer m_product_id,
//            @ApiParam(value = "Unique code for identifier crm",
//                    required = false) @FormParam("crm_id") Integer crm_id,
//            @ApiParam(value = "Name of product", required = true) @FormParam("name") String name,
//            @ApiParam(value = "Short description of organization",
//                    required = false) @FormParam("description") String description,
//            @ApiParam(
//                    value = "Search key for the organization in the format required - must be unique",
//                    required = false) @FormParam("value") String value,
//            @ApiParam(value = "Unique code for identifier product category",
//                    required = true) @FormParam("m_product_category_id") Integer m_product_category_id,
//            @ApiParam(value = "Unique code for identifier unit of measure",
//                    required = true) @FormParam("c_uom_id") Integer c_uom_id,
//            @ApiParam(value = "Unique code for identifier tax category",
//                    required = true) @FormParam("c_taxcategory_id") Integer c_taxcategory_id,
//            @ApiParam(value = "Unique code for identifier product price",
//                    required = false) @FormParam("m_productprice_id") Integer m_productprice_id,
//            @ApiParam(value = "Unique code for identifier pricelist version",
//                    required = false) @FormParam("m_pricelist_version_id") Integer m_pricelist_version_id,
//            @ApiParam(value = "List price",
//                    required = false) @FormParam("pricelist") Integer pricelist,
//            @ApiParam(value = "Standard price",
//                    required = false) @FormParam("pricestd") Integer pricestd,
//            @ApiParam(value = "Limit price",
//                    required = false) @FormParam("pricelimit") Integer pricelimit) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setCrm_id(crm_id);
//            param.setM_product_id(m_product_id);
//            param.setName(name);
//            param.setDescription(description);
//            param.setValue(value);
//            param.setM_product_category_id(m_product_category_id);
//            param.setC_uom_id(c_uom_id);
//            param.setC_taxcategory_id(c_taxcategory_id);
//            param.setM_productprice_id(m_productprice_id);
//            param.setM_pricelist_version_id(m_pricelist_version_id);
//            param.setPricelist(pricelist);
//            param.setPricestd(pricestd);
//            param.setPricelimit(pricelimit);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "Product");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setProductCategory")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 37, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to update product catgegory")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setProductCategory(@ApiParam(
//            value = "Unique code for identifier product category data",
//            required = false) @FormParam("m_product_category_id") Integer m_product_category_id,
//            @ApiParam(value = "Unique code for identifier crm",
//                    required = false) @FormParam("crm_id") Integer crm_id,
//            @ApiParam(value = "Name of product category",
//                    required = true) @FormParam("name") String name,
//            @ApiParam(value = "Short description of organization",
//                    required = false) @FormParam("description") String description,
//            @ApiParam(
//                    value = "Search key for the organization in the format required - must be unique",
//                    required = false) @FormParam("value") String value) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setCrm_id(crm_id);
//            param.setM_product_category_id(m_product_category_id);
//            param.setName(name);
//            param.setDescription(description);
//            param.setValue(value);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "ProductCategory");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setTaxCategory")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 38, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to update tax catgegory")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setTaxCategory(
//            @ApiParam(value = "Unique code for identifier tax category data",
//                    required = false) @FormParam("c_taxcategory_id") Integer c_taxcategory_id,
//            @ApiParam(value = "Unique code for identifier crm",
//                    required = false) @FormParam("crm_id") Integer crm_id,
//            @ApiParam(value = "Name of tax category",
//                    required = true) @FormParam("name") String name,
//            @ApiParam(value = "Short description of organization",
//                    required = false) @FormParam("description") String description) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setCrm_id(crm_id);
//            param.setC_taxcategory_id(c_taxcategory_id);
//            param.setName(name);
//            param.setDescription(description);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "TaxCategory");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setTax")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 39, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to update tax rate")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setTax(
//            @ApiParam(value = "Unique code for identifier tax data",
//                    required = false) @FormParam("c_tax_id") Integer c_tax_id,
//            @ApiParam(value = "Unique code for identifier crm",
//                    required = false) @FormParam("crm_id") Integer crm_id,
//            @ApiParam(value = "Name of tax", required = true) @FormParam("name") String name,
//            @ApiParam(value = "Short description of tax",
//                    required = false) @FormParam("description") String description,
//            @ApiParam(value = "Unique code for identifier tax category data",
//                    required = true) @FormParam("c_taxcategory_id") Integer c_taxcategory_id,
//            @ApiParam(
//                    value = "Sales tax applies to sales situation, Purchase tax to purchase situations",
//                    required = false) @FormParam("sopotype") String sopotype) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_tax_id(c_tax_id);
//            param.setCrm_id(crm_id);
//            param.setC_taxcategory_id(c_taxcategory_id);
//            param.setName(name);
//            param.setDescription(description);
//            param.setSopotype(sopotype);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "Tax");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setChannel")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 40, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to update channel")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setChannel(
//            @ApiParam(value = "Unique code for identifier channel data",
//                    required = true) @FormParam("c_channel_id") Integer c_channel_id,
//            @ApiParam(value = "Name of channel", required = false) @FormParam("name") String name,
//            @ApiParam(value = "Short description of organization",
//                    required = false) @FormParam("description") String description) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_channel_id(c_channel_id);
//            param.setName(name);
//            param.setDescription(description);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "Channel");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setCampaign")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 41, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to update campaign")
//    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
//            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
//    public Response setCampaign(
//            @ApiParam(value = "Unique code for identifier campaign data",
//                    required = true) @FormParam("c_campaign_id") Integer c_campaign_id,
//            @ApiParam(value = "Unique code for identifier channel",
//                    required = true) @FormParam("c_channel_id") Integer c_channel_id,
//            @ApiParam(value = "Name of campaign", required = false) @FormParam("name") String name,
//            @ApiParam(value = "Short description of organization",
//                    required = false) @FormParam("description") String description) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_channel_id(c_channel_id);
//            param.setC_campaign_id(c_campaign_id);
//            param.setName(name);
//            param.setDescription(description);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "Campaign");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/setUserMobile")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 42, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to create user mobile",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response insertUserMobile(
//            @ApiParam(value = "Unique code for identifier device",
//                    required = true) @FormParam("forca_device_id") String forca_device_id,
//            @ApiParam(value = "Name of platform",
//                    required = true) @FormParam("forca_platform") String forca_platform,
//            @ApiParam(value = "Name of model",
//                    required = true) @FormParam("forca_model") String forca_model,
//            @ApiParam(value = "Name of manufaktur",
//                    required = true) @FormParam("forca_manufaktur") String forca_manufaktur,
//            @ApiParam(value = "The number of version",
//                    required = true) @FormParam("forca_version") String forca_version,
//            @ApiParam(value = "App name of version",
//                    required = false) @FormParam("forca_versionappname") String forca_versionappname,
//            @ApiParam(value = "App code of version",
//                    required = false) @FormParam("forca_versionappcode") String forca_versionappcode) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setForca_platform(forca_platform);
//            param.setForca_device_id(forca_device_id);
//            param.setForca_model(forca_model);
//            param.setForca_manufaktur(forca_manufaktur);
//            param.setForca_version(forca_version);
//            param.setForca_versionappname(forca_versionappname);
//            param.setForca_versionappcode(forca_versionappcode);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.insert(param, "UserMobile");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/deleteUserMobile")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 43, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to delete user mobile",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response deleteUserMobile(@ApiParam(value = "Unique code for identifier device",
//            required = true) @FormParam("forca_device_id") String forca_device_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setForca_device_id(forca_device_id);
//
//            AuthenticationApi.setContextByToken(token);
//            MasterService master = new MasterService();
//            result = master.set(param, "UserMobile");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getCharge")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 46, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve one or many user data.",
//            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getCharge(
//            @ApiParam(value = "Document Type for transaction",
//                    required = true) @FormParam("c_doctype_id") Integer c_doctype_id,
//            @ApiParam(value = "Amount of data showed per page",
//            required = false) @FormParam("perpage") Integer perpage,
//            @ApiParam(value = "The page that you want to display",
//            required = false) @FormParam("page") Integer page) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_doctype_id(c_doctype_id);
//            param.setPage(page);
//            param.setPerpage(perpage);
//            
//            AuthenticationApi.setContextByToken(token);
//            MasterImpl master = new MasterImpl();
//            result = master.getCharge(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//    
}
