/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : Other FormData v1                                
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.path;

import java.math.BigDecimal;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.compiere.util.Trx;
import org.springframework.stereotype.Component;
//import com.sun.jersey.core.header.FormDataContentDisposition;
//import com.sun.jersey.multipart.FormDataBodyPart;
//import com.sun.jersey.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.Authorization;
import id.sisi.forca.base.api.filter.AuthenticationApi;
import id.sisi.forca.base.api.filter.RestFilter;
import id.sisi.forca.base.api.impl.OtherImpl;
import id.sisi.forca.base.api.impl.other.SaasServiceImplOther;
import id.sisi.forca.base.api.request.ParamInOutOther;
import id.sisi.forca.base.api.request.ParamOrderOther;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.request.ParamSaasServiceOther;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.service.OtherService;
import id.sisi.forca.base.api.util.Constants;
import id.sisi.forca.base.api.util.WebserviceUtil;

@Component
@Path("/other/v1")
@Api(position = 4, value = "Other", description = "Other Data in Forca")
public class Other_FormData_v1 {
    WebserviceUtil util = new WebserviceUtil();
    AuthenticationApi authAPI = new AuthenticationApi();

//    @POST
//    @Path("/getTempOrder")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 1, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve specific temporary order Data")
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getTempOrder(
//            @ApiParam(value = "Unique code for identifier reference order data",
//                    required = true) @FormParam("ref_order_id") String ref_order_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setRef_order_id(ref_order_id);
//
//            AuthenticationApi.setContextByToken(token);
//            OtherImpl impl = new OtherImpl();
//            result = impl.getTempOrder(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/getTempPayment")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 2, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve specific temporary payment data")
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getTempPayment(
//            @ApiParam(value = "Unique code for identifier reference payment data",
//                    required = true) @FormParam("ref_payment_id") String ref_payment_id) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setRef_payment_id(ref_payment_id);
//
//            AuthenticationApi.setContextByToken(token);
//            OtherImpl impl = new OtherImpl();
//            result = impl.getTempPayment(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//    
//    
//    //ANI
//    @POST
//    @Path("/getTempBPartner")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 3, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve specific temporary business partner data")
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getTempBPartner(
//            @ApiParam(value = "Unique code for identifier ref customer",
//            required = true) @FormParam("ref_customer_id") String ref_customer_id) {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setRef_customer_id(ref_customer_id);
//            
//            AuthenticationApi.setContextByToken(token);
//            OtherImpl impl = new OtherImpl();
//            result = impl.getTempBPartner(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//    
//    @POST
//    @Path("/getTempClient")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 4, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve specific CRM client Data")
//    public Response getTempClient(
//            @ApiParam(value = "Unique code for identifier client data",
//                    required = true) @FormParam("ad_client_id") Integer ad_client_id,
//            @ApiParam(value = "Username",
//                    required = true) @FormParam("username") String username) {
//
//        ResponseData result = new ResponseData();
//
//        ParamRequest param = new ParamRequest();
//        param.setAd_client_id(ad_client_id);
//        param.setUsername(username);
//
//        OtherImpl impl = new OtherImpl();
//        result = impl.getTempClient(param);
//
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/insertTempBPartner")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 5, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to create temporary business partner data",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response insertTempBPartner(
//            @ApiParam(value = "Code from sap that will be sent to create business partner",
//                    required = true) @FormParam("sap_code") String sap_code,
//            @ApiParam(value = "Unique code for identifier customer",
//                    required = false) @FormParam("id_customer") String id_customer,
//            @ApiParam(value = "Name of temporary business partner",
//                    required = true) @FormParam("name") String name,
//            @ApiParam(
//                    value = "Total outstanding invoice amounts allowed \"on account\" in primary accounting currency",
//                    required = false) @FormParam("so_creditlimit") String so_creditlimit,
//            @ApiParam(value = "Unique code for identifier ref customer",
//                    required = true) @FormParam("ref_customer_id") String ref_customer_id,
//            @ApiParam(value = "Address of customer",
//                    required = true) @FormParam("address_customer") String address_customer,
//            @ApiParam(value = "The phone number of temporary business partner",
//                    required = false) @FormParam("phone") String phone) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setSap_code(sap_code);
//            param.setName(name);
//            param.setId_customer(id_customer);
//            param.setSo_credit_limit(so_creditlimit);
//            param.setRef_customer_id(ref_customer_id);
//            param.setAddress_customer(address_customer);
//            param.setPhone(phone);
//
//            AuthenticationApi.setContextByToken(token);
//            OtherService srv = new OtherService();
//            result = srv.insert(param, "TempBPartner");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/insertTempOrder")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 6, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to create temporary order data",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response insertTempOrder(
//            @ApiParam(value = "Unique code for identifier business partner",
//                    required = true) @FormParam("c_bpartner_id") Integer bpartner_id,
//            @ApiParam(
//                    value = "Unique code for identifier product. This form for input multiple product. Format : id_1/id_2/etc",
//                    required = true) @FormParam("multiple_m_product_id") String multiple_m_product_id,
//            @ApiParam(
//                    value = "The Quantity indicates the number of a specific product or item for this payment. This form for inout multiple quantity. Format : qty1/qty2/etc",
//                    required = true) @FormParam("multiple_qty") String multiple_qty,
//            @ApiParam(value = "Unique code for identifier reference order id",
//                    required = true) @FormParam("ref_order_id") String ref_order_id,
//            @ApiParam(value = "Date of order. Format : YYYY-MM-DD",
//                    required = true) @FormParam("dateorder") String dateordered,
//            @ApiParam(value = "Unique code for identifier retail",
//                    required = true) @FormParam("retail_id") String retail_id) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setC_bpartner_id(bpartner_id);
//            param.setMultiple_m_product_id(multiple_m_product_id);
//            param.setMultiple_qty(multiple_qty);
//            param.setRef_order_id(ref_order_id);
//            param.setRetail_id(retail_id);
//            param.setDateordered(dateordered);
//
//            AuthenticationApi.setContextByToken(token);
//            OtherService srv = new OtherService();
//            result = srv.insert(param, "TempOrder");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/insertTempPayment")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 7, httpMethod = "POST", value = "x-www-form-urlencoded",
//            notes = "Use this method to create temporary payment data",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response insertTempPayment(
//            @ApiParam(value = "Unique code for identifier pos order",
//                    required = true) @FormParam("pos_order_id") String pos_order_id,
//            @ApiParam(value = "Unique code for identifier pos payment data",
//                    required = true) @FormParam("pos_payment_id") String pos_payment_id,
//            @ApiParam(value = "Amount being paid",
//                    required = true) @FormParam("PayAmt") BigDecimal pay_amt) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//            param.setRef_order_id(pos_order_id);
//            param.setRef_payment_id(pos_payment_id);
//            param.setPay_amt(pay_amt);
//
//            AuthenticationApi.setContextByToken(token);
//            OtherService srv = new OtherService();
//            result = srv.insert(param, "TempPayment");
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    // ANI
//    @POST
//    @Path("/getProcessName")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 8, httpMethod = "POST", value = "application/x-www-form-urlencoded",
//            notes = "Use this method to retrieve name of the process")
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response getProcessName() {
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            ParamRequest param = new ParamRequest();
//
//            AuthenticationApi.setContextByToken(token);
//            OtherImpl impl = new OtherImpl();
//            result = impl.getProcessName(param);
//        }
//        return ResponseData.finalResponse(result);
//    }
//
//    @POST
//    @Path("/setSalesOrderCompletewithFiles")    
//    @Consumes(MediaType.MULTIPART_FORM_DATA)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 9, httpMethod = "POST", value = "multipart/form-data",
//        notes = "API attach file")
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//        required = true, dataType = "string", paramType = "header")})
//    public Response setSalesOrderCompletewithFiles(
//            @FormDataParam("file") List<FormDataBodyPart> bodyParts,
//            @FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
//            @FormDataParam("param") FormDataBodyPart paramEn) throws Exception{
//        
//        paramEn.setMediaType(MediaType.APPLICATION_JSON_TYPE);
//        ParamOrderOther param = paramEn.getValueAs(ParamOrderOther.class);
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            String trxLocalName = Trx.createTrxName("setOrder");
//            Trx trxLocal = Trx.get(trxLocalName, true);
//            try {
//                AuthenticationApi.setContextByToken(token);
//                OtherImpl impl = new OtherImpl();
//                result = impl.setOrderCompletewithFiles(param, bodyParts, trxLocal);
//            } catch (Exception e) {
//                result = ResponseData.errorResponse(e.getMessage());
//                trxLocal.rollback();
//            } 
//                finally {
//                if (trxLocal.isActive()) {
//                    trxLocal.commit();
//                }
//                trxLocal.close();
//            }
//        }
//        return ResponseData.finalResponse(result);
//    }
//    
//    @POST
//    @Path("/setInOutComplete")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 10, httpMethod = "POST", value = "application/json",
//            notes = "Use this method to set or insert in out data. Fill m_inout_id for set purpose.",
//            authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//            required = true, dataType = "string", paramType = "header")})
//    public Response setInOut(ParamInOutOther param) {
//
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//
//        if (result.getCodestatus().equals("S")) {
//            String trxLocalName = Trx.createTrxName("setInOut");
//            Trx trxLocal = Trx.get(trxLocalName, true);
//            try {
//                AuthenticationApi.setContextByToken(token);
//                OtherImpl impl = new OtherImpl();
//                result = impl.setInOutComplete(param, trxLocal);
//            } catch (Exception e) {
//                result = ResponseData.errorResponse(e.getMessage());
//                trxLocal.rollback();
//            } finally {
//                if (trxLocal.isActive()) {
//                    trxLocal.commit();
//                }
//                trxLocal.close();
//            }
//        }
//        return ResponseData.finalResponse(result);
//    }
//    
//    @POST
//    @Path("/setSaasAction")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @ApiOperation(position = 11, httpMethod = "POST", value = "application/json",
//    notes = "Use this method to set or insert in out data. Fill ad_client_id for set purpose.",
//    authorizations = {@Authorization(value = "Forca-Token")})
//    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
//    required = true, dataType = "string", paramType = "header")})
//    public Response setSaasAction(ParamSaasServiceOther param) {
//        
//        ResponseData result = new ResponseData();
//        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
//        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
//        result = ResponseData.cekToken(token);
//        
//        if (result.getCodestatus().equals("S")) {
//            String trxLocalName = Trx.createTrxName("setSaasAction");
//            Trx trxLocal = Trx.get(trxLocalName, true);
//            try {
//                AuthenticationApi.setContextByToken(token);
//                SaasServiceImplOther impl = new SaasServiceImplOther();
//                result = impl.performSaasServiceAction(param, trxLocal);
//            } catch (Exception e) {
//                result = ResponseData.errorResponse(e.getMessage());
//                trxLocal.rollback();
//            } finally {
//                if (trxLocal.isActive()) {
//                    trxLocal.commit();
//                }
//                trxLocal.close();
//            }
//        }
//        return ResponseData.finalResponse(result);
//    }
    
}