/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : Transaction                              
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.ProductNotOnPriceListException;
import org.adempiere.model.POWrapper;
import org.compiere.db.Database;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MCurrency;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MLocator;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.model.MPriceList;
import org.compiere.model.MProduct;
import org.compiere.model.MUOM;
import org.compiere.model.MUser;
import org.compiere.model.MWarehouse;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.compiere.wf.MWFActivity;
import org.compiere.wf.MWFNode;
import org.compiere.wf.MWorkflow;
//import org.idempiere.model.ForcaObject;
import id.sisi.forca.base.api.impl.transaction.CementReceivedImplTrans;
import id.sisi.forca.base.api.impl.transaction.DocumentApprovedImplTrans;
import id.sisi.forca.base.api.impl.transaction.InOutImplTrans;
import id.sisi.forca.base.api.impl.transaction.InvoiceImplTrans;
import id.sisi.forca.base.api.impl.transaction.MovementImplTrans;
import id.sisi.forca.base.api.impl.transaction.OrderImplTrans;
import id.sisi.forca.base.api.impl.transaction.PaymentRuleImplTrans;
import id.sisi.forca.base.api.impl.transaction.WorkflowImplTrans;
import id.sisi.forca.base.api.model.I_AD_User;
import id.sisi.forca.base.api.model.MFORCAAttendance;
import id.sisi.forca.base.api.model.MFORCALeave;
import id.sisi.forca.base.api.request.ParamInOut;
import id.sisi.forca.base.api.request.ParamInOutLine;
import id.sisi.forca.base.api.request.ParamInventoryMove;
import id.sisi.forca.base.api.request.ParamInventoryMoveLine;
import id.sisi.forca.base.api.request.ParamInvoice;
import id.sisi.forca.base.api.request.ParamInvoiceLine;
import id.sisi.forca.base.api.request.ParamInvoices;
import id.sisi.forca.base.api.request.ParamInvoicesLine;
import id.sisi.forca.base.api.request.ParamOrder;
import id.sisi.forca.base.api.request.ParamOrderLine;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.response.PaginationData;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.Constants;
import id.sisi.forca.base.api.util.ProcessUtil;
import id.sisi.forca.base.api.util.WebserviceUtil;
import id.sisi.forca.base.api.util.WsUtil;

public class TransactionImpl {
    private CLogger log = CLogger.getCLogger(getClass());
    private OrderImplTrans Order = null;
    private MovementImplTrans Move = null;
    private InOutImplTrans InOut = null;
    private InvoiceImplTrans Invoice = null;
    private DocumentApprovedImplTrans DocApprove = null;
    private CementReceivedImplTrans CementReceived = null;
    private WorkflowImplTrans Workflow = null;
    private PaymentRuleImplTrans PaymentRule = null;
    private static final String NATIVE_MARKER = "NATIVE_" + Database.DB_POSTGRESQL + "_KEYWORK";

//    public ResponseData getOrder(ParamRequest param, Trx trx) throws Exception {
//        if (param.getC_order_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//
//        try {
//            Integer c_order_id = param.getC_order_id();
//            MOrder sOrder =
//                    ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, c_order_id, trxName);
//            MPriceList pricelist = ForcaObject.getObject(Env.getCtx(), MPriceList.Table_Name,
//                    sOrder.getM_PriceList_ID(), trxName);
//            MBPartner bpartner = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name,
//                    sOrder.getC_BPartner_ID(), trxName);
//            MWarehouse wh = ForcaObject.getObject(Env.getCtx(), MWarehouse.Table_Name,
//                    sOrder.getM_Warehouse_ID(), trxName);
//            MUser salesrep = ForcaObject.getObject(Env.getCtx(), MUser.Table_Name,
//                    sOrder.getSalesRep_ID(), trxName);
//            PaymentRule = new PaymentRuleImplTrans();
//            MCurrency cur = ForcaObject.getObject(Env.getCtx(), MCurrency.Table_Name,
//                    sOrder.getC_Currency_ID(), trxName);
//
//            NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
//
//            if (sOrder != null) {
//                map.put("c_bpartner_id", sOrder.getC_BPartner_ID());
//                map.put("m_pricelist_id", pricelist.get_ID());
//                map.put("pricelist", pricelist.getName());
//                map.put("nominal",
//                        cur.getCurSymbol() + " " + numberFormat.format(sOrder.getGrandTotal()));
//                map.put("c_bpartner_name", bpartner.getName());
//                map.put("warehouse", wh.getName());
//                map.put("salesrep_name", salesrep.getName());
//                map.put("c_order_id", sOrder.getC_Order_ID());
//                map.put("documentno", sOrder.getDocumentNo());
//                map.put("docstatus", sOrder.getDocStatus());
//                map.put("dateordered", Constants.sdf.format(sOrder.getDateOrdered()));
//                map.put("status", WsUtil.descDocStatus(Env.getCtx(), sOrder.getDocStatus()));
//                map.put("docaction", sOrder.getDocAction());
//                map.put("crm_id", sOrder.get_Value("crm_id"));
//                map.put("paymentrule",
//                        PaymentRule.getPaymentRuleName(sOrder.getPaymentRule(), trxName));
//                map.put("profitcenter_id", sOrder.getUser2_ID());
//                map.put("description", sOrder.getDescription());
//
//                Order = new OrderImplTrans();
//                map.put("c_orderline", Order.getOrderLineByOrderID(c_order_id, trxName));
//                map.put("m_inout", Order.getInOutByOrderID(c_order_id, trxName));
//                map.put("c_invoice", Order.getInvoiceByOrderID(c_order_id, trxName));
//                map.put("c_project", Order.getProjectByOrderID(c_order_id, trxName));
//                map.put("m_pricelist_version", Order
//                        .getPricelistVersionByPricelistID(sOrder.getM_PriceList_ID(), trxName));
//
//                resp = ResponseData.successResponse(Constants.SUCCEED_DataFound, map);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData getOrderList(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new PaginationData();
//        List<Object> listdata = new ArrayList<Object>();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//        List<Object> params = new ArrayList<>();
//        String trxName = trx.getTrxName();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
//
//        //@formatter:off
//        sql.append(""
//                + "WITH cte AS ("
//                + " SELECT "
//                + "     ord.c_order_id, "
//                + "     bp.name, "
//                + "     ord.documentno, "
//                + "     ord.grandtotal, "
//                + "     ord.dateordered, " 
//                + "     ord.docstatus, "
//                + "     cur.cursymbol "
//                + " FROM "
//                + "     c_order ord "
//                + " INNER JOIN c_bpartner bp ON "
//                + "     ord.c_bpartner_id = bp.c_bpartner_id "
//                + " LEFT JOIN c_currency cur ON "
//                + "     cur.c_currency_id = ord.c_currency_id "
//                + " WHERE "
//                + "     ord.ad_client_id = ?");
//        //@formatter:on
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        if (param.getDatefrom() != null) {
//            sql.append(" " + "AND to_char( trunc( ord.dateordered ), " + "'YYYY-MM-DD' ) >= ? ");
//
//            Date date_from = null;
//            try {
//                date_from = Constants.df.parse(param.getDatefrom());
//            } catch (ParseException e) {
//                resp = PaginationData.errorResponse("Parameter invalid date format");
//                e.printStackTrace();
//                throw new Exception(e);
//            }
//            params.add(Constants.sdf.format(date_from));
//        }
//
//        if (param.getDateto() != null) {
//            sql.append(" " + "AND to_char( trunc( ord.dateordered ), " + "'YYYY-MM-DD' ) <= ? ");
//
//            Date date_to = null;
//            try {
//                date_to = Constants.df.parse(param.getDateto());
//            } catch (ParseException e) {
//                resp = PaginationData.errorResponse("Parameter invalid date format");
//                e.printStackTrace();
//                throw new Exception(e);
//            }
//            params.add(Constants.sdf.format(date_to));
//        }
//
//        if (param.getIssotrx() != null) {
//            sql.append(" AND ord.issotrx = ? ");
//            params.add(param.getIssotrx());
//        }
//
//        if (param.getM_warehouse_id() != null) {
//            sql.append(" AND ord.m_warehouse_id = ? ");
//            params.add(param.getM_warehouse_id());
//        }
//
//        if (param.getC_bpartner_id() != null) {
//            sql.append(" AND ord.c_bpartner_id = ? ");
//            params.add(param.getC_bpartner_id());
//        }
//
//        if (param.getDocumentno() != null) {
//            sql.append(" AND ord.documentno = ? ");
//            params.add(param.getDocumentno());
//        }
//
//        LinkedHashMap<String, String> docStatus = WsUtil.docStatus(Env.getCtx());
//
//        if (param.getStatus() != null) {
//            sql.append(" AND ord.docstatus = ? ");
//            params.add(docStatus.get(param.getStatus()));
//        }
//
//        //@formatter:off
//        sql.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE  cte "
//            + "   ORDER  BY c_order_id "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), trxName);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//            boolean first = true;
//            int totaldata = 0;
//
//            while (rs.next()) {
//                if (rs.getString("grandtotal") != null) {
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("c_order_id", rs.getString("c_order_id"));
//                    map.put("name", rs.getString("name"));
//                    map.put("documentno", rs.getString("documentno"));
//                    map.put("dateordered", rs.getString("dateordered"));
//                    map.put("grandtotal", rs.getString("cursymbol") + " "
//                            + numberFormat.format(new BigDecimal(rs.getString("grandtotal"))));
//                    map.put("status", docStatus.get(rs.getString("docstatus")));
//
//                    if (rs.getString("c_order_id") != null) {
//                        listdata.add(map);
//                    }
//                }
//
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//
//            resp.setCodestatus("S");
//            resp.setMessage(totaldata + " Data Found");
//            resp.setResultdata(listdata);
//        } catch (SQLException e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData getOrderLine(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new PaginationData();
//        List<Object> listdata = new ArrayList<Object>();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//        List<Object> params = new ArrayList<>();
//        String trxName = trx.getTrxName();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
//
//        //@formatter:off
//        sql.append(""
//                + "WITH cte AS ("
//                + "SELECT "
//                + "     o.c_order_id, "
//                + "     o.c_orderline_id, "
//                + "     o.line AS line_number, "
//                + "     o.c_bpartner_id, "
//                + "     bp.name AS c_bpartner_name, "
//                + "     o.m_product_id, "
//                + "     p.name AS m_product_name, "
//                + "     bpl.c_bpartner_location_id, "
//                + "     bpl.name AS c_bpartner_location_name, "
//                + "     o.dateordered, "
//                + "     o.m_warehouse_id, "
//                + "     wh.name AS m_warehouse_name, "
//                + "     o.qtyentered, "
//                + "     o.qtyreserved, "
//                + "     o.c_uom_id, "
//                + "     u.name AS c_uom_name, "
//                + "     o.priceactual, "
//                + "     o.pricelist, "
//                + "     o.priceentered, "
//                + "     o.c_tax_id, "
//                + "     t.name AS c_tax_name, "
//                + "     cur.cursymbol "
//                + "FROM "
//                + "     c_orderline o "
//                + "LEFT JOIN m_product p ON "
//                + "     p.m_product_id = o.m_product_id "
//                + "LEFT JOIN c_uom u ON "
//                + "     u.c_uom_id = o.c_uom_id "
//                + "LEFT JOIN c_bpartner bp ON "
//                + "     bp.c_bpartner_id = o.c_bpartner_id "
//                + "LEFT JOIN c_bpartner_location bpl ON "
//                + "     bpl.c_bpartner_id = bp.c_bpartner_id "
//                + "LEFT JOIN m_warehouse wh ON "
//                + "     wh.m_warehouse_id = o.m_warehouse_id "
//                + "LEFT JOIN c_tax t ON "
//                + "     t.c_tax_id = o.c_tax_id "
//                + "LEFT JOIN c_currency cur ON "
//                + "     cur.c_currency_id = o.c_currency_id"
//                + " WHERE "
//                + "     o.ad_client_id = ?");
//        //@formatter:on
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        if (param.getC_order_id() > 0) {
//            sql.append(" AND o.c_order_id = ? ");
//            params.add(param.getC_order_id());
//        }
//
//        if (param.getC_orderline_id() > 0) {
//            sql.append(" AND o.c_orderline_Id = ? ");
//            params.add(param.getC_orderline_id());
//        }
//
//        if (param.getProduct_name() != null) {
//            sql.append(" AND p.name = ? ");
//            params.add(param.getProduct_name());
//        }
//
//        if (param.getM_product_id() > 0) {
//            sql.append(" AND o.m_product_id = ? ");
//            params.add(param.getM_product_id());
//        }
//
//        //@formatter:off
//        sql.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE  cte "
//            + "   ORDER  BY c_orderline_id "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), trxName);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//            boolean first = true;
//            int totaldata = 0;
//
//            while (rs.next()) {
//                if (rs.getString("c_orderline_id") != null) {
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("c_order_id", rs.getString("c_order_id"));
//                    map.put("c_orderline_id", rs.getString("c_orderline_id"));
//                    map.put("line_number", rs.getString("line_number"));
//                    map.put("c_bpartner_id", rs.getString("c_bpartner_id"));
//                    map.put("c_bpartner_name", rs.getString("c_bpartner_name"));
//                    map.put("m_product_id", rs.getString("m_product_id"));
//                    map.put("m_product_name", rs.getString("m_product_name"));
//                    map.put("c_bpartner_location_id", rs.getString("c_bpartner_location_id"));
//                    map.put("c_bpartner_location_name", rs.getString("c_bpartner_location_name"));
//                    map.put("dateordered", rs.getString("dateordered"));
//                    map.put("m_warehouse_id", rs.getString("m_warehouse_id"));
//                    map.put("m_warehouse_name", rs.getString("m_warehouse_name"));
//                    map.put("qtyentered", rs.getString("qtyentered"));
//                    map.put("qtyreserved", rs.getString("qtyreserved"));
//                    map.put("c_uom_id", rs.getString("c_uom_id"));
//                    map.put("c_uom_name", rs.getString("c_uom_name"));
//                    map.put("priceentered", rs.getString("cursymbol") + " "
//                            + numberFormat.format(new BigDecimal(rs.getString("priceentered"))));
//                    map.put("pricelist", rs.getString("cursymbol") + " "
//                            + numberFormat.format(new BigDecimal(rs.getString("pricelist"))));
//                    map.put("priceactual", rs.getString("cursymbol") + " "
//                            + numberFormat.format(new BigDecimal(rs.getString("priceactual"))));
//                    map.put("c_tax_id", rs.getString("c_tax_id"));
//                    map.put("c_tax_name", rs.getString("c_tax_name"));
//                    Order = new OrderImplTrans();
//                    if (rs.getString("c_orderline_id") != null) {
//                        map.put("uom_conversion", Order.getUOMConversionByProductID(
//                                rs.getInt("c_orderline_id"), rs.getInt("m_product_id")));
//                        listdata.add(map);
//                    }
//                }
//
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//
//            resp.setCodestatus("S");
//            resp.setMessage(totaldata + " Data Found");
//            resp.setResultdata(listdata);
//        } catch (SQLException e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData getInOut(ParamRequest param, Trx trx) throws Exception {
//        if (param.getM_inout_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//
//        try {
//            Integer m_inout_id = param.getM_inout_id();
//            MInOut inout =
//                    ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, m_inout_id, trxName);
//
//            MBPartner bpartner = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name,
//                    inout.getC_BPartner_ID(), trxName);
//            MWarehouse wh = ForcaObject.getObject(Env.getCtx(), MWarehouse.Table_Name,
//                    inout.getM_Warehouse_ID(), trxName);
//            MBPartnerLocation bploc = ForcaObject.getObject(Env.getCtx(),
//                    MBPartnerLocation.Table_Name, inout.getC_BPartner_Location_ID(), trxName);
//            MOrder order = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name,
//                    inout.getC_Order_ID(), trxName);
//            MCurrency cur = ForcaObject.getObject(Env.getCtx(), MCurrency.Table_Name,
//                    inout.getC_Currency_ID(), trxName);
//
//            NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
//
//            if (inout != null) {
//                map.put("m_inout_id", inout.get_ID());
//                map.put("c_bpartner_id", inout.getC_BPartner_ID());
//                map.put("c_bpartner_location_id", inout.getC_BPartner_Location_ID());
//                map.put("c_bpartner_name", bpartner.getName());
//                map.put("warehouse", wh.getName());
//                map.put("m_warehouse_id", inout.getM_Warehouse_ID());
//                map.put("c_order_id", inout.getC_Order_ID());
//                map.put("documentno", inout.getDocumentNo());
//                map.put("documentno_order", order.getDocumentNo());
//                map.put("docstatus", inout.getDocStatus());
//                map.put("movementdate", inout.getMovementDate());
//                map.put("status", WsUtil.descDocStatus(Env.getCtx(), inout.getDocStatus()));
//                map.put("docaction", inout.getDocAction());
//                map.put("crm_id", inout.get_Value("crm_id"));
//                map.put("description", inout.getDescription());
//
//                InOut = new InOutImplTrans();
//                map.put("m_inoutline", InOut.getInOutLineByInOutID(m_inout_id, trxName));
//                map.put("profitcenter_id", inout.getUser2_ID());
//                map.put("c_project", InOut.getProjectByInOutID(m_inout_id, trxName));
//
//                resp = ResponseData.successResponse(Constants.SUCCEED_DataFound, map);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData getInOutList(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new PaginationData();
//        List<Object> listdata = new ArrayList<Object>();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//        List<Object> params = new ArrayList<>();
//        String trxName = trx.getTrxName();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
//
//        //@formatter:off
//        sql.append(""
//                + "WITH cte AS ("
//                + " SELECT "
//                + "     io.m_inout_id, "
//                + "     bp.name, "
//                + "     bp.c_bpartner_id, "
//                + "     io.documentno, "
//                + "     io.movementdate, " 
//                + "     io.docstatus,"
//                + "     io.m_warehouse_id "
//                + " FROM "
//                + "     m_inout io "
//                + " INNER JOIN c_bpartner bp ON "
//                + "     io.c_bpartner_id = bp.c_bpartner_id "
//                + " WHERE "
//                + "     io.ad_client_id = ? "
//                + "     AND io.m_inout_id is not null ");
//        //@formatter:on
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        if (param.getDatefrom() != null) {
//            sql.append(" " + "AND to_char( trunc( io.movementdate ), " + "'YYYY-MM-DD' ) >= ? ");
//
//            Date date_from = null;
//            try {
//                date_from = Constants.df.parse(param.getDatefrom());
//            } catch (ParseException e) {
//                resp = PaginationData.errorResponse("Parameter invalid date format");
//                e.printStackTrace();
//                throw new Exception(e);
//            }
//            params.add(Constants.sdf.format(date_from));
//        }
//
//        if (param.getDateto() != null) {
//            sql.append(" " + "AND to_char( trunc( io.movementdate ), " + "'YYYY-MM-DD' ) <= ? ");
//
//            Date date_to = null;
//            try {
//                date_to = Constants.df.parse(param.getDateto());
//            } catch (ParseException e) {
//                resp = PaginationData.errorResponse("Parameter invalid date format");
//                e.printStackTrace();
//                throw new Exception(e);
//            }
//            params.add(Constants.sdf.format(date_to));
//        }
//
//        LinkedHashMap<String, String> docStatus = WsUtil.docStatus(Env.getCtx());
//
//        if (param.getStatus() != null) {
//            sql.append(" AND io.docstatus = ? ");
//            params.add(docStatus.get(param.getStatus()));
//        }
//        if (param.getDocumentno() != null) {
//            sql.append(" AND io.documentno = ? ");
//            params.add(param.getDocumentno());
//        }
//
//        //@formatter:off
//        sql.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE  cte "
//            + "   ORDER  BY m_inout_id "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), trxName);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//            boolean first = true;
//            int totaldata = 0;
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("m_inout_id", rs.getString("m_inout_id"));
//                map.put("name", rs.getString("name"));
//                map.put("c_bpartner_id", rs.getString("c_bpartner_id"));
//                map.put("m_warehouse_id", rs.getString("m_warehouse_id"));
//                map.put("documentno", rs.getString("documentno"));
//                map.put("movementdate", rs.getString("movementdate"));
//                map.put("status", docStatus.get(rs.getString("docstatus")));
//
//                if (rs.getString("m_inout_id") != null) {
//                    listdata.add(map);
//                }
//
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//
//            resp.setCodestatus("S");
//            resp.setMessage(totaldata + " Data Found");
//            resp.setResultdata(listdata);
//        } catch (SQLException e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData getInOutLine(ParamRequest param, Trx trx) throws Exception {
//        if (param.getM_inoutline_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//
//        try {
//            Integer m_inoutline_id = param.getM_inoutline_id();
//            MInOutLine il = ForcaObject.getObject(Env.getCtx(), MInOutLine.Table_Name,
//                    m_inoutline_id, trxName);
//            MProduct prd = ForcaObject.getObject(Env.getCtx(), MProduct.Table_Name,
//                    il.getM_Product_ID(), trxName);
//            MLocator loc = ForcaObject.getObject(Env.getCtx(), MLocator.Table_Name,
//                    il.getM_Locator_ID(), trxName);
//            MUOM uom =
//                    ForcaObject.getObject(Env.getCtx(), MUOM.Table_Name, il.getC_UOM_ID(), trxName);
//
//            if (il != null) {
//                map.put("m_inout_id", il.getM_InOut_ID());
//                map.put("line_number", il.getLine());
//                map.put("m_inoutline_id", il.getM_InOutLine_ID());
//                map.put("c_orderline_id", il.getC_OrderLine_ID());
//                map.put("m_product_id", il.getM_Product_ID());
//                map.put("product_name", prd.getName());
//                map.put("m_locator_id", il.getM_Locator_ID());
//                map.put("locator_name", loc.getValue());
//                map.put("qtyentered", il.getQtyEntered());
//                map.put("c_uom_id", il.getC_UOM_ID());
//                map.put("uom_name", uom.getName());
//
//                resp = ResponseData.successResponse(Constants.SUCCEED_DataFound, map);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData getInvoice(ParamRequest param, Trx trx) throws Exception {
//        if (param.getC_invoice_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//
//        try {
//            Integer c_invoice_id = param.getC_invoice_id();
//            MInvoice invoice =
//                    ForcaObject.getObject(Env.getCtx(), MInvoice.Table_Name, c_invoice_id, trxName);
//            MBPartner bpartner = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name,
//                    invoice.getC_BPartner_ID(), trxName);
//            MBPartnerLocation bploc = ForcaObject.getObject(Env.getCtx(),
//                    MBPartnerLocation.Table_Name, invoice.getC_BPartner_Location_ID(), trxName);
//            MPriceList pricelist = ForcaObject.getObject(Env.getCtx(), MPriceList.Table_Name,
//                    invoice.getM_PriceList_ID(), trxName);
//            MCurrency cur = ForcaObject.getObject(Env.getCtx(), MCurrency.Table_Name,
//                    invoice.getC_Currency_ID(), trxName);
//
//            NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
//
//            if (invoice != null) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("c_bpartner_id", invoice.getC_BPartner_ID());
//                map.put("c_bpartner_name", bpartner.getName());
//                map.put("c_order_id", invoice.getC_Order_ID());
//                map.put("documentno", invoice.getDocumentNo());
//                map.put("docstatus", invoice.getDocStatus());
//                map.put("dateinvoiced", invoice.getDateInvoiced());
//                map.put("status", WsUtil.descDocStatus(Env.getCtx(), invoice.getDocStatus()));
//                map.put("docaction", invoice.getDocAction());
//                map.put("crm_id", invoice.get_Value("crm_id"));
//                map.put("c_bpartner_location_id", invoice.getC_BPartner_Location_ID());
//                map.put("c_bpartner_location_name", bploc.getName());
//                map.put("m_pricelist_id", invoice.getM_PriceList_ID());
//                map.put("m_pricelist_name", pricelist.getName());
//                map.put("profitcenter_id", invoice.getUser2_ID());
//                map.put("grandtotal",
//                        cur.getCurSymbol() + " " + numberFormat.format(invoice.getGrandTotal()));
//
//                Invoice = new InvoiceImplTrans();
//                map.put("c_invoiceline", Invoice.getInvoiceLineByInvoiceID(c_invoice_id, trxName));
//                map.put("c_order", Invoice.getOrderByInvoiceID(c_invoice_id, trxName));
//                map.put("c_project", Invoice.getProjectByInvoiceID(c_invoice_id, trxName));
//
//                resp = ResponseData.successResponse(Constants.SUCCEED_DataFound, map);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//    
//    public ResponseData getInvoiceMulti(ParamRequest param, Trx trx) throws Exception {
//        if (param.getValue() == null) {
//            return ResponseData.parameterRequired();
//        }
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//        
//        StringBuilder sql = new StringBuilder();
//
//        StringBuilder invoiceString = new StringBuilder();
//        String[] invoiceArray = param.getValue().split(";");
//        for (int a = 0; a < invoiceArray.length; a++) {
//            if (a > 0) {
//                invoiceString.append(",");
//            }
//            invoiceString.append(invoiceArray[a]);
//        }
//        
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//                //@formatter:off
//        sql.append(
//                "SELECT " + 
//                "    c_invoice_id, " + 
//                "    docstatus " + 
//                "FROM c_invoice " + 
//                "WHERE c_invoice_id IN ("+invoiceString.toString()+") "
//                );
//        //@formatter:on
//        
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("c_invoice_id", rs.getString("c_invoice_id"));
//                map.put("docstatus", rs.getString("docstatus"));
//                listdata.add(map);
//            }
//
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getInvoiceList(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new PaginationData();
//        List<Object> listdata = new ArrayList<Object>();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//        List<Object> params = new ArrayList<>();
//        String trxName = trx.getTrxName();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
//
//        //@formatter:off
//        sql.append(""
//                + "WITH cte AS ("
//                + " SELECT "
//                + "     ci.c_invoice_id, "
//                + "     bp.name, "
//                + "     bp.c_bpartner_id, "
//                + "     ci.documentno, "
//                + "     ci.dateinvoiced, " 
//                + "     ci.docstatus "
//                + " FROM "
//                + "     c_invoice ci "
//                + " INNER JOIN c_bpartner bp ON "
//                + "     ci.c_bpartner_id = bp.c_bpartner_id "
//                + " WHERE "
//                + "     ci.ad_client_id = ? "
//                + "     AND ci.c_invoice_id is not null ");
//        //@formatter:on
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        if (param.getDatefrom() != null) {
//            sql.append(" " + "AND to_char( trunc( ci.dateinvoiced ), " + "'YYYY-MM-DD' ) >= ? ");
//
//            Date date_from = null;
//            try {
//                date_from = Constants.df.parse(param.getDatefrom());
//            } catch (ParseException e) {
//                resp = PaginationData.errorResponse("Parameter invalid date format");
//                e.printStackTrace();
//                throw new Exception(e);
//            }
//            params.add(Constants.sdf.format(date_from));
//        }
//
//        if (param.getDateto() != null) {
//            sql.append(" " + "AND to_char( trunc( ci.dateinvoiced ), " + "'YYYY-MM-DD' ) <= ? ");
//
//            Date date_to = null;
//            try {
//                date_to = Constants.df.parse(param.getDateto());
//            } catch (ParseException e) {
//                resp = PaginationData.errorResponse("Parameter invalid date format");
//                e.printStackTrace();
//                throw new Exception(e);
//            }
//            params.add(Constants.sdf.format(date_to));
//        }
//
//        LinkedHashMap<String, String> docStatus = WsUtil.docStatus(Env.getCtx());
//
//        if (param.getStatus() != null) {
//            sql.append(" AND ci.docstatus = ? ");
//            params.add(docStatus.get(param.getStatus()));
//        }
//
//        //@formatter:off
//        sql.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE  cte "
//            + "   ORDER  BY c_invoice_id "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), trxName);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//            boolean first = true;
//            int totaldata = 0;
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("c_invoice_id", rs.getString("c_invoice_id"));
//                map.put("name", rs.getString("name"));
//                map.put("c_bpartner_id", rs.getString("c_bpartner_id"));
//                map.put("documentno", rs.getString("documentno"));
//                map.put("dateinvoiced", rs.getString("dateinvoiced"));
//                map.put("status", docStatus.get(rs.getString("docstatus")));
//
//                if (rs.getString("c_invoice_id") != null) {
//                    listdata.add(map);
//                }
//
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//
//            resp.setCodestatus("S");
//            resp.setMessage(totaldata + " Data Found");
//            resp.setResultdata(listdata);
//        } catch (SQLException e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData getInvoiceLine(ParamRequest param, Trx trx) throws Exception {
//        if (param.getC_invoiceline_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//
//        try {
//            Integer c_invoiceline_id = param.getC_invoiceline_id();
//            MInvoiceLine il = ForcaObject.getObject(Env.getCtx(), MInvoiceLine.Table_Name,
//                    c_invoiceline_id, trxName);
//            MProduct prd = ForcaObject.getObject(Env.getCtx(), MProduct.Table_Name,
//                    il.getM_Product_ID(), trxName);
//            MUOM uom =
//                    ForcaObject.getObject(Env.getCtx(), MUOM.Table_Name, il.getC_UOM_ID(), trxName);
//
//            if (il != null) {
//                map.put("c_invoice_id", il.getC_Invoice_ID());
//                map.put("c_invoiceline_id", il.getC_InvoiceLine_ID());
//                map.put("m_product_id", il.getM_Product_ID());
//                map.put("product_name", prd.getName());
//                map.put("qtyentered", il.getQtyEntered());
//                map.put("c_uom_id", il.getC_UOM_ID());
//                map.put("uom_name", uom.getName());
//
//                resp = ResponseData.successResponse(Constants.SUCCEED_DataFound, map);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData getPayment(ParamRequest param) {
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        //@formatter:off
//        select.append(""
//                + " SELECT "
//                + "    b.c_payment_id, " 
//                + "    b.ad_client_id, " 
//                + "    b.ad_org_id, " 
//                + "    b.documentno, " 
//                + "    b.docstatus, " 
//                + "    b.c_bankaccount_id, " 
//                + "    b.c_doctype_id, " 
//                + "    b.isreceipt,"
//                + "    b.datetrx, " 
//                + "    b.dateacct, " 
//                + "    b.description description_header, " 
//                + "    b.c_bpartner_id, " 
//                + "    b.c_invoice_id, " 
//                + "    b.c_order_id, " 
//                + "    b.c_charge_id, " 
//                + "    b.payamt, " 
//                + "    b.c_currency_id,"
//                + "    b.overunderamt, "
//                + "    cur.cursymbol ");
//        join.append(""
//                + " FROM ");
//        join.append(""
//                + "     c_allocationline a " 
//                + " RIGHT JOIN c_payment b ON "
//                + "     b.c_payment_id = a.c_payment_id "
//                + " LEFT JOIN c_currency cur ON "
//                + "     cur.c_currency_id = b.c_currency_id ");
//        whereCond.append(""
//                + " WHERE "
//                + "     b.ad_client_id = ? ");
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        
//        if (param.getC_payment_id() != null) {
//            whereCond.append(""
//                    + "     AND b.c_payment_id = ? ");
//            params.add(param.getC_payment_id());
//        } 
//        
//        if (param.getDocumentno() != null) {
//            whereCond.append(" "
//                    + " AND b.c_order_id in ( " 
//                    + "    select " 
//                    + "        c_order_id " 
//                    + "    from c_order a " 
//                    + "    where documentno = ? " // #1
//                    + " ) "
//                    + " OR a.c_invoice_id in ( " 
//                    + "    SELECT " 
//                    + "        c_invoice_id " 
//                    + "    FROM "
//                    + "        c_invoiceline a " 
//                    + "    WHERE "
//                    + "    a.c_orderline_id in ( " 
//                    + "        SELECT " 
//                    + "            a.c_orderline_id " 
//                    + "        FROM c_orderline a " 
//                    + "        INNER JOIN c_order b on b.c_order_id = a.c_order_id " 
//                    + "        WHERE b.documentno = ? "  // #2
//                    + "    ) " 
//                    + "    OR a.m_inoutline_id in ( " 
//                    + "        SELECT " 
//                    + "            c.m_inoutline_id " 
//                    + "        FROM c_orderline a " 
//                    + "        INNER JOIN c_order b on b.c_order_id = a.c_order_id " 
//                    + "        INNER JOIN m_inoutline c on c.c_orderline_id = a.c_orderline_id " 
//                    + "        WHERE b.documentno = ? " // #3
//                    + "    ) " 
//                    + " ) " 
//                    + " OR a.c_order_id in ( " 
//                    + "    select " 
//                    + "        c_order_id " 
//                    + "    from c_order a " 
//                    + "    where documentno = ? " // #4
//                    + " ) "
//                    );
//            params.add(param.getDocumentno());
//            params.add(param.getDocumentno());
//            params.add(param.getDocumentno());
//            params.add(param.getDocumentno());
//        }
//        //@formatter:on
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("c_payment_id", rs.getString("c_payment_id"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("ad_org_id", rs.getString("ad_org_id"));
//                map.put("documentno", rs.getString("documentno"));
//                map.put("docstatus", rs.getString("docstatus"));
//                map.put("c_bankaccount_id", rs.getString("c_bankaccount_id"));
//                map.put("c_doctype_id", rs.getString("c_doctype_id"));
//                map.put("isreceipt", rs.getString("isreceipt"));
//                map.put("datetrx", rs.getString("datetrx"));
//                map.put("dateacct", rs.getString("dateacct"));
//                map.put("description_header", rs.getString("description_header"));
//                map.put("c_bpartner_id", rs.getString("c_bpartner_id"));
//                map.put("c_invoice_id", rs.getString("c_invoice_id"));
//                map.put("c_order_id", rs.getString("c_order_id"));
//                map.put("c_charge_id", rs.getString("c_charge_id"));
//                map.put("payamt", rs.getString("cursymbol") + " " + rs.getString("payamt"));
//                map.put("c_currency_id", rs.getString("c_currency_id"));
//                map.put("overunderamt",
//                        rs.getString("cursymbol") + " " + rs.getString("overunderamt"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    // Dokumen Approved / setelah di approve
//    public ResponseData getDocumentApproved(ParamRequest param, String path, Trx trx)
//            throws Exception {
//        if (path.equals("Header") && (param.getDatefrom() == null || param.getDateto() == null
//                || param.getStatus() == null)) {
//            return ResponseData.parameterRequired();
//        } else if (path.equals("Detail") && param.getAd_wf_activity_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> params = new ArrayList<>();
//
//        //@formatter:off
//        String xsql = ""
//                + " SELECT " 
//                + "     wf.ad_wf_activity_id, " 
//                + "     wf.ad_client_id, " 
//                + "     wf.ad_org_id, " 
//                + "     w.name AS wfname, " 
//                + "     wf.wfstate, " 
//                + "     case " 
//                + "         when wf.wfstate = 'CC' then 'Accept' " 
//                + "             else 'Reject' " 
//                + "         end wfstate_name, " 
//                + "         wn.name AS nodename, " 
//                + "         usr.ad_user_id AS responsibleid, " 
//                + "     usr.name AS responsiblename, " 
//                + "     wf.textmsg, " 
//                + "     usr2.ad_user_id AS fromid, " 
//                + "     usr2.name AS fromname, " 
//                + "     trunc(wf.created::timestamp with time zone) AS created, " 
//                + "     trunc(wf.updated::timestamp with time zone) AS updated, " 
//                + "     wf.ad_table_id,  " 
//                + "     wf.record_id " 
//                + " FROM ad_wf_activity wf " 
//                + " JOIN ad_wf_process wp ON wp.ad_wf_process_id = wf.ad_wf_process_id " 
//                + " JOIN ad_workflow w ON w.ad_workflow_id = wf.ad_workflow_id " 
//                + " JOIN ad_wf_node wn ON wn.ad_wf_node_id = wf.ad_wf_node_id " 
//                + " JOIN ad_user usr ON usr.ad_user_id = wf.ad_user_id " 
//                + " JOIN ad_user usr2 ON usr2.ad_user_id = wp.ad_user_id " 
//                + " INNER JOIN ad_user rsp on usr.name = rsp.Name " 
//                + " WHERE usr.ad_client_id = ? ";
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        //@formatter:on
//
//        String status = "CC";
//        if (path.equals("Detail")) {
//            xsql += " AND wf.ad_wf_activity_id = ? ";
//            params.add(Integer.valueOf(param.getAd_wf_activity_id()));
//        } else {
//            if (param.getStatus().equals("Reject")) {
//                status = "CA";
//            }
//
//            //@formatter:off
//            xsql += " "
//                    + " AND to_char(wf.updated,'YYYY-MM-DD') BETWEEN ? AND ? "
//                    + " AND wf.wfstate = ?::bpchar "
//                    + " AND wn.action = 'C'::bpchar "
//                    + " AND wf.ad_user_id = ? ";
//            //@formatter:on
//
//            Date date_from = null;
//            Date date_to = null;
//
//            try {
//                date_from = Constants.df.parse(param.getDatefrom());
//                date_to = Constants.df.parse(param.getDateto());
//            } catch (ParseException e) {
//                resp.setCodestatus("E");
//                resp.setMessage("Parameter invalid date format");
//                e.printStackTrace();
//                throw new Exception(e);
//            }
//
//            params.add(Constants.sdf.format(date_from));
//            params.add(Constants.sdf.format(date_to));
//            params.add(status);
//            params.add(Env.getAD_User_ID(Env.getCtx()));
//
//            String process_name = param.getProcess_name();
//            if (!Util.isEmpty(process_name)) {
//                xsql += " AND w.name = ? ";
//                params.add(process_name);
//            }
//        }
//        xsql += "  ORDER BY wf.ad_workflow_id, wf.record_id";
//
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try {
//            pstmt = DB.prepareStatement(xsql, trxName);
//            pstmt = ProcessUtil.setMultiParam(pstmt, params);
//            if (pstmt.getConnection().getAutoCommit()) {
//                pstmt.getConnection().setAutoCommit(false);
//            }
//            pstmt.setFetchSize(100);
//            rs = pstmt.executeQuery();
//
//            // define
//            List<Object> resultdata = new ArrayList<Object>();
//            int n = 0;
//            DocApprove = new DocumentApprovedImplTrans();
//            while (rs.next()) {
//                MWFActivity activity = ForcaObject.getObject(Env.getCtx(), MWFActivity.Table_Name,
//                        rs.getInt("ad_wf_activity_id"), trxName);
//                if (activity.getPO() != null) {
//                    Map<String, Object> map = new LinkedHashMap<String, Object>();
//                    map.put("ad_wf_activity_id", rs.getString("ad_wf_activity_id"));
//                    map.put("wfname", rs.getString("wfname"));
//                    map.put("nodename", rs.getString("nodename"));
//                    map.put("fromname", rs.getString("fromname"));
//                    map.put("wfstate_name", rs.getString("wfstate_name"));
//                    map.put("textmsg", rs.getString("textmsg"));
//                    map.put("updated", rs.getString("updated"));
//                    map.put("detail1", activity.getSummary());
//                    map.put("nominal", DocApprove.getDescDetail(activity.getPO(), "price"));
//                    map.put("detail3", DocApprove.getDescDetail(activity.getPO(), "description"));
//
//                    if (!Util.isEmpty(rs.getString("ad_table_id"))
//                            && !Util.isEmpty(rs.getString("record_id"))) {
//                        List<Object> listDocData = DocApprove.getDataByTableNameRecord(
//                                rs.getString("ad_table_id"), rs.getString("record_id"), trxName);
//                        map.put("description", listDocData.get(0));
//                        map.put("doc_type", listDocData.get(1));
//                        map.put("doc_number", listDocData.get(2));
//                        map.put("bpartner_name", listDocData.get(3));
//                        map.put("doc_date", listDocData.get(4));
//                        map.put("grandtotal", listDocData.get(5));
//                        map.put("data_line", listDocData.get(6));
//                    }
//
//                    if (path.equals("Detail")) {
//                        map.put("history",
//                                DocApprove.detailHistory(activity.getAD_WF_Process_ID(), trxName));
//                        // 17750
//                        map.put("additional", DocApprove.getAdditionalData(
//                                rs.getString("ad_table_id"), rs.getString("record_id"), trxName));
//                        // 17750 - end
//                        resp.setResultdata(map);
//                    }
//                    resultdata.add(map);
//                    n++;
//                }
//            }
//            resp.setCodestatus("S");
//            resp.setMessage("Data found, total = " + n);
//            if (path.equals("Header")) {
//                resp.setResultdata(resultdata);
//            }
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, xsql, e);
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new SQLException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        } finally {
//            DB.close(rs, pstmt);
//            rs = null;
//            pstmt = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getCementReceived(ParamRequest param, Boolean isBSA) {
//        ResponseData resp = new ResponseData();
//        Map<String, Object> sqlParams = new LinkedHashMap<>();
//        CementReceived = new CementReceivedImplTrans();
//
//        String dateFrom = param.getDatefrom();
//        String dateTo = param.getDateto();
//
//        if (!isBSA) {
//            String distributorcode = param.getDistributorcode();
//            String accountdate = param.getAccountdate();
//            sqlParams = CementReceived.getSQLCementReceived(distributorcode, accountdate, dateFrom,
//                    dateTo);
//        } else {
//            sqlParams = CementReceived.getSQLCementReceivedBSA(dateFrom, dateTo);
//        }
//
//        String sql = sqlParams.get("sql").toString();
//        List<Object> params = new ArrayList<>();
//        if (WebserviceUtil.isCollection(sqlParams.get("params"))) {
//            params = WebserviceUtil.convertObjectToList(sqlParams.get("params"));
//        }
//
//        PreparedStatement statement = null;
//        ResultSet rs = null;
//        List<Object> resultdata = new ArrayList<Object>();
//        try {
//            statement = DB.prepareStatement(sql, null);
//            statement = ProcessUtil.setMultiParam(statement, params);
//            rs = statement.executeQuery();
//            while (rs.next()) {
//                Map<String, Object> map = new LinkedHashMap<String, Object>();
//                // "KD_ASAL_DATA" = null
//                map.put("distributorcode", rs.getString("kd_distributor_si")); // KD_DISTRIBUTOR
//                map.put("distributorname", rs.getString("name")); // NM_DISTRIBUTOR
//                // JENIS_DATA" = null
//                map.put("movementdate", rs.getTimestamp("movementdate")); // TANGGAL_KIRIM
//                map.put("customercode", rs.getString("kdshipto")); // KD_CUSTOMER
//                map.put("productcode", rs.getString("value2")); // KD_PRODUK
//                map.put("customername", rs.getString("nmshipto")); // NAMA_CUSTOMER
//                map.put("spj_no", rs.getString("no_spj")); // NO_SPJ
//                map.put("do_no", rs.getString("no_do")); // NO_DO
//                map.put("qty", rs.getBigDecimal("movementqty")); // QTY
//                map.put("unit", rs.getString("uom")); // SATUAN
//                map.put("date", rs.getTimestamp("movementdate")); // TANGGAL
//                map.put("datedocument", rs.getTimestamp("datedoc")); // TANGGAL_DOKUMEN
//                // "OPCO" = 7000
//                resultdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(resultdata.size() + " data found");
//            resp.setResultdata(resultdata);
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            e.getStackTrace();
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, statement);
//            rs = null;
//            statement = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getUserInfo(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> params = new ArrayList<>();
//
//        // @formatter:off
//        StringBuilder sqlSelect = new StringBuilder(""
//                + "SELECT "
//                + " i.binarydata, "
//                + " bp.referenceno, "
//                + " bp.name, "
//                + " u.email, "
//                + " bpl.phone, "
//                + " u.isusefacedetection, "
//                + " u.latitude, "
//                + " u.longitude, "
//                // issue - 20097
//                + " u.ischecklocation "
//                // end issue - 20097
//                + "FROM "
//                + " c_bpartner bp "
//                + "LEFT JOIN ad_user u ON "
//                + " u.c_bpartner_id = bp.c_bpartner_id "
//                + "LEFT JOIN ad_image i ON "
//                + " i.ad_image_id = bp.logo_id "
//                + "LEFT JOIN C_BPartner_Location bpl ON "
//                + " bpl.c_bpartner_id = bp.c_bpartner_id "
//                + "WHERE "
//                + " u.ad_user_id = ?");
//        // @formatter:on
//
//        List<Object> resultdata = new ArrayList<Object>();
//        PreparedStatement statement = null;
//        ResultSet rs = null;
//        params.add(param.getAd_user_id());
//        try {
//            statement = DB.prepareStatement(sqlSelect.toString(), null);
//            statement = ProcessUtil.setMultiParam(statement, params);
//            rs = statement.executeQuery();
//            while (rs.next()) {
//                Map<String, Object> map = new LinkedHashMap<String, Object>();
//                map.put("image", rs.getString("binarydata"));
//                map.put("nik", rs.getString("referenceno"));
//                map.put("name", rs.getString("name"));
//                map.put("email", rs.getString("email"));
//                map.put("phone", rs.getString("phone"));
//                map.put("isusefacedetection", rs.getString("isusefacedetection"));
//                map.put("latitude", rs.getString("latitude"));
//                map.put("longitude", rs.getString("longitude"));
//                // issue - 20097
//                map.put("ischecklocation", rs.getString("ischecklocation"));
//                // end issue - 20097
//                resultdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(resultdata.size() + " data found");
//            resp.setResultdata(resultdata);
//        } catch (SQLException e) {
//            e.getStackTrace();
//            log.log(Level.SEVERE, sqlSelect.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, statement);
//            rs = null;
//            statement = null;
//        }
//        return resp;
//    }
//    
//    public ResponseData getAttendanceHistory(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new PaginationData();
//        List<Object> params = new ArrayList<>();
//
//        // @formatter:off
//        StringBuilder sqlSelect = new StringBuilder(
//                "WITH cte AS (" +
//                "SELECT " + 
//                "    a.forca_attendance_id, " + 
//                "    a.value nik, " + 
//                "    a.name, " + 
//                "    to_char(a.datedoc,'YYYY-MM-DD HH24:MI') datedoc, " + 
//                "    a.status, " + 
//                "    a.imageurl, " + 
//                "    a.latitude, " + 
//                "    a.longitude, " + 
//                "    a.forca_branch_id, " + 
//                "    b.name branch_name, " + 
//                "    a.forca_distance, " + 
//                "    a.forca_deviceid, " + 
//                "    a.forca_devicemodel, " + 
//                "    a.description, " + 
//                "    a.forca_location " + 
//                "FROM forca_attendance a " + 
//                "LEFT JOIN forca_branch b ON b.forca_branch_id = a.forca_branch_id " + 
//                "WHERE a.isactive = 'Y' " +
//                "AND a.ad_client_id = ? ");
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        if (param.getNik() != null) {
//            sqlSelect.append("  and a.value = ? ");
//            params.add(param.getNik());
//        }
//        if (param.getName() != null) {
//            sqlSelect.append("  and a.name = ? ");
//            params.add(param.getName());
//        }
//        if (param.getDatefrom() != null) {
//            sqlSelect.append(" and to_char(a.datedoc,'YYYY-MM-DD') >= ? ");
//            params.add(param.getDatefrom());
//        }
//        if (param.getDateto() != null) {
//            sqlSelect.append(" and to_char(a.datedoc,'YYYY-MM-DD') <= ? ");
//            params.add(param.getDateto());
//        }
//        if (param.getStatus() != null) {
//            sqlSelect.append("  and a.status = ? ");
//            params.add(param.getStatus());
//        }
//        if (param.getForca_branch_id() > 0) {
//            sqlSelect.append("  and a.forca_branch_id = ? ");
//            params.add(param.getForca_branch_id());
//        }
//        sqlSelect.append("ORDER BY a.created DESC ");
//        // @formatter:on
//
//        List<Object> resultdata = new ArrayList<Object>();
//        PreparedStatement statement = null;
//        ResultSet rs = null;
//        
//      //@formatter:off
//        sqlSelect.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM ( "
//            + "   TABLE cte "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//        try {
//            statement = DB.prepareStatement(sqlSelect.toString(), null);
//            statement = ProcessUtil.setMultiParam(statement, params);
//            rs = statement.executeQuery();
//            
//            boolean first = true;
//            int totaldata = 0;
//            while (rs.next()) {
//                Map<String, Object> map = new LinkedHashMap<String, Object>();
//                map.put("forca_attendance_id", rs.getString("forca_attendance_id"));
//                map.put("nik", rs.getString("nik"));
//                map.put("name", rs.getString("name"));
//                map.put("datedoc", rs.getString("datedoc"));
//                map.put("status", rs.getString("status"));
//                map.put("imageurl", rs.getString("imageurl"));
//                map.put("latitude", rs.getString("latitude"));
//                map.put("longitude", rs.getString("longitude"));
//                map.put("forca_branch_id", rs.getString("forca_branch_id"));
//                map.put("branch_name", rs.getString("branch_name"));
//                map.put("forca_distance", rs.getString("forca_distance"));
//                map.put("forca_deviceid", rs.getString("forca_deviceid"));
//                map.put("forca_devicemodel", rs.getString("forca_devicemodel"));
//                map.put("description", rs.getString("description"));
//                map.put("forca_location", rs.getString("forca_location"));
//                resultdata.add(map);
//                
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(resultdata.size() + " data found");
//            resp.setResultdata(resultdata);
//        } catch (SQLException e) {
//            e.getStackTrace();
//            log.log(Level.SEVERE, sqlSelect.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, statement);
//            rs = null;
//            statement = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getLocation(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> params = new ArrayList<>();
//
//        // @formatter:off
//        StringBuilder sqlSelect = new StringBuilder(""
//                + "SELECT "
//                + " latitude, "
//                + " longitude, "
//                + " NULL AS name, "
//                + " isusefacedetection, "
//                + " NULL AS forca_branch_id,"
//                + " NULL AS forca_distance "
//                + "FROM "
//                + " ad_user "
//                + "WHERE "
//                + " ad_user_id = ? "
//                + "UNION SELECT "
//                + " latitude, "
//                + " longitude, "
//                + " name, "
//                + " NULL AS isusefacedetection, "
//                + " forca_branch_id,"
//                + " forca_distance "
//                + "FROM "
//                + " forca_branch");
//        // @formatter:on
//
//        List<Object> resultdata = new ArrayList<Object>();
//        PreparedStatement statement = null;
//        ResultSet rs = null;
//        params.add(param.getAd_user_id());
//        try {
//            statement = DB.prepareStatement(sqlSelect.toString(), null);
//            statement = ProcessUtil.setMultiParam(statement, params);
//            rs = statement.executeQuery();
//            while (rs.next()) {
//                Map<String, Object> map = new LinkedHashMap<String, Object>();
//                map.put("latitude", rs.getString("latitude"));
//                map.put("longitude", rs.getString("longitude"));
//                map.put("name", rs.getString("name"));
//                map.put("isusefacedetection", rs.getString("isusefacedetection"));
//                map.put("forca_branch_id", rs.getString("forca_branch_id"));
//                map.put("forca_distance", rs.getString("forca_distance"));
//
//                resultdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(resultdata.size() + " data found");
//            resp.setResultdata(resultdata);
//        } catch (SQLException e) {
//            e.getStackTrace();
//            log.log(Level.SEVERE, sqlSelect.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, statement);
//            rs = null;
//            statement = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getLeaveType(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> params = new ArrayList<>();
//        // @formatter:off
//        StringBuilder sqlSelect = new StringBuilder(""
//                + "SELECT "
//                + " name, "
//                + " forca_totalday, "
//                + " forca_leavetype_id "
//                + "FROM "
//                + " forca_leavetype "
//                + "WHERE "
//                + " isactive = 'Y' "
//                + " AND ad_client_id = ? ");
//        // @formatter:on
//        List<Object> resultdata = new ArrayList<Object>();
//        PreparedStatement statement = null;
//        ResultSet rs = null;
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        try {
//            statement = DB.prepareStatement(sqlSelect.toString(), null);
//            statement = ProcessUtil.setMultiParam(statement, params);
//            rs = statement.executeQuery();
//            while (rs.next()) {
//                Map<String, Object> map = new LinkedHashMap<String, Object>();
//                map.put("name", rs.getString("name"));
//                map.put("forca_totalday", rs.getString("forca_totalday"));
//                map.put("forca_leavetype_id", rs.getString("forca_leavetype_id"));
//                resultdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(resultdata.size() + " data found");
//            resp.setResultdata(resultdata);
//        } catch (SQLException e) {
//            e.getStackTrace();
//            log.log(Level.SEVERE, sqlSelect.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, statement);
//            rs = null;
//            statement = null;
//        }
//        return resp;
//    }
//    
//    public ResponseData getLeave(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new PaginationData();
//        List<Object> params = new ArrayList<>();
//        // @formatter:off
//        StringBuilder sqlSelect = new StringBuilder(
//                "WITH cte AS (" +
//                "SELECT " + 
//                "    a.forca_leave_id, " + 
//                "    b.name, " + 
//                "    a.datefrom, " + 
//                "    a.dateto, " + 
//                "    a.forca_totalday, " + 
//                "    c.name AS docstatus, " + 
//                "    a.description " + 
//                "FROM Forca_Leave a " + 
//                "LEFT JOIN Forca_LeaveType b ON b.Forca_LeaveType_ID = a.Forca_LeaveType_ID " +
//                "LEFT JOIN AD_Ref_List c ON c.AD_Reference_ID = 131 AND c.Value = a.docstatus " + 
//                "WHERE a.isactive = 'Y' " +
//                "AND a.ad_client_id = ? " +
//                "AND a.ad_user_id = ? ");
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        params.add(param.getAd_user_id());
//        sqlSelect.append("ORDER BY a.created DESC ");
//        // @formatter:on
//        List<Object> resultdata = new ArrayList<Object>();
//        PreparedStatement statement = null;
//        ResultSet rs = null; 
//        //@formatter:off
//        sqlSelect.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE cte "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//        try {
//            statement = DB.prepareStatement(sqlSelect.toString(), null);
//            statement = ProcessUtil.setMultiParam(statement, params);
//            rs = statement.executeQuery();       
//            boolean first = true;
//            int totaldata = 0;
//            while (rs.next()) {
//                Map<String, Object> map = new LinkedHashMap<String, Object>();
//                map.put("forca_leave_id", rs.getString("forca_leave_id"));
//                map.put("name", rs.getString("name"));
//                map.put("datefrom", rs.getString("datefrom"));
//                map.put("dateto", rs.getString("dateto"));
//                map.put("forca_totalday", rs.getString("forca_totalday"));
//                map.put("description", rs.getString("description"));
//                map.put("docstatus", rs.getString("docstatus"));
//                resultdata.add(map);
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(resultdata.size() + " data found");
//            resp.setResultdata(resultdata);
//        } catch (SQLException e) {
//            e.getStackTrace();
//            log.log(Level.SEVERE, sqlSelect.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, statement);
//            rs = null;
//            statement = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getWorkflow(ParamRequest param, String type, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> params = new ArrayList<>();
//        String trxName = trx.getTrxName();
//
//        Integer ad_client_id = Env.getAD_Client_ID(Env.getCtx());
//        Integer ad_user_id = Env.getAD_User_ID(Env.getCtx());
//        MWFNode wf_node = null;
//        String inWflow = "";
//
//        if (!DB.isConnected()) {
//            resp.setCodestatus("E");
//            resp.setMessage("No Database Connection");
//            return resp;
//        }
//
//        if (type == null || type.isEmpty()) {
//            type = "";
//        }
//
//        String ad_wf_name = "";
//        Integer ad_wf_node_id = 0;
//        Integer ad_wf_activity_id = 0;
//        if (type.equals("List")) {
//            if (param.getAd_wf_name() == null || param.getAd_wf_node_id() == null) {
//                return ResponseData.parameterRequired();
//            } else {
//                ad_wf_name = param.getAd_wf_name();
//                ad_wf_node_id = param.getAd_wf_node_id();
//            }
//        } else if (type.equals("Detail")) {
//            if (param.getAd_wf_name() == null || param.getAd_wf_activity_id() == null) {
//                return ResponseData.parameterRequired();
//            } else {
//                ad_wf_name = param.getAd_wf_name();
//                ad_wf_activity_id = param.getAd_wf_activity_id();
//            }
//        }
//
//        Properties wsenv = ResponseData.getDefaultCtx();
//        wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
//        wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
//
//        String sql = "";
//        params.add(ad_user_id);
//        params.add(ad_user_id);
//        params.add(ad_user_id);
//        params.add(ad_user_id);
//        params.add(ad_client_id);
//
//        Workflow = new WorkflowImplTrans();
//        if (type.equals("List")) {
//            sql = Workflow.getSqlByWfName(ad_wf_name, ad_wf_node_id);
//            params.add(ad_wf_name);
//            params.add(ad_wf_node_id);
//        } else if (type.equals("Detail")) {
//            sql = Workflow.getSqlWfDetail(ad_wf_name, ad_wf_activity_id);
//            params.add(ad_wf_name);
//            params.add(ad_wf_activity_id);
//        } else { // Group atau All
//            sql = Workflow.getSqlAllWorkflow();
//        }
//
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try {
//            pstmt = DB.prepareStatement(sql, trxName);
//            pstmt = ProcessUtil.setMultiParam(pstmt, params);
//            if (pstmt.getConnection().getAutoCommit()) {
//                pstmt.getConnection().setAutoCommit(false);
//            }
//            pstmt.setFetchSize(100);
//            rs = pstmt.executeQuery();
//
//            // define
//            List<Object> resultdata = new ArrayList<Object>();
//
//            if (type.equals("Group")) {
//                int z = 0;
//                while (rs.next()) {
//                    MWFActivity activity =
//                            ForcaObject.getObject(Env.getCtx(), MWFActivity.Table_Name,
//                                    rs.getInt(MWFActivity.COLUMNNAME_AD_WF_Activity_ID), null);
//                    if (activity.getPO() != null) {
//                        inWflow += (z > 0) ? "," : "";
//                        inWflow += rs.getInt(MWFActivity.COLUMNNAME_AD_WF_Activity_ID);
//                        z++;
//                    }
//                }
//                if (!rs.wasNull()) {
//                    resultdata =
//                            Workflow.getSqlGrupWorkflow(ad_client_id, ad_user_id, inWflow, trxName);
//                }
//            } else {
//                DocApprove = new DocumentApprovedImplTrans();
//                while (rs.next()) {
//                    MWFActivity activity =
//                            ForcaObject.getObject(Env.getCtx(), MWFActivity.Table_Name,
//                                    rs.getInt(MWFActivity.COLUMNNAME_AD_WF_Activity_ID), null);
//                    if (activity.getPO() != null) {
//                        Map<String, Object> map = new LinkedHashMap<String, Object>();
//
//                        map.put("ad_wf_activity_id", rs.getString("ad_wf_activity_id"));
//                        map.put("wfname", rs.getString("wfname"));
//                        map.put("nodename", rs.getString("nodename"));
//                        map.put("record_id", rs.getString("record_id"));
//                        map.put("wfstate", rs.getString("wfstate"));
//                        map.put("ad_wf_node_id", rs.getString("ad_wf_node_id"));
//                        map.put("ad_wf_process_id", rs.getString("ad_wf_process_id"));
//                        map.put("approve_dtl", rs.getString("approve_dtl"));
//                        map.put("created", rs.getString("created"));
//                        map.put("detail1", Workflow.getWfSummary(activity.getPO(), trxName));
//                        map.put("nominal", DocApprove.getDescDetail(activity.getPO(), "price"));
//                        map.put("detail3",
//                                DocApprove.getDescDetail(activity.getPO(), "description"));
//
//                        if (type.equals("List") || type.equals("Detail")) {
//                            if (!Util.isEmpty(rs.getString("ad_table_id"))
//                                    && !Util.isEmpty(rs.getString("record_id"))) {
//                                List<Object> listDocData = DocApprove.getDataByTableNameRecord(
//                                        rs.getString("ad_table_id"), rs.getString("record_id"),
//                                        trxName);
//                                map.put("description", listDocData.get(0));
//                                map.put("doc_type", listDocData.get(1));
//                                map.put("doc_number", listDocData.get(2));
//                                map.put("bpartner_name", listDocData.get(3));
//                                map.put("doc_date", listDocData.get(4));
//                                map.put("grandtotal", listDocData.get(5));
//                                map.put("data_line", listDocData.get(6));
//                            }
//                        }
//
//                        wf_node = ForcaObject.getObject(Env.getCtx(), MWFNode.Table_Name,
//                                rs.getInt(MWFNode.COLUMNNAME_AD_WF_Node_ID), null);
//                        if (!MWFNode.ACTION_UserChoice.equals(wf_node.getAction())) {
//                            map.put("yesno", "N");
//                        } else {
//                            map.put("yesno", "Y");
//                        }
//                        if (type.equals("Detail")) {
//                            map.put("history", DocApprove
//                                    .detailHistory(activity.getAD_WF_Process_ID(), trxName));
//                            map.put("param_Ad_wf_name", param.getAd_wf_name());
//                            map.put("param_Ad_wf_activity_id", param.getAd_wf_activity_id());
//                            // 17750
//                            map.put("additional",
//                                    DocApprove.getAdditionalData(rs.getString("ad_table_id"),
//                                            rs.getString("record_id"), trxName));
//                            // 17750 - end
//                            resp.setResultdata(map);
//                            resultdata.add(map);
//                        } else {
//                            resultdata.add(map);
//                        }
//                    }
//                }
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(resultdata.size() + " Data Founded");
//            if (!type.equals("Detail")) {
//                resp.setResultdata(resultdata);
//            }
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql, e);
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new SQLException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        } finally {
//            DB.close(rs, pstmt);
//            rs = null;
//            pstmt = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getMaterialStock(ParamRequest param) {
//        ResponseData resp = new ResponseData();
//        List<Object> params = new ArrayList<>();
//
//        String movementdate = param.getMovementdate();
//        if (movementdate == null) {
//            resp = ResponseData.parameterRequired();
//        }
//        String distributorcode = param.getDistributorcode();
//
//        StringBuilder whereClause = new StringBuilder();
//        // @formatter:off
//        whereClause.append(" "
//                + "WHERE "
//                + "m_transaction.ad_client_id IN ( "
//                + " SELECT "
//                + "    ad_client_id "
//                + " FROM "
//                + "    ad_client "
//                + " WHERE "
//                + "    kode_dist IS NOT NULL ) "
//                + "AND m_transaction.ad_client_id = ? "
//                + "AND m_transaction.m_product_id IN ( "
//                + " SELECT "
//                + "    CAST( COALESCE( value, "
//                + "    '0' ) AS INTEGER ) AS value "
//                + " FROM "
//                + "    M_MappingERPDist "
//                + " WHERE "
//                + "    M_MappingERPDist.name = 'Product' ) "
//                + "AND mw.isactive = 'Y' "
//                + "AND m_product.isactive = 'Y' "
//                + "AND to_char( trunc( m_transaction.movementdate ), "
//                + "'YYYY-MM-DD' ) <= ? ");
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        params.add(movementdate);
//        // @formatter:on
//        if (distributorcode != null) {
//            whereClause.append(" AND ad_client.kode_dist = ? ");
//            params.add(distributorcode);
//        }
//        // @formatter:off
//        StringBuilder sqlSelect = new StringBuilder(" "
//                + "SELECT "
//                + "    ad_client.name AS distname, "
//                + "    ad_client.kode_dist AS distcode, "
//                + "    ad_org.name AS namagudang, "
//                + "    ad_org.kode_gudang AS kodegudang, "
//                + "    m_product.value AS productvalue, "
//                + "    M_MappingERPDist.description AS productname, "
//                + "    M_MappingERPDist.value2 AS productcode, "
//                + "    cl.address1, "
//                + "    c_uom.name AS uom, "
//                + "    mw.isintransitspjsi, "
//                + "    mw.isintransit, "
//                + "    mw.name AS namaintransit, "
//                + "    SUM( movementqty ) AS qty "
//                + "FROM "
//                + "    m_transaction "
//                + "LEFT JOIN ad_client ON "
//                + "    ad_client.ad_client_id = m_transaction.ad_client_id "
//                + "LEFT JOIN m_locator ml ON "
//                + "    ml.m_locator_id = m_transaction.m_locator_id "
//                + "LEFT JOIN m_warehouse mw ON "
//                + "    mw.m_warehouse_id = ml.m_warehouse_id "
//                + "LEFT JOIN ad_org ON "
//                + "    ad_org.ad_org_id = mw.ad_org_id "
//                + "LEFT JOIN ad_orginfo aoi ON "
//                + "    aoi.ad_org_id = ad_org.ad_org_id "
//                + "LEFT JOIN c_location cl ON "
//                + "    cl.c_location_id = aoi.c_location_id "
//                + "LEFT JOIN m_product ON "
//                + "    m_product.m_product_id = m_transaction.m_product_id "
//                + "LEFT JOIN c_uom ON "
//                + "    c_uom.c_uom_id = m_product.c_uom_id "
//                + "LEFT JOIN M_MappingERPDist ON "
//                + "    CAST( COALESCE( M_MappingERPDist.value, "
//                + "    '0' ) AS INTEGER ) = m_transaction.m_product_id "
//                + "    AND M_MappingERPDist.name = 'Product' "
//                + whereClause 
//                + "GROUP BY "
//                + "    ad_client.name, "
//                + "    ad_client.kode_dist, "
//                + "    ad_org.kode_gudang, "
//                + "    ad_org.name, "
//                + "    m_product.value, "
//                + "    M_MappingERPDist.description, "
//                + "    M_MappingERPDist.value2, "
//                + "    c_uom.name, "
//                + "    cl.address1, "
//                + "    mw.isintransitspjsi, "
//                + "    mw.isintransit, "
//                + "    mw.name ");
//        // @formatter:on
//        List<Object> resultdata = new ArrayList<Object>();
//        PreparedStatement statement = null;
//        ResultSet rs = null;
//        try {
//            statement = DB.prepareStatement(sqlSelect.toString(), null);
//            statement = ProcessUtil.setMultiParam(statement, params);
//            rs = statement.executeQuery();
//            while (rs.next()) {
//                Map<String, Object> map = new LinkedHashMap<String, Object>();
//                map.put("shipto_name", rs.getString("namagudang")); // NAMA_SHIPTO
//                map.put("shipto_address", rs.getString("address1")); // ALAMAT_SHIPTO
//                map.put("distributorcode", rs.getString("distcode")); // KD_DISTRIBUTOR
//                map.put("shipto_code", rs.getString("kodegudang")); // KODE_SHIPTO
//                map.put("item_no", rs.getString("productcode")); // ITEM_NO
//                map.put("product", rs.getString("productname")); // PRODUK
//                map.put("stock", rs.getInt("qty")); // STOK
//                map.put("sold_to", rs.getString("distcode")); // SOLD_TO
//                map.put("sold_to_name", rs.getString("distname")); // NM_SOLD_TO
//                map.put("date", movementdate); // LAST_UPDATE
//                // TGL_TRANSAKSI = movementdate
//                // DELETE_MARK = 0
//                // OPCO = 7000
//                map.put("ISINTRANSITSPJSI", rs.getString("isintransitspjsi")); // ISINTRANSITSPJSI
//                map.put("ISINTRANSIT", rs.getString("isintransit")); // ISINTRANSIT
//                map.put("NAMA_INTRANSIT", rs.getString("namaintransit")); // NAMA_INTRANSIT
//                resultdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(resultdata.size() + " data found");
//            resp.setResultdata(resultdata);
//        } catch (SQLException e) {
//            e.getStackTrace();
//            log.log(Level.SEVERE, sqlSelect.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, statement);
//            rs = null;
//            statement = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getInventoryMove(ParamRequest param, Trx trx) throws Exception {
//        if (param.getM_movement_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//
//        try {
//            Integer m_movement_id = param.getM_movement_id();
//            MMovement iMove = ForcaObject.getObject(Env.getCtx(), MMovement.Table_Name,
//                    m_movement_id, trxName);
//            MBPartner bpartner = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name,
//                    iMove.getC_BPartner_ID(), trxName);
//            MUser salesrep = ForcaObject.getObject(Env.getCtx(), MUser.Table_Name,
//                    iMove.getSalesRep_ID(), trxName);
//            NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
//
//            if (iMove != null) {
//                map.put("c_bpartner_id", iMove.getC_BPartner_ID());
//                map.put("c_bpartner_name", bpartner.getName());
//                map.put("salesrep_name", salesrep.getName());
//                map.put("m_movement_id", iMove.getM_Movement_ID());
//                map.put("documentno", iMove.getDocumentNo());
//                map.put("docstatus", iMove.getDocStatus());
//                map.put("movementdate", Constants.sdf.format(iMove.getMovementDate()));
//                map.put("status", WsUtil.descDocStatus(Env.getCtx(), iMove.getDocStatus()));
//                map.put("docaction", iMove.getDocAction());
//                map.put("crm_id", iMove.get_Value("crm_id"));
//                map.put("istransit", iMove.get_Value("isintransit"));
//                map.put("description", iMove.get_Value("description"));
//
//                Move = new MovementImplTrans();
//                map.put("m_movementline", Move.getMovementLineByMovementID(m_movement_id, trxName));
//
//                resp = ResponseData.successResponse(Constants.SUCCEED_DataFound, map);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData getInventoryMoveList(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new PaginationData();
//        List<Object> listdata = new ArrayList<Object>();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//        List<Object> params = new ArrayList<>();
//        String trxName = trx.getTrxName();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        //@formatter:off
//        sql.append(""
//                + "WITH cte AS ("
//                + " SELECT "
//                + "    mv.m_movement_id, "
//                + "    bp.name, "
//                + "    mv.documentno, "
//                + "    mv.movementdate, "
//                + "    mv.docstatus "
//                + "FROM "
//                + "    m_movement mv "
//                + "INNER JOIN c_bpartner bp ON "
//                + "    mv.c_bpartner_id = bp.c_bpartner_id"
//                + " WHERE "
//                + "     mv.ad_client_id = ?");
//        //@formatter:on
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        if (param.getDatefrom() != null) {
//            sql.append(" " + "AND to_char( trunc( mv.movementdate ), " + "'YYYY-MM-DD' ) >= ? ");
//
//            Date date_from = null;
//            try {
//                date_from = Constants.df.parse(param.getDatefrom());
//            } catch (ParseException e) {
//                resp = PaginationData.errorResponse("Parameter invalid date format");
//                e.printStackTrace();
//                throw new Exception(e);
//            }
//            params.add(Constants.sdf.format(date_from));
//        }
//
//        if (param.getDateto() != null) {
//            sql.append(" " + "AND to_char( trunc( mv.movementdate ), " + "'YYYY-MM-DD' ) <= ? ");
//
//            Date date_to = null;
//            try {
//                date_to = Constants.df.parse(param.getDateto());
//            } catch (ParseException e) {
//                resp = PaginationData.errorResponse("Parameter invalid date format");
//                e.printStackTrace();
//                throw new Exception(e);
//            }
//            params.add(Constants.sdf.format(date_to));
//        }
//
//        if (param.getIsapproved() != null) {
//            sql.append(" AND mv.isapproved = ? ");
//            params.add(param.getIsapproved());
//        }
//
//        LinkedHashMap<String, String> docStatus = WsUtil.docStatus(Env.getCtx());
//
//        if (param.getStatus() != null) {
//            sql.append(" AND mv.docstatus = ? ");
//            params.add(docStatus.get(param.getStatus()));
//        }
//        if (param.getDocumentno() != null) {
//            sql.append(" AND mv.documentno = ? ");
//            params.add(param.getDocumentno());
//        }
//
//        //@formatter:off
//        sql.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE  cte "
//            + "   ORDER  BY m_movement_id "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), trxName);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//            boolean first = true;
//            int totaldata = 0;
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("m_movement_id", rs.getString("m_movement_id"));
//                map.put("bp_name", rs.getString("name"));
//                map.put("documentno", rs.getString("documentno"));
//                map.put("movementdate", rs.getString("movementdate"));
//                map.put("status", docStatus.get(rs.getString("docstatus")));
//
//                if (rs.getString("m_movement_id") != null) {
//                    listdata.add(map);
//                }
//
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//
//            resp.setCodestatus("S");
//            resp.setMessage(totaldata + " Data Found");
//            resp.setResultdata(listdata);
//        } catch (SQLException e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData getInventoryMoveLine(ParamRequest param, Trx trx) throws Exception {
//        if (param.getM_movementline_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//
//        try {
//            Integer m_movementline_id = param.getM_movementline_id();
//            MMovementLine iml = ForcaObject.getObject(Env.getCtx(), MMovementLine.Table_Name,
//                    m_movementline_id, trxName);
//            MProduct prd = ForcaObject.getObject(Env.getCtx(), MProduct.Table_Name,
//                    iml.getM_Product_ID(), trxName);
//            MLocator loc = ForcaObject.getObject(Env.getCtx(), MLocator.Table_Name,
//                    iml.getM_Locator_ID(), trxName);
//            MLocator locto = ForcaObject.getObject(Env.getCtx(), MLocator.Table_Name,
//                    iml.getM_LocatorTo_ID(), trxName);
//
//            MUOM uom = ForcaObject.getObject(Env.getCtx(), MUOM.Table_Name, prd.getC_UOM_ID(),
//                    trxName);
//
//            if (iml != null) {
//                map.put("m_movement_id", iml.getM_Movement_ID());
//                map.put("line_number", iml.getLine());
//                map.put("m_movementline_id", iml.getM_MovementLine_ID());
//                map.put("m_product_id", iml.getM_Product_ID());
//                map.put("product_name", prd.getName());
//                map.put("m_locator_id", iml.getM_Locator_ID());
//                map.put("locator_name", loc.getValue());
//                map.put("m_locatorto_id", iml.getM_LocatorTo_ID());
//                map.put("locatorto_name", locto.getValue());
//                map.put("qtyentered", iml.getMovementQty());
//                map.put("movementqty", iml.getMovementQty());
//                map.put("forca_c_uom_id", prd.getC_UOM_ID());
//                map.put("uom_name", uom.getName());
//
//                resp = ResponseData.successResponse(Constants.SUCCEED_DataFound, map);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData setOrderDoc(ParamRequest param, String action, Trx trx) throws Exception {
//        if (param.getC_order_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        String trxName = trx.getTrxName();
//        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//
//        MOrder order = null;
//        try {
//            order = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, param.getC_order_id(),
//                    trxName);
//            if (action.equals("CO")) {
//                order.setDocAction(DocAction.ACTION_Complete);
//                order.processIt(DocAction.ACTION_Complete);
//                if (order.getDocStatus().equals(DocAction.STATUS_Completed)) {
//                    xmap.put("docstatus", order.getDocStatus());
//                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), order.getDocStatus()));
//                    xmap.put("docaction", order.getDocAction());
//                    xmap.put("crm_id", order.get_Value("crm_id"));
//                    xmap.put("c_order_id", order.get_ID());
//                    xmap.put("documentno", order.getDocumentNo());
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success Complete Order");
//                    resp.setResultdata(listdata);
//                } else {
//                    resp.setMessage("Failed Complete Order");
//                }
//            } else if (action.equals("VO")) {
//                MWorkflow.runDocumentActionWorkflow(order, DocAction.ACTION_Void);
//                order.setDocStatus(DocAction.ACTION_Void);
//                if (order.getDocStatus().equals(DocAction.STATUS_Voided)) {
//                    xmap.put("docstatus", order.getDocStatus());
//                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), order.getDocStatus()));
//                    xmap.put("docaction", order.getDocAction());
//                    xmap.put("crm_id", order.get_Value("crm_id"));
//                    xmap.put("c_order_id", order.get_ID());
//                    xmap.put("documentno", order.getDocumentNo());
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success Void Order");
//                    resp.setResultdata(listdata);
//                } else {
//                    resp.setMessage("Success Void Order");
//                }
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            if (e.getMessage() == null) {
//                resp.setMessage(order.get_Logger().toString());
//            } else {
//                resp = ResponseData.errorResponse(e.getMessage());
//            }
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setInOutDoc(ParamRequest param, String action, Trx trx) throws Exception {
//        if (param.getM_inout_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        String trxName = trx.getTrxName();
//        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//
//        MInOut inout = null;
//        try {
//            inout = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, param.getM_inout_id(),
//                    trxName);
//            if (action.equals("CO")) {
//                if (inout.processIt(DocAction.ACTION_Complete)) {
//                    xmap.put("docstatus", inout.getDocStatus());
//                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), inout.getDocStatus()));
//                    xmap.put("docaction", inout.getDocAction());
//                    xmap.put("crm_id", inout.get_Value("crm_id"));
//                    xmap.put("m_inout_id", inout.get_ID());
//                    xmap.put("documentno", inout.getDocumentNo());
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success Complete In Out");
//                    resp.setResultdata(listdata);
//                } else {
//                    resp.setMessage("Failed Complete In Out");
//                }
//            } else if (action.equals("VO")) {
//                MWorkflow.runDocumentActionWorkflow(inout, DocAction.ACTION_Void);
//                inout.setDocStatus(DocAction.ACTION_Void);
//                if (inout.getDocStatus().equals(DocAction.STATUS_Voided)) {
//                    xmap.put("docstatus", inout.getDocStatus());
//                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), inout.getDocStatus()));
//                    xmap.put("docaction", inout.getDocAction());
//                    xmap.put("crm_id", inout.get_Value("crm_id"));
//                    xmap.put("m_inout_id", inout.get_ID());
//                    xmap.put("documentno", inout.getDocumentNo());
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success Void In Out");
//                    resp.setResultdata(listdata);
//
//                } else {
//                    log.warning("In Out Process Failed: " + inout + " - " + inout.getProcessMsg());
//                    throw new IllegalStateException(
//                            "In Out Process Failed: " + inout + " - " + inout.getProcessMsg());
//                }
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            if (e.getMessage() == null) {
//                resp.setMessage(inout.get_Logger().toString());
//            } else {
//                resp = ResponseData.errorResponse(e.getMessage());
//            }
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setInventoryMoveDoc(ParamRequest param, String action, Trx trx)
//            throws Exception {
//        if (param.getM_movement_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        String trxName = trx.getTrxName();
//        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//
//        MMovement move = null;
//        try {
//            move = ForcaObject.getObject(Env.getCtx(), MMovement.Table_Name,
//                    param.getM_movement_id(), trxName);
//            if (action.equals("CO")) {
//                if (move.processIt(DocAction.ACTION_Complete)) {
//                    xmap.put("docstatus", move.getDocStatus());
//                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), move.getDocStatus()));
//                    xmap.put("docaction", move.getDocAction());
//                    xmap.put("crm_id", move.get_Value("crm_id"));
//                    xmap.put("m_movement_id", move.get_ID());
//                    xmap.put("documentno", move.getDocumentNo());
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success Complete Inventory Move");
//                    resp.setResultdata(listdata);
//                } else {
//                    resp.setMessage("Failed Complete Inventory Move");
//                }
//            } else if (action.equals("VO")) {
//                MWorkflow.runDocumentActionWorkflow(move, DocAction.ACTION_Void);
//                move.setDocStatus(DocAction.ACTION_Void);
//                if (move.getDocStatus().equals(DocAction.STATUS_Voided)) {
//                    xmap.put("docstatus", move.getDocStatus());
//                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), move.getDocStatus()));
//                    xmap.put("docaction", move.getDocAction());
//                    xmap.put("crm_id", move.get_Value("crm_id"));
//                    xmap.put("m_movement_id", move.get_ID());
//                    xmap.put("documentno", move.getDocumentNo());
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success Void Inventory Move");
//                    resp.setResultdata(listdata);
//                } else {
//                    resp.setMessage("Failed Void Inventory Move");
//                }
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            if (e.getMessage() == null) {
//                resp.setMessage(move.get_Logger().toString());
//            } else {
//                resp = ResponseData.errorResponse(e.getMessage());
//            }
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setOrder(ParamOrder param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        try {
//            Integer c_order_id = param.getC_order_id();
//            Integer c_bpartner_id = param.getC_bpartner_id();
//            String PaymentRule = param.getPayment_rule();
//            List<ParamOrderLine> listLine = param.getList_line();
//            Boolean isInsert = false;
//
//            MOrder o = null;
//            if (c_order_id.intValue() <= 0) {
//                o = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, 0, trxName);
//                isInsert = true;
//            } else {
//                o = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, c_order_id, trxName);
//                if (o == null) {
//                    o = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            }
//
//            if (isInsert) {
//                if (c_bpartner_id.intValue() <= 0 || Util.isEmpty(param.getIssotrx())
//                        || param.getM_warehouse_id().intValue() <= 0
//                        || param.getM_pricelist_id().intValue() <= 0) {
//                    String par = "";
//                    if (c_bpartner_id.intValue() <= 0) {
//                        par += "c_bpartner, ";
//                    }
//                    if (Util.isEmpty(param.getIssotrx())) {
//                        par += "issotrx, ";
//                    }
//                    if (param.getM_warehouse_id().intValue() <= 0) {
//                        par += "m_warehouse_id, ";
//                    }
//                    if (param.getM_pricelist_id().intValue() <= 0) {
//                        par += "m_pricelist_id, ";
//                    }
//                    return ResponseData.errorResponse(par + "is required");
//                }
//
//                // insert
//                if (param.getIssotrx().equals("Y")) {
//                    o.setIsSOTrx(true);
//                } else {
//                    o.setIsSOTrx(false);
//                }
//
//                if (!Util.isEmpty((param.getDateordered()))) {
//                    o.setDateOrdered(WsUtil.convertFormatTgl(param.getDateordered()));
//                } else {
//                    o.setDateOrdered(new Timestamp(new Date().getTime()));
//                }
//
//                o.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    o.setC_BPartner_Location_ID(param.getC_bpartner_location_id().intValue());
//                } else {
//                    // c_bpartner_id pasti > 0
//                    o.setC_BPartner_Location_ID(WsUtil.getDefaultBPartnerLocationID(c_bpartner_id));
//                }
//
//                o.setM_Warehouse_ID(param.getM_warehouse_id());
//                o.setM_PriceList_ID(param.getM_pricelist_id());
//                o.setSalesRep_ID(
//                        WsUtil.getDefaultSalesRepIDByName(Env.getCtx(), param.getSalerep_name()));
//
//
//                if (Util.isEmpty(PaymentRule)) {
//                    PaymentRule = WsUtil.getPaymentRule();
//                }
//
//                o.setC_Currency_ID(WsUtil.getDefaultCountryID());
//
//                if (param.getDropship_bpartner_id().intValue() > 0) {
//                    o.setDropShip_BPartner_ID(param.getDropship_bpartner_id());
//                    if (param.getDropship_location_id().intValue() == 0) {
//                        o.setDropShip_Location_ID(WsUtil
//                                .getDefaultBPartnerLocationID(param.getDropship_bpartner_id()));
//                    }
//                }
//
//                if (param.getDropship_location_id().intValue() > 0) {
//                    o.setDropShip_Location_ID(param.getDropship_location_id().intValue());
//                }
//
//                if (!Util.isEmpty(param.getDescription())) {
//                    o.setDescription(param.getDescription());
//                }
//
//                if (param.getC_project_id() != null) {
//                    o.setC_Project_ID(param.getC_project_id());
//                }
//
//                if (param.getUser2_id().intValue() > 0) {
//                    o.setUser2_ID(param.getUser2_id());
//                }
//
//                if (param.getC_paymentterm_id() > 0) {
//                    o.setC_Payment_ID(param.getC_paymentterm_id());
//                }
//
//            } else {
//                // set
//                if (!Util.isEmpty((param.getDateordered()))) {
//                    o.setDateOrdered(WsUtil.convertFormatTgl(param.getDateordered()));
//                }
//
//                if (param.getAd_org_id().intValue() > 0) {
//                    o.setAD_Org_ID(param.getAd_org_id());
//                }
//
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    o.setC_BPartner_Location_ID(param.getC_bpartner_location_id());
//                }
//
//                if (param.getM_warehouse_id().intValue() > 0) {
//                    o.setM_Warehouse_ID(param.getM_warehouse_id().intValue());
//                }
//
//                // cuma untuk case SO
//                if (param.getM_pricelist_id().intValue() > 0) {
//                    o.setM_PriceList_ID(param.getM_pricelist_id().intValue());
//                } else if (o.isSOTrx()) {
//                    WsUtil.defaultPriceJual(Env.getCtx());
//                }
//
//                if (!Util.isEmpty(param.getSalerep_name())) {
//                    o.setSalesRep_ID(WsUtil.getDefaultSalesRepIDByName(Env.getCtx(),
//                            param.getSalerep_name()));
//                }
//
//                if (param.getAd_orgtrx_id().intValue() > 0) {
//                    o.set_ValueOfColumn("ad_orgtrx_id", param.getAd_orgtrx_id());
//                }
//
//                if (param.getCrm_id().intValue() > 0) {
//                    o.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//
//                if (param.getDropship_bpartner_id().intValue() > 0) {
//                    o.setDropShip_BPartner_ID(param.getDropship_bpartner_id());
//                    if (param.getDropship_location_id().intValue() == 0) {
//                        o.setDropShip_Location_ID(WsUtil
//                                .getDefaultBPartnerLocationID(param.getDropship_bpartner_id()));
//                    }
//                }
//
//                if (param.getDropship_location_id().intValue() > 0) {
//                    o.setDropShip_Location_ID(param.getDropship_location_id().intValue());
//                }
//
//                if (!Util.isEmpty(param.getDescription())) {
//                    o.setDescription(param.getDescription());
//                }
//
//                if (param.getC_project_id().intValue() > 0) {
//                    o.setC_Project_ID(param.getC_project_id());
//                }
//
//                if (param.getUser2_id().intValue() > 0) {
//                    o.setUser2_ID(param.getUser2_id());
//                }
//
//                if (param.getC_paymentterm_id() > 0) {
//                    o.setC_Payment_ID(param.getC_paymentterm_id());
//                }
//
//            }
//
//
//            // set_insert
//            o.setC_DocTypeTarget_ID(WsUtil.getDefaultDocTypeID(Env.getCtx()));
//
//            if (c_bpartner_id.intValue() > 0) {
//                o.setC_BPartner_ID(c_bpartner_id);
//            }
//
//
//            if (param.getC_opportunity_id().intValue() > 0) {
//                o.setC_Opportunity_ID(param.getC_opportunity_id().intValue());
//            }
//
//            if (param.getC_campaign_id().intValue() > 0) {
//                o.setC_Campaign_ID(param.getC_campaign_id().intValue());
//            }
//
//            if (param.getDocumentno() != null) {
//                o.setDocumentNo(param.getDocumentno());
//            }
//
//            if (param.getAd_user_id().intValue() > 0) {
//                o.setAD_User_ID(param.getAd_user_id().intValue());
//            }
//
//            if (!Util.isEmpty(param.getPoreference())) {
//                o.setPOReference(param.getPoreference());
//            }
//
//            o.setPaymentRule(PaymentRule);
//
//            o.saveEx();
//
//            Integer line = 0;
//            for (ParamOrderLine pol : listLine) {
//                Integer tax_id = 0;
//                if (pol.getC_tax_id().intValue() > 0) {
//                    tax_id = Integer.valueOf(pol.getC_tax_id());
//                } else {
//                    tax_id = WsUtil.getDefaultTaxID(Env.getCtx());
//                }
//
//                MOrderLine ol = null;
//                if (o.getLines().length <= 0) {
//                    ol = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name, 0, trxName);
//                } else {
//                    int c_orderline_id = o.getLines()[line].get_ID();
//                    ol = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name, c_orderline_id,
//                            trxName);
//                    if (ol == null) {
//                        ol = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name, 0, trxName);
//                    }
//                }
//
//                ol.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//                ol.setC_Order_ID(o.get_ID());
//                ol.setLine((line + 1) * 10);
//                ol.setM_Product_ID(Integer.valueOf(pol.getM_product_id()));
//                ol.setC_UOM_ID(Integer.valueOf(pol.getC_uom_id()));
//                ol.setQtyEntered(new BigDecimal(pol.getQty_entered()));
//                ol.setQtyOrdered(new BigDecimal(pol.getQty_entered()));
//                ol.setPriceEntered(pol.getPrice_entered());
//                ol.setPriceActual(pol.getPrice_entered());
//                ol.setC_Tax_ID(tax_id);
//                if (pol.getDiscount() != null) {
//                    ol.setDiscount(pol.getDiscount());
//                }
//                ol.saveEx();
//
//                line++;
//            }
//
//            if (!Util.isEmpty(param.getDocstatus())) {
//                if (param.getDocstatus().equals(DocAction.ACTION_Complete)) {
//                    try {
//                        o.setDocAction(DocAction.ACTION_Complete);
//                        o.processIt(DocAction.ACTION_Complete);
//                        if (!o.getDocStatus().equals(DocAction.STATUS_Completed)) {
//                            throw new AdempiereException(
//                                    "Failed to Create Complete Order, Please Check Your Parameter");
//                        }
//                    } catch (AdempiereException e) {
//                        throw new AdempiereException(e.getMessage());
//                    }
//                } else {
//                    throw new AdempiereException("Document Action False");
//                }
//            }
//
//            resp.setCodestatus("S");
//            if (isInsert)
//                resp.setMessage("Insert succeeded");
//            else
//                resp.setMessage("Update succeeded");
//
//            Map<String, Object> xmap = new LinkedHashMap<>();
//            xmap.put("crm_id", o.get_Value("crm_id"));
//            xmap.put("c_order_id", o.get_ID());
//            xmap.put("documentno", o.getDocumentNo());
//            resp.setResultdata(xmap);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData setOrderLine(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        MOrder order = null;
//        MOrderLine orderLine = null;
//        try {
//            boolean isInsert = false;
//
//            Integer c_order_id = param.getC_order_id();
//            Integer c_orderline_id = param.getC_orderline_id();
//            if (param.getC_orderline_id() > 0) {
//                orderLine = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name,
//                        c_orderline_id, trxName);
//                if (orderLine == null) {
//                    orderLine =
//                            ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            } else {
//                orderLine = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name, 0, trxName);
//                isInsert = true;
//            }
//
//            if (isInsert) {
//                if (param.getC_order_id() != null) {
//                    order = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name,
//                            param.getC_order_id().intValue(), trxName);
//                    orderLine.setC_Order_ID(c_order_id);
//                    orderLine.setOrder(order);
//                } else if (param.getC_order_id() == null || param.getM_product_id() == null
//                        || param.getQty() == null || param.getC_uom_id() == null) {
//                    ResponseData.parameterRequired();
//                }
//            }
//
//
//            if (param.getAd_org_id() != null)
//                orderLine.setAD_Org_ID(order.getAD_Org_ID());
//            else
//                orderLine.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//            if (param.getM_product_id() != null)
//                orderLine.setM_Product_ID(param.getM_product_id().intValue());
//
//            if (param.getQty() != null)
//                orderLine.setQty(param.getQty());
//            else
//                orderLine.setQty(Env.ONE);
//
//            if (param.getPriceactual() != null)
//                orderLine.setPriceActual(param.getPriceactual());
//            else
//                orderLine.setPriceActual(Env.ONEHUNDRED);
//
//            if (param.getPriceentered() != null)
//                orderLine.setPriceEntered(param.getPriceentered());
//            else
//                orderLine.setPriceEntered(Env.ONEHUNDRED);
//
//            if (param.getC_uom_id() != null)
//                orderLine.setC_UOM_ID(param.getC_uom_id().intValue());
//
//            if (param.getCrm_id() != null)
//                orderLine.set_ValueOfColumn("crm_id", param.getCrm_id());
//
//            if (param.getAd_orgtrx_id() != null)
//                orderLine.set_ValueOfColumn("ad_orgtrx_id", param.getAd_orgtrx_id());
//
//            if (param.getC_tax_id() != null)
//                orderLine.setC_Tax_ID(param.getC_tax_id());
//
//            if (param.getDescription() != null) {
//                orderLine.setDescription(param.getDescription());
//            }
//
//            orderLine.saveEx();
//
//            resp.setCodestatus("S");
//            if (isInsert)
//                resp.setMessage("Insert data order lines succeeded");
//            else
//                resp.setMessage("Update data order lines succeeded");
//
//            Map<String, Object> xmap = new LinkedHashMap<>();
//            xmap.put("crm_id", orderLine.get_Value("crm_id"));
//            xmap.put("c_orderline_id", Integer.valueOf(orderLine.getC_OrderLine_ID()));
//            xmap.put("line_number", orderLine.getLine());
//            resp.setResultdata(xmap);
//
//        } catch (ProductNotOnPriceListException e) {
//            resp.setMessage(e.getStackTrace().toString());
//            throw new Exception(e);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData setMultiDocumentApproved(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        Integer ad_client_id = Env.getAD_Client_ID(Env.getCtx());
//        Integer ad_user_id = Env.getAD_User_ID(Env.getCtx());
//        String multiple_ad_wf_activity_id = param.getMultiple_ad_wf_activity_id();
//        String message = param.getMessage();
//        String value = param.getYesno().toUpperCase();
//        int dt = DisplayType.YesNo;
//
//        Properties wsenv = ResponseData.getDefaultCtx();
//        wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
//        wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
//        Env.setCtx(wsenv);
//
//        try {
//            String[] split = multiple_ad_wf_activity_id.split("/");
//            for (int i = 0; i < split.length; i++) {
//                MWFActivity m_activity = ForcaObject.getObject(Env.getCtx(), MWFActivity.Table_Name,
//                        Integer.valueOf(split[i]), trx.getTrxName());
//                m_activity.setUserChoice(ad_user_id, value, dt, message);
//            }
//
//            resp.setCodestatus("S");
//            if (value.equals("Y"))
//                resp.setMessage("Document has been Approved");
//            else {
//                resp.setMessage("Document Has Been Rejected");
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//
//        return resp;
//    }
//
//
//    public ResponseData insertInvoiceVendor(ParamInvoice param, String token, Trx trx)
//            throws Exception {
//        MInvoice i = null;
//        MInvoiceLine il = null;
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        ParamRequest param2 = new ParamRequest();
//        param2.setToken(token);
//        Integer ad_client_id = Env.getAD_Client_ID(Env.getCtx());
//        Integer ad_user_id = Env.getAD_User_ID(Env.getCtx());
//
//        try {
//            String hDesc = param.getDescription_header();
//            Integer C_BPartner_ID = Integer.valueOf(param.getC_bpartner_id());
//            String dateInvoiced = param.getDate_invoiced();
//            String m_pricelist_id = param.getM_pricelist_id();
//            List<ParamInvoiceLine> listLine = param.getLine_list();
//            Integer ad_org_id = Integer.valueOf(param.getAd_org_id());
//            String paymentrule = param.getPaymentrule();
//
//            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD");
//
//            Integer line = 10;
//
//            String whereClause;
//            whereClause = " ISO_Code = 'IDR' ";
//            MCurrency cur =
//                    new Query(Env.getCtx(), MCurrency.Table_Name, whereClause, trxName).first();
//
//            whereClause = " Name = 'AP Invoice' AND AD_Client_ID = ? ";
//            MDocType docType = new Query(Env.getCtx(), MDocType.Table_Name, whereClause, trxName)
//                    .setParameters(ad_client_id).first();
//
//            whereClause = " C_BPartner_ID = ? AND AD_Client_ID = ? ";
//            MBPartnerLocation bpLoc =
//                    new Query(Env.getCtx(), MBPartnerLocation.Table_Name, whereClause, trxName)
//                            .setParameters(Integer.valueOf(C_BPartner_ID), ad_client_id).first();
//
//            i = ForcaObject.getObject(Env.getCtx(), MInvoice.Table_Name, 0, trxName);
//
//            i.setAD_Org_ID(ad_org_id);
//            i.setIsActive(true);
//            i.setC_DocTypeTarget_ID(docType.get_ID());
//            i.setC_DocType_ID(docType.get_ID());
//            i.setDateInvoiced(new Timestamp(sdf.parse(dateInvoiced).getTime()));
//            i.setC_BPartner_ID(Integer.valueOf(C_BPartner_ID));
//            i.setC_BPartner_Location_ID(bpLoc.get_ID());
//            i.setDateAcct(new Timestamp(sdf.parse(dateInvoiced).getTime()));
//            i.setM_PriceList_ID(Integer.valueOf(m_pricelist_id));
//            i.setC_Currency_ID(cur.get_ID());
//            i.setPaymentRule(paymentrule);
//            i.setAD_User_ID(ad_user_id);
//            i.setSalesRep_ID(Integer.valueOf(param.getSalesrep_id()));
//            i.setDescription(hDesc);
//            i.setIsSOTrx(false);
//            i.saveEx();
//
//            for (ParamInvoiceLine pil : listLine) {
//                il = ForcaObject.getObject(Env.getCtx(), MInvoiceLine.Table_Name, 0, trxName);
//                il.setAD_Org_ID(ad_org_id);
//                il.setC_Invoice_ID(i.getC_Invoice_ID());
//                il.setLine(line);
//                il.setC_Charge_ID(Integer.valueOf(pil.getC_charge_id()));
//                il.setQtyEntered(Env.ONE);
//                il.setQtyInvoiced(Env.ONE);
//                il.setPriceEntered(new BigDecimal(pil.getPrice_entered()));
//                il.setPriceActual(new BigDecimal(pil.getPrice_entered()));
//                il.setC_Tax_ID(Integer.valueOf(pil.getC_tax_id()));
//                il.setDescription(pil.getDescription_line());
//
//                il.saveEx();
//                line += 10;
//
//            }
//
//            // MWorkflow.runDocumentActionWorkflow(i, DocAction.ACTION_Complete);
//            resp.setCodestatus("S");
//            resp.setMessage("Insert Successed");
//            Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//            xmap.put("c_invoice_id", i.get_ID());
//            xmap.put("documentno", i.getDocumentNo());
//            resp.setResultdata(xmap);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setInventoryMove(ParamInventoryMove param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        try {
//            List<ParamInventoryMoveLine> listLine = param.getList_line();
//            Boolean isInsert = false;
//
//            MMovement mv = null;
//            if (param.getM_movement_id().intValue() <= 0) {
//                mv = ForcaObject.getObject(Env.getCtx(), MMovement.Table_Name, 0, trxName);
//                isInsert = true;
//            } else {
//                mv = ForcaObject.getObject(Env.getCtx(), MMovement.Table_Name,
//                        param.getM_movement_id(), trxName);
//                if (mv == null) {
//                    mv = ForcaObject.getObject(Env.getCtx(), MMovement.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            }
//
//            if (isInsert) {
//                if (param.getC_bpartner_id().intValue() <= 0) {
//                    String par = "";
//                    if (param.getC_bpartner_id().intValue() <= 0) {
//                        par += "c_bpartner, ";
//                    }
//                    return ResponseData.errorResponse(par + "is required");
//                }
//
//                // insert
//                if (!Util.isEmpty((param.getMovementdate()))) {
//                    mv.setMovementDate(WsUtil.convertFormatTgl(param.getMovementdate()));
//                } else {
//                    mv.setMovementDate(new Timestamp(new Date().getTime()));
//                }
//
//                mv.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    mv.setC_BPartner_Location_ID(param.getC_bpartner_location_id().intValue());
//                } else {
//                    // c_bpartner_id pasti > 0
//                    mv.setC_BPartner_Location_ID(
//                            WsUtil.getDefaultBPartnerLocationID(param.getC_bpartner_id()));
//                }
//
//                if (!Util.isEmpty(param.getDescription())) {
//                    mv.setDescription(param.getDescription());
//                }
//
//                if (!Util.isEmpty(param.getIsintransit())) {
//                    if (param.getIsintransit().equals("Y")) {
//                        mv.setIsInTransit(true);
//                    } else {
//                        mv.setIsInTransit(false);
//                    }
//                }
//
//                mv.setSalesRep_ID(
//                        WsUtil.getDefaultSalesRepIDByName(Env.getCtx(), param.getSalerep_name()));
//
//            } else {
//                // set
//                if (!Util.isEmpty((param.getMovementdate()))) {
//                    mv.setMovementDate(WsUtil.convertFormatTgl(param.getMovementdate()));
//                }
//
//                if (param.getAd_org_id().intValue() > 0) {
//                    mv.setAD_Org_ID(param.getAd_org_id());
//                }
//
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    mv.setC_BPartner_Location_ID(param.getC_bpartner_location_id());
//                }
//
//                if (!Util.isEmpty(param.getSalerep_name())) {
//                    mv.setSalesRep_ID(WsUtil.getDefaultSalesRepIDByName(Env.getCtx(),
//                            param.getSalerep_name()));
//                }
//
//                if (param.getAd_orgtrx_id().intValue() > 0) {
//                    mv.set_ValueOfColumn("ad_orgtrx_id", param.getAd_orgtrx_id());
//                }
//
//                if (param.getCrm_id().intValue() > 0) {
//                    mv.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//
//                if (!Util.isEmpty(param.getDescription())) {
//                    mv.setDescription(param.getDescription());
//                }
//
//                if (!Util.isEmpty(param.getIsintransit())) {
//                    if (param.getIsintransit().equals("Y")) {
//                        mv.setIsInTransit(true);
//                    } else {
//                        mv.setIsInTransit(false);
//                    }
//                }
//            }
//
//
//            // set_insert
//            mv.setC_DocType_ID(WsUtil.getDefaultDocTypeID(Env.getCtx()));
//
//            if (param.getC_bpartner_id().intValue() > 0) {
//                mv.setC_BPartner_ID(param.getC_bpartner_id());
//            }
//
//            if (param.getC_campaign_id().intValue() > 0) {
//                mv.setC_Campaign_ID(param.getC_campaign_id().intValue());
//            }
//
//            if (param.getAd_user_id().intValue() > 0) {
//                mv.setAD_User_ID(param.getAd_user_id().intValue());
//            }
//
//            mv.saveEx();
//
//            Integer line = 0;
//            for (ParamInventoryMoveLine pol : listLine) {
//
//                MMovementLine mvl = null;
//                if (mv.getLines(false).length <= 0) {
//                    mvl = ForcaObject.getObject(Env.getCtx(), MMovementLine.Table_Name, 0, trxName);
//                } else {
//                    int m_movementline_id = mv.getLines(false)[line].get_ID();
//                    mvl = ForcaObject.getObject(Env.getCtx(), MMovementLine.Table_Name,
//                            m_movementline_id, trxName);
//                    if (mvl == null) {
//                        mvl = ForcaObject.getObject(Env.getCtx(), MMovementLine.Table_Name, 0,
//                                trxName);
//                    }
//                }
//
//                mvl.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//                mvl.setM_Movement_ID(mv.get_ID());
//                mvl.setLine((line + 1) * 10);
//                mvl.setM_Product_ID(Integer.valueOf(pol.getM_product_id()));
//                if (!(pol.getMovementqty().compareTo(BigDecimal.ZERO) == 0)) {
//                    mvl.setMovementQty(pol.getMovementqty());
//                } else {
//                    mvl.setMovementQty(pol.getForca_qtyentered());
//                }
//                mvl.setM_Locator_ID(pol.getM_locator_id());
//                mvl.setM_LocatorTo_ID(pol.getM_locatorto_id());
//                mvl.set_ValueOfColumn("forca_qtyentered", pol.getForca_qtyentered());
//                mvl.set_ValueOfColumn("forca_c_uom_id", pol.getForca_c_uom_id());
//                mvl.saveEx();
//
//                line++;
//            }
//
//            resp.setCodestatus("S");
//            if (isInsert)
//                resp.setMessage("Insert succeeded");
//            else
//                resp.setMessage("Update succeeded");
//
//            Map<String, Object> xmap = new LinkedHashMap<>();
//            xmap.put("crm_id", mv.get_Value("crm_id"));
//            xmap.put("m_movement_id", mv.get_ID());
//            xmap.put("documentno", mv.getDocumentNo());
//            resp.setResultdata(xmap);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setInventoryMoveLine(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        MMovement mv = null;
//        MMovementLine mvl = null;
//        try {
//            boolean isInsert = false;
//
//            Integer m_movement_id = param.getM_movement_id();
//
//            if (param.getM_movementline_id() != 0) {
//                mvl = ForcaObject.getObject(Env.getCtx(), MMovementLine.Table_Name,
//                        param.getM_movementline_id(), trxName);
//                if (mvl == null) {
//                    mvl = ForcaObject.getObject(Env.getCtx(), MMovementLine.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            } else {
//                mvl = ForcaObject.getObject(Env.getCtx(), MMovementLine.Table_Name, 0, trxName);
//                isInsert = true;
//            }
//
//            if (isInsert) {
//                if (param.getM_movement_id() != null) {
//                    mv = ForcaObject.getObject(Env.getCtx(), MMovement.Table_Name,
//                            param.getM_movement_id().intValue(), trxName);
//                    mvl.setM_Movement_ID(m_movement_id);
//
//                } else if (param.getM_movement_id() == null || param.getM_product_id() == null
//                        || param.getM_locator_id() == null || param.getM_locatorto_id() == null
//                        || param.getForca_qtyentered() == null
//                        || param.getForca_c_uom_id() == null) {
//                    ResponseData.parameterRequired();
//                }
//            }
//
//            if (param.getAd_org_id() != null)
//                mvl.setAD_Org_ID(mv.getAD_Org_ID());
//            else
//                mvl.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//            mvl.setM_Product_ID(param.getM_product_id().intValue());
//            mvl.setM_Locator_ID(param.getM_locator_id());
//            mvl.setM_LocatorTo_ID(param.getM_locatorto_id());
//            mvl.set_ValueOfColumn("forca_qtyentered", param.getForca_qtyentered());
//            mvl.set_ValueOfColumn("forca_c_uom_id", param.getForca_c_uom_id());
//            mvl.setMovementQty(param.getForca_qtyentered());
//
//            if (param.getCrm_id() != null)
//                mvl.set_ValueOfColumn("crm_id", param.getCrm_id());
//
//            if (param.getAd_orgtrx_id() != null)
//                mvl.set_ValueOfColumn("ad_orgtrx_id", param.getAd_orgtrx_id());
//
//            mvl.saveEx();
//
//            Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//            xmap.put("crm_id", param.getCrm_id());
//            xmap.put("m_movementline_id", Integer.valueOf(mvl.getM_MovementLine_ID()));
//            xmap.put("line_number", mvl.getLine());
//            listdata.add(xmap);
//
//            if (isInsert)
//                resp = ResponseData.successResponse("Insert data inventory move lines succeeded",
//                        listdata);
//            else
//                resp = ResponseData.successResponse("Update data inventory move lines succeeded",
//                        listdata);
//
//        } catch (ProductNotOnPriceListException e) {
//            resp.setMessage(e.getStackTrace().toString());
//            throw new Exception(e);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setInOut(ParamInOut param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<MInOutLine> list = new ArrayList<MInOutLine>();
//        List<PO> objData = new ArrayList<PO>();
//
//        try {
//            Integer m_inout_id = param.getM_inout_id();
//            Integer c_bpartner_id = param.getC_bpartner_id();
//            List<ParamInOutLine> listLine = param.getList_line();
//            Boolean isInsert = false;
//
//            MInOut inout = null;
//            if (m_inout_id.intValue() <= 0) {
//                inout = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, 0, trxName);
//                isInsert = true;
//            } else {
//                inout = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, m_inout_id, trxName);
//                if (inout == null) {
//                    inout = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            }
//
//            if (isInsert) {
//                if (param.getC_order_id() <= 0 || c_bpartner_id.intValue() <= 0
//                        || Util.isEmpty(param.getIssotrx())
//                        || param.getM_warehouse_id().intValue() <= 0
//                        || param.getC_bpartner_location_id().intValue() <= 0
//                        || Util.isEmpty(param.getMovementdate())) {
//                    String par = "";
//                    if (param.getC_order_id() < 0) {
//                        par += "c_order_id, ";
//                    }
//                    if (c_bpartner_id.intValue() <= 0) {
//                        par += "c_bpartner, ";
//                    }
//                    if (Util.isEmpty(param.getIssotrx())) {
//                        par += "issotrx, ";
//                    }
//                    if (param.getM_warehouse_id().intValue() <= 0) {
//                        par += "m_warehouse_id, ";
//                    }
//                    if (param.getC_bpartner_location_id().intValue() <= 0) {
//                        par += "c_bpartner_location_id, ";
//                    }
//                    if (Util.isEmpty(param.getMovementdate())) {
//                        par += "movementdate, ";
//                    }
//                    return ResponseData.errorResponse(par + "is required");
//                }
//
//                // insert
//                if (param.getIssotrx().equals("Y")) {
//                    String whereClause = " Name = 'MM Shipment' AND AD_Client_ID = ? ";
//                    MDocType docType =
//                            new Query(Env.getCtx(), MDocType.Table_Name, whereClause, trxName)
//                                    .setParameters(Env.getAD_Client_ID(Env.getCtx())).first();
//                    inout.setC_DocType_ID(docType.get_ID());
//                    inout.setIsSOTrx(true);
//                    inout.setMovementType(MInOut.MOVEMENTTYPE_CustomerShipment);
//                } else {
//                    String whereClause = " Name = 'MM Receipt' AND AD_Client_ID = ? ";
//                    MDocType docType =
//                            new Query(Env.getCtx(), MDocType.Table_Name, whereClause, trxName)
//                                    .setParameters(Env.getAD_Client_ID(Env.getCtx())).first();
//                    inout.setC_DocType_ID(docType.get_ID());
//                    inout.setIsSOTrx(false);
//                    inout.setMovementType(MInOut.MOVEMENTTYPE_VendorReceipts);
//                }
//
//                if (!Util.isEmpty((param.getMovementdate()))) {
//                    inout.setMovementDate(WsUtil.convertFormatTgl(param.getMovementdate()));
//                } else {
//                    inout.setMovementDate(new Timestamp(new Date().getTime()));
//                }
//
//                inout.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    inout.setC_BPartner_Location_ID(param.getC_bpartner_location_id().intValue());
//                } else {
//                    // c_bpartner_id pasti > 0
//                    inout.setC_BPartner_Location_ID(
//                            WsUtil.getDefaultBPartnerLocationID(c_bpartner_id));
//                }
//
//                inout.setM_Warehouse_ID(param.getM_warehouse_id());
//
//                inout.setC_Order_ID(param.getC_order_id());
//                inout.setPriorityRule(MInOut.PRIORITYRULE_Medium);
//                inout.setFreightCostRule(MInOut.FREIGHTCOSTRULE_FreightIncluded);
//
//            } else {
//                // set
//                if (!Util.isEmpty((param.getMovementdate()))) {
//                    inout.setMovementDate(WsUtil.convertFormatTgl(param.getMovementdate()));
//                }
//
//                if (param.getAd_org_id().intValue() > 0) {
//                    inout.setAD_Org_ID(param.getAd_org_id());
//                }
//
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    inout.setC_BPartner_Location_ID(param.getC_bpartner_location_id());
//                }
//
//                if (param.getM_warehouse_id().intValue() > 0) {
//                    inout.setM_Warehouse_ID(param.getM_warehouse_id().intValue());
//                }
//
//                if (param.getAd_orgtrx_id().intValue() > 0) {
//                    inout.set_ValueOfColumn("ad_orgtrx_id", param.getAd_orgtrx_id());
//                }
//
//                if (param.getCrm_id().intValue() > 0) {
//                    inout.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//            }
//
//            // set_insert
//            if (param.getDescription() != null) {
//                inout.setDescription(param.getDescription());
//            }
//
//            if (c_bpartner_id.intValue() > 0) {
//                inout.setC_BPartner_ID(c_bpartner_id);
//            }
//
//            if (param.getC_project_id().intValue() > 0) {
//                inout.setC_Project_ID(param.getC_project_id().intValue());
//            }
//
//            if (param.getUser2_id().intValue() > 0) {
//                inout.setUser2_ID(param.getUser2_id().intValue());
//            }
//
//            if (param.getAd_user_id().intValue() > 0) {
//                inout.setAD_User_ID(param.getAd_user_id().intValue());
//            }
//            if (param.getCrm_id().intValue() > 0) {
//                inout.set_ValueOfColumn("crm_id", param.getCrm_id());
//            }
//
//            inout.saveEx();
//
//            Integer line = 0;
//
//            for (ParamInOutLine piol : listLine) {
//                MOrderLine orderline = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name,
//                        piol.getC_orderline_id(), trxName);
//                MInOutLine inoutline = new MInOutLine(inout);
//                MOrder order = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name,
//                        param.getC_order_id(), trxName);
//
//                if (piol.getC_orderline_id() != 0) {
//                    inoutline.setC_OrderLine_ID(piol.getC_orderline_id());
//                } else {
//                    inoutline.setC_OrderLine_ID(orderline.getC_OrderLine_ID());
//                }
//
//                if (piol.getM_product_id() != 0) {
//                    inoutline.setM_Product_ID(piol.getM_product_id());
//                } else {
//                    inoutline.setM_Product_ID(orderline.getM_Product_ID(), orderline.getC_UOM_ID());
//                }
//
//                if (orderline.getM_Product_ID() == 0)
//                    inoutline.setC_Charge_ID(orderline.getC_Charge_ID());
//
//                inoutline.setC_UOM_ID(piol.getC_uom_id());
//                inoutline.setQty(piol.getQty());
//                inoutline.setM_AttributeSetInstance_ID(orderline.getM_AttributeSetInstance_ID());
//
//                if (piol.getDescription() != null) {
//                    inoutline.setDescription(piol.getDescription());
//                } else {
//                    inoutline.setDescription(orderline.getDescription());
//                }
//
//                inoutline.setC_Project_ID(orderline.getC_Project_ID());
//                inoutline.setC_ProjectPhase_ID(orderline.getC_ProjectPhase_ID());
//                inoutline.setC_ProjectTask_ID(orderline.getC_ProjectTask_ID());
//                inoutline.setC_Activity_ID(orderline.getC_Activity_ID());
//                inoutline.setC_Campaign_ID(orderline.getC_Campaign_ID());
//                inoutline.setAD_OrgTrx_ID(orderline.getAD_OrgTrx_ID());
//                inoutline.setUser1_ID(orderline.getUser1_ID());
//                inoutline.setUser2_ID(orderline.getUser2_ID());
//
//                inoutline.setM_Locator_ID(piol.getM_locator_id());
//
//                if (piol.getQty().intValue() > (orderline.getQtyReserved().intValue()))
//                    throw new Exception("Quantity of inout cannot exceed the quantity of order");
//
//                inoutline.saveEx();
//
//
//                list.add(inoutline);
//                inoutline = new MInOutLine(inout);
//
//                if (trxName == null)
//                    objData.add(inoutline);
//
//                if (order != null && order.getC_Order_ID() != 0) {
//                    inout.setAD_OrgTrx_ID(order.getAD_OrgTrx_ID());
//                    inout.setC_Project_ID(order.getC_Project_ID());
//                    inout.setC_Campaign_ID(order.getC_Campaign_ID());
//                    inout.setC_Activity_ID(order.getC_Activity_ID());
//                    inout.setUser1_ID(order.getUser1_ID());
//                    inout.setUser2_ID(order.getUser2_ID());
//                    if (order.isDropShip()) {
//                        inout.setM_Warehouse_ID(order.getM_Warehouse_ID());
//                        inout.setIsDropShip(order.isDropShip());
//                        inout.setDropShip_BPartner_ID(order.getDropShip_BPartner_ID());
//                        inout.setDropShip_Location_ID(order.getDropShip_Location_ID());
//                        inout.setDropShip_User_ID(order.getDropShip_User_ID());
//                    }
//                    inout.saveEx();
//                }
//            }
//
//            if (!Util.isEmpty(param.getDocstatus())) {
//                if (param.getDocstatus().equals(DocAction.ACTION_Complete)) {
//                    try {
//                        if (!inout.processIt(DocAction.ACTION_Complete)) {
//                            throw new AdempiereException(
//                                    "Failed to Create Complete In Out, Please Check Your Parameter");
//                        }
//                    } catch (AdempiereException e) {
//                        throw new AdempiereException(e.getMessage());
//                    }
//                } else {
//                    throw new AdempiereException("Document Action False");
//                }
//            }
//
//            resp.setCodestatus("S");
//            if (isInsert)
//                resp.setMessage("Insert succeeded");
//            else
//                resp.setMessage("Update succeeded");
//
//            Map<String, Object> xmap = new LinkedHashMap<>();
//            xmap.put("crm_id", inout.get_Value("crm_id"));
//            xmap.put("m_inout_id", inout.get_ID());
//            xmap.put("documentno", inout.getDocumentNo());
//            resp.setResultdata(xmap);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setInOutLine(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        MInOut inout = null;
//        MInOutLine inoutline = null;
//        try {
//            boolean isInsert = false;
//
//            Integer m_inout_id = param.getM_inout_id();
//            if (param.getM_inoutline_id() == 0) {
//                inoutline = ForcaObject.getObject(Env.getCtx(), MInOutLine.Table_Name, 0, trxName);
//                isInsert = true;
//            } else {
//                inoutline = ForcaObject.getObject(Env.getCtx(), MInOutLine.Table_Name,
//                        param.getM_inoutline_id().intValue(), trxName);
//                if (inoutline == null) {
//                    inoutline =
//                            ForcaObject.getObject(Env.getCtx(), MInOutLine.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            }
//
//            // locator, qty, product, uom, orderline
//            if (isInsert) {
//                if (param.getM_inout_id() != null) {
//                    inout = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name,
//                            param.getM_inout_id(), trxName);
//                    inoutline.setM_InOut_ID(m_inout_id);
//                } else if (param.getM_inout_id() == null || param.getM_locator_id() == null
//                        || param.getQty() == null || param.getM_product_id() == null
//                        || param.getC_uom_id() == null || param.getC_orderline_id() == null) {
//                    ResponseData.parameterRequired();
//                }
//            }
//
//
//            if (param.getAd_org_id() != null)
//                inoutline.setAD_Org_ID(inout.getAD_Org_ID());
//            else
//                inoutline.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//            inoutline.setQty(param.getQty());
//            inoutline.setM_Locator_ID(param.getM_locator_id());
//            inoutline.setM_Product_ID(param.getM_product_id());
//            inoutline.setC_UOM_ID(param.getC_uom_id());
//            inoutline.setC_OrderLine_ID(param.getC_orderline_id());
//
//            if (param.getDescription() != null)
//                inoutline.setDescription(param.getDescription());
//
//            if (param.getCrm_id() != null)
//                inoutline.set_ValueOfColumn("crm_id", param.getCrm_id());
//
//            if (param.getAd_orgtrx_id() != null)
//                inoutline.set_ValueOfColumn("ad_orgtrx_id", param.getAd_orgtrx_id());
//
//            if (param.getC_project_id() != null)
//                inoutline.setC_Project_ID(param.getC_project_id());
//
//            if (param.getUser2_id() != null)
//                inoutline.setUser2_ID(param.getUser2_id());
//
//            inoutline.saveEx();
//
//            Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//            xmap.put("crm_id", inoutline.get_Value("crm_id"));
//            xmap.put("m_inoutline_id", Integer.valueOf(inoutline.getM_InOutLine_ID()));
//            xmap.put("line_number", inoutline.getLine());
//            listdata.add(xmap);
//
//            if (isInsert)
//                resp = ResponseData.successResponse("Insert data in out lines succeeded", listdata);
//            else
//                resp = ResponseData.successResponse("Update data in out lines succeeded", listdata);
//
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData inactiveInventoryMove(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        MMovement mv = null;
//
//        try {
//            mv = ForcaObject.getObject(Env.getCtx(), MMovement.Table_Name, param.getM_movement_id(),
//                    trxName);
//
//            mv.setIsActive(false);
//            mv.saveEx();
//
//            resp = ResponseData.successResponse("Inactive Inventory Move Succeeded", null);
//
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData inactiveInOut(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        MInOut inout = null;
//
//        try {
//            inout = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, param.getM_inout_id(),
//                    trxName);
//
//            inout.setIsActive(false);
//            inout.saveEx();
//
//            resp = ResponseData.successResponse("Inactive In Out Succeeded", null);
//
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData inactiveOrder(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        MOrder order = null;
//
//        try {
//            order = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, param.getC_order_id(),
//                    trxName);
//            order.setIsActive(false);
//            order.saveEx();
//
//            resp = ResponseData.successResponse("Inactive Order Succeeded", null);
//
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData deleteInventoryMove(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> params = new ArrayList<>();
//        String trxName = trx.getTrxName();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        MMovement mv = null;
//        MMovementLine mvl = null;
//
//        //@formatter:off
//        sql.append("SELECT "
//                + "    m_movementline_id, "
//                + "    m_movement_id "
//                + "FROM "
//                + "    m_movementline "
//                + "WHERE "
//                + "    m_movement_id = ?");
//        //@formatter:on
//        params.add(param.getM_movement_id());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), trxName);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//
//            mv = ForcaObject.getObject(Env.getCtx(), MMovement.Table_Name, param.getM_movement_id(),
//                    trxName);
//
//            if (mv == null) {
//                resp = ResponseData.errorResponse("No Data Inventory Move Found");
//            } else if (!mv.getDocStatus().equals("DR")) {
//                resp = ResponseData.errorResponse("Document status of this document is "
//                        + WsUtil.descDocStatus(Env.getCtx(), mv.getDocStatus())
//                        + ", failed delete inventory move");
//            } else {
//                while (rs.next()) {
//                    if (rs.getString("m_movementline_id") != null) {
//                        mvl = ForcaObject.getObject(Env.getCtx(), MMovementLine.Table_Name,
//                                rs.getInt("m_movementline_id"), trxName);
//                        mvl.deleteEx(true, trxName);
//                    }
//                }
//
//                Map<String, Object> xmap = new LinkedHashMap<>();
//                xmap.put("m_movement_id", mv.get_ID());
//                xmap.put("documentno", mv.getDocumentNo());
//
//                mv.deleteEx(true, trxName);
//
//                resp = ResponseData.successResponse("Delete Inventory Move Succeeded", xmap);
//            }
//        } catch (SQLException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData deleteInventoryMoveLine(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        MMovementLine mvl = null;
//        MMovement mv = null;
//
//        try {
//            mvl = ForcaObject.getObject(Env.getCtx(), MMovementLine.Table_Name,
//                    param.getM_movementline_id(), trxName);
//            if (mvl != null) {
//                mv = ForcaObject.getObject(Env.getCtx(), MMovement.Table_Name,
//                        mvl.getM_Movement_ID(), trxName);
//            }
//
//            if (mvl == null) {
//                resp = ResponseData.errorResponse("No Data Inventory Move Line Found");
//            } else if (!mv.getDocStatus().equals("DR")) {
//                resp = ResponseData.errorResponse("Document status of inventory move header is "
//                        + WsUtil.descDocStatus(Env.getCtx(), mv.getDocStatus())
//                        + ", failed delete inventory move line");
//            } else {
//                Map<String, Object> xmap = new LinkedHashMap<>();
//                xmap.put("m_movementline_id", mvl.get_ID());
//                xmap.put("line_number", mvl.getLine());
//
//                mvl.deleteEx(true, trxName);
//
//                resp = ResponseData.successResponse("Delete Inventory Move Line Succeeded", xmap);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData deleteInOut(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> params = new ArrayList<>();
//        String trxName = trx.getTrxName();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        MInOut io = null;
//        MInOutLine iol = null;
//
//        //@formatter:off
//        sql.append("SELECT "
//                + "    m_inoutline_id, "
//                + "    m_inout_id "
//                + "FROM "
//                + "    m_inoutline "
//                + "WHERE "
//                + "    m_inout_id = ?");
//        //@formatter:on
//        params.add(param.getM_inout_id());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), trxName);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//
//            io = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, param.getM_inout_id(),
//                    trxName);
//
//            if (io == null) {
//                resp = ResponseData.errorResponse("No Data InOut Found");
//            } else if (!io.getDocStatus().equals("DR")) {
//                resp = ResponseData.errorResponse("Document status of this document is "
//                        + WsUtil.descDocStatus(Env.getCtx(), io.getDocStatus())
//                        + ", failed delete in out");
//            } else {
//                while (rs.next()) {
//                    if (rs.getString("m_inoutline_id") != null) {
//                        iol = ForcaObject.getObject(Env.getCtx(), MInOutLine.Table_Name,
//                                rs.getInt("m_inoutline_id"), trxName);
//                        iol.deleteEx(true, trxName);
//                    }
//                }
//
//                Map<String, Object> xmap = new LinkedHashMap<>();
//                xmap.put("m_inout_id", io.get_ID());
//                xmap.put("documentno", io.getDocumentNo());
//
//                io.deleteEx(true, trxName);
//
//                resp = ResponseData.successResponse("Delete In Out Succeeded", xmap);
//            }
//        } catch (SQLException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData deleteInOutLine(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        MInOutLine iol = null;
//        MInOut io = null;
//
//        try {
//            iol = ForcaObject.getObject(Env.getCtx(), MInOutLine.Table_Name,
//                    param.getM_inoutline_id(), trxName);
//            if (iol != null) {
//                io = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, iol.getM_InOut_ID(),
//                        trxName);
//            }
//
//            if (iol == null) {
//                resp = ResponseData.errorResponse("No Data InOutLine Found");
//            } else if (!io.getDocStatus().equals("DR")) {
//                resp = ResponseData.errorResponse("Document status of in out header is "
//                        + WsUtil.descDocStatus(Env.getCtx(), io.getDocStatus())
//                        + ", failed delete in out line");
//            } else {
//                Map<String, Object> xmap = new LinkedHashMap<>();
//                xmap.put("m_inoutline_id", iol.get_ID());
//                xmap.put("line_number", iol.getLine());
//
//                iol.deleteEx(true, trxName);
//
//                resp = ResponseData.successResponse("Delete In Out Line Succeeded", xmap);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData deleteOrder(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> params = new ArrayList<>();
//        String trxName = trx.getTrxName();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        MOrder o = null;
//        MOrderLine ol = null;
//
//        //@formatter:off
//        sql.append("SELECT "
//                + "    c_orderline_id, "
//                + "    c_order_id "
//                + "FROM "
//                + "    c_orderline "
//                + "WHERE "
//                + "    c_order_id = ?");
//        //@formatter:on
//        params.add(param.getC_order_id());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), trxName);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//
//            o = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, param.getC_order_id(),
//                    trxName);
//
//            if (o == null) {
//                resp = ResponseData.errorResponse("No Data Order Found");
//            } else if (!o.getDocStatus().equals("DR")) {
//                resp = ResponseData.errorResponse("Document status of this document is "
//                        + WsUtil.descDocStatus(Env.getCtx(), o.getDocStatus())
//                        + ", failed delete order");
//            } else {
//                while (rs.next()) {
//                    if (rs.getString("c_orderline_id") != null) {
//                        ol = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name,
//                                rs.getInt("c_orderline_Id"), trxName);
//                        ol.deleteEx(true, trxName);
//                    }
//                }
//
//                Map<String, Object> xmap = new LinkedHashMap<>();
//                xmap.put("c_order_id", o.get_ID());
//                xmap.put("documentno", o.getDocumentNo());
//
//                o.deleteEx(true, trxName);
//
//                resp = ResponseData.successResponse("Delete Order Succeeded", xmap);
//            }
//        } catch (SQLException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData deleteOrderLine(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        MOrderLine ol = null;
//        MOrder o = null;
//
//        try {
//            ol = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name,
//                    param.getC_orderline_id(), trxName);
//            if (ol != null) {
//                o = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, ol.getC_Order_ID(),
//                        trxName);
//            }
//
//            if (ol == null) {
//                resp = ResponseData.errorResponse("No Data Orderline Found");
//            } else if (!o.getDocStatus().equals("DR")) {
//                resp = ResponseData.errorResponse("Document status of order header is "
//                        + WsUtil.descDocStatus(Env.getCtx(), o.getDocStatus())
//                        + ", failed delete order line");
//            } else {
//                Map<String, Object> xmap = new LinkedHashMap<>();
//                xmap.put("c_orderline_id", ol.get_ID());
//                xmap.put("line_number", ol.getLine());
//
//                ol.deleteEx(true, trxName);
//
//                resp = ResponseData.successResponse("Delete Order Line Succeeded", xmap);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData getInvoiceCustomerSAAS(ParamRequest param, Trx trx) throws Exception {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        sql.append(
//                "SELECT "
//                + "     ci.c_invoice_id, "
//                + "     bp.forca_bpgroupkey, "
//                + "     bp.value AS bpkey, "
//                + "     bp.name AS bpname, "
//                + "     p.value AS productkey, "
//                + "     p.name AS productname, "
//                + "     ci.documentno, "
//                + "     ci.description, "
//                + "     ci.dateacct, "
//                + "     ci.dateinvoiced,"
//                + "     paymenttermduedate(ci.c_paymentterm_id, "
//                + "     ci.dateacct::TIMESTAMP WITH TIME ZONE) AS duedate, "
//                + "     ci.grandtotal, "
//                + "     invoiceopen(ci.c_invoice_id, 0::NUMERIC) AS openamt, "
//                + "     ci.ispaid, "
//                + "     cur.cursymbol, "
//                + "     ci.totallines, "
//                + "     ci.grandtotal-ci.totallines AS taxamt, "
//                + "     bpl.name AS alamat, "
//                + "     bp.taxid AS npwp, "
//                + "     pt.netdays, "
//                + "     cil.qtyentered AS qty, "
//                + "     u.name AS uom, "
//                + "     cil.priceentered AS price, "
//                + "     ci.poreference, "
//                + "     o.documentno AS ordernumber, "
//                + "     ci.taxinvoicenumber AS faktur "
//                + "FROM "
//                + "     c_invoice ci "
//                + "LEFT JOIN c_invoiceline cil ON "
//                + "     cil.c_invoice_id = ci.c_invoice_id "
//                + "LEFT JOIN c_bpartner bp ON "
//                + "     ci.c_bpartner_id = bp.c_bpartner_id "
//                + "LEFT JOIN m_product p ON "
//                + "     p.m_product_id = cil.m_product_id "
//                + "LEFT JOIN c_currency cur ON "
//                + "     cur.c_currency_id = ci.c_currency_id "
//                + "LEFT JOIN c_bpartner_location bpl ON "
//                + " bpl.c_bpartner_location_id = ci.c_bpartner_location_id "
//                + "LEFT JOIN c_paymentterm pt ON "
//                + " pt.c_paymentterm_id = ci.c_paymentterm_id "
//                + "LEFT JOIN c_uom u ON "
//                + " u.c_uom_id = cil.c_uom_id "
//                + "LEFT JOIN c_order o ON "
//                + " o.c_order_id = ci.c_order_id "
//                + "WHERE "
//                + "     bp.forca_bpgroupkey = '01' "
//                + "     AND p.value IN ('1000003', '1003126', '1001206') "
//                + "     AND ci.issotrx = 'Y' "
//                + "     AND ci.docstatus IN ('CO', 'CL')"
//                + "     AND ci.ad_client_id = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        if (param.getC_invoice_id() != null) {
//            sql.append(" AND ci.c_invoice_id = ? ");
//            params.add(param.getC_invoice_id());
//        }
//        if (!Util.isEmpty(param.getBpkey())) {
//            sql.append("AND bp.value = ? ");
//            params.add(param.getBpkey());
//        }
//        if (!Util.isEmpty(param.getProductkey())) {
//            sql.append("AND p.value = ? ");
//            params.add(param.getProductkey());
//        }
//        if (!Util.isEmpty(param.getIspaid())) {
//            sql.append("AND ci.ispaid = ? ");
//            params.add(param.getIspaid());
//        }
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("c_invoice_id", rs.getString("c_invoice_id"));
//                map.put("bpkey", rs.getString("bpkey"));
//                map.put("bpname", rs.getString("bpname"));
//                map.put("productkey", rs.getString("productkey"));
//                map.put("productname", rs.getString("productname"));
//                map.put("documentno", rs.getString("documentno"));
//                map.put("description", rs.getString("description"));
//                map.put("dateacct", rs.getString("dateacct"));
//                map.put("dateinvoiced", rs.getString("dateinvoiced"));
//                map.put("duedate", rs.getString("duedate"));
//                map.put("grandtotal", rs.getInt("grandtotal"));
//                map.put("openamt", rs.getInt("openamt"));
//                map.put("ispaid", rs.getString("ispaid"));
//                map.put("totallines", rs.getInt("totallines"));
//                map.put("taxamt", rs.getInt("taxamt"));
//                map.put("alamat", rs.getString("alamat"));
//                map.put("npwp", rs.getString("npwp"));
//                map.put("netdays", rs.getString("netdays"));
//                map.put("qty", rs.getInt("qty"));
//                map.put("uom", rs.getString("uom"));
//                map.put("price", rs.getString("price"));
//                map.put("poreference", rs.getString("poreference"));
//                map.put("ordernumber", rs.getString("ordernumber"));
//                map.put("faktur", rs.getString("faktur"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData calculateUOMConversion(ParamRequest param, Trx trx) throws Exception {
//        if (param.getM_product_id() == null || param.getC_uom_from_id() == null
//                || param.getC_uom_from_id() == null || param.getQty() == null) {
//            return ResponseData.parameterRequired();
//        }
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        MProduct prd = ForcaObject.getObject(Env.getCtx(), MProduct.Table_Name,
//                param.getM_product_id(), trxName);
//
//        //@formatter:off
//        sql.append(""
//                + "SELECT "
//                + "     uc.m_product_id, "
//                + "     uc.c_uom_id c_uom_from_id, "
//                + "     u1.name AS uom_from_name, "
//                + "     uc.c_uom_to_id, "
//                + "     u2.name AS uom_to_name, "
//                + "     uc.multiplyrate, "
//                + "     uc.dividerate "
//                + "FROM "
//                + "     c_uom_conversion uc "
//                + "LEFT JOIN c_uom u1 ON "
//                + "     u1.c_uom_id = uc.c_uom_id "
//                + "LEFT JOIN c_uom u2 ON "
//                + "     u2.c_uom_id = uc.c_uom_to_id "
//                + "WHERE "
//                + "     uc.ad_client_id = ? "
//                + "     AND uc.m_product_id = ? "
//                + "     AND uc.c_uom_id = ? "
//                + "     AND uc.c_uom_to_id = ? "
//                + "UNION SELECT "
//                + "     uc.m_product_id, "
//                + "     u1.c_uom_id c_uom_from_id, "
//                + "     u1.name AS uom_from_name, "
//                + "     u1.c_uom_id c_uom_to_id, "
//                + "     u1.name AS uom_to_name, "
//                + "     1 AS multiplyrate, "
//                + "     1 AS dividerate "
//                + "FROM "
//                + "     c_uom u1 "
//                + "LEFT JOIN c_uom_conversion uc ON "
//                + "     u1.c_uom_id = uc.c_uom_id "
//                + "WHERE "
//                + "     u1.ad_client_id = ? "
//                + "     AND uc.m_product_id = ? "
//                + "     AND u1.c_uom_id = ? "
//                + "     AND u1.c_uom_id = ? "
//                + NATIVE_MARKER 
//                + "LIMIT 1"
//                + NATIVE_MARKER);
//        //@formatter:on
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        params.add(param.getM_product_id());
//        params.add(prd.getC_UOM_ID());
//        params.add(param.getC_uom_to_id());
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        params.add(param.getM_product_id());
//        params.add(prd.getC_UOM_ID());
//        params.add(param.getC_uom_to_id());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("m_product_id", rs.getString("m_product_id"));
//                map.put("c_uom_from_id", rs.getString("c_uom_from_id"));
//                map.put("uom_from_name", rs.getString("uom_from_name"));
//                map.put("c_uom_to_id", rs.getString("c_uom_to_id"));
//                map.put("uom_to_name", rs.getString("uom_to_name"));
//                map.put("multiplyrate", rs.getString("multiplyrate"));
//                map.put("dividerate", rs.getString("dividerate"));
//                map.put("qty", param.getQty().multiply(rs.getBigDecimal("dividerate")));
//                listdata.add(map);
//            }
//
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData setInvoice(ParamInvoices param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<MInvoiceLine> list = new ArrayList<MInvoiceLine>();
//        List<PO> objData = new ArrayList<PO>();
//
//        try {
//            Integer c_invoice_id = param.getC_invoice_id();
//            Integer c_bpartner_id = param.getC_bpartner_id();
//            List<ParamInvoicesLine> listLine = param.getLine_list();
//            Boolean isInsert = false;
//
//            MInvoice inv = null;
//            if (c_invoice_id.intValue() <= 0) {
//                inv = ForcaObject.getObject(Env.getCtx(), MInvoice.Table_Name, 0, trxName);
//                isInsert = true;
//            } else {
//                inv = ForcaObject.getObject(Env.getCtx(), MInvoice.Table_Name, c_invoice_id,
//                        trxName);
//                if (inv == null) {
//                    inv = ForcaObject.getObject(Env.getCtx(), MInvoice.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            }
//
//            if (isInsert) {
//                if (param.getC_doctypetarget_id() <= 0 || c_bpartner_id.intValue() <= 0
//                        || Util.isEmpty(param.getIssotrx())
//                        || param.getM_pricelist_id().intValue() <= 0
//                        || param.getC_bpartner_location_id().intValue() <= 0
//                        || Util.isEmpty(param.getPaymentrule())
//                        || param.getAd_org_id().intValue() < 0) {
//                    String par = "";
//                    if (param.getC_doctypetarget_id() < 0) {
//                        par += "c_doctypetarget_id, ";
//                    }
//                    if (c_bpartner_id.intValue() <= 0) {
//                        par += "c_bpartner, ";
//                    }
//                    if (Util.isEmpty(param.getIssotrx())) {
//                        par += "issotrx, ";
//                    }
//                    if (param.getM_pricelist_id().intValue() <= 0) {
//                        par += "m_pricelist_id, ";
//                    }
//                    if (param.getC_bpartner_location_id().intValue() <= 0) {
//                        par += "c_bpartner_location_id, ";
//                    }
//                    if (Util.isEmpty(param.getPaymentrule())) {
//                        par += "paymentrule, ";
//                    }
//                    if (param.getAd_org_id().intValue() < 0) {
//                        par += "ad_org_id";
//                    }
//                    return ResponseData.errorResponse(par + "is required");
//                }
//
//                // insert
//                if (param.getIssotrx().equals("Y")) {
//                    inv.setIsSOTrx(true);
//                } else {
//                    inv.setIsSOTrx(false);
//                }
//
//                if (Util.isEmpty(param.getDateordered())) {
//                    inv.setDateOrdered(new Timestamp(new Date().getTime()));
//                } else {
//                    inv.setDateOrdered(WsUtil.convertFormatTgl(param.getDateordered()));
//                }
//
//                if (Util.isEmpty(param.getDateacct())) {
//                    inv.setDateAcct(new Timestamp(new Date().getTime()));
//                } else {
//                    inv.setDateAcct(WsUtil.convertFormatTgl(param.getDateacct()));
//                }
//
//                if (Util.isEmpty(param.getDateinvoiced())) {
//                    inv.setDateInvoiced(new Timestamp(new Date().getTime()));
//                } else {
//                    inv.setDateInvoiced(WsUtil.convertFormatTgl(param.getDateinvoiced()));
//                }
//
//                inv.setC_BPartner_ID(c_bpartner_id);
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    inv.setC_BPartner_Location_ID(param.getC_bpartner_location_id().intValue());
//                } else {
//                    // c_bpartner_id pasti > 0
//                    inv.setC_BPartner_Location_ID(
//                            WsUtil.getDefaultBPartnerLocationID(c_bpartner_id));
//                }
//
//                inv.setAD_Org_ID(param.getAd_org_id());
//                inv.setC_DocTypeTarget_ID(param.getC_doctypetarget_id());
//                inv.setM_PriceList_ID(param.getM_pricelist_id());
//                inv.setPaymentRule(param.getPaymentrule());
//                if(param.getC_currency_id() > 0) {
//                    inv.setC_Currency_ID(param.getC_currency_id());
//                } else {
//                    inv.setC_Currency_ID(WsUtil.getDefaultCountryID());
//                }
//                inv.setC_PaymentTerm_ID(param.getC_paymentterm_id());
//
//            } else {
//                // set
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    inv.setC_BPartner_Location_ID(param.getC_bpartner_location_id());
//                }
//            }
//
//            // set_insert
//            if (param.getDescription_header() != null) {
//                inv.setDescription(param.getDescription_header());
//            }
//
//            if (c_bpartner_id.intValue() > 0) {
//                inv.setC_BPartner_ID(c_bpartner_id);
//            }
//
//            if (param.getAd_user_id().intValue() > 0) {
//                inv.setAD_User_ID(param.getAd_user_id());
//            }
//
//            if (!Util.isEmpty(param.getPoreference())) {
//                inv.setPOReference(param.getPoreference());
//            }
//
//            if (param.getC_order_id().intValue() > 0) {
//                inv.setC_Order_ID(param.getC_order_id());
//            }
//            
//            inv.saveEx();
//
//            Integer line = 0;
//
//            for (ParamInvoicesLine pil : listLine) {
//                MInvoiceLine invoiceline = ForcaObject.getObject(Env.getCtx(), MInvoiceLine.Table_Name, 0, trxName);
//                invoiceline.setForcaAttribute(inv);
//                if (pil.getM_inoutline_id().intValue() > 0) {
//                    MInOutLine inoutline = ForcaObject.getObject(Env.getCtx(), MInOutLine.Table_Name,
//                            pil.getM_inoutline_id(), trxName);
//                    invoiceline.setM_InOutLine_ID(pil.getM_inoutline_id());
//                    if (pil.getM_product_id() != 0) {
//                        invoiceline.setM_Product_ID(pil.getM_product_id(), pil.getC_uom_id());
//                    } else {
//                        if (pil.getC_orderline_id() > 0) {
//                            MOrderLine orderline = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name,
//                                    pil.getC_orderline_id(), trxName);
//                            invoiceline.setM_Product_ID(inoutline.getM_Product_ID(),
//                                    orderline.getC_UOM_ID());
//                        } else {
//                            invoiceline.setM_Product_ID(inoutline.getM_Product_ID());
//                        }
//                    }
//
//                    if (pil.getC_charge_id() != 0) {
//                        invoiceline.setC_Charge_ID(pil.getC_charge_id());
//                    } else {
//                        invoiceline.setC_Charge_ID(inoutline.getC_Charge_ID());
//                    }
//
//                    invoiceline.setQty(inoutline.getQtyEntered());
//
//                    if (pil.getDescription_line() != null) {
//                        invoiceline.setDescription(pil.getDescription_line());
//                    } else {
//                        invoiceline.setDescription(inoutline.getDescription());
//                    }
//
//                    if (pil.getPriceentered() != null) {
//                        invoiceline.setPrice(pil.getPriceentered());
//                    } else {
//                        if (pil.getC_orderline_id() > 0) {
//                            MOrderLine orderline = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name,
//                                    pil.getC_orderline_id(), trxName);
//                            invoiceline.setPrice(orderline.getPriceEntered());
//                        }
//                    }
//
//                    invoiceline.setC_Tax_ID(pil.getC_tax_id());
//                    invoiceline
//                            .setM_AttributeSetInstance_ID(inoutline.getM_AttributeSetInstance_ID());
//                    invoiceline.setC_Project_ID(inoutline.getC_Project_ID());
//                    invoiceline.setC_ProjectPhase_ID(inoutline.getC_ProjectPhase_ID());
//                    invoiceline.setC_ProjectTask_ID(inoutline.getC_ProjectTask_ID());
//                    invoiceline.setC_Activity_ID(inoutline.getC_Activity_ID());
//                    invoiceline.setC_Campaign_ID(inoutline.getC_Campaign_ID());
//                    invoiceline.setAD_OrgTrx_ID(inoutline.getAD_OrgTrx_ID());
//                    invoiceline.setUser1_ID(inoutline.getUser1_ID());
//                    invoiceline.setUser2_ID(inoutline.getUser2_ID());
//                } else if (pil.getC_orderline_id().intValue() > 0) {
//                    MOrderLine orderline = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name,
//                            pil.getC_orderline_id(), trxName);
//                    invoiceline.setC_OrderLine_ID(pil.getC_orderline_id());
//                    if (pil.getM_product_id() != 0) {
//                        invoiceline.setM_Product_ID(pil.getM_product_id(), pil.getC_uom_id());
//                    } else {
//                        invoiceline.setM_Product_ID(orderline.getM_Product_ID(),
//                                orderline.getC_UOM_ID());
//                    }
//
//                    if (pil.getC_charge_id() != 0) {
//                        invoiceline.setC_Charge_ID(pil.getC_charge_id());
//                    } else {
//                        invoiceline.setC_Charge_ID(orderline.getC_Charge_ID());
//                    }
//
//                    if (pil.getQty().intValue() > 0) {
//                        invoiceline.setQty(pil.getQty());
//                    } else {
//                        invoiceline.setQty(orderline.getQtyEntered());
//                    }
//
//                    if (pil.getDescription_line() != null) {
//                        invoiceline.setDescription(pil.getDescription_line());
//                    } else {
//                        invoiceline.setDescription(orderline.getDescription());
//                    }
//
//                    if (pil.getPriceentered() != null) {
//                        invoiceline.setPrice(pil.getPriceentered());
//                    }
//
//                    invoiceline.setC_Tax_ID(pil.getC_tax_id());
//                    invoiceline
//                            .setM_AttributeSetInstance_ID(orderline.getM_AttributeSetInstance_ID());
//                    invoiceline.setC_Project_ID(orderline.getC_Project_ID());
//                    invoiceline.setC_ProjectPhase_ID(orderline.getC_ProjectPhase_ID());
//                    invoiceline.setC_ProjectTask_ID(orderline.getC_ProjectTask_ID());
//                    invoiceline.setC_Activity_ID(orderline.getC_Activity_ID());
//                    invoiceline.setC_Campaign_ID(orderline.getC_Campaign_ID());
//                    invoiceline.setAD_OrgTrx_ID(orderline.getAD_OrgTrx_ID());
//                    invoiceline.setUser1_ID(orderline.getUser1_ID());
//                    invoiceline.setUser2_ID(orderline.getUser2_ID());
//
//                    if (pil.getQty().intValue() > (orderline.getQtyInvoiced().intValue()))
//                        throw new Exception(
//                                "Quantity of inout cannot exceed the quantity of order");
//                } else {
//                    invoiceline.setC_OrderLine_ID(pil.getC_orderline_id());
//                    invoiceline.setC_Charge_ID(pil.getC_charge_id());
//                    invoiceline.setQty(pil.getQty());
//                    if (pil.getDescription_line() != null) {
//                        invoiceline.setDescription(pil.getDescription_line());
//                    }
//                    invoiceline.setPrice(pil.getPriceentered());
//                    invoiceline.setC_Tax_ID(pil.getC_tax_id());
//                    if (pil.getC_uom_id() > 0) {
//                        invoiceline.setC_UOM_ID(pil.getC_uom_id());
//                    }
//                    if (pil.getC_charge_id() > 0) {
//                        invoiceline.setC_Charge_ID(pil.getC_charge_id());
//                    }
//                }
//                invoiceline.setLine((line + 1) * 10);
//                if (pil.getCost_center() > 0 ) {
//                    invoiceline.setUser1_ID(pil.getCost_center());
//                }
//                invoiceline.saveEx();
//
//                if (trxName == null)
//                    objData.add(invoiceline);
//            }
//
//            resp.setCodestatus("S");
//            if (isInsert)
//                resp.setMessage("Insert succeeded");
//            else
//                resp.setMessage("Update succeeded");
//
//            Map<String, Object> xmap = new LinkedHashMap<>();
//            xmap.put("c_invoice_id", inv.get_ID());
//            xmap.put("documentno", inv.getDocumentNo());
//            resp.setResultdata(xmap);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setInvoiceLine(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        MInvoice invoice = null;
//        MInvoiceLine invoiceline = null;
//        try {
//            boolean isInsert = false;
//
//            Integer c_invoice_id = param.getC_invoice_id();
//            if (param.getC_invoiceline_id() == 0) {
//                invoiceline =
//                        ForcaObject.getObject(Env.getCtx(), MInvoiceLine.Table_Name, 0, trxName);
//                isInsert = true;
//            } else {
//                invoiceline = ForcaObject.getObject(Env.getCtx(), MInvoiceLine.Table_Name,
//                        param.getC_invoiceline_id().intValue(), trxName);
//                if (invoiceline == null) {
//                    invoiceline = ForcaObject.getObject(Env.getCtx(), MInvoiceLine.Table_Name, 0,
//                            trxName);
//                    isInsert = true;
//                }
//            }
//
//            // locator, qty, product, uom, orderline
//            if (isInsert) {
//                if (param.getC_invoice_id() != null) {
//                    invoice = ForcaObject.getObject(Env.getCtx(), MInvoice.Table_Name,
//                            param.getC_invoice_id(), trxName);
//                    invoiceline.setC_Invoice_ID(c_invoice_id);
//                } else if (param.getC_invoice_id() == null) {
//                    ResponseData.parameterRequired();
//                }
//            }
//
//
//            if (param.getAd_org_id() != null)
//                invoiceline.setAD_Org_ID(invoice.getAD_Org_ID());
//            else
//                invoiceline.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//            if (param.getQty() != null)
//                invoiceline.setQty(param.getQty());
//            if (param.getM_product_id() != null)
//                invoiceline.setM_Product_ID(param.getM_product_id());
//            if (param.getC_uom_id() != null)
//                invoiceline.setC_UOM_ID(param.getC_uom_id());
//            if (param.getC_orderline_id() != null)
//                invoiceline.setC_OrderLine_ID(param.getC_orderline_id());
//            if (param.getM_inoutline_id() != null)
//                invoiceline.setM_InOutLine_ID(param.getM_inoutline_id());
//            if (param.getPriceentered() != null)
//                invoiceline.setPrice(param.getPriceentered());
//            if (param.getDescription() != null)
//                invoiceline.setDescription(param.getDescription());
//
//            invoiceline.saveEx();
//
//            Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//            xmap.put("c_invoiceline_id", Integer.valueOf(invoiceline.getC_InvoiceLine_ID()));
//            xmap.put("line_number", invoiceline.getLine());
//            listdata.add(xmap);
//
//            if (isInsert)
//                resp = ResponseData.successResponse("Insert data invoice lines succeeded",
//                        listdata);
//            else
//                resp = ResponseData.successResponse("Update data invoice lines succeeded",
//                        listdata);
//
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setInvoiceDoc(ParamRequest param, String action, Trx trx) throws Exception {
//        if (param.getC_invoice_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        String trxName = trx.getTrxName();
//        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//
//        MInvoice invoice = null;
//        try {
//            invoice = ForcaObject.getObject(Env.getCtx(), MInvoice.Table_Name,
//                    param.getC_invoice_id(), trxName);
//            if (action.equals("CO")) {
//                MWorkflow.runDocumentActionWorkflow(invoice, DocAction.ACTION_Complete);
//                invoice = ForcaObject.getObject(Env.getCtx(), MInvoice.Table_Name,
//                        invoice.get_ID(), trxName);
////                if (invoice.getDocStatus().equals(DocAction.STATUS_Completed)) {
////                    xmap.put("docstatus", invoice.getDocStatus());
////                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), invoice.getDocStatus()));
////                    xmap.put("docaction", invoice.getDocAction());
////                    xmap.put("c_invoice_id", invoice.get_ID());
////                    xmap.put("documentno", invoice.getDocumentNo());
////                    listdata.add(xmap);
////                    resp.setCodestatus("S");
////                    resp.setMessage("Success Complete Invoice");
////                    resp.setResultdata(listdata);
////                } else {
////                    resp.setMessage("Failed Complete Invoice");
////                }
//                xmap.put("docstatus", invoice.getDocStatus());
//                xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), invoice.getDocStatus()));
//                xmap.put("docaction", invoice.getDocAction());
//                xmap.put("c_invoice_id", invoice.get_ID());
//                xmap.put("documentno", invoice.getDocumentNo());
//                listdata.add(xmap);
//                resp.setCodestatus("S");
//                resp.setMessage("Success Complete Invoice");
//                resp.setResultdata(listdata);
//            } else if (action.equals("VO")) {
//                MWorkflow.runDocumentActionWorkflow(invoice, DocAction.ACTION_Void);
//                invoice.setDocStatus(DocAction.ACTION_Void);
//                if (invoice.getDocStatus().equals(DocAction.STATUS_Voided)) {
//                    xmap.put("docstatus", invoice.getDocStatus());
//                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), invoice.getDocStatus()));
//                    xmap.put("docaction", invoice.getDocAction());
//                    xmap.put("c_invoice_id", invoice.get_ID());
//                    xmap.put("documentno", invoice.getDocumentNo());
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success Void Invoice");
//                    resp.setResultdata(listdata);
//                } else {
//                    resp.setMessage("Failed Void Invoice");
//                }
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            if (e.getMessage() == null) {
//                resp.setMessage(invoice.get_Logger().toString());
//            } else {
//                resp = ResponseData.errorResponse(e.getMessage());
//            }
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setPayment(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        MPayment payment = null;
//        try {
//            boolean isInsert = false;
//
//            if (param.getC_payment_id() == 0) {
//                payment = ForcaObject.getObject(Env.getCtx(), MPayment.Table_Name, 0, trxName);
//                isInsert = true;
//            } else {
//                payment = ForcaObject.getObject(Env.getCtx(), MPayment.Table_Name,
//                        param.getC_payment_id().intValue(), trxName);
//                if (payment == null) {
//                    payment = ForcaObject.getObject(Env.getCtx(), MPayment.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            }
//
//            if (param.getAd_org_id() != null)
//                payment.setAD_Org_ID(param.getAd_org_id());
//            else
//                payment.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//            if (param.getC_bankaccount_id() != null)
//                payment.setC_BankAccount_ID(param.getC_bankaccount_id());
//            if (param.getC_doctype_id() != null)
//                payment.setC_DocType_ID(param.getC_doctype_id());
//            if (param.getC_bpartner_id() != null)
//                payment.setC_BPartner_ID(param.getC_bpartner_id());
//            if (param.getC_invoice_id() != null)
//                payment.setC_Invoice_ID(param.getC_invoice_id());
//            if (param.getDescription() != null)
//                payment.setDescription(param.getDescription());
//
//            if (param.getDateacct() != null)
//                payment.setDateAcct(WsUtil.convertFormatTgl(param.getDateacct()));
//            else
//                payment.setDateAcct(new Timestamp(new Date().getTime()));
//
//            if (param.getDatetrx() != null)
//                payment.setDateTrx(WsUtil.convertFormatTgl(param.getDateacct()));
//            else
//                payment.setDateTrx(new Timestamp(new Date().getTime()));
//
//            payment.setC_Currency_ID(WsUtil.getDefaultCountryID());
//
//            payment.saveEx();
//
//            Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//            xmap.put("c_payment_id", payment.get_ID());
//            xmap.put("documentno", payment.getDocumentNo());
//            listdata.add(xmap);
//
//            if (isInsert)
//                resp = ResponseData.successResponse("Insert data payment succeeded", listdata);
//            else
//                resp = ResponseData.successResponse("Update data payment succeeded", listdata);
//
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setPaymentAllocate(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        MPaymentAllocate payment = null;
//        MPayment py = null;
//        try {
//            boolean isInsert = false;
//
//            if (param.getC_paymentallocate_id() == 0) {
//                payment = ForcaObject.getObject(Env.getCtx(), MPaymentAllocate.Table_Name, 0,
//                        trxName);
//                isInsert = true;
//            } else {
//                payment = ForcaObject.getObject(Env.getCtx(), MPaymentAllocate.Table_Name,
//                        param.getC_paymentallocate_id().intValue(), trxName);
//                if (payment == null) {
//                    payment = ForcaObject.getObject(Env.getCtx(), MPaymentAllocate.Table_Name, 0,
//                            trxName);
//                    isInsert = true;
//                }
//            }
//
//            if (param.getAd_org_id() != null)
//                payment.setAD_Org_ID(param.getAd_org_id());
//            else
//                payment.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//            if (param.getC_payment_id() != null) {
//                payment.setC_Payment_ID(param.getC_payment_id());
//                py = ForcaObject.getObject(Env.getCtx(), MPayment.Table_Name,
//                        param.getC_payment_id(), trxName);
//            }
//            if (param.getC_invoice_id() != null) {
//                MInvoice inv = ForcaObject.getObject(Env.getCtx(), MInvoice.Table_Name,
//                        param.getC_invoice_id(), trxName);
//                payment.setC_Invoice_ID(param.getC_invoice_id());
//                payment.setAmount(inv.getGrandTotal());
//                payment.setInvoiceAmt(inv.getGrandTotal());
//                py.setPayAmt(inv.getGrandTotal());
//            }
//
//            payment.saveEx();
//            py.saveEx();
//
//            Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//            xmap.put("c_paymentallocate_id", payment.get_ID());
//            xmap.put("documentno_payment", py.getDocumentNo());
//            listdata.add(xmap);
//
//            if (isInsert)
//                resp = ResponseData.successResponse("Insert data payment allocate succeeded",
//                        listdata);
//            else
//                resp = ResponseData.successResponse("Update data payment allocate succeeded",
//                        listdata);
//
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setPaymentDoc(ParamRequest param, String action, Trx trx) throws Exception {
//        if (param.getC_payment_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        String trxName = trx.getTrxName();
//        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//
//        MPayment payment = null;
//        try {
//            payment = ForcaObject.getObject(Env.getCtx(), MPayment.Table_Name,
//                    param.getC_payment_id(), trxName);
//            if (action.equals("CO")) {
//                payment.processIt(DocAction.ACTION_Complete);
//                if (payment.getDocStatus().equals(DocAction.STATUS_Completed)) {
//                    xmap.put("docstatus", payment.getDocStatus());
//                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), payment.getDocStatus()));
//                    xmap.put("docaction", payment.getDocAction());
//                    xmap.put("c_payment_id", payment.get_ID());
//                    xmap.put("documentno", payment.getDocumentNo());
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success Complete Payment");
//                    resp.setResultdata(listdata);
//                } else {
//                    resp.setMessage("Failed Complete Payment");
//                }
//            } else if (action.equals("VO")) {
//                MWorkflow.runDocumentActionWorkflow(payment, DocAction.ACTION_Void);
//                payment.setDocStatus(DocAction.ACTION_Void);
//                if (payment.getDocStatus().equals(DocAction.STATUS_Voided)) {
//                    xmap.put("docstatus", payment.getDocStatus());
//                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), payment.getDocStatus()));
//                    xmap.put("docaction", payment.getDocAction());
//                    xmap.put("c_payment_id", payment.get_ID());
//                    xmap.put("documentno", payment.getDocumentNo());
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success Void Payment");
//                    resp.setResultdata(listdata);
//                } else {
//                    resp.setMessage("Failed Void Payment");
//                }
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            if (e.getMessage() == null) {
//                resp.setMessage(payment.get_Logger().toString());
//            } else {
//                resp = ResponseData.errorResponse(e.getMessage());
//            }
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData insertAttendance(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        String trxName = trx.getTrxName();
//        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//
//        try {
//            MFORCAAttendance attendance =
//                    ForcaObject.getObject(Env.getCtx(), MFORCAAttendance.Table_Name, 0, trxName);
//            attendance.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//            attendance.setClient(Env.getAD_Client_ID(Env.getCtx()));
//            attendance.setValue(param.getNik());
//            attendance.setName(param.getName());
//            attendance.setStatus(param.getStatus());
//            attendance.setImageURL(param.getImageurl());
//            attendance.setLatitude(param.getLatitude());
//            attendance.setLongitude(param.getLongitude());
//            attendance.setFORCA_Distance(param.getForca_distance());
//            attendance.setForca_Branch_ID(param.getForca_branch_id());
//            attendance.setForca_DeviceID(param.getForca_deviceid());
//            attendance.setForca_DeviceModel(param.getForca_devicemodel());
//            //#20588
//            attendance.setForca_Location(param.getForca_location());
//            attendance.setDescription(param.getDescription());
//            attendance.saveEx();
//            attendance.setDateDoc(attendance.getCreated());
//            attendance.saveEx();
//
//            xmap.put("forca_attendance_id", attendance.get_ID());
//            listdata.add(xmap);
//            resp = ResponseData.successResponse("Insert data Attendance succeeded", listdata);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData insertPersonID(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        String trxName = trx.getTrxName();
//        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//
//        try {
//            MUser user = ForcaObject.getObject(Env.getCtx(), MUser.Table_Name,
//                    param.getAd_user_id(), trxName);
//            I_AD_User users = POWrapper.create(user, I_AD_User.class);
//            users.setFORCA_Person_ID(param.getForca_person_id());
//            user.saveEx();
//
//            xmap.put("ad_user_id", user.get_ID());
//            listdata.add(xmap);
//            resp = ResponseData.successResponse("Insert data Person ID succeeded", listdata);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//    
//    public ResponseData insertDeviceID(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        String trxName = trx.getTrxName();
//        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//
//        try {
//            MUser user = ForcaObject.getObject(Env.getCtx(), MUser.Table_Name,
//                    param.getAd_user_id(), trxName);
//            I_AD_User users = POWrapper.create(user, I_AD_User.class);
//            users.setForca_DeviceID(param.getForca_deviceid());
//            user.saveEx();
//
//            xmap.put("ad_user_id", user.get_ID());
//            listdata.add(xmap);
//            resp = ResponseData.successResponse("Insert data Device ID succeeded", listdata);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//    
//    public ResponseData insertLeave(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        String trxName = trx.getTrxName();
//        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//
//        try {
//            MFORCALeave leave = ForcaObject.getObject(Env.getCtx(), MFORCALeave.Table_Name, 0, trxName);
//            leave.setAD_User_ID(param.getAd_user_id());
//            leave.setForca_LeaveType_ID(param.getForca_leavetype_id());
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            Date parsedDate = dateFormat.parse(param.getDatefrom());
//            Timestamp dateFrom = new java.sql.Timestamp(parsedDate.getTime());
//            leave.setDateFrom(dateFrom);
//            parsedDate = dateFormat.parse(param.getDateto());
//            Timestamp dateTo = new java.sql.Timestamp(parsedDate.getTime());
//            leave.setDateTo(dateTo);
//            leave.setFORCA_TotalDay(param.getForca_totalday());
//            leave.setDescription(param.getDescription());
//            leave.saveEx();
//            MWorkflow.runDocumentActionWorkflow(leave, DocAction.ACTION_Complete);
//            leave.saveEx();
//            xmap.put(leave.COLUMNNAME_Forca_Leave_ID, leave.get_ID());
//            listdata.add(xmap);
//            resp = ResponseData.successResponse("Insert data Leave succeeded", listdata);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
}
