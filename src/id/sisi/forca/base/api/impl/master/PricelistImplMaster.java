/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Fitriani Rohmah Hardiyanti                                                                                
   Email          : fitrianirohmahhardiyanti@gmail.com 
   Subject        : Pricelist Impl Master                              
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.impl.master;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.ProcessUtil;

public class PricelistImplMaster {
    private CLogger log = CLogger.getCLogger(getClass());
    
    public Object getPriceListVersionByPricelistID(Integer m_pricelist_id) {
        List<Object> params = new ArrayList<>();
        ResponseData resp = new ResponseData();
        StringBuilder sql = new StringBuilder();
        List<Object> listPricelistVersion = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        //@formatter:off
        sql.append(
                "SELECT "
                    + "    m_pricelist_version_id, "
                    + "    name, "
                    + "    description, "
                    + "    validfrom "
                    + "FROM "
                    + "    m_pricelist_version "
                    + "WHERE m_pricelist_id = ? ");
        //@formatter:on

        params.add(m_pricelist_id);

        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map2 = new LinkedHashMap<String, Object>();
                map2.put("m_pricelist_version_id", rs.getString("m_pricelist_version_id"));
                map2.put("name", rs.getString("name"));
                map2.put("description", rs.getString("description"));
                map2.put("validfrom", rs.getString("validfrom"));
                listPricelistVersion.add(map2);
            }

            resp = ResponseData.successResponse(listPricelistVersion.size() + " Data Found",
                    listPricelistVersion);
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql.toString(), e);
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } finally {
            DB.close(rs, ps);
            ps = null;
            rs = null;
        }
        return listPricelistVersion;
    }
}
