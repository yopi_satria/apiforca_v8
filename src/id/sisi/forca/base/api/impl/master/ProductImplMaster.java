/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Fitriani Rohmah Hardiyanti                                                                                
   Email          : fitrianirohmahhardiyanti@gmail.com 
   Subject        : Product Impl Master                              
  ---------------------------------------------------------------------------*/
package id.sisi.forca.base.api.impl.master;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.compiere.db.Database;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.Constants;
import id.sisi.forca.base.api.util.ProcessUtil;

public class ProductImplMaster {
    private static final String NATIVE_MARKER = "NATIVE_" + Database.DB_POSTGRESQL + "_KEYWORK";
    private CLogger log = CLogger.getCLogger(getClass());
    
    public Object getPriceListByProductID(Integer m_product_id, Date datefrom, Integer c_uom_to_id,
            Integer m_pricelist_id) {
        List<Object> params = new ArrayList<>();
        ResponseData resp = new ResponseData();
        StringBuilder sql = new StringBuilder();
        StringBuilder whereCond = new StringBuilder();
        StringBuilder select = new StringBuilder();
        StringBuilder join = new StringBuilder();
        StringBuilder whereCond2 = new StringBuilder();
        StringBuilder select2 = new StringBuilder();
        StringBuilder join2 = new StringBuilder();
        StringBuilder order = new StringBuilder();
        List<Object> listPricelist = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        //@formatter:off
        select.append(
                "SELECT DISTINCT * FROM (SELECT "
                        + "    DISTINCT b.validfrom, "
                        + "    c.m_pricelist_id, "
                        + "    c.name pricelist_name, "
                        + "    c.c_currency_id, "
                        + "    d.iso_code, "
                        + "    a.pricelist, "
                        + "    prd.c_uom_id, "
                        + "    CASE WHEN (uc.dividerate NOTNULL) "
                        + "         THEN a.pricestd*uc.dividerate "
                        + "         ELSE a.pricestd "
                        + "    END AS standard_price, "
                        + "    a.pricelimit, "
                        + "    c.issopricelist, "
                        + "    d.cursymbol "
//                        + "    uc.c_uom_to_id "  
                        );
        join.append(
                "FROM "
                        + "    m_productprice a "
                        + "LEFT JOIN m_pricelist_version b ON "
                        + "    b.m_pricelist_version_id = a.m_pricelist_version_id "
                        + "LEFT JOIN m_pricelist c ON "
                        + "    c.m_pricelist_id = b.m_pricelist_id "
                        + "LEFT JOIN c_currency d ON "
                        + "    d.c_currency_id = c.c_currency_id "
                        + "LEFT JOIN m_product prd ON "
                        + "    prd.m_product_id = a.m_product_id "
                        + "LEFT JOIN c_uom_conversion uc ON "
                        + "    prd.m_product_id = uc.m_product_id "
                        + "WHERE "
                        + "    a.m_productprice_id IS NOT NULL "
                        );
        
        if(m_product_id != null) {
            whereCond.append(
                            " AND a.m_product_id = ? ");
            params.add(m_product_id);
        }
        
        if(c_uom_to_id != null) {
            whereCond.append("AND uc.c_uom_to_id = ? ");
            params.add(c_uom_to_id);
        }
        if(m_pricelist_id != null) {
            whereCond.append("AND c.m_pricelist_id = ? ");
            params.add(m_pricelist_id);
        }
        
        if(datefrom != null) {
            whereCond.append(
                    " AND to_char( trunc( b.validfrom ), " + "'YYYY-MM-DD' ) <= ? ");
            params.add(Constants.sdf.format(datefrom));
        }
        
        select2.append(
                "UNION SELECT "
                        + "    DISTINCT b.validfrom, "
                        + "    c.m_pricelist_id, "
                        + "    c.name pricelist_name, "
                        + "    c.c_currency_id, "
                        + "    d.iso_code, "
                        + "    a.pricelist, "
                        + "    prd.c_uom_id, "
                        + "    a.pricestd AS standard_price, "
                        + "    a.pricelimit, "
                        + "    c.issopricelist, "
                        + "    d.cursymbol "
//                        + "    prd.c_uom_id AS c_uom_to_id "  
                        );
        join2.append(
                "FROM "
                        + "    m_productprice a "
                        + "LEFT JOIN m_pricelist_version b ON "
                        + "    b.m_pricelist_version_id = a.m_pricelist_version_id "
                        + "LEFT JOIN m_pricelist c ON "
                        + "    c.m_pricelist_id = b.m_pricelist_id "
                        + "LEFT JOIN c_currency d ON "
                        + "    d.c_currency_id = c.c_currency_id "
                        + "LEFT JOIN m_product prd ON "
                        + "    prd.m_product_id = a.m_product_id "
                        + "WHERE "
                        + "    a.m_productprice_id IS NOT NULL "
                        );
        
        if(m_product_id != null) {
            whereCond2.append(
                            " AND a.m_product_id = ? ");
            params.add(m_product_id);
        }
        
        if(c_uom_to_id != null) {
            whereCond2.append("AND prd.c_uom_id = ? ");
            params.add(c_uom_to_id);
        }
        if(m_pricelist_id != null) {
            whereCond2.append("AND c.m_pricelist_id = ? ");
            params.add(m_pricelist_id);
        }
        
        if(datefrom != null) {
            whereCond2.append(
                    " AND to_char( trunc( b.validfrom ), " + "'YYYY-MM-DD' ) <= ? ");
            params.add(Constants.sdf.format(datefrom));
            order.append(
                    ") a "
                    + "ORDER BY validfrom DESC "
                    + NATIVE_MARKER + "LIMIT 1" + NATIVE_MARKER
                    );
        } else {
            order.append(
                ") a "
                + " ORDER BY validfrom DESC ");
        }
        
        
        //@formatter:on

        sql.append(select.toString());
        sql.append(join.toString());
        sql.append(whereCond.toString());
        sql.append(select2.toString());
        sql.append(join2.toString());
        sql.append(whereCond2.toString());
        sql.append(order.toString());

        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map2 = new LinkedHashMap<String, Object>();
                map2.put("m_pricelist_id", rs.getString("m_pricelist_id"));
                map2.put("pricelist_name", rs.getString("pricelist_name"));
                map2.put("c_currency_id", rs.getString("c_currency_id"));
                map2.put("iso_code", rs.getString("iso_code"));
                map2.put("standard_price", rs.getString("standard_price"));
                map2.put("pricelist", rs.getString("pricelist"));
                map2.put("pricelimit", rs.getString("pricelimit"));
                map2.put("issopricelist", rs.getString("issopricelist"));
                map2.put("validfrom", rs.getString("validfrom"));
                listPricelist.add(map2);
            }

            resp = ResponseData.successResponse(listPricelist.size() + " Data Found",
                    listPricelist);
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql.toString(), e);
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } finally {
            DB.close(rs, ps);
            ps = null;
            rs = null;
        }
        return listPricelist;
    }
    
    public Object getUOMByProductID(Integer m_product_id) {
        List<Object> params = new ArrayList<>();
        ResponseData resp = new ResponseData();
        StringBuilder sql = new StringBuilder();
        StringBuilder whereCond = new StringBuilder();
        StringBuilder select = new StringBuilder();
        StringBuilder join = new StringBuilder();
        List<Object> listUom = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        //@formatter:off
        select.append(
                " select " + 
                        "    a.c_uom_id, " + 
                        "    a.name as uom_name " 
                        );
        join.append(
                "from c_uom a "
                + " left join m_product b on a.c_uom_id = b.c_uom_id " + 
                        " where a.c_uom_id is not null "
                        );
        whereCond.append(
                " and b.m_product_id = ? ");
        //@formatter:on
        params.add(m_product_id);

        sql.append(select.toString());
        sql.append(join.toString());
        sql.append(whereCond.toString());

        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map2 = new LinkedHashMap<String, Object>();
                map2.put("c_uom_id", rs.getString("c_uom_id"));
                map2.put("uom_name", rs.getString("uom_name"));
                map2.put("multiplyrate", 1);
                map2.put("dividerate", 1);
                listUom.add(map2);
            }

            resp = ResponseData.successResponse(listUom.size() + " Data Found", listUom);
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql.toString(), e);
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } finally {
            DB.close(rs, ps);
            ps = null;
            rs = null;
        }
        return listUom;
    }
    
    public Object getUOMConversionByProductID(Integer m_product_id) {
        List<Object> params = new ArrayList<>();
        ResponseData resp = new ResponseData();
        StringBuilder sql = new StringBuilder();
        StringBuilder whereCond = new StringBuilder();
        StringBuilder select = new StringBuilder();
        StringBuilder join = new StringBuilder();
        List<Object> listUom = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        //@formatter:off
        select.append(
                " select " + 
                        "    c.c_uom_id, " + 
                        "    c.name as uom_name, "
                        + "  a.multiplyrate, "
                        + "  a.dividerate " 
                        );
        join.append(
                "from c_uom_conversion a "
                + " left join m_product b on a.m_product_id = b.m_product_id "
                + " left join c_uom c on c.c_uom_id = a.c_uom_to_id " + 
                        " where a.c_uom_conversion_id is not null "
                        );
        whereCond.append(
                " and b.m_product_id = ? ");
        //@formatter:on
        params.add(m_product_id);

        sql.append(select.toString());
        sql.append(join.toString());
        sql.append(whereCond.toString());

        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map2 = new LinkedHashMap<String, Object>();
                map2.put("c_uom_id", rs.getString("c_uom_id"));
                map2.put("uom_name", rs.getString("uom_name"));
                map2.put("multiplyrate", rs.getString("multiplyrate"));
                map2.put("dividerate", rs.getString("dividerate"));
                listUom.add(map2);
            }

            resp = ResponseData.successResponse(listUom.size() + " Data Found", listUom);
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql.toString(), e);
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } finally {
            DB.close(rs, ps);
            ps = null;
            rs = null;
        }
        return listUom;
    }
    
    public Object getPriceListVersionByProductID(Integer m_product_id) {
        List<Object> params = new ArrayList<>();
        ResponseData resp = new ResponseData();
        StringBuilder sql = new StringBuilder();
        List<Object> listPricelistVersion = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        //@formatter:off
        sql.append(
                "SELECT "
                    + "    plv.m_pricelist_version_id, "
                    + "    plv.name, "
                    + "    plv.description, "
                    + "    plv.validfrom "
                    + "FROM "
                    + "    m_pricelist_version plv "
                    + "LEFT JOIN m_pricelist pl ON "
                    + "    plv.m_pricelist_id = pl.m_pricelist_id "
                    + "LEFT JOIN m_productprice pp ON "
                    + "    pp.m_pricelist_version_id = plv.m_pricelist_version_id "
                    + "LEFT JOIN m_product prd ON "
                    + "    prd.m_product_id = pp.m_product_id "
                    + "WHERE prd.m_product_id = ? "
                    + "ORDER BY "
                    + "    prd.m_product_id, "
                    + "    plv.m_pricelist_version_id");
        //@formatter:on

        params.add(m_product_id);

        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map2 = new LinkedHashMap<String, Object>();
                map2.put("m_pricelist_version_id", rs.getString("m_pricelist_version_id"));
                map2.put("name", rs.getString("name"));
                map2.put("description", rs.getString("description"));
                map2.put("validfrom", rs.getString("validfrom"));
                listPricelistVersion.add(map2);
            }

            resp = ResponseData.successResponse(listPricelistVersion.size() + " Data Found",
                    listPricelistVersion);
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql.toString(), e);
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } finally {
            DB.close(rs, ps);
            ps = null;
            rs = null;
        }
        return listPricelistVersion;
    }
}
