/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Fitriani Rohmah Hardiyanti                                                                                
   Email          : fitrianirohmahhardiyanti@gmail.com 
   Subject        : User Impl Master                              
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.impl.master;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.ProcessUtil;

public class UserImplMaster {
    private CLogger log = CLogger.getCLogger(getClass());
    
    public Object getRoleByUserID(Integer ad_user_id) {
        List<Object> params = new ArrayList<>();
        ResponseData resp = new ResponseData();
        StringBuilder sql = new StringBuilder();
        StringBuilder whereCond = new StringBuilder();
        StringBuilder select = new StringBuilder();
        StringBuilder join = new StringBuilder();
        List<Object> listRole = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        //@formatter:off
        select.append(
                " select " + 
                        "    a.ad_role_id, " + 
                        "    a.name as role_name " 
                        );
        join.append(
                "from ad_role a "
                + " left join ad_user b on a.ad_client_id = b.ad_client_id " + 
                        " where a.ad_role_id is not null "
                        );
        whereCond.append(
                " and b.ad_user_id = ? ");
        //@formatter:on
        params.add(ad_user_id);

        sql.append(select.toString());
        sql.append(join.toString());
        sql.append(whereCond.toString());

        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map2 = new LinkedHashMap<String, Object>();
                map2.put("ad_role_id", rs.getString("ad_role_id"));
                map2.put("role_name", rs.getString("role_name"));
                listRole.add(map2);
            }

            resp = ResponseData.successResponse(listRole.size() + " Data Found", listRole);
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql.toString(), e);
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } finally {
            DB.close(rs, ps);
            ps = null;
            rs = null;
        }
        return listRole;
    }
}
