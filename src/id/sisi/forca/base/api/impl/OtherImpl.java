/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : Other                              
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.impl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAttachment;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.Util;
//import org.idempiere.model.ForcaObject;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import id.sisi.forca.base.api.impl.other.InOutImplOther;
import id.sisi.forca.base.api.impl.other.OrderImplOther;
import id.sisi.forca.base.api.model.MFORCACustomerPOS;
import id.sisi.forca.base.api.model.MFORCAPOSOrderRequest;
import id.sisi.forca.base.api.model.MFORCAPOSPayment;
import id.sisi.forca.base.api.request.ParamInOutLineOther;
import id.sisi.forca.base.api.request.ParamInOutOther;
import id.sisi.forca.base.api.request.ParamOrderLineOther;
import id.sisi.forca.base.api.request.ParamOrderOther;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.Constants;
import id.sisi.forca.base.api.util.ProcessUtil;
import id.sisi.forca.base.api.util.WebserviceUtil;
import id.sisi.forca.base.api.util.WsUtil;

public class OtherImpl {
    private CLogger log = CLogger.getCLogger(getClass());
    private OrderImplOther Order = null;
    private InOutImplOther InOut = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

//    public ResponseData getTempOrder(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        if (param.getRef_order_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        //@formatter:off
//        sql.append(""
//                + " SELECT "
//                + "     Ref_POS_Order_ID, "
//                + "     Retail_ID, "
//                + "     C_Order_ID "
//                + " FROM "
//                + "     FORCA_POS_Order_Request "
//                + " WHERE "
//                + "     Ref_POS_Order_ID = ? "
//                + "     AND AD_Client_ID = ? ");
//        //@formatter:on
//        params.add(param.getRef_order_id());
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//            List<Object> listdata = new ArrayList<Object>();
//
//            if (rs.next()) {
//                resp.setCodestatus("S");
//                resp.setMessage(Constants.SUCCEED_DataFound);
//                do {
//                    Map<String, Object> map = new LinkedHashMap<String, Object>();
//                    map.put("ref_order_id",
//                            rs.getString(MFORCAPOSOrderRequest.COLUMNNAME_Ref_POS_Order_ID));
//                    map.put("retail_id", rs.getString(MFORCAPOSOrderRequest.COLUMNNAME_Retail_ID));
//                    map.put("c_order_id",
//                            rs.getString(MFORCAPOSOrderRequest.COLUMNNAME_C_Order_ID));
//                    listdata.add(map);
//                } while (rs.next());
//                resp.setResultdata(listdata);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            rs = null;
//            ps = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getTempPayment(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        if (param.getRef_payment_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        //@formatter:off
//        sql.append(""
//                + " SELECT "
//                + "     Ref_POS_Payment_ID, "
//                + "     C_Payment_ID "
//                + " FROM "
//                + "     FORCA_POS_Payment "
//                + " WHERE "
//                + "     Ref_POS_Payment_ID = ? "
//                + "     AND AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(param.getRef_payment_id());
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//            List<Object> listdata = new ArrayList<Object>();
//
//            if (rs.next()) {
//                resp.setCodestatus("S");
//                resp.setMessage(Constants.SUCCEED_DataFound);
//                do {
//                    Map<String, Object> map = new LinkedHashMap<String, Object>();
//                    map.put("ref_payment_id",
//                            rs.getString(MFORCAPOSPayment.COLUMNNAME_Ref_POS_Payment_ID));
//                    map.put("c_payment_id", rs.getString(MFORCAPOSPayment.COLUMNNAME_C_Payment_ID));
//                    listdata.add(map);
//                } while (rs.next());
//                resp.setResultdata(listdata);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            rs = null;
//            ps = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData insertTempOrder(ParamRequest param, Trx trx) throws Exception {
//        Integer c_bpartner_id = param.getC_bpartner_id();
//        String multiple_m_product_id = param.getMultiple_m_product_id(),
//                multiple_qty = param.getMultiple_qty(), pos_order_id = param.getRef_order_id(),
//                retail_id = param.getRetail_id();
//        Timestamp date_order = WebserviceUtil.stringToTimeStamp(param.getDateordered());
//
//        if (c_bpartner_id == null || multiple_m_product_id == null || multiple_qty == null
//                || pos_order_id == null || param.getDateordered() == null || retail_id == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        Map<String, Object> map = new LinkedHashMap<>();
//        StringBuilder sql = new StringBuilder();
//
//        try {
//            // @formatter:off
//            sql.append(""
//                    + "SELECT "
//                    + "    FORCA_POS_Order_Request_ID "
//                    + "FROM "
//                    + "    FORCA_POS_Order_Request "
//                    + "WHERE "
//                    + "    IsActive = 'Y' "
//                    + "    AND ref_pos_order_id =?"
//                    + "    AND retail_id =?"
//                    + "    AND ad_client_id = ? ");
//            // @formatter:on
//            int FORCA_POS_Order_Request_ID = DB.getSQLValueEx(trxName, sql.toString(), pos_order_id,
//                    retail_id, Env.getAD_Client_ID(Env.getCtx()));
//
//            if (FORCA_POS_Order_Request_ID != -1) {
//                map.put("ref_order_request_id", FORCA_POS_Order_Request_ID);
//                resp = ResponseData.errorResponse(Constants.ERROR_DataIsAtSystem, map);
//            } else {
//                MFORCAPOSOrderRequest order = ForcaObject.getObject(Env.getCtx(),
//                        MFORCAPOSOrderRequest.Table_Name, 0, trxName);
//                order.setMultiple_Qty(multiple_qty);
//                order.setC_BPartner_ID(c_bpartner_id);
//                order.setIsSOTrx(true);
//                order.setMultiple_M_Product_ID(multiple_m_product_id);
//                order.setDateOrdered(date_order);
//                order.setRetail_ID(retail_id);
//                order.setRef_POS_Order_ID(pos_order_id);
//                order.saveEx();
//                map.put("ref_order_request_id", order.getFORCA_POS_Order_Request_ID());
//                resp = ResponseData.successResponse(Constants.SUCCEED_DataSaved, map);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData insertTempPayment(ParamRequest param, Trx trx) throws Exception {
//        String pos_payment_id = param.getRef_payment_id();
//        String pos_order_id = param.getRef_order_id();
//        BigDecimal pay_amt = param.getPay_amt();
//
//        if (pos_order_id == null || pos_payment_id == null || pay_amt == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        Map<String, Object> map = new LinkedHashMap<>();
//        StringBuilder sql = new StringBuilder();
//
//        try {
//            // @formatter:off
//            sql.append(""
//                    + "SELECT "
//                    + "    FORCA_POS_Payment_ID "
//                    + "FROM "
//                    + "    FORCA_POS_Payment "
//                    + "WHERE "
//                    + "    IsActive = 'Y' "
//                    + "    AND ref_pos_payment_id =?"
//                    + "    AND ad_client_id = ? ");
//            // @formatter:on
//
//            int FORCA_POS_Payment_ID = DB.getSQLValueEx(trxName, sql.toString(), pos_payment_id,
//                    Env.getAD_Client_ID(Env.getCtx()));
//
//            if (FORCA_POS_Payment_ID != -1) {
//                map.put("ref_payment_request_id", FORCA_POS_Payment_ID);
//                resp = ResponseData.errorResponse(Constants.ERROR_DataIsAtSystem, map);
//            } else {
//                MFORCAPOSPayment pay = ForcaObject.getObject(Env.getCtx(),
//                        MFORCAPOSPayment.Table_Name, 0, trxName);
//                pay.setRef_POS_Order_ID(pos_order_id);
//                pay.setRef_POS_Payment_ID(pos_payment_id);
//                pay.setPayAmt(pay_amt);
//                pay.saveEx();
//                map.put("ref_payment_request_id", pay.getFORCA_POS_Payment_ID());
//                resp = ResponseData.successResponse(Constants.SUCCEED_DataSaved, map);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    // ANI
//    public ResponseData insertTempBPartner(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        Map<String, Object> map = new LinkedHashMap<>();
//        StringBuilder sql = new StringBuilder();
//
//        if (param.getSap_code() == null || param.getRef_customer_id() == null
//                || param.getAddress_customer() == null || param.getName() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        try {
//            // @formatter:off
//            sql.append(""
//                    + "SELECT "
//                    + "    FORCA_CustomerPOS_ID "
//                    + "FROM "
//                    + "    FORCA_CustomerPOS "
//                    + "WHERE "
//                    + "    IsActive = 'Y' "
//                    + "    AND pos_customer_id =?");
//            // @formatter:on
//            int FORCA_CustomerPOS_ID =
//                    DB.getSQLValueEx(trxName, sql.toString(), param.getRef_customer_id());
//
//            if (FORCA_CustomerPOS_ID != -1) {
//                map.put(MFORCACustomerPOS.COLUMNNAME_pos_customer_id, FORCA_CustomerPOS_ID);
//                resp = ResponseData.errorResponse(Constants.ERROR_DataIsAtSystem, map);
//            } else {
//                MFORCACustomerPOS cp = ForcaObject.getObject(Env.getCtx(),
//                        MFORCACustomerPOS.Table_Name, 0, trxName);
//                cp.setName(param.getName());
//                cp.setSAP_Code(param.getSap_code());
//                cp.setpos_customer_id(param.getRef_customer_id());
//                cp.setAddress_Customer(param.getAddress_customer());
//                cp.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//                if (param.getId_customer() != null) {
//                    cp.setID_Customer(param.getId_customer());
//                }
//                if (param.getSo_credit_limit() != null) {
//                    cp.setSO_CreditLimit(new BigDecimal(param.getSo_credit_limit()));
//                }
//                if (param.getPhone() != null) {
//                    cp.setPhone(param.getPhone());
//                }
//
//                cp.saveEx();
//                map.put("ref_customer_request_id", cp.getFORCA_CustomerPOS_ID());
//                resp = ResponseData.successResponse(Constants.SUCCEED_DataSaved, map);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    // ANI
//    public ResponseData getProcessName(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT DISTINCT " + 
//                "    a.Name "
//                );
//        join.append(
//                "FROM AD_Workflow a " + 
//                "LEFT JOIN AD_Wf_Activity b ON " + 
//                        "b.AD_Workflow_ID = a.AD_Workflow_ID ");
//        whereCond.append("WHERE a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("process_name", rs.getString("name"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//        } catch (SQLException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getTempBPartner(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        if (param.getRef_customer_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        //@formatter:off
//        sql.append(""
//                + " SELECT "
//                + "     POS_Customer_ID, "
//                + "     Sap_Code, "
//                + "     So_CreditLimit, "
//                + "     C_BPartner_ID "
//                + " FROM "
//                + "     FORCA_CustomerPOS "
//                + " WHERE "
//                + "     POS_Customer_ID = ? "
//                + "     AND AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(param.getRef_customer_id());
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//            List<Object> listdata = new ArrayList<Object>();
//
//            if (rs.next()) {
//                resp.setCodestatus("S");
//                resp.setMessage(Constants.SUCCEED_DataFound);
//                do {
//                    Map<String, Object> map = new LinkedHashMap<String, Object>();
//                    map.put("ref_customer_id",
//                            rs.getString(MFORCACustomerPOS.COLUMNNAME_pos_customer_id));
//                    map.put("c_bpartner_id",
//                            rs.getString(MFORCACustomerPOS.COLUMNNAME_C_BPartner_ID));
//                    map.put("sap_code", rs.getString(MFORCACustomerPOS.COLUMNNAME_SAP_Code));
//                    map.put("so_creditlimit",
//                            rs.getString(MFORCACustomerPOS.COLUMNNAME_SO_CreditLimit));
//                    listdata.add(map);
//                } while (rs.next());
//                resp.setResultdata(listdata);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            rs = null;
//            ps = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getTempClient(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        if (param.getAd_client_id() == null || param.getUsername() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        ResponseData resp = new ResponseData();
//        StringBuilder sql = new StringBuilder();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        //@formatter:off
//        sql.append(""
//                + " SELECT "
//                + "     forca_isusercrm "
//                + " FROM "
//                + "     AD_User "
//                + " WHERE "
//                + "     Name = ? "
//                + "     AND AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(param.getUsername());
//        params.add(param.getAd_client_id());
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//            List<Object> listdata = new ArrayList<Object>();
//
//            if (rs.next()) {
//                resp.setCodestatus("S");
//                resp.setMessage(Constants.SUCCEED_DataFound);
//                Map<String, Object> map = new LinkedHashMap<String, Object>();
//                map.put("forca_isusercrm", rs.getString("forca_isusercrm"));
//                listdata.add(map);
//                resp.setResultdata(listdata);
//            } else {
//                resp = ResponseData.errorResponse(Constants.ERROR_DataNotFound);
//            }
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            rs = null;
//            ps = null;
//        }
//        return resp;
//    }
//
//    public ResponseData setOrderCompletewithFiles(ParamOrderOther param,
//            List<FormDataBodyPart> bodyParts, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        MAttachment attach = null;
//
//        try {
//            Integer c_order_id = param.getC_order_id();
//            Integer c_bpartner_id = param.getC_bpartner_id();
//            String PaymentRule = param.getPayment_rule();
//            List<ParamOrderLineOther> listLine = param.getList_line();
//            Boolean isInsert = false;
//
//            MOrder o = null;
//            if (c_order_id.intValue() <= 0) {
//                o = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, 0, trxName);
//                isInsert = true;
//            } else {
//                o = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, c_order_id, trxName);
//                if (o == null) {
//                    o = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            }
//
//            if (isInsert) {
//                if (c_bpartner_id.intValue() <= 0 || Util.isEmpty(param.getIssotrx())
//                        || param.getM_warehouse_id().intValue() <= 0
//                        || param.getM_pricelist_id().intValue() <= 0) {
//                    String par = "";
//                    if (c_bpartner_id.intValue() <= 0) {
//                        par += "c_bpartner, ";
//                    }
//                    if (Util.isEmpty(param.getIssotrx())) {
//                        par += "issotrx, ";
//                    }
//                    if (param.getM_warehouse_id().intValue() <= 0) {
//                        par += "m_warehouse_id, ";
//                    }
//                    if (param.getM_pricelist_id().intValue() <= 0) {
//                        par += "m_pricelist_id, ";
//                    }
//                    return ResponseData.errorResponse(par + "is required");
//                }
//
//                // insert
//                if (param.getIssotrx().equals("Y")) {
//                    o.setIsSOTrx(true);
//                } else {
//                    o.setIsSOTrx(false);
//                }
//
//                if (!Util.isEmpty((param.getDateordered()))) {
//                    o.setDateOrdered(WsUtil.convertFormatTgl(param.getDateordered()));
//                } else {
//                    o.setDateOrdered(new Timestamp(new Date().getTime()));
//                }
//
//                o.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    o.setC_BPartner_Location_ID(param.getC_bpartner_location_id().intValue());
//                } else {
//                    // c_bpartner_id pasti > 0
//                    o.setC_BPartner_Location_ID(WsUtil.getDefaultBPartnerLocationID(c_bpartner_id));
//                }
//
//                o.setM_Warehouse_ID(param.getM_warehouse_id());
//                o.setM_PriceList_ID(param.getM_pricelist_id());
//                o.setSalesRep_ID(
//                        WsUtil.getDefaultSalesRepIDByName(Env.getCtx(), param.getSalerep_name()));
//
//
//                if (Util.isEmpty(PaymentRule)) {
//                    PaymentRule = WsUtil.getPaymentRule();
//                }
//
//                o.setC_Currency_ID(WsUtil.getDefaultCountryID());
//
//                if (param.getDropship_bpartner_id().intValue() > 0) {
//                    o.setDropShip_BPartner_ID(param.getDropship_bpartner_id());
//                    if (param.getDropship_location_id().intValue() == 0) {
//                        o.setDropShip_Location_ID(WsUtil
//                                .getDefaultBPartnerLocationID(param.getDropship_bpartner_id()));
//                    }
//                }
//
//                if (param.getDropship_location_id().intValue() > 0) {
//                    o.setDropShip_Location_ID(param.getDropship_location_id().intValue());
//                }
//
//                if (!Util.isEmpty(param.getDescription())) {
//                    o.setDescription(param.getDescription());
//                }
//
//                if (param.getC_project_id() != null) {
//                    o.setC_Project_ID(param.getC_project_id());
//                }
//
//                if (param.getUser2_id().intValue() > 0) {
//                    o.setUser2_ID(param.getUser2_id());
//                }
//
//                if (param.getC_paymentterm_id() > 0) {
//                    o.setC_Payment_ID(param.getC_paymentterm_id());
//                }
//
//            } else {
//                // set
//                if (!Util.isEmpty((param.getDateordered()))) {
//                    o.setDateOrdered(WsUtil.convertFormatTgl(param.getDateordered()));
//                }
//
//                if (param.getAd_org_id().intValue() > 0) {
//                    o.setAD_Org_ID(param.getAd_org_id());
//                }
//
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    o.setC_BPartner_Location_ID(param.getC_bpartner_location_id());
//                }
//
//                if (param.getM_warehouse_id().intValue() > 0) {
//                    o.setM_Warehouse_ID(param.getM_warehouse_id().intValue());
//                }
//
//                // cuma untuk case SO
//                if (param.getM_pricelist_id().intValue() > 0) {
//                    o.setM_PriceList_ID(param.getM_pricelist_id().intValue());
//                } else if (o.isSOTrx()) {
//                    WsUtil.defaultPriceJual(Env.getCtx());
//                }
//
//                if (!Util.isEmpty(param.getSalerep_name())) {
//                    o.setSalesRep_ID(WsUtil.getDefaultSalesRepIDByName(Env.getCtx(),
//                            param.getSalerep_name()));
//                }
//
//                if (param.getAd_orgtrx_id().intValue() > 0) {
//                    o.set_ValueOfColumn("ad_orgtrx_id", param.getAd_orgtrx_id());
//                }
//
//                if (param.getCrm_id().intValue() > 0) {
//                    o.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//
//                if (param.getDropship_bpartner_id().intValue() > 0) {
//                    o.setDropShip_BPartner_ID(param.getDropship_bpartner_id());
//                    if (param.getDropship_location_id().intValue() == 0) {
//                        o.setDropShip_Location_ID(WsUtil
//                                .getDefaultBPartnerLocationID(param.getDropship_bpartner_id()));
//                    }
//                }
//
//                if (param.getDropship_location_id().intValue() > 0) {
//                    o.setDropShip_Location_ID(param.getDropship_location_id().intValue());
//                }
//
//                if (!Util.isEmpty(param.getDescription())) {
//                    o.setDescription(param.getDescription());
//                }
//
//                if (param.getC_project_id().intValue() > 0) {
//                    o.setC_Project_ID(param.getC_project_id());
//                }
//
//                if (param.getUser2_id().intValue() > 0) {
//                    o.setUser2_ID(param.getUser2_id());
//                }
//
//                if (param.getC_paymentterm_id() > 0) {
//                    o.setC_Payment_ID(param.getC_paymentterm_id());
//                }
//            }
//
//            // set_insert
//            o.setC_DocTypeTarget_ID(WsUtil.getDefaultDocTypeID(Env.getCtx()));
//
//            if (c_bpartner_id.intValue() > 0) {
//                o.setC_BPartner_ID(c_bpartner_id);
//            }
//
//
//            if (param.getC_opportunity_id().intValue() > 0) {
//                o.setC_Opportunity_ID(param.getC_opportunity_id().intValue());
//            }
//
//            if (param.getC_campaign_id().intValue() > 0) {
//                o.setC_Campaign_ID(param.getC_campaign_id().intValue());
//            }
//
//            if (param.getDocumentno() != null) {
//                o.setDocumentNo(param.getDocumentno());
//            }
//
//            if (param.getAd_user_id().intValue() > 0) {
//                o.setAD_User_ID(param.getAd_user_id().intValue());
//            }
//
//            if (!Util.isEmpty(param.getPoreference())) {
//                o.setPOReference(param.getPoreference());
//            }
//
//            o.setPaymentRule(PaymentRule);
//
//            o.saveEx();
//
//            // Selalu Create Attachment Baru, Meskipun Update SO
//            attach = ForcaObject.getObject(Env.getCtx(), MAttachment.Table_Name, 0, trxName);
//            attach.setAD_Table_ID(MOrder.Table_ID);
//            attach.setRecord_ID(o.get_ID());
//            for (int i = 0; i < bodyParts.size(); i++) {
//                BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.get(i).getEntity();
//                String fileName = bodyParts.get(i).getContentDisposition().getFileName();
//
//                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
//                int nRead;
//                byte[] data = new byte[1024];
//                InputStream fileInputStream = bodyPartEntity.getInputStream();
//                while ((nRead = fileInputStream.read(data, 0, data.length)) != -1) {
//                    buffer.write(data, 0, nRead);
//                }
//
//                buffer.flush();
//                byte[] binaryFile = buffer.toByteArray();
//                buffer.close();
//                attach.addEntry(fileName, binaryFile);
//            }
//            attach.saveEx();
//
//            Integer line = 0;
//            for (ParamOrderLineOther pol : listLine) {
//                Integer tax_id = 0;
//                if (pol.getC_tax_id().intValue() > 0) {
//                    tax_id = Integer.valueOf(pol.getC_tax_id());
//                } else {
//                    tax_id = WsUtil.getDefaultTaxID(Env.getCtx());
//                }
//
//                MOrderLine ol = null;
//                if (o.getLines().length <= 0) {
//                    ol = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name, 0, trxName);
//                } else {
//                    int c_orderline_id = o.getLines()[line].get_ID();
//                    ol = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name, c_orderline_id,
//                            trxName);
//                    if (ol == null) {
//                        ol = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name, 0, trxName);
//                    }
//                }
//
//                ol.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//                ol.setC_Order_ID(o.get_ID());
//                if (pol.getLine_number() != 0) {
//                    ol.setLine(pol.getLine_number());
//                } else {
//                    ol.setLine((line + 1) * 10);
//                }
//                ol.setM_Product_ID(Integer.valueOf(pol.getM_product_id()));
//                ol.setC_UOM_ID(Integer.valueOf(pol.getC_uom_id()));
//                ol.setQtyEntered(new BigDecimal(pol.getQty_entered()));
//                ol.setQtyOrdered(new BigDecimal(pol.getQty_entered()));
//                ol.setPriceEntered(pol.getPrice_entered());
//                ol.setPriceActual(pol.getPrice_entered());
//                ol.setC_Tax_ID(tax_id);
//                if (pol.getDiscount() != null) {
//                    ol.setDiscount(pol.getDiscount());
//                }
//                ol.saveEx();
//
//                line++;
//            }
//
//            if (!Util.isEmpty(param.getDocstatus())) {
//                if (param.getDocstatus().equals(DocAction.ACTION_Complete)) {
//                    try {
//                        o.setDocAction(DocAction.ACTION_Complete);
//                        o.processIt(DocAction.ACTION_Complete);
//                        if (!o.getDocStatus().equals(DocAction.STATUS_Completed)) {
//                            throw new AdempiereException(
//                                    "Failed to Create Complete Order, Please Check Your Parameter");
//                        }
//                    } catch (AdempiereException e) {
//                        throw new AdempiereException(e.getMessage());
//                    }
//                } else {
//                    throw new AdempiereException("Document Action False");
//                }
//            }
//
//            resp.setCodestatus("S");
//            if (isInsert)
//                resp.setMessage("Insert succeeded");
//            else
//                resp.setMessage("Update succeeded");
//
//            Map<String, Object> xmap = new LinkedHashMap<>();
//            xmap.put("crm_id", o.get_Value("crm_id"));
//            xmap.put("c_order_id", o.get_ID());
//            xmap.put("documentno", o.getDocumentNo());
//            xmap.put("docstatus", o.getDocStatus());
//            Order = new OrderImplOther();
//            xmap.put("c_orderline", Order.getOrderLineByOrderID(o.get_ID(), trxName));
//            resp.setResultdata(xmap);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setInOutComplete(ParamInOutOther param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<MInOutLine> list = new ArrayList<MInOutLine>();
//        List<PO> objData = new ArrayList<PO>();
//
//        try {
//            Integer m_inout_id = param.getM_inout_id();
//            Integer c_bpartner_id = param.getC_bpartner_id();
//            List<ParamInOutLineOther> listLine = param.getList_line();
//            Boolean isInsert = false;
//
//            MInOut inout = null;
//            if (m_inout_id.intValue() <= 0) {
//                inout = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, 0, trxName);
//                isInsert = true;
//            } else {
//                inout = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, m_inout_id, trxName);
//                if (inout == null) {
//                    inout = ForcaObject.getObject(Env.getCtx(), MInOut.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            }
//
//            if (isInsert) {
//                if (param.getC_order_id() <= 0 || c_bpartner_id.intValue() <= 0
//                        || Util.isEmpty(param.getIssotrx())
//                        || param.getM_warehouse_id().intValue() <= 0
//                        || param.getC_bpartner_location_id().intValue() <= 0
//                        || Util.isEmpty(param.getMovementdate())) {
//                    String par = "";
//                    if (param.getC_order_id() < 0) {
//                        par += "c_order_id, ";
//                    }
//                    if (c_bpartner_id.intValue() <= 0) {
//                        par += "c_bpartner, ";
//                    }
//                    if (Util.isEmpty(param.getIssotrx())) {
//                        par += "issotrx, ";
//                    }
//                    if (param.getM_warehouse_id().intValue() <= 0) {
//                        par += "m_warehouse_id, ";
//                    }
//                    if (param.getC_bpartner_location_id().intValue() <= 0) {
//                        par += "c_bpartner_location_id, ";
//                    }
//                    if (Util.isEmpty(param.getMovementdate())) {
//                        par += "movementdate, ";
//                    }
//                    return ResponseData.errorResponse(par + "is required");
//                }
//
//                // insert
//                if (param.getIssotrx().equals("Y")) {
//                    String whereClause = " Name = 'MM Shipment' AND AD_Client_ID = ? ";
//                    MDocType docType =
//                            new Query(Env.getCtx(), MDocType.Table_Name, whereClause, trxName)
//                                    .setParameters(Env.getAD_Client_ID(Env.getCtx())).first();
//                    inout.setC_DocType_ID(docType.get_ID());
//                    inout.setIsSOTrx(true);
//                    inout.setMovementType(MInOut.MOVEMENTTYPE_CustomerShipment);
//                } else {
//                    String whereClause = " Name = 'MM Receipt' AND AD_Client_ID = ? ";
//                    MDocType docType =
//                            new Query(Env.getCtx(), MDocType.Table_Name, whereClause, trxName)
//                                    .setParameters(Env.getAD_Client_ID(Env.getCtx())).first();
//                    inout.setC_DocType_ID(docType.get_ID());
//                    inout.setIsSOTrx(false);
//                    inout.setMovementType(MInOut.MOVEMENTTYPE_VendorReceipts);
//                }
//
//                if (!Util.isEmpty((param.getMovementdate()))) {
//                    inout.setMovementDate(WsUtil.convertFormatTgl(param.getMovementdate()));
//                } else {
//                    inout.setMovementDate(new Timestamp(new Date().getTime()));
//                }
//
//                inout.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    inout.setC_BPartner_Location_ID(param.getC_bpartner_location_id().intValue());
//                } else {
//                    // c_bpartner_id pasti > 0
//                    inout.setC_BPartner_Location_ID(
//                            WsUtil.getDefaultBPartnerLocationID(c_bpartner_id));
//                }
//
//                inout.setM_Warehouse_ID(param.getM_warehouse_id());
//
//                inout.setC_Order_ID(param.getC_order_id());
//                inout.setPriorityRule(MInOut.PRIORITYRULE_Medium);
//                inout.setFreightCostRule(MInOut.FREIGHTCOSTRULE_FreightIncluded);
//
//            } else {
//                // set
//                if (!Util.isEmpty((param.getMovementdate()))) {
//                    inout.setMovementDate(WsUtil.convertFormatTgl(param.getMovementdate()));
//                }
//
//                if (param.getAd_org_id().intValue() > 0) {
//                    inout.setAD_Org_ID(param.getAd_org_id());
//                }
//
//                if (param.getC_bpartner_location_id().intValue() > 0) {
//                    inout.setC_BPartner_Location_ID(param.getC_bpartner_location_id());
//                }
//
//                if (param.getM_warehouse_id().intValue() > 0) {
//                    inout.setM_Warehouse_ID(param.getM_warehouse_id().intValue());
//                }
//
//                if (param.getAd_orgtrx_id().intValue() > 0) {
//                    inout.set_ValueOfColumn("ad_orgtrx_id", param.getAd_orgtrx_id());
//                }
//
//                if (param.getCrm_id().intValue() > 0) {
//                    inout.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//            }
//
//            // set_insert
//            if (param.getDescription() != null) {
//                inout.setDescription(param.getDescription());
//            }
//
//            if (c_bpartner_id.intValue() > 0) {
//                inout.setC_BPartner_ID(c_bpartner_id);
//            }
//
//            if (param.getC_project_id().intValue() > 0) {
//                inout.setC_Project_ID(param.getC_project_id().intValue());
//            }
//
//            if (param.getUser2_id().intValue() > 0) {
//                inout.setUser2_ID(param.getUser2_id().intValue());
//            }
//
//            if (param.getAd_user_id().intValue() > 0) {
//                inout.setAD_User_ID(param.getAd_user_id().intValue());
//            }
//            if (param.getCrm_id().intValue() > 0) {
//                inout.set_ValueOfColumn("crm_id", param.getCrm_id());
//            }
//
//            inout.saveEx();
//
//            Integer line = 0;
//
//            for (ParamInOutLineOther piol : listLine) {
//                MOrderLine orderline = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name,
//                        piol.getC_orderline_id(), trxName);
//                MInOutLine inoutline = new MInOutLine(inout);
//                MOrder order = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name,
//                        param.getC_order_id(), trxName);
//
//                if (piol.getLine_number() != 0) {
//                    inoutline.setLine(piol.getLine_number());
//                } else {
//                    inoutline.setLine((line + 1) * 10);
//                }
//
//                if (piol.getC_orderline_id() != 0) {
//                    inoutline.setC_OrderLine_ID(piol.getC_orderline_id());
//                } else {
//                    inoutline.setC_OrderLine_ID(orderline.getC_OrderLine_ID());
//                }
//
//                if (piol.getM_product_id() != 0) {
//                    inoutline.setM_Product_ID(piol.getM_product_id());
//                } else {
//                    inoutline.setM_Product_ID(orderline.getM_Product_ID(), orderline.getC_UOM_ID());
//                }
//
//                if (orderline.getM_Product_ID() == 0)
//                    inoutline.setC_Charge_ID(orderline.getC_Charge_ID());
//
//                inoutline.setC_UOM_ID(piol.getC_uom_id());
//                inoutline.setQty(piol.getQty());
//                inoutline.setM_AttributeSetInstance_ID(orderline.getM_AttributeSetInstance_ID());
//
//                if (piol.getDescription() != null) {
//                    inoutline.setDescription(piol.getDescription());
//                } else {
//                    inoutline.setDescription(orderline.getDescription());
//                }
//
//                inoutline.setC_Project_ID(orderline.getC_Project_ID());
//                inoutline.setC_ProjectPhase_ID(orderline.getC_ProjectPhase_ID());
//                inoutline.setC_ProjectTask_ID(orderline.getC_ProjectTask_ID());
//                inoutline.setC_Activity_ID(orderline.getC_Activity_ID());
//                inoutline.setC_Campaign_ID(orderline.getC_Campaign_ID());
//                inoutline.setAD_OrgTrx_ID(orderline.getAD_OrgTrx_ID());
//                inoutline.setUser1_ID(orderline.getUser1_ID());
//                inoutline.setUser2_ID(orderline.getUser2_ID());
//
//                inoutline.setM_Locator_ID(piol.getM_locator_id());
//
//                if (piol.getQty().intValue() > (orderline.getQtyReserved().intValue()))
//                    throw new Exception("Quantity of inout cannot exceed the quantity of order");
//
//                inoutline.saveEx();
//
//
//                list.add(inoutline);
//                inoutline = new MInOutLine(inout);
//
//                if (trxName == null)
//                    objData.add(inoutline);
//
//                if (order != null && order.getC_Order_ID() != 0) {
//                    inout.setAD_OrgTrx_ID(order.getAD_OrgTrx_ID());
//                    inout.setC_Project_ID(order.getC_Project_ID());
//                    inout.setC_Campaign_ID(order.getC_Campaign_ID());
//                    inout.setC_Activity_ID(order.getC_Activity_ID());
//                    inout.setUser1_ID(order.getUser1_ID());
//                    inout.setUser2_ID(order.getUser2_ID());
//                    if (order.isDropShip()) {
//                        inout.setM_Warehouse_ID(order.getM_Warehouse_ID());
//                        inout.setIsDropShip(order.isDropShip());
//                        inout.setDropShip_BPartner_ID(order.getDropShip_BPartner_ID());
//                        inout.setDropShip_Location_ID(order.getDropShip_Location_ID());
//                        inout.setDropShip_User_ID(order.getDropShip_User_ID());
//                    }
//                    inout.saveEx();
//                }
//            }
//
//            if (!Util.isEmpty(param.getDocstatus())) {
//                if (param.getDocstatus().equals(DocAction.ACTION_Complete)) {
//                    try {
//                        if (!inout.processIt(DocAction.ACTION_Complete)) {
//                            throw new AdempiereException(
//                                    "Failed to Create Complete In Out, Please Check Your Parameter");
//                        }
//                    } catch (AdempiereException e) {
//                        throw new AdempiereException(e.getMessage());
//                    }
//                } else {
//                    throw new AdempiereException("Document Action False");
//                }
//            }
//
//            resp.setCodestatus("S");
//            if (isInsert)
//                resp.setMessage("Insert succeeded");
//            else
//                resp.setMessage("Update succeeded");
//
//            Map<String, Object> xmap = new LinkedHashMap<>();
//            xmap.put("crm_id", inout.get_Value("crm_id"));
//            xmap.put("m_inout_id", inout.get_ID());
//            xmap.put("documentno", inout.getDocumentNo());
//            xmap.put("docstatus", inout.getDocStatus());
//            InOut = new InOutImplOther();
//            xmap.put("m_inoutline_id", InOut.getInOutLineByInOutID(inout.get_ID(), trxName));
//            resp.setResultdata(xmap);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
}
