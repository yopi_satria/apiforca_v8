package id.sisi.forca.base.api.impl.transaction;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;
import id.sisi.forca.base.api.util.ProcessUtil;

public class InOutImplTrans {
    public Object getInOutLineByInOutID(Integer m_inout_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listInOutLine = new ArrayList<>();
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);

        //@formatter:off
        sql.append(""
                + "SELECT "
                + "    il.m_inoutline_id, "
                + "    il.c_orderline_id,"
                + "    il.m_locator_id, "
                + "    il.m_product_id, "
                + "    il.c_uom_id, "
                + "    loc.value AS locator_name, "
                + "    prd.name AS product_name, "
                + "    il.movementqty, "
                + "    il.qtyentered as inout_qtyentered, "
                + "    il.isinvoiced, "
                + "    u.name AS uom_name, "
                + "    t.name AS tax_name, "
                + "    ol.linenetamt AS total, "
                + "    cur.cursymbol, "
                + "    ol.qtyentered, "
                + "    ol.qtydelivered, "
                + "    ol.qtyreserved, "
                + "    ol.qtyinvoiced, "
                + "    ol.discount, "
                + "    ol.priceentered, "
                + "    il.line "
                + "FROM "
                + "    m_inoutline il "
                + "LEFT JOIN c_orderline ol ON "
                + "    ol.c_orderline_id = il.c_orderline_id "
                + "LEFT JOIN m_product prd ON "
                + "    prd.m_product_id = il.m_product_id "
                + "LEFT JOIN c_uom u ON "
                + "    u.c_uom_id = il.c_uom_id "
                + "LEFT JOIN c_tax t ON "
                + "    t.c_tax_id = ol.c_tax_id "
                + "LEFT JOIN c_currency cur ON "
                + "    cur.c_currency_id = ol.c_currency_id "
                + "LEFT JOIN m_locator loc ON "
                + "    loc.m_locator_id = il.m_locator_id"
                + " WHERE "
                + "     il.m_inout_id = ? "
                + "     AND il.ad_client_id = ? ");
        params.add(m_inout_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("line_number", rs.getString("line"));
                map.put("m_inoutline_id", rs.getString("m_inoutline_id"));
                map.put("m_locator_id", rs.getString("m_locator_id"));
                map.put("locator_name", rs.getString("locator_name"));
                map.put("m_product_id", rs.getString("m_product_id"));
                map.put("c_uom_id", rs.getString("c_uom_id"));
                map.put("product_name", rs.getString("product_name"));
                map.put("inout_qtyentered", rs.getString("inout_qtyentered"));
                map.put("movementqty", rs.getString("movementqty"));
                map.put("uom_name", rs.getString("uom_name"));
                map.put("c_orderline_id", rs.getString("c_orderline_id"));
                map.put("isinvoiced", rs.getString("isinvoiced"));

                if (!Util.isEmpty(rs.getString("total")) && !Util.isEmpty(rs.getString("cursymbol"))) {
                    map.put("total", rs.getString("cursymbol") + " "
                            + rs.getBigDecimal("total"));
                } else {
                    if (!Util.isEmpty(rs.getString("cursymbol"))) {
                        map.put("total", rs.getString("cursymbol") + " " + rs.getInt("total"));
                    } else {
                        map.put("total", rs.getInt("total"));
                    }
                }

                if (!Util.isEmpty(rs.getString("qtyentered"))) {
                    map.put("qtyentered", rs.getBigDecimal("qtyentered"));
                } else {
                    map.put("qtyentered", "0");
                }
                
                if (!Util.isEmpty(rs.getString("qtyreserved"))) {
                    map.put("qtyreserved", rs.getBigDecimal("qtyreserved"));
                } else {
                    map.put("qtyreserved", "0");
                }

                if (!Util.isEmpty(rs.getString("qtydelivered"))) {
                    map.put("qtydelivered", rs.getBigDecimal("qtydelivered"));
                } else {
                    map.put("qtydelivered", "0");
                }

                if (!Util.isEmpty(rs.getString("qtyinvoiced"))) {
                    map.put("qtyinvoiced", rs.getBigDecimal("qtyinvoiced"));
                } else {
                    map.put("qtyinvoiced", "0");
                }

                if (!Util.isEmpty(rs.getString("priceentered")) && !Util.isEmpty(rs.getString("cursymbol"))) {
                    map.put("priceentered", rs.getString("cursymbol") + " "
                            + new BigDecimal(rs.getString("priceentered")));
                } else {
                    if (!Util.isEmpty(rs.getString("cursymbol"))) {
                        map.put("priceentered",
                                rs.getString("cursymbol") + " " + rs.getInt("priceentered"));
                    } else {
                        map.put("priceentered", rs.getInt("priceentered"));
                    }
                }

                if (!Util.isEmpty(rs.getString("discount"))) {
                    map.put("discount", rs.getBigDecimal("discount") + "%");
                } else {
                    map.put("discount", rs.getInt("discount") + "%");
                }

                listInOutLine.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listInOutLine;
    }

    public Object getProjectByInOutID(Integer m_inout_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listProject = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     prj.c_project_id, "
                + "     prj.value,"
                + "     prj.name,"
                + "     prj.datecontract,"
                + "     prj.datefinish "
                + " FROM "
                + "     m_inout io"
                + " LEFT JOIN c_project prj ON prj.c_project_id = io.c_project_id "
                + " WHERE "
                + "     io.m_inout_id = ? "
                + "     AND io.ad_client_id = ? ");
        params.add(m_inout_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                if (rs.getString("c_project_id") != null) {
                    Map<String, Object> map = new LinkedHashMap<String, Object>();
                    map.put("c_project_id", rs.getString("c_project_id"));
                    map.put("value", rs.getString("value"));
                    map.put("name", rs.getString("name"));
                    map.put("datecontract", rs.getString("datecontract"));
                    map.put("datefinish", rs.getString("datefinish"));
                    map.put("c_projectline",
                            getProjectLineByProjectID(rs.getInt("c_project_id"), trxName));
                    listProject.add(map);
                }
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listProject;
    }

    public Object getProjectLineByProjectID(Integer c_project_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listProjectLine = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     pl.c_projectline_id, "
                + "     pl.plannedqty, "
                + "     pl.plannedprice, "
                + "     pl.plannedamt, "
                + "     prd.name as product_name "
                + " FROM "
                + "     c_projectline pl "
                + " LEFT JOIN m_product prd ON prd.m_product_id = pl.m_product_id "
                + " WHERE "
                + "     pl.c_project_id = ? "
                + "     AND pl.ad_client_id = ? ");
        params.add(c_project_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("c_projectline_id", rs.getString("c_projectline_id"));
                map.put("plannedqty", rs.getString("plannedqty"));
                map.put("plannedprice", rs.getString("plannedprice"));
                map.put("plannedamt", rs.getString("plannedamt"));
                map.put("product_name", rs.getString("product_name"));
                listProjectLine.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listProjectLine;
    }
}
