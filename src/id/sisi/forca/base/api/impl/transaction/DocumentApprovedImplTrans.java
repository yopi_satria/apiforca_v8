/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : Document Approved Implement Transaction                              
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.impl.transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MColumn;
import org.compiere.model.MCurrency;
import org.compiere.model.MInvoice;
import org.compiere.model.MOrder;
import org.compiere.model.MPayment;
import org.compiere.model.MProject;
import org.compiere.model.MRequisition;
import org.compiere.model.MTable;
import org.compiere.model.MUser;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.compiere.wf.MWFEventAudit;
import org.compiere.wf.MWFNode;
//import org.idempiere.model.ForcaObject;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.ProcessUtil;

public class DocumentApprovedImplTrans {
    private CLogger log = CLogger.getCLogger(getClass());

//    public String getDescDetail(PO po, String tipe) throws Exception {
//        int index = -1;
//        if (po == null)
//            return null;
//        StringBuilder sb = new StringBuilder();
//
//
//        if (po.get_TableName().equalsIgnoreCase(MRequisition.Table_Name)) {
//            index = po.get_ColumnIndex(MRequisition.COLUMNNAME_M_Requisition_ID);
//            if (index != -1) {
//                try {
//                    MRequisition requisition =
//                            ForcaObject.getObject(po.getCtx(), MRequisition.Table_Name,
//                                    ((Integer) po.get_Value(index)).intValue(), po.get_TrxName());
//                    if (tipe.equals("price")) {
//                        sb.append("Amount: ")
//                                .append(((MCurrency) ForcaObject.getObject(po.getCtx(),
//                                        MCurrency.Table_Name, requisition.getC_Currency_ID(),
//                                        po.get_TrxName())).getCurSymbol() == null
//                                                ? ""
//                                                : ((MCurrency) ForcaObject.getObject(po.getCtx(),
//                                                        MCurrency.Table_Name,
//                                                        requisition.getC_Currency_ID(),
//                                                        po.get_TrxName())).getCurSymbol())
//                                .append(" ").append(ResponseData
//                                        .toRupiahFormat(requisition.getTotalLines().toString()));
//                    } else {
//                        sb.append(requisition.getDescription());
//                    }
//                } catch (AdempiereException e) {
//                    throw new AdempiereException(e);
//                } catch (Exception e) {
//                    throw new Exception(e);
//                }
//            }
//        } else if (po.get_TableName().equalsIgnoreCase(MOrder.Table_Name)) {
//            index = po.get_ColumnIndex(MOrder.COLUMNNAME_C_Order_ID);
//            if (index != -1) {
//                try {
//                    MOrder order = ForcaObject.getObject(po.getCtx(), MOrder.Table_Name,
//                            ((Integer) po.get_Value(index)).intValue(), po.get_TrxName());
//                    if (tipe.equals("price")) {
//                        sb.append("Amount: ")
//                                .append(((MCurrency) ForcaObject.getObject(po.getCtx(),
//                                        MCurrency.Table_Name, order.getC_Currency_ID(),
//                                        po.get_TrxName())).getCurSymbol() == null
//                                                ? ""
//                                                : ((MCurrency) ForcaObject.getObject(po.getCtx(),
//                                                        MCurrency.Table_Name,
//                                                        order.getC_Currency_ID(), po.get_TrxName()))
//                                                                .getCurSymbol())
//                                .append(" ").append(ResponseData
//                                        .toRupiahFormat(order.getGrandTotal().toString()));
//                    } else {
//                        sb.append(order.getDescription());
//                    }
//                } catch (AdempiereException e) {
//                    throw new AdempiereException(e);
//                } catch (Exception e) {
//                    throw new Exception(e);
//                }
//            }
//        } else if (po.get_TableName().equalsIgnoreCase(MInvoice.Table_Name)) {
//            index = po.get_ColumnIndex(MInvoice.COLUMNNAME_C_Invoice_ID);
//            if (index != -1) {
//                try {
//                    MInvoice invoice = ForcaObject.getObject(po.getCtx(), MInvoice.Table_Name,
//                            ((Integer) po.get_Value(index)).intValue(), po.get_TrxName());
//                    if (tipe.equals("price")) {
//                        sb.append("Amount: ")
//                                .append(((MCurrency) ForcaObject.getObject(po.getCtx(),
//                                        MCurrency.Table_Name, invoice.getC_Currency_ID(),
//                                        po.get_TrxName())).getCurSymbol() == null
//                                                ? ""
//                                                : ((MCurrency) ForcaObject.getObject(po.getCtx(),
//                                                        MCurrency.Table_Name,
//                                                        invoice.getC_Currency_ID(),
//                                                        po.get_TrxName())).getCurSymbol())
//                                .append(" ").append(ResponseData
//                                        .toRupiahFormat(invoice.getGrandTotal().toString()));
//                    } else {
//                        sb.append(invoice.getDescription());
//                    }
//                } catch (AdempiereException e) {
//                    throw new AdempiereException(e);
//                } catch (Exception e) {
//                    throw new Exception(e);
//                }
//            }
//        } else if (po.get_TableName().equalsIgnoreCase(MPayment.Table_Name)) {
//            index = po.get_ColumnIndex(MPayment.COLUMNNAME_C_Payment_ID);
//            if (index != -1) {
//                try {
//                    MPayment payment = ForcaObject.getObject(po.getCtx(), MPayment.Table_Name,
//                            ((Integer) po.get_Value(index)).intValue(), po.get_TrxName());
//                    if (tipe.equals("price")) {
//                        sb.append("Amount: ")
//                                .append(((MCurrency) ForcaObject.getObject(po.getCtx(),
//                                        MCurrency.Table_Name, payment.getC_Currency_ID(),
//                                        po.get_TrxName())).getCurSymbol() == null
//                                                ? ""
//                                                : ((MCurrency) ForcaObject.getObject(po.getCtx(),
//                                                        MCurrency.Table_Name,
//                                                        payment.getC_Currency_ID(),
//                                                        po.get_TrxName())).getCurSymbol())
//                                .append(" ").append(ResponseData
//                                        .toRupiahFormat(payment.getPayAmt().toString()));
//                    } else {
//                        sb.append(payment.getDescription());
//                    }
//                } catch (AdempiereException e) {
//                    throw new AdempiereException(e);
//                } catch (Exception e) {
//                    throw new Exception(e);
//                }
//            }
//        } else if (po.get_TableName().equalsIgnoreCase("FORCA_SPPD")) {
//            index = po.get_ColumnIndex("FORCA_SPPD_ID");
//            if (index != -1) {
//                List<Object> params = new ArrayList<>();
//                PreparedStatement stmt = null;
//                ResultSet rs = null;
//
//                // @formatter:off
//                String sql = "" 
//                        + " SELECT " 
//                        + "     grandtotal," 
//                        + "     description"
//                        + " FROM " 
//                        + "     FORCA_SPPD " 
//                        + " WHERE "
//                        + "     ad_client_id =?"
//                        + "     AND ad_user_id = ? " 
//                        + "     AND FORCA_SPPD_ID =? "
//                        + "     AND documentno =? ";
//                params.add(po.get_ValueAsInt("AD_Client_ID"));
//                params.add(po.get_ValueAsInt("AD_User_ID"));
//                params.add(((Integer) po.get_Value(index)).intValue());
//                params.add(po.get_Value("DocumentNo").toString());
//                // @formatter:on
//
//                try {
//                    stmt = DB.prepareStatement(sql, po.get_TrxName());
//                    stmt = ProcessUtil.setMultiParam(stmt, params);
//                    rs = stmt.executeQuery();
//
//                    String grndtotal = "";
//                    String spdesc = "";
//
//                    if (rs.next()) {
//                        grndtotal = rs.getString("grandtotal");
//                        spdesc = rs.getString("description");
//                    }
//
//                    if (tipe.equals("price")) {
//                        sb.append("Amount:");
//                        sb.append(" ").append(ResponseData.toRupiahFormat(grndtotal.toString()));
//                    } else {
//                        sb.append(spdesc);
//                    }
//                } catch (SQLException e) {
//                    throw new SQLException(e);
//                } catch (Exception e) {
//                    throw new Exception(e);
//                } finally {
//                    DB.close(rs, stmt);
//                    rs = null;
//                    stmt = null;
//                }
//            }
//        } else if (po.get_TableName().equalsIgnoreCase("FORCA_PrepaymentRequest")) {
//            index = po.get_ColumnIndex("FORCA_PrepaymentRequest_ID");
//            if (index != -1) {
//                List<Object> params = new ArrayList<>();
//                PreparedStatement stmt = null;
//                ResultSet rs = null;
//
//                // @formatter:off
//                String sql = "" 
//                        + " SELECT " 
//                        + "     grandtotal," 
//                        + "     description"
//                        + " FROM " 
//                        + "     FORCA_PrepaymentRequest " 
//                        + " WHERE "
//                        + "     ad_client_id =?"
//                        + "     AND FORCA_PrepaymentRequest_ID =? "
//                        + "     AND documentno =? ";
//                params.add(po.get_ValueAsInt("AD_Client_ID"));
//                params.add(((Integer) po.get_Value(index)).intValue());
//                params.add(po.get_Value("DocumentNo").toString());
//                // @formatter:on
//
//                try {
//                    stmt = DB.prepareStatement(sql, po.get_TrxName());
//                    stmt = ProcessUtil.setMultiParam(stmt, params);
//                    rs = stmt.executeQuery();
//
//                    String grndtotal = "";
//                    String spdesc = "";
//
//                    if (rs.next()) {
//                        grndtotal = rs.getString("grandtotal");
//                        spdesc = rs.getString("description");
//                    }
//
//                    if (tipe.equals("price")) {
//                        sb.append("Amount:");
//                        sb.append(" ").append(ResponseData.toRupiahFormat(grndtotal.toString()));
//                    } else {
//                        sb.append(spdesc);
//                    }
//                } catch (SQLException e) {
//                    throw new SQLException(e);
//                } catch (Exception e) {
//                    throw new Exception(e);
//                } finally {
//                    DB.close(rs, stmt);
//                    rs = null;
//                    stmt = null;
//                }
//            }
//        } else if (po.get_TableName().equalsIgnoreCase("ZDPRequest")) {
//            index = po.get_ColumnIndex("ZDPRequest_ID");
//            if (index != -1) {
//                List<Object> params = new ArrayList<>();
//                PreparedStatement stmt = null;
//                ResultSet rs = null;
//
//                // @formatter:off
//                String sql = "" 
//                        + " SELECT " 
//                        + "     grandtotal," 
//                        + "     c_currency_id," 
//                        + "     description"
//                        + " FROM " 
//                        + "     ZDPRequest " 
//                        + " WHERE "
//                        + "     ad_client_id =?"
//                        + "     AND ZDPRequest_ID =? "
//                        + "     AND documentno =? ";
//                params.add(po.get_ValueAsInt("AD_Client_ID"));
//                params.add(((Integer) po.get_Value(index)).intValue());
//                params.add(po.get_Value("DocumentNo").toString());
//                // @formatter:on
//
//                try {
//                    stmt = DB.prepareStatement(sql, po.get_TrxName());
//                    stmt = ProcessUtil.setMultiParam(stmt, params);
//                    rs = stmt.executeQuery();
//
//                    String grndtotal = "";
//                    String spdesc = "";
//                    MCurrency cur = null;
//
//                    if (rs.next()) {
//                        grndtotal = rs.getString("grandtotal");
//                        spdesc = rs.getString("description");
//                        cur = ForcaObject.getObject(po.getCtx(), MCurrency.Table_Name,
//                                rs.getInt("c_currency_id"), null);
//                    }
//
//                    if (tipe.equals("price")) {
//                        sb.append("Amount: ")
//                                .append(cur.getCurSymbol() == null ? "" : cur.getCurSymbol())
//                                .append(" ").append(ResponseData.toRupiahFormat(grndtotal));
//                    } else {
//                        sb.append(spdesc);
//                    }
//                } catch (SQLException e) {
//                    throw new SQLException(e);
//                } catch (Exception e) {
//                    throw new Exception(e);
//                } finally {
//                    DB.close(rs, stmt);
//                    rs = null;
//                    stmt = null;
//                }
//            }
//        } else if (po.get_TableName().equalsIgnoreCase(MProject.Table_Name)) {
//            index = po.get_ColumnIndex(MProject.COLUMNNAME_C_Project_ID);
//            if (index != -1) {
//                try {
//                    MProject project = ForcaObject.getObject(po.getCtx(), MProject.Table_Name,
//                            ((Integer) po.get_Value(index)).intValue(), po.get_TrxName());
//                    if (tipe.equals("price")) {
//                        sb.append("Amount: ")
//                                .append(((MCurrency) ForcaObject.getObject(po.getCtx(),
//                                        MCurrency.Table_Name, project.getC_Currency_ID(),
//                                        po.get_TrxName())).getCurSymbol() == null
//                                                ? ""
//                                                : ((MCurrency) ForcaObject.getObject(po.getCtx(),
//                                                        MCurrency.Table_Name,
//                                                        project.getC_Currency_ID(),
//                                                        po.get_TrxName())).getCurSymbol())
//                                .append(" ").append(ResponseData
//                                        .toRupiahFormat(project.getPlannedAmt().toString()));
//                    } else {
//                        sb.append(project.getDescription());
//                    }
//                } catch (AdempiereException e) {
//                    throw new AdempiereException(e);
//                } catch (Exception e) {
//                    throw new Exception(e);
//                }
//            }
//        }
//        return sb.toString();
//    } // getDescDetail
//
//
//    public List<Object> getDataByTableNameRecord(String ad_table_id, String record_id,
//            String trxName) throws Exception {
//        List<Object> result = new ArrayList<>();
//        List<Object> params = new ArrayList<>();
//        MTable table = ForcaObject.getObject(Env.getCtx(), MTable.Table_Name,
//                Integer.valueOf(ad_table_id), trxName);
//
//
//        MColumn cDocType = new Query(Env.getCtx(), MColumn.Table_Name,
//                "AD_Table_ID=? AND ColumnName='C_DocType_ID'", trxName)
//                        .setParameters(Integer.valueOf(ad_table_id)).first();
//        MColumn cDocumentNo = new Query(Env.getCtx(), MColumn.Table_Name,
//                "AD_Table_ID=? AND ColumnName='DocumentNo'", trxName)
//                        .setParameters(Integer.valueOf(ad_table_id)).first();
//        MColumn cBPartnerId = new Query(Env.getCtx(), MColumn.Table_Name,
//                "AD_Table_ID=? AND ColumnName='C_BPartner_ID'", trxName)
//                        .setParameters(Integer.valueOf(ad_table_id)).first();
//        MColumn cDateAcct = new Query(Env.getCtx(), MColumn.Table_Name,
//                "AD_Table_ID=? AND ColumnName='DateAcct'", trxName)
//                        .setParameters(Integer.valueOf(ad_table_id)).first();
//        MColumn cDateDoc = new Query(Env.getCtx(), MColumn.Table_Name,
//                "AD_Table_ID=? AND ColumnName='DateDoc'", trxName)
//                        .setParameters(Integer.valueOf(ad_table_id)).first();
//        MColumn cGrandTotal = new Query(Env.getCtx(), MColumn.Table_Name,
//                "AD_Table_ID=? AND ColumnName='GrandTotal'", trxName)
//                        .setParameters(Integer.valueOf(ad_table_id)).first();
//        MColumn cPayAmt =
//                new Query(Env.getCtx(), MColumn.Table_Name, "AD_Table_ID=? AND ColumnName='PayAmt'",
//                        trxName).setParameters(Integer.valueOf(ad_table_id)).first();
//        MColumn cCurrency = new Query(Env.getCtx(), MColumn.Table_Name,
//                "AD_Table_ID=? AND ColumnName='C_Currency_ID'", trxName)
//                        .setParameters(Integer.valueOf(ad_table_id)).first();
//
//        String select = " select coalesce(a.description,'') description ";
//        String join = " from " + table.getTableName() + " a";
//
//        if (cDocType != null) {
//            select += " ,coalesce(b.name,'') doctype ";
//            join += " left join c_doctype b on b.c_doctype_id = a.c_doctype_id ";
//        } else {
//            select += " ,'' doctype ";
//        }
//        if (cDocumentNo != null) {
//            select += " ,a.documentno ";
//        } else {
//            select += " ,'' document_no ";
//        }
//        if (cBPartnerId != null) {
//            select += " ,c.name bpartner ";
//            join += " left join c_bpartner c on c.c_bpartner_id = a.c_bpartner_id ";
//        } else {
//            select += " ,'' bpartner ";
//        }
//        if (cDateAcct != null) {
//            select += " ,to_char(a.dateacct,'DD-MM-YYYY') docdate ";
//        } else if (cDateDoc != null) {
//            select += " ,to_char(a.datedoc,'DD-MM-YYYY') docdate ";
//        } else {
//            select += " ,'' docdate ";
//        }
//
//        String cur = "''";
//        if (cCurrency != null) {
//            cur = "d.cursymbol";
//            join += " left join c_currency d on d.c_currency_id = a.c_currency_id ";
//        }
//        if (cGrandTotal != null) {
//            select += " ,concat(" + cur
//                    + ",' ',trim(replace(to_char(a.grandtotal,'999,999,999,999,999'),',','.')),',00') grandtotal ";
//        } else if (cPayAmt != null) {
//            select += " ,concat(" + cur
//                    + ",' ',trim(replace(to_char(a.payamt,'999,999,999,999,999'),',','.')),',00') grandtotal ";
//        } else {
//            select += " ,'' grandtotal ";
//        }
//
//        String sql = select + join + " " + " where " + "     a." + table.getTableName() + "_ID = ? "
//                + "     AND a.ad_client_id = ? ";
//        params.add(Integer.valueOf(record_id));
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            ps = DB.prepareStatement(sql, trxName);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            rs = ps.executeQuery();
//
//            if (rs.next()) {
//                result.add(rs.getString(1));
//                result.add(rs.getString(2));
//                result.add(rs.getString(3));
//                result.add(rs.getString(4));
//                result.add(rs.getString(5));
//                result.add(rs.getString(6));
//                result.add(getDataLine(ad_table_id, record_id, trxName));
//            }
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql, e);
//            throw new SQLException(e);
//        } catch (Exception e) {
//            throw new SQLException(e);
//        } finally {
//            DB.close(rs, ps);
//            rs = null;
//            ps = null;
//        }
//        return result;
//    } // getDataByTableNameRecord
//
//
//    public String detailHistory(int WfProsesid, String trxName) throws Exception {
//        StringBuilder sb = new StringBuilder();
//        try {
//            MWFEventAudit[] events = MWFEventAudit.get(Env.getCtx(), WfProsesid, null);
//            for (int i = 0; i < events.length; i++) {
//                MWFEventAudit audit = events[i];
//                if (audit.getWFState().equals("CC")) {
//                    MUser user = ForcaObject.getObject(Env.getCtx(), MUser.Table_Name,
//                            audit.getAD_User_ID(), trxName);
//                    MWFNode node = ForcaObject.getObject(audit.getCtx(), MWFNode.Table_Name,
//                            audit.getAD_WF_Node_ID(), null);
//                    String tgl = audit.getCreated().toString();
//                    sb.append(tgl.substring(0, 16)).append(" :").append(user.getDescription())
//                            .append(" (").append(node.getName()).append(")");
//                    sb.append("<br/>");
//                }
//            }
//        } catch (AdempiereException e) {
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            throw new Exception(e);
//        }
//        return sb.toString();
//    } // detailHistory
//
//
//    public Object getDataLine(String ad_table_id, String record_id, String trxName)
//            throws Exception {
//        MTable t = ForcaObject.getObject(Env.getCtx(), MTable.Table_Name,
//                Integer.valueOf(ad_table_id), trxName);
//        List<Map<String, String>> resultList = new ArrayList<>();
//        List<Object> params = new ArrayList<>();
//
//        String whereClause = " TableName = '" + t.getTableName() + "Line' ";
//        MTable tLine = new Query(Env.getCtx(), MTable.Table_Name, whereClause, trxName).first();
//
//        StringBuilder select = new StringBuilder();
//        StringBuilder from = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//
//        if (tLine != null) {
//            MColumn cCurrency = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='C_Currency_ID'", trxName)
//                            .setParameters(Integer.valueOf(ad_table_id)).first();
//
//            MColumn product = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='M_Product_ID'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn uom = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='C_UOM_ID'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn qty_entered = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='QtyEntered'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn qty = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='Qty'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn movementqty = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='MovementQty'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn price_entered = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='PriceEntered'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn price_actual = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='PriceActual'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn price = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='Price'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn description = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='Description'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn line = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='Line'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn linenetamt = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='LineNetAmt'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn linetotalamt = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='LineTotalAmt'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn datefrom = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='DateFrom'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn dateto = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='DateTo'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn charge = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='C_Charge_ID'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn locatorFrom = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='M_Locator_ID'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn locatorTo = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='M_LocatorTo_ID'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn qtyinternaluse = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='QtyInternalUse'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn tax = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='C_Tax_ID'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//            MColumn taxAmt = new Query(Env.getCtx(), MColumn.Table_Name,
//                    "AD_Table_ID=? AND ColumnName='TaxAmt'", trxName)
//                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
//
//            select.append(" select ");
//            if (line != null) {
//                select.append(" a.line ");
//            } else {
//                select.append(" a.lineno line");
//            }
//            from.append(" from " + tLine.getTableName() + " a");
//            if (product != null) {
//                select.append(" ,coalesce(b.name,'') product ");
//                from.append(
//                        " left join m_product b on b.m_product_id = a.m_product_id and b.ad_client_id = a.ad_client_id ");
//            } else {
//                select.append(" ,'' product");
//            }
//            if (uom != null) {
//                select.append(" ,coalesce(c.name,'') uom ");
//                from.append(
//                        " left join c_uom c on c.c_uom_id = a.c_uom_id and c.ad_client_id = a.ad_client_id  ");
//            } else {
//                select.append(" ,'' uom");
//            }
//            if (qtyinternaluse != null) {
//                select.append(
//                        ",trim(replace(to_char(coalesce(a.qtyinternaluse,0),'999,999,999,999,999'),',','.')) qty");
//            } else if (qty_entered != null) {
//                select.append(
//                        ",trim(replace(to_char(coalesce(a.qtyentered,0),'999,999,999,999,999'),',','.')) qty");
//            } else if (qty != null) {
//                select.append(
//                        ",trim(replace(to_char(coalesce(a.qty,0),'999,999,999,999,999'),',','.')) qty");
//            } else if (movementqty != null) {
//                select.append(
//                        ",trim(replace(to_char(coalesce(a.movementqty,0),'999,999,999,999,999'),',','.')) qty");
//            } else {
//                select.append(" ,'' qty");
//            }
//
//            String cur = "''";
//            if (cCurrency != null) {
//                cur = "e.cursymbol";
//                from.append(" left join " + t.getTableName() + " d on d." + t.getTableName()
//                        + "_ID = a." + t.getTableName() + "_ID ");
//                from.append(" left join c_currency e on e.c_currency_id = d.c_currency_id ");
//            }
//            if (price_entered != null) {
//                select.append(" ,concat(" + cur
//                        + ",' ',trim(replace(to_char(a.priceentered,'999,999,999,999,999'),',','.')),',00') price ");
//            } else if (price_actual != null) {
//                select.append(" ,concat(" + cur
//                        + ",' ',trim(replace(to_char(a.priceactual,'999,999,999,999,999'),',','.')),',00') price ");
//            } else if (price != null) {
//                select.append(" ,concat(" + cur
//                        + ",' ',trim(replace(to_char(a.price,'999,999,999,999,999'),',','.')),',00') price ");
//            } else {
//                select.append(" ,'' price ");
//            }
//            if (description != null) {
//                select.append(" ,coalesce(a.description,'') description ");
//            } else {
//                select.append(" ,'' description ");
//            }
//            if (linenetamt != null) {
//                select.append(" ,concat(" + cur
//                        + ",' ',trim(replace(to_char(a.linenetamt,'999,999,999,999,999'),',','.')),',00') subtotal ");
//            } else if (linetotalamt != null) {
//                select.append(" ,concat(" + cur
//                        + ",' ',trim(replace(to_char(a.linetotalamt,'999,999,999,999,999'),',','.')),',00') subtotal ");
//            } else {
//                select.append(" ,'' subtotal ");
//            }
//            if (datefrom != null) {
//                select.append(" ,to_char(a.datefrom,'DD-MM-YYYY') datefrom ");
//            } else {
//                select.append(" ,'' datefrom ");
//            }
//            if (dateto != null) {
//                select.append(" ,to_char(a.dateto,'DD-MM-YYYY') dateto ");
//            } else {
//                select.append(" ,'' dateto ");
//            }
//            if (locatorFrom != null) {
//                select.append(" ,coalesce(f.value,'') locator_from ");
//                from.append(
//                        " left join m_locator f on f.m_locator_id = a.m_locator_id and f.ad_client_id = a.ad_client_id ");
//            } else {
//                select.append(" ,'' locator_from");
//            }
//            if (locatorTo != null) {
//                select.append(" ,coalesce(g.value,'') locator_to ");
//                from.append(
//                        " left join m_locator g on g.m_locator_id = a.m_locatorto_id and g.ad_client_id = a.ad_client_id ");
//            } else {
//                select.append(" ,'' locator_to");
//            }
//            if (charge != null) {
//                select.append(" ,coalesce(h.name,'') charge ");
//                from.append(
//                        " left join c_charge h on h.c_charge_id = a.c_charge_id and h.ad_client_id = a.ad_client_id ");
//            } else {
//                select.append(" ,'' charge");
//            }
//            if (tax != null) {
//                select.append(" ,coalesce(i.description,'') tax ");
//                from.append(
//                        " left join c_tax i on i.c_tax_id = a.c_tax_id and i.ad_client_id = a.ad_client_id ");
//            } else {
//                select.append(" ,'' tax");
//            }
//            if (taxAmt != null) {
//                select.append(" ,concat(" + cur
//                        + ",' ',trim(replace(to_char(coalesce(a.taxamt,0),'999,999,999,999,999'),',','.')),',00') taxamt ");
//            } else {
//                select.append(" ,'' taxamt");
//            }
//
//
//            whereCond.append(" " + " where " + "     a." + t.getTableName() + "_ID = ? "
//                    + "     AND a.ad_client_id = ? ");
//            String sql = select.toString() + from.toString() + whereCond.toString();
//            params.add(Integer.valueOf(record_id));
//            params.add(Env.getAD_Client_ID(Env.getCtx()));
//            PreparedStatement ps = null;
//            ResultSet rs = null;
//
//            try {
//                ps = DB.prepareStatement(sql, trxName);
//                ps = ProcessUtil.setMultiParam(ps, params);
//                rs = ps.executeQuery();
//                while (rs.next()) {
//                    Map<String, String> map = new LinkedHashMap<>();
//                    StringBuilder key = new StringBuilder();
//                    StringBuilder value = new StringBuilder();
//
//                    String sLine = rs.getString("line");
//                    String sProduct = rs.getString("product");
//                    String sUom = rs.getString("uom");
//                    String sQty = rs.getString("qty");
//                    String sPrice = rs.getString("price");
//                    String sDescription = rs.getString("description");
//                    String sSubtotal = rs.getString("subtotal");
//                    String sCharge = rs.getString("charge");
//                    String sTax = rs.getString("tax");
//                    String sTaxamt = rs.getString("taxamt");
//                    String sDatefrom = rs.getString("datefrom");
//                    String sDateto = rs.getString("dateto");
//                    String sLocatorfrom = rs.getString("locator_from");
//                    String sLocatorto = rs.getString("locator_to");
//
//                    switch (tLine.getTableName()) {
//                        case ("M_RequisitionLine"):
//                            key.append("line|product|uom|qty|price|description|subtotal");
//                            value.append(sLine);
//                            value.append("|");
//                            value.append(sProduct);
//                            value.append("|");
//                            value.append(sUom);
//                            value.append("|");
//                            value.append(sQty);
//                            value.append("|");
//                            value.append(sPrice);
//                            value.append("|");
//                            value.append(sDescription);
//                            value.append("|");
//                            value.append(sSubtotal);
//                            break;
//                        case "C_OrderLine":
//                            key.append(
//                                    "line|product/charge|uom|qty|price|tax|description|subtotal");
//                            value.append(sLine);
//                            value.append("|");
//                            if (!Util.isEmpty(sProduct)) {
//                                value.append(sProduct);
//                            } else {
//                                value.append(sCharge);
//                            }
//                            value.append("|");
//                            value.append(sUom);
//                            value.append("|");
//                            value.append(sQty);
//                            value.append("|");
//                            value.append(sPrice);
//                            value.append("|");
//                            value.append(sTax);
//                            value.append("|");
//                            value.append(sDescription);
//                            value.append("|");
//                            value.append(sSubtotal);
//                            break;
//                        case "M_InOutLine":
//                            key.append("line|product/charge|uom|qty|description");
//                            value.append(sLine);
//                            value.append("|");
//                            if (!Util.isEmpty(sProduct)) {
//                                value.append(sProduct);
//                            } else {
//                                value.append(sCharge);
//                            }
//                            value.append("|");
//                            value.append(sUom);
//                            value.append("|");
//                            value.append(sQty);
//                            value.append("|");
//                            value.append(sDescription);
//                            break;
//                        case "FORCA_SPPDLine":
//                            key.append(
//                                    "line|product|uom|qty|price|description|subtotal|date from|date to");
//                            value.append(sLine);
//                            value.append("|");
//                            value.append(sProduct);
//                            value.append("|");
//                            value.append(sUom);
//                            value.append("|");
//                            value.append(sQty);
//                            value.append("|");
//                            value.append(sPrice);
//                            value.append("|");
//                            value.append(sDescription);
//                            value.append("|");
//                            value.append(sSubtotal);
//                            value.append("|");
//                            value.append(sDatefrom);
//                            value.append("|");
//                            value.append(sDateto);
//                            break;
//                        case "C_InvoiceLine":
//                            key.append(
//                                    "line|product/charge|uom|tax|tax amt|qty|price|description|subtotal");
//                            value.append(sLine);
//                            value.append("|");
//                            if (!Util.isEmpty(sProduct)) {
//                                value.append(sProduct);
//                            } else {
//                                value.append(sCharge);
//                            }
//                            value.append("|");
//                            value.append(sUom);
//                            value.append("|");
//                            value.append(sTax);
//                            value.append("|");
//                            value.append(sTaxamt);
//                            value.append("|");
//                            value.append(sQty);
//                            value.append("|");
//                            value.append(sPrice);
//                            value.append("|");
//                            value.append(sDescription);
//                            value.append("|");
//                            value.append(sSubtotal);
//                            break;
//                        case "M_MovementLine":
//                            key.append("line|product|qty|description|locator from|locator to");
//                            value.append(sLine);
//                            value.append("|");
//                            value.append(sProduct);
//                            value.append("|");
//                            value.append(sQty);
//                            value.append("|");
//                            value.append(sDescription);
//                            value.append("|");
//                            value.append(sLocatorfrom);
//                            value.append("|");
//                            value.append(sLocatorto);
//                            break;
//                        case "M_InventoryLine":
//                            key.append("line|product|charge|uom|locator|qty|description");
//                            value.append(sLine);
//                            value.append("|");
//                            value.append(sProduct);
//                            value.append("|");
//                            value.append(sCharge);
//                            value.append("|");
//                            value.append(sUom);
//                            value.append("|");
//                            value.append(sLocatorfrom);
//                            value.append("|");
//                            value.append(sQty);
//                            value.append("|");
//                            value.append(sDescription);
//                            break;
//                        default:
//                            break;
//                    }
//
//                    if (key.length() > 0) {
//                        map.put("column", key.toString());
//                        map.put("value", value.toString());
//                        resultList.add(map);
//                    }
//                }
//            } catch (SQLException e) {
//                log.log(Level.SEVERE, sql, e);
//                throw new SQLException(e);
//            } catch (Exception e) {
//                throw new Exception(e);
//            } finally {
//                DB.close(rs);
//                ps = null;
//                rs = null;
//            }
//        }
//        return resultList;
//    } // getDataLine
//
//    // 17550
//    public Object getAdditionalData(String ad_table_id, String record_id, String trxName)
//            throws Exception {
//        List<Object> result = new ArrayList<>();
//        //@formatter:off
//        String tableName = DB.getSQLValueStringEx(
//                trxName, 
//                "SELECT " + 
//                "    lower(tablename) tablename " + 
//                "FROM ad_table " + 
//                "WHERE ad_table_id = ?", 
//                Integer.valueOf(ad_table_id));
//        //@formatter:on
//        List<Object> params = new ArrayList<>();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        StringBuilder sql = new StringBuilder();
//        ProcessUtil pu = new ProcessUtil();
//        if (tableName.equalsIgnoreCase("frc_hr_overtimeworks")) {
//            //@formatter:off
//            sql.append(
//                    "SELECT " + 
//                    "    totalqty, " + 
//                    "    CASE " + 
//                    "        WHEN extract(dow from dateworkstart) = 0 THEN 'Minggu' " + 
//                    "        WHEN extract(dow from dateworkstart) = 1 THEN 'Senin' " + 
//                    "        WHEN extract(dow from dateworkstart) = 2 THEN 'Selasa' " + 
//                    "        WHEN extract(dow from dateworkstart) = 3 THEN 'Rabu' " + 
//                    "        WHEN extract(dow from dateworkstart) = 4 THEN 'Kamis' " + 
//                    "        WHEN extract(dow from dateworkstart) = 5 THEN 'Jumat' " + 
//                    "        WHEN extract(dow from dateworkstart) = 6 THEN 'Sabtu' " + 
//                    "    END || ', ' || to_char(dateworkstart,'DD-MM-YYYY') hari " + 
//                    "FROM frc_hr_overtimeworks " + 
//                    "WHERE frc_hr_overtimeworks_id = ?");
//            params.add(Integer.valueOf(record_id));
//            //@formatter:on
//
//            try {
//                ps = DB.prepareStatement(sql.toString(), trxName);
//                ps = ProcessUtil.setMultiParam(ps, params);
//                ps.setFetchSize(100);
//                rs = ps.executeQuery();
//                while (rs.next()) {
//                    LinkedHashMap<String, Object> map = new LinkedHashMap<>();
//                    StringBuilder column = new StringBuilder();
//                    StringBuilder value = new StringBuilder();
//                    column.append("Qty Overtime|Hari Overtime");
//                    value.append(String.valueOf(rs.getInt("totalqty")));
//                    value.append("|");
//                    value.append(rs.getString("hari"));
//                    map.put("column", column.toString());
//                    map.put("value", value.toString());
//                    result.add(map);
//                }
//            } catch (SQLException e) {
//                log.log(Level.SEVERE, sql.toString(), e);
//                throw new SQLException(e);
//            } catch (Exception e) {
//                throw new Exception(e);
//            } finally {
//                DB.close(rs, ps);
//                rs = null;
//                ps = null;
//            }
//        }
//
//        return result;
//    }
    // 17550 - end

}
