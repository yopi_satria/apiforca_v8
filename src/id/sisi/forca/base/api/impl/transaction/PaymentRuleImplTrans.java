package id.sisi.forca.base.api.impl.transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import id.sisi.forca.base.api.util.ProcessUtil;

public class PaymentRuleImplTrans {
    private CLogger log = CLogger.getCLogger(getClass());

    public String getPaymentRule(String trxName) throws Exception {
        String result = "";
        List<Object> params = new ArrayList<>();
        
        // @formatter:off
        String sql = ""
                + "SELECT " 
                + "    b.value " 
                + "FROM "
                + "     AD_Reference a " 
                + "LEFT JOIN AD_Ref_List b ON "
                + "     b.AD_Reference_ID = a.AD_Reference_ID " 
                + "WHERE "
                + "     LOWER(a.Name) like LOWER('%_Payment Rule%') " 
                + "     AND LOWER(b.Name) LIKE LOWER('%Check%') "
                + "     AND a.ad_client_id = ? "
                + "     AND a.ad_user_id = ?";
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        params.add(Env.getAD_User_ID(Env.getCtx()));
        // @formatter:on

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DB.prepareStatement(sql, trxName);
            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getString(1);
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql, e);
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return result;
    }
    
    public String getPaymentRuleName(String value, String trxName) throws Exception {
        String result = "";
        List<Object> params = new ArrayList<>();
        StringBuilder sql = new StringBuilder();
        
        // @formatter:off
        sql.append(" SELECT " 
                 + "    b.name " 
                 + " FROM "
                 + "     AD_Reference a " 
                 + " LEFT JOIN AD_Ref_List b ON "
                 + "     b.AD_Reference_ID = a.AD_Reference_ID " 
                 + " WHERE "
                 + "     LOWER(a.Name) like LOWER('%_Payment Rule%') "
                 + "     AND LOWER(b.value) like LOWER (?) " 
                 + "     AND a.ad_client_id = 0 ");
        params.add(value);
        // @formatter:on

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getString("name");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql.toString(), e);
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return result;
    }
}
