/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : Workflow Implement Transaction                              
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.impl.transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MUser;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;
//import org.idempiere.model.ForcaObject;
import id.sisi.forca.base.api.util.ProcessUtil;

public class WorkflowImplTrans {
    private CLogger log = CLogger.getCLogger(getClass());

    public String getSqlAllWorkflow() {
        String psql = getSqlWorkflow(null);
        return psql;
    }

    public String getSqlByWfName(String wfName, Integer wfnodeid) {
        //@formatter:off
        String where = ""
                + " and B.name = ? " 
                + " and c.ad_wf_node_id = ? ";
        //@formatter:on
        String psql = getSqlWorkflow(where);
        return psql;
    }

    public String getSqlWfDetail(String wfName, Integer wfID) {
        //@formatter:off
        String where = ""
                + " and B.name = ?"
                + " and a.ad_wf_activity_id = ?";
        //@formatter:on
        String psql = getSqlWorkflow(where);
        return psql;
    }

    private String getSqlWorkflow(String customWhere) {
        if (customWhere == null || customWhere.isEmpty()) {
            customWhere = "";
        }
        //@formatter:off
        final String where = ""
                + " a.Processed='N' "
                + " AND a.WFState='OS' "
                // Owner of Activity
                + " AND ( "
                + " a.AD_User_ID=? "  // #1
                // Invoker (if no invoker = all)
                + " OR EXISTS ( "
                + "     SELECT "
                + "         * "
                + "     FROM "
                + "         AD_WF_Responsible r "
                + "     WHERE "
                + "         a.AD_WF_Responsible_ID=r.AD_WF_Responsible_ID "
                + "         AND r.ResponsibleType='H' "
                + "         AND COALESCE(r.AD_User_ID,0)=0 "
                + "         AND COALESCE(r.AD_Role_ID,0)=0 "
                + "         AND (a.AD_User_ID=? OR a.AD_User_ID IS NULL)) " // #2
                // Responsible User
                + " OR EXISTS ( "
                + "     SELECT "
                + "         * "
                + "     FROM "
                + "         AD_WF_Responsible r "
                + "     WHERE "
                + "     a.AD_WF_Responsible_ID=r.AD_WF_Responsible_ID "
                + "     AND r.ResponsibleType='H' AND r.AD_User_ID=?) " // #3
                // Responsible Role
                + " OR EXISTS ("
                + "     SELECT "
                + "         * "
                + "     FROM "
                + "         AD_WF_Responsible r "
                + "     INNER JOIN AD_User_Roles ur ON "
                + "         (r.AD_Role_ID=ur.AD_Role_ID)"
                + "     WHERE "
                + "         a.AD_WF_Responsible_ID=r.AD_WF_Responsible_ID "
                + "         AND r.ResponsibleType='R' "
                + "         AND ur.AD_User_ID=?)) " // #4
                + " AND a.AD_Client_ID=? "; // #5


        String xsql = ""
                + "select " 
                + "    a.ad_wf_activity_id, " 
                + "    b.name as wfname, "
                + "    b.VALUE as wfdesc, " 
                + "    a.ad_client_id, " 
                + "    a.ad_org_id, "
                + "    a.isactive, " 
                + "    a.created, " 
                + "    a.createdby, " 
                + "    a.updated, "
                + "    a.updatedby, " 
                + "    a.ad_wf_process_id, " 
                + "    a.ad_wf_node_id, "
                + "    a.ad_wf_responsible_id, " 
                + "    a.ad_user_id, " 
                + "    a.wfstate, "
                + "    a.ad_message_id, " 
                + "    a.processing, " 
                + "    a.processed, "
                + "    coalesce(a.textmsg,'') textmsg, " 
                + "    a.ad_workflow_id, " 
                + "    a.ad_table_id, "
                + "    a.record_id, " 
                + "    a.priority, " 
                + "    a.endwaittime, "
                + "    a.datelastalert, " 
                + "    a.dynprioritystart, " 
                + "    a.ad_wf_activity_uu, "
                + "    C.NAME as nodename, " 
                + "    coalesce(D.textmsg,'') as approve_dtl " 
                + "from "
                + "    AD_WF_Activity A " 
                + "left join ad_workflow b on "
                + "    A.ad_workflow_id = b.ad_workflow_id " 
                + "left join ad_wf_node C on "
                + "    A.ad_wf_node_id = C.ad_wf_node_id " 
                + "left join ad_wf_process D on "
                + "    A.ad_wf_process_id = D.ad_wf_process_id " 
                + "where " 
                + where 
                + customWhere
                + "order by " 
                + "    a.Priority desc, " 
                + "    a.created ";
        //@formatter:on
        return xsql;
    }

    public List<Object> getSqlGrupWorkflow(Integer ad_client_id, Integer ad_user_id,
            String inwflow, String trxName) throws Exception {
        List<Object> params = new ArrayList<>();
        List<Object> result = new ArrayList<Object>();
        String inwhere = "";
        
        if (!Util.isEmpty(inwflow)) {
            inwhere = " and a.ad_wf_activity_id in (" + inwflow + ") ";
        } else {
            inwhere = " and a.ad_wf_activity_id = 0 ";
        }
        //@formatter:off
        String psql = " "
                + " SELECT "
                + "     wfname,"
                + "     nodename,"
                + "     wfdesc,"
                + "     ad_workflow_id,"
                + "     ad_wf_node_id, "
                + "     count(*) as total "
                + " FROM ( " 
                + getSqlWorkflow(inwhere)
                + " ) a "
                + " GROUP BY "
                + "     wfname,"
                + "     wfdesc,"
                + "     ad_workflow_id,"
                + "     nodename,"
                + "     ad_wf_node_id ";
        //@formatter:on
        
        params.add(ad_user_id);
        params.add(ad_user_id);
        params.add(ad_user_id);
        params.add(ad_user_id);
        params.add(ad_client_id);
        PreparedStatement pstmtx = null;
        ResultSet rsx = null;
        try {
            pstmtx = DB.prepareStatement(psql, trxName);
            pstmtx = ProcessUtil.setMultiParam(pstmtx, params);
            if (pstmtx.getConnection().getAutoCommit()) {
                pstmtx.getConnection().setAutoCommit(false);
            }
            pstmtx.setFetchSize(100);
            rsx = pstmtx.executeQuery();
            while (rsx.next()) {
                Map<String, String> map = new LinkedHashMap<String, String>();

                map.put("wfname", rsx.getString("wfname"));
                map.put("nodename", rsx.getString("nodename"));
                map.put("wfdesc", rsx.getString("wfdesc"));
                map.put("ad_workflow_id", rsx.getString("ad_workflow_id"));
                map.put("wfnode", rsx.getString("ad_wf_node_id"));
                map.put("total", rsx.getString("total"));
                result.add(map);
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, psql, e);
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rsx, pstmtx);
            rsx = null;
            pstmtx = null;
        }
        return result;
    }
    
//    public String getWfSummary(PO po, String trxName) throws Exception {
//        if (po == null)
//            return null;
//        
//        StringBuilder sb = new StringBuilder();
//        String[] keyColumns = po.get_KeyColumns();
//        
//        if ((keyColumns != null) && (keyColumns.length > 0))
//            sb.append(Msg.getElement(Env.getCtx(), keyColumns[0])).append(" ");
//        
//        int index = po.get_ColumnIndex("DocumentNo");
//        if (index != -1)
//            sb.append(po.get_Value(index)).append(": ");
//        
//        index = po.get_ColumnIndex("SalesRep_ID");        
//        Integer sr = null;
//        if (index != -1)
//            sr = (Integer) po.get_Value(index);
//        else {
//            index = po.get_ColumnIndex("AD_User_ID");
//            if (index != -1)
//                sr = (Integer) po.get_Value(index);
//        }
//        
//        try {
//            if (sr != null) {
//                MUser user = ForcaObject.getObject(Env.getCtx(), MUser.Table_Name, sr.intValue(), trxName);
//                if (user != null)
//                    sb.append(user.getName()).append(" ");
//            }
//            
//            index = po.get_ColumnIndex("C_BPartner_ID");
//            if (index != -1) {
//                Integer bp = (Integer) po.get_Value(index);
//                if (bp != null) {
//                    MBPartner partner = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name, bp.intValue(), trxName);
//                    if (partner != null)
//                        sb.append(partner.getName()).append(" ");
//                }
//            }
//        } catch (AdempiereException e) {
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            throw new Exception(e);
//        } 
//        
//        return sb.toString();
//    } // getSummary
    
}
