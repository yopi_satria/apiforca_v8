package id.sisi.forca.base.api.impl.transaction;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.compiere.util.Env;

public class CementReceivedImplTrans {

    public Map<String, Object> getSQLCementReceivedBSA(String dateFrom, String dateTo) {
        List<Object> params = new ArrayList<>();
        Map<String, Object> map = new LinkedHashMap<>();

        // @formatter:off
        String sql = " "
                + "SELECT "
                + "    adc.ad_client_id, "
                + "    adc. NAME, "
                + "    org.kode_gudang AS kdshipto, "
                + "    org. NAME AS nmshipto, "
                + "    asi.description AS no_spj, "
                + "    adc.kode_dist AS kd_distributor_si, "
                + "    SUM( mvl.movementqty ) AS movementqty, "
                + "    mp.m_product_id, "
                + "    cu. NAME AS uom, "
                + "    mv.movementdate, "
                + "    mm.value2, "
                + "    mv.movementdate AS datedoc, "
                + "    NULL AS no_do "
                + "FROM "
                + "    m_movement mv "
                + "LEFT JOIN ad_client adc ON "
                + "    adc.ad_client_id = mv.ad_client_id "
                + "LEFT JOIN m_movementline mvl ON "
                + "    mvl.m_movement_id = mv.m_movement_id "
                + "LEFT JOIN m_attributesetinstance asi ON "
                + "    asi.m_attributesetinstance_id = mvl.m_attributesetinstanceto_id "
                + "LEFT JOIN m_locator ml ON "
                + "    ml.m_locator_id = mvl.m_locatorto_id "
                + "LEFT JOIN m_warehouse mw ON "
                + "    mw.m_warehouse_id = ml.m_warehouse_id "
                + "LEFT JOIN ad_org org ON "
                + "    org.ad_org_id = mw.ad_org_id "
                + "LEFT JOIN m_product mp ON "
                + "    mp.m_product_id = mvl.m_product_id "
                + "JOIN FORCA_MapProduct_SIDIGI fm ON "
                + "    fm.m_product_id = mvl.m_product_id "
                + "JOIN M_MappingERPDist mm ON "
                + "    CAST ( COALESCE ( mm. VALUE, "
                + "    '0' ) AS INTEGER ) = mp.m_product_id "
                + "    AND mm. NAME = 'Product' "
                + "LEFT JOIN c_doctype cd ON "
                + "    cd.c_doctype_id = mv.c_doctype_id "
                + "LEFT JOIN c_uom cu ON "
                + "    cu.c_uom_id = mp.c_uom_id "
                + "WHERE "
                + "    mv.ad_user_id =? "
                + "    AND mv.ad_client_id = ? "
                + "    AND mv.docstatus = 'CO' "
                + "    AND cd.issotrx = 'N' "
                + "    AND fm.isactive = 'Y' "
                + "    AND adc.kode_dist IS NOT NULL "
                + "    AND to_char( trunc( mv.movementdate ), "
                + "    'YYYY-MM-DD' ) >= ? "
                + "    AND to_char( trunc( mv.movementdate ), "
                + "    'YYYY-MM-DD' ) <= ? "
                + "GROUP BY "
                + "    adc.ad_client_id, "
                + "    adc. NAME, "
                + "    org.kode_gudang, "
                + "    org. NAME, "
                + "    asi.description, "
                + "    adc.kode_dist, "
                + "    cu. NAME, "
                + "    mv.movementdate, "
                + "    mm.value2, "
                + "    mp.m_product_id "
                + "UNION SELECT "
                + "    adc.ad_client_id, "
                + "    adc. NAME, "
                + "    org.kode_gudang AS kdshipto, "
                + "    mw. NAME AS nmshipto, "
                + "    asi.description AS no_spj, "
                + "    adc.kode_dist AS kd_distributor_si, "
                + "    mil.movementqty, "
                + "    mp.m_product_id, "
                + "    cu. NAME AS uom, "
                + "    mi.movementdate, "
                + "    mm.value2, "
                + "    mi.movementdate AS datedoc, "
                + "    NULL AS no_do "
                + "FROM "
                + "    m_inout mi "
                + "LEFT JOIN ad_client adc ON "
                + "    adc.ad_client_id = mi.ad_client_id "
                + "LEFT JOIN m_inoutline mil ON "
                + "    mil.m_inout_id = mi.m_inout_id "
                + "LEFT JOIN m_attributesetinstance asi ON "
                + "    asi.m_attributesetinstance_id = mil.m_attributesetinstance_id "
                + "LEFT JOIN m_locator ml ON "
                + "    ml.m_locator_id = mil.m_locator_id "
                + "LEFT JOIN m_warehouse mw ON "
                + "    mw.m_warehouse_id = ml.m_warehouse_id "
                + "LEFT JOIN ad_org org ON "
                + "    org.ad_org_id = mw.ad_org_id "
                + "LEFT JOIN m_product mp ON "
                + "    mp.m_product_id = mil.m_product_id "
                + "JOIN FORCA_MapProduct_SIDIGI fm ON "
                + "    fm.m_product_id = mil.m_product_id "
                + "JOIN M_MappingERPDist mm ON "
                + "    CAST ( COALESCE ( mm. VALUE, "
                + "    '0' ) AS INTEGER ) = mp.m_product_id "
                + "    AND mm. NAME = 'Product' "
                + "LEFT JOIN c_doctype cd ON "
                + "    cd.c_doctype_id = mi.c_doctype_id "
                + "LEFT JOIN c_uom cu ON "
                + "    cu.c_uom_id = mp.c_uom_id "
                + "WHERE "
                + "    mi.AD_User_ID =? "
                + "    AND mi.ad_client_id = ? "
                + "    AND mi.docstatus = 'CO' "
                + "    AND cd.issotrx = 'N' "
                + "    AND fm.isactive = 'Y' "
                
                // + "    AND org.kode_gudang NOT IN ( '2430001000' ) " -> sengaja di comment karena hardcode.
                
                + "    AND adc.kode_dist IS NOT NULL "
                + "    AND to_char( trunc( mi.movementdate ), "
                + "    'YYYY-MM-DD' ) >= ? "
                + "    AND to_char( trunc( mi.movementdate ), "
                + "    'YYYY-MM-DD' ) <= ? ";
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        params.add(Env.getAD_User_ID(Env.getCtx()));
        params.add(dateFrom);
        params.add(dateTo);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        params.add(Env.getAD_User_ID(Env.getCtx()));
        params.add(dateFrom);
        params.add(dateTo);
        // @formatter:on

        map.put("sql", sql);
        map.put("params", params);

        return map;
    }

    public Map<String, Object> getSQLCementReceived(String distributorcode, String accountdate,
            String dateFrom, String dateTo) {
        List<Object> params = new ArrayList<>();
        Map<String, Object> map = new LinkedHashMap<>();

        StringBuilder whereClause;
        whereClause = new StringBuilder();
        // @formatter:off
        whereClause.append(" "
                + "WHERE "
                + "mi.ad_user_id =? "
                + "AND mi.ad_client_id = ? "
                + "AND cd.issotrx = 'N' "
                + "AND docl.c_do_client_id IS NOT NULL "
                + "AND adc.kode_dist IS NOT NULL "
                + "AND mv.docstatus = 'CO' "
                + "AND mic.docstatus = 'CO' ");
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        params.add(Env.getAD_User_ID(Env.getCtx()));
        if (distributorcode != null) {
            whereClause.append(" AND adc.kode_dist = ? ");
            params.add(distributorcode);
        }
        if (dateFrom != null && dateTo != null) {
            whereClause.append(""
                    + "AND to_char ( trunc (mic.datedoc), "
                    + "'YYYY-MM-DD' ) >= ? "
                    + "AND to_char ( trunc (mic.datedoc), "
                    + "'YYYY-MM-DD' ) <= ? ");
            params.add(dateFrom);
            params.add(dateTo);
        }
        if (accountdate != null) {
            whereClause.append(""
                    + "AND to_char ( trunc ( mic.datedoc ), "
                    + "'YYYY-MM-DD' ) >= ? "
                    + "AND to_char ( trunc ( mic.datedoc ), "
                    + "'YYYY-MM-DD' ) <= ? ");
            params.add(accountdate);
            params.add(accountdate);
        }
        String sql = ""
                + "SELECT "
                + "    DISTINCT ON "
                + "    ( docl.no_spj ) adc.ad_client_id, "
                + "    adc.NAME, "
                + "    docl.no_do, "
                + "    COALESCE( org.kode_gudang, "
                + "    docl.kdshipto ) AS kdshipto, "
                + "    COALESCE( org.name, "
                + "    docl.nmshipto ) AS nmshipto, "
                + "    docl.no_spj, "
                + "    mp.m_product_id, "
                + "    mvl.movementqty, "
                + "    cu.NAME AS uom, "
                + "    adc.kode_dist AS kd_distributor_si, "
                + "    mi.movementdate, "
                + "    mm.value2, "
                + "    mic.datedoc "
                + "FROM "
                + "    m_inoutconfirm mic "
                + "LEFT JOIN M_inout mi ON "
                + "    mi.m_inout_id = mic.m_inout_id "
                + "LEFT JOIN m_inoutline mil ON "
                + "    mil.m_inout_id = mi.m_inout_id "
                + "LEFT JOIN ad_client adc ON "
                + "    adc.ad_client_id = mic.ad_client_id "
                + "LEFT JOIN C_DO_Client docl ON "
                + "    docl.m_inout_id = mi.m_inout_id "
                + "LEFT JOIN m_attributesetinstance asi ON "
                + "    asi.description = docl.no_spj "
                + "LEFT JOIN m_movementline mvl ON "
                + "    mvl.m_attributesetinstance_id = asi.m_attributesetinstance_id "
                + "LEFT JOIN m_movement mv ON "
                + "    mv.m_movement_id = mvl.m_movement_id "
                + "LEFT JOIN m_locator ml ON "
                + "    ml.m_locator_id = mvl.m_locatorto_id "
                + "LEFT JOIN m_warehouse mw ON "
                + "    mw.m_warehouse_id = ml.m_warehouse_id "
                + "LEFT JOIN ad_org org ON "
                + "    org.ad_org_id = mw.ad_org_id "
                + "LEFT JOIN m_product mp ON "
                + "    mp.m_product_id = mil.m_product_id "
                + "JOIN M_MappingERPDist mm ON "
                + "    CAST( COALESCE ( mm.VALUE, "
                + "    '0' ) AS INTEGER ) = mp.m_product_id "
                + "    AND mm.NAME = 'Product' "
                + "LEFT JOIN c_doctype cd ON "
                + "    cd.c_doctype_id = mi.c_doctype_id "
                + "LEFT JOIN c_uom cu ON "
                + "    cu.c_uom_id = mp.c_uom_id" 
                + whereClause;
        // @formatter:on
        map.put("sql", sql);
        map.put("params", params);

        return map;
    }

}
