/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : POS Implement Transaction                              
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.impl.transaction;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MUOMConversion;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.Constants;
import id.sisi.forca.base.api.util.ProcessUtil;

public class OrderImplTrans {

    public Object getInvoiceLineByInvoiceID(Integer c_invoice_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listInvoiceLine = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     PriceEntered "
                + " FROM "
                + "     c_invoiceline "
                + " WHERE "
                + "     c_invoice_id = ? "
                + "     AND ad_client_id = ? ");
        params.add(c_invoice_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("PriceEntered", rs.getString("PriceEntered"));
                listInvoiceLine.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listInvoiceLine;
    }

    public Object getInvoiceByOrderID(Integer c_order_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listInvoice = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     c_invoice_id, "
                + "     documentno, "
                + "     docstatus, "
                + "     dateinvoiced "
                + " FROM "
                + "     c_invoice "
                + " WHERE "
                + "     c_order_id = ? "
                + "     AND ad_client_id = ? ");
        params.add(c_order_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("c_invoice_id", rs.getString("c_invoice_id"));
                map.put("documentno", rs.getString("documentno"));
                map.put("docstatus", rs.getString("docstatus"));
                map.put("dateinvoiced", Constants.sdf.format(rs.getDate("dateinvoiced")));
                map.put("c_invoiceline",
                        getInvoiceLineByInvoiceID(rs.getInt("c_invoice_id"), trxName));
                listInvoice.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listInvoice;
    }

    public Object getInOutByOrderID(Integer c_order_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listInOut = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     m_inout_id, "
                + "     documentno, "
                + "     docstatus, "
                + "     movementdate "
                + " FROM "
                + "     m_inout "
                + " WHERE "
                + "     c_order_id = ? "
                + "     AND ad_client_id = ? ");
        params.add(c_order_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("m_inout_id", rs.getString("m_inout_id"));
                map.put("documentno", rs.getString("documentno"));
                map.put("docstatus", rs.getString("docstatus"));
                map.put("movementdate", Constants.sdf.format(rs.getDate("movementdate")));
                listInOut.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listInOut;
    }

    public Object getOrderLineByOrderID(Integer c_order_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listOrderLine = new ArrayList<>();
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     l.c_orderline_id, "
                + "     prd.m_product_id, "
                + "     prd.name AS product_name, "
                + "     l.QtyOrdered, "
                + "     l.QtyReserved, "
                + "     l.QtyDelivered, "
                + "     l.QtyInvoiced, "
                + "     l.QtyEntered, "
                + "     l.Discount, "
                + "     l.priceentered, "
                + "		uom.c_uom_id AS uom_id, "
                + "     uom.Name AS uom_name, "
                + "		tax.c_tax_id AS tax_id, "
                + "     tax.name AS tax_name, "
                + "     l.linenetamt AS total, "
                + "     cur.Cursymbol, "
                + "     l.line "
                + " FROM "
                + "     c_orderline l "
                + " LEFT JOIN M_Product prd "
                + "     ON prd.M_Product_ID = l.M_Product_ID "
                + " LEFT JOIN C_Uom uom "
                + "     ON uom.C_Uom_ID = l.C_Uom_ID "
                + " LEFT JOIN C_Tax tax "
                + "     ON tax.C_Tax_ID = l.C_Tax_ID "
                + " LEFT JOIN C_Currency cur "
                + "     ON cur.C_Currency_ID = l.C_Currency_ID "
                + " WHERE "
                + "     l.c_order_id = ? "
                + "     AND l.ad_client_id = ? ");
        params.add(c_order_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("line_number", rs.getString("line"));
                map.put("c_orderline_id", rs.getString("c_orderline_id"));
                map.put("m_product_id", rs.getString("m_product_id"));
                map.put("product_name", rs.getString("product_name"));
                map.put("qtyentered",
                        numberFormat.format(new BigDecimal(rs.getString("qtyentered"))));
                map.put("qtyordered",
                        numberFormat.format(new BigDecimal(rs.getString("qtyordered"))));
                map.put("qtyreserved",
                        numberFormat.format(new BigDecimal(rs.getString("qtyreserved"))));
                map.put("qtydelivered",
                        numberFormat.format(new BigDecimal(rs.getString("qtydelivered"))));
                map.put("qtyinvoiced",
                        numberFormat.format(new BigDecimal(rs.getString("qtyinvoiced"))));
                map.put("priceentered", rs.getString("cursymbol") + " "
                        + numberFormat.format(new BigDecimal(rs.getString("priceentered"))));
                map.put("discount", rs.getString("discount"));
                map.put("c_uom_id", rs.getString("uom_id"));
                map.put("uom_name", rs.getString("uom_name"));
                map.put("c_tax_id", rs.getString("tax_id"));
                map.put("tax_name", rs.getString("tax_name"));
                map.put("total", rs.getString("cursymbol") + " "
                        + numberFormat.format(new BigDecimal(rs.getString("total"))));
                listOrderLine.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listOrderLine;
    }

    public Object getProjectByOrderID(Integer c_order_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listProject = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     prj.c_project_id, "
                + "     prj.value,"
                + "     prj.name,"
                + "     prj.datecontract,"
                + "     prj.datefinish "
                + " FROM "
                + "     c_order ord"
                + " LEFT JOIN c_project prj ON prj.c_project_id = ord.c_project_id "
                + " WHERE "
                + "     ord.c_order_id = ? "
                + "     AND ord.ad_client_id = ? ");
        params.add(c_order_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                if (rs.getString("c_project_id") != null) {
                    Map<String, Object> map = new LinkedHashMap<String, Object>();
                    map.put("c_project_id", rs.getString("c_project_id"));
                    map.put("value", rs.getString("value"));
                    map.put("name", rs.getString("name"));
                    map.put("datecontract", rs.getString("datecontract"));
                    map.put("datefinish", rs.getString("datefinish"));
                    map.put("c_projectline",
                            getProjectLineByProjectID(rs.getInt("c_project_id"), trxName));
                    listProject.add(map);
                }
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listProject;
    }

    public Object getProjectLineByProjectID(Integer c_project_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listProjectLine = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     pl.c_projectline_id, "
                + "     pl.plannedqty, "
                + "     pl.plannedprice, "
                + "     pl.plannedamt, "
                + "     prd.name as product_name "
                + " FROM "
                + "     c_projectline pl "
                + " LEFT JOIN m_product prd ON prd.m_product_id = pl.m_product_id "
                + " WHERE "
                + "     pl.c_project_id = ? "
                + "     AND pl.ad_client_id = ? ");
        params.add(c_project_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("c_projectline_id", rs.getString("c_projectline_id"));
                map.put("plannedqty", rs.getString("plannedqty"));
                map.put("plannedprice", rs.getString("plannedprice"));
                map.put("plannedamt", rs.getString("plannedamt"));
                map.put("product_name", rs.getString("product_name"));
                listProjectLine.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listProjectLine;
    }
    
    public Object getPricelistVersionByPricelistID(Integer m_pricelist_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listPricelistVersion = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     plv.m_pricelist_version_id, "
                + "     plv.name, "
                + "     plv.description "
                + " FROM "
                + "     m_pricelist_version plv "
                + " LEFT JOIN m_pricelist pl ON plv.m_pricelist_id = pl.m_pricelist_id "
                + " WHERE "
                + "     plv.m_pricelist_id = ? "
                + "     AND pl.ad_client_id = ? ");
        params.add(m_pricelist_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("m_pricelist_version_id", rs.getString("m_pricelist_version_id"));
                map.put("name", rs.getString("name"));
                map.put("description", rs.getString("description"));
                listPricelistVersion.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listPricelistVersion;
    }
    
    public Object getUOMConversionByProductID(Integer c_orderline_id, Integer m_product_id) throws Exception {
        List<Object> params = new ArrayList<>();
        ResponseData resp = new ResponseData();
        StringBuilder sql = new StringBuilder();
        List<Object> listUom = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        //@formatter:off
        sql.append(
                "SELECT "
                    + " uom.c_uom_id, "
                    + " uom.name AS uom_name, "
                    + " ol.qtyreserved "
                    + "FROM "
                    + " c_orderline ol "
                    + "LEFT JOIN c_uom uom ON "
                    + " ol.c_uom_id = uom.c_uom_id "
                    + "WHERE "
                    + " ol.c_orderline_id = ? "
                    + " AND ol.m_product_id = ? "
                    + "UNION ALL "
                    + "SELECT "
                    + " uomc.c_uom_to_id AS c_uom_id, "
                    + " uomto.name AS uom_name, "
                    + " ol.qtyreserved "
                    + "FROM "
                    + " c_orderline ol "
                    + "LEFT JOIN c_uom uom ON "
                    + " ol.c_uom_id = uom.c_uom_id "
                    + "LEFT JOIN c_uom_conversion uomc ON "
                    + " uom.c_uom_id = uomc.c_uom_id "
                    + "LEFT JOIN c_uom uomto ON "
                    + " uomto.c_uom_id = uomc.c_uom_to_id "
                    + "WHERE "
                    + " ol.c_orderline_id = ? "
                    + " AND uomc.m_product_id = ?");
        //@formatter:on
        params.add(c_orderline_id);
        params.add(m_product_id);
        params.add(c_orderline_id);
        params.add(m_product_id);
        
        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map2 = new LinkedHashMap<String, Object>();
                map2.put("c_uom_id", rs.getString("c_uom_id"));
                map2.put("c_uom_name", rs.getString("uom_name"));
                map2.put("qtyreserved", MUOMConversion.convertProductTo(Env.getCtx(), m_product_id, rs.getInt("c_uom_id"),
                        rs.getBigDecimal("qtyreserved")));
                listUom.add(map2);
            }

            resp = ResponseData.successResponse(listUom.size() + " Data Found", listUom);
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listUom;
    }
    
    public List<MOrderLine> getLines(MOrder order, String trxName) {
        String sqlWhere = "AD_Client_ID = " + order.getAD_Client_ID() + " AND C_Order_ID = "
                + order.get_ID();
        Query query = new Query(Env.getCtx(), MOrderLine.Table_Name, sqlWhere, trxName);
        query.setOnlyActiveRecords(true);
        List<MOrderLine> orderLine = query.list();
        return orderLine;
    }
}
