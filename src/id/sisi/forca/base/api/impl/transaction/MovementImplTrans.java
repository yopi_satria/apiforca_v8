package id.sisi.forca.base.api.impl.transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.compiere.util.DB;
import org.compiere.util.Env;
import id.sisi.forca.base.api.util.ProcessUtil;

public class MovementImplTrans {
    public Object getMovementLineByMovementID(Integer m_movement_id, String trxName)
            throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listMovementLine = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + "SELECT "
                + "    ml.m_movementline_id, "
                + "    ml.m_locator_id, "
                + "    ml.m_locatorto_id, "
                + "    ml.m_product_id, "
                + "    ml.forca_c_uom_id, "
                + "    l.value AS m_locator, "
                + "    lto.value AS m_locator_to, "
                + "    prd.name AS product_name, "
                + "    ml.movementqty, "
                + "    forca_qtyentered, "
                + "    uom.name AS uom_name, "
                + "    ml.line "
                + "FROM "
                + "    m_movementline ml "
                + "LEFT JOIN m_locator l ON "
                + "    l.m_locator_id = ml.m_locator_id "
                + "LEFT JOIN m_locator lto ON "
                + "    lto.m_locator_id = ml.m_locatorto_id "
                + "LEFT JOIN m_product prd ON "
                + "    prd.m_product_id = ml.m_product_id "
                + "LEFT JOIN c_uom uom ON "
                + "    uom.c_uom_id = ml.forca_c_uom_id"
                + " WHERE "
                + "     ml.m_movement_id = ? "
                + "     AND ml.ad_client_id = ? ");
        params.add(m_movement_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("line_number", rs.getString("line"));
                map.put("m_movementline_id", rs.getString("m_movementline_id"));
                map.put("m_locator", rs.getString("m_locator"));
                map.put("m_locator_id", rs.getString("m_locator_id"));
                map.put("m_locator_to", rs.getString("m_locator_to"));
                map.put("m_locator_to_id", rs.getString("m_locatorto_id"));
                map.put("m_product_id", rs.getString("m_product_id"));
                map.put("product_name", rs.getString("product_name"));
                map.put("forca_c_uom_id", rs.getString("forca_c_uom_id"));
                map.put("uom_name", rs.getString("uom_name"));
                map.put("movementqty", rs.getString("movementqty"));
                map.put("forca_qtyentered", rs.getString("forca_qtyentered"));
                listMovementLine.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listMovementLine;
    }
}
