package id.sisi.forca.base.api.impl.transaction;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.compiere.util.DB;
import org.compiere.util.Env;
import id.sisi.forca.base.api.util.Constants;
import id.sisi.forca.base.api.util.ProcessUtil;

public class InvoiceImplTrans {
    public Object getInvoiceLineByInvoiceID(Integer c_invoice_id, String trxName)
            throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listInvoiceLine = new ArrayList<>();
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);

        //@formatter:off
        sql.append("SELECT "
                + "    cil.c_invoiceline_id, "
                + "    cil.m_product_id, "
                + "    prd.name AS product_name, "
                + "    cil.c_uom_id, "
                + "    uom.name AS uom_name, "
                + "    cil.m_inoutline_id, "
                + "    cil.c_charge_id, "
                + "    cil.qtyentered, "
                + "    cil.qtyinvoiced, "
                + "    cil.priceentered, "
                + "    cil.priceactual, "
                + "    cil.pricelimit, "
                + "    cil.pricelist, "
                + "    cil.c_tax_id, "
                + "    tax.name AS tax_name, "
                + "    cil.linenetamt, "
                + "    cur.cursymbol "
                + "FROM "
                + "    c_invoiceline cil "
                + "LEFT JOIN m_product prd ON "
                + "    prd.m_product_id = cil.m_product_id "
                + "LEFT JOIN c_uom uom ON "
                + "    uom.c_uom_id = cil.c_uom_id "
                + "LEFT JOIN c_tax tax ON "
                + "    tax.c_tax_id = cil.c_tax_id "
                + "LEFT JOIN c_invoice ci ON "
                + "    ci.c_invoice_id = cil.c_invoice_id "
                + "LEFT JOIN c_currency cur ON "
                + "    cur.c_currency_id = ci.c_currency_id "
                + "WHERE "
                + "    cil.c_invoice_id = ? "
                + "    AND cil.ad_client_id = ?");
        params.add(c_invoice_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("c_invoiceline_id", rs.getString("c_invoiceline_id"));
                map.put("m_product_id", rs.getString("m_product_id"));
                map.put("product_name", rs.getString("product_name"));
                map.put("c_uom_id", rs.getString("c_uom_id"));
                map.put("uom_name", rs.getString("uom_name"));
                map.put("m_inoutline_id", rs.getString("m_inoutline_id"));
                map.put("c_charge_id", rs.getString("c_charge_id"));
                map.put("qtyentered", numberFormat.format(new BigDecimal(rs.getString("qtyentered"))));
                map.put("qtyinvoiced", numberFormat.format(new BigDecimal(rs.getString("qtyinvoiced"))));
                map.put("priceentered", rs.getString("cursymbol")+" "+numberFormat.format(new BigDecimal(rs.getString("priceentered"))));
                map.put("priceactual", rs.getString("cursymbol")+" "+numberFormat.format(new BigDecimal(rs.getString("priceactual"))));
                map.put("pricelimit", rs.getString("cursymbol")+" "+numberFormat.format(new BigDecimal(rs.getString("pricelimit"))));
                map.put("pricelist", rs.getString("cursymbol")+" "+numberFormat.format(new BigDecimal(rs.getString("pricelist"))));
                map.put("linenetamt", rs.getString("cursymbol")+" "+numberFormat.format(new BigDecimal(rs.getString("linenetamt"))));
                map.put("c_tax_id", rs.getString("c_tax_id"));
                map.put("tax_name", rs.getString("tax_name"));
                listInvoiceLine.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listInvoiceLine;
    }
    
    public Object getProjectByInvoiceID(Integer c_invoice_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listProject = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     prj.c_project_id, "
                + "     prj.value,"
                + "     prj.name,"
                + "     prj.datecontract,"
                + "     prj.datefinish "
                + " FROM "
                + "     c_invoice ci"
                + " LEFT JOIN c_project prj ON prj.c_project_id = ci.c_project_id "
                + " WHERE "
                + "     ci.c_invoice_id = ? "
                + "     AND ci.ad_client_id = ? ");
        params.add(c_invoice_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                if (rs.getString("c_project_id") != null) {
                    Map<String, Object> map = new LinkedHashMap<String, Object>();
                    map.put("c_project_id", rs.getString("c_project_id"));
                    map.put("value", rs.getString("value"));
                    map.put("name", rs.getString("name"));
                    map.put("datecontract", rs.getString("datecontract"));
                    map.put("datefinish", rs.getString("datefinish"));
                    map.put("c_projectline",
                            getProjectLineByProjectID(rs.getInt("c_project_id"), trxName));
                    listProject.add(map);
                }
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listProject;
    }
    
    public Object getProjectLineByProjectID(Integer c_project_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listProjectLine = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     pl.c_projectline_id, "
                + "     pl.plannedqty, "
                + "     pl.plannedprice, "
                + "     pl.plannedamt, "
                + "     prd.name as product_name "
                + " FROM "
                + "     c_projectline pl "
                + " LEFT JOIN m_product prd ON prd.m_product_id = pl.m_product_id "
                + " WHERE "
                + "     pl.c_project_id = ? "
                + "     AND pl.ad_client_id = ? ");
        params.add(c_project_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("c_projectline_id", rs.getString("c_projectline_id"));
                map.put("plannedqty", rs.getString("plannedqty"));
                map.put("plannedprice", rs.getString("plannedprice"));
                map.put("plannedamt", rs.getString("plannedamt"));
                map.put("product_name", rs.getString("product_name"));
                listProjectLine.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listProjectLine;
    }
    
    public Object getOrderByInvoiceID(Integer c_invoice, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listOrder = new ArrayList<>();

        //@formatter:off
        sql.append("SELECT "
                + "    ord.c_order_id, "
                + "    ord.documentno, "
                + "    ord.docstatus, "
                + "    ord.dateordered "
                + "FROM "
                + "    c_order ord "
                + "LEFT JOIN c_invoice ci ON "
                + "    ci.c_order_id = ord.c_order_id "
                + "WHERE "
                + "    ci.c_invoice_id = ? "
                + "    AND ci.ad_client_id = ?");
        params.add(c_invoice);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("c_order_id", rs.getString("c_order_id"));
                map.put("documentno", rs.getString("documentno"));
                map.put("docstatus", rs.getString("docstatus"));
                map.put("dateordered", Constants.sdf.format(rs.getDate("dateordered")));
                map.put("c_orderline",
                        getOrderLineByOrderID(rs.getInt("c_order_id"), trxName));
                listOrder.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listOrder;
    }
    
    public Object getOrderLineByOrderID(Integer c_order_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listOrderLine = new ArrayList<>();
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     l.c_orderline_id, "
                + "     prd.m_product_id, "
                + "     prd.name AS product_name, "
                + "     l.QtyOrdered, "
                + "     l.QtyDelivered, "
                + "     l.QtyInvoiced, "
                + "     l.Discount, "
                + "     l.priceentered, "
                + "     uom.c_uom_id AS uom_id, "
                + "     uom.Name AS uom_name, "
                + "     tax.c_tax_id AS tax_id, "
                + "     tax.name AS tax_name, "
                + "     l.linenetamt AS total, "
                + "     cur.Cursymbol "
                + " FROM "
                + "     c_orderline l "
                + " LEFT JOIN M_Product prd "
                + "     ON prd.M_Product_ID = l.M_Product_ID "
                + " LEFT JOIN C_Uom uom "
                + "     ON uom.C_Uom_ID = l.C_Uom_ID "
                + " LEFT JOIN C_Tax tax "
                + "     ON tax.C_Tax_ID = l.C_Tax_ID "
                + " LEFT JOIN C_Currency cur "
                + "     ON cur.C_Currency_ID = l.C_Currency_ID "
                + " WHERE "
                + "     l.c_order_id = ? "
                + "     AND l.ad_client_id = ? ");
        params.add(c_order_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("c_orderline_id", rs.getString("c_orderline_id"));
                map.put("m_product_id", rs.getString("m_product_id"));
                map.put("product_name", rs.getString("product_name"));
                map.put("qtyordered",
                        numberFormat.format(new BigDecimal(rs.getString("qtyordered"))));
                map.put("qtydelivered",
                        numberFormat.format(new BigDecimal(rs.getString("qtydelivered"))));
                map.put("qtyinvoiced",
                        numberFormat.format(new BigDecimal(rs.getString("qtyinvoiced"))));
                map.put("priceentered", rs.getString("cursymbol") + " "
                        + numberFormat.format(new BigDecimal(rs.getString("priceentered"))));
                map.put("discount", rs.getString("discount"));
                map.put("c_uom_id", rs.getString("uom_id"));
                map.put("uom_name", rs.getString("uom_name"));
                map.put("c_tax_id", rs.getString("tax_id"));
                map.put("tax_name", rs.getString("tax_name"));
                map.put("total", rs.getString("cursymbol") + " "
                        + numberFormat.format(new BigDecimal(rs.getString("total"))));
                listOrderLine.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listOrderLine;
    }
}
