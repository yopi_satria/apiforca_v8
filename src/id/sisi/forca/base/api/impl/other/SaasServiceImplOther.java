/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Novan Dwi Nugroho                                                                                
   Email          : novan.nugroho@sisi.id                                                                  
   Subject        :                                  
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.impl.other;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.adempiere.model.MBroadcastMessage;
import org.compiere.model.MMessage;
import org.compiere.model.MProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
//import org.idempiere.model.ForcaObject;
import id.sisi.forca.base.api.request.ParamSaasServiceOther;
import id.sisi.forca.base.api.response.ResponseData;

public class SaasServiceImplOther {

    CLogger log = CLogger.getCLogger(SaasServiceImplOther.class);
    private static final String SAAS_WARNING_MESSAGE = "SaasWarningMessage";

//    public ResponseData performSaasServiceAction(ParamSaasServiceOther param, Trx trx) {
//
//        StringBuilder query = new StringBuilder();
//        List<Object> params = new ArrayList<Object>();
//        int data = 0;
//        try {
//            if (param.getAction().equalsIgnoreCase("W")) {
//                MMessage msg=MMessage.get(Env.getCtx(), SAAS_WARNING_MESSAGE);
//                if(msg==null)
//                    return ResponseData.errorResponse("SaasWarningMessage Belum dimaintain!");
//                String message = msg.getMsgText();
//                MBroadcastMessage broadcast = ForcaObject.getObject(Env.getCtx(),
//                        MBroadcastMessage.Table_Name, 0, trx.getTrxName());
//                broadcast.setBroadcastMessage(message);
//                broadcast.setBroadcastType(MBroadcastMessage.BROADCASTTYPE_Login);
//                broadcast.setTarget(MBroadcastMessage.TARGET_Client);
//                broadcast.setBroadcastFrequency(MBroadcastMessage.BROADCASTFREQUENCY_JustOnce);
//                broadcast.saveEx();
//
//                MProcess publish = MProcess.get(Env.getCtx(),
//                        MProcess.getProcess_ID("AD_PublishBroadcastMessage", trx.getTrxName()));
//                trx.commit();
//                publish.processIt(broadcast.get_ID(), trx);
//
//            } else if (param.getAction().equalsIgnoreCase("S")) {
//            //@formatter:off
//            query.append(
//                    "UPDATE "
//                    + "AD_User SET "
//                    + "IsLocked = 'Y', "
//                    + "DateAccountLocked=?, "
//                    + "FailedLoginCount=1, "
//                    + "DateLastLogin=?, "
//                    + "Updated=SysDate ")
//            .append(" WHERE IsLocked='N' AND AD_Client_ID = ? ")
//            .append(" AND DateAccountLocked IS NULL ");
//         
//          //@formatter:on
//
//                params.add(new Timestamp(System.currentTimeMillis()));
//                params.add(new Timestamp(System.currentTimeMillis()));
//                params.add(param.getAd_client_id());
//
//                data = DB.executeUpdateEx(query.toString(), params.toArray(), trx.getTrxName());
//
//            } else if (param.getAction().equalsIgnoreCase("R")) {
//
//          //@formatter:off
//            query.append("UPDATE "
//                    + "AD_User SET "
//                    + "IsLocked = 'N', "
//                    + "DateAccountLocked=NULL, "
//                    + "FailedLoginCount=0, "
//                    + "DateLastLogin=NULL, "
//                    + "Updated=SysDate ")
//            .append(" WHERE IsLocked='Y' AND AD_Client_ID = ? ")
//            .append(" AND DateAccountLocked IS NOT NULL ");
//          //@formatter:on
//                params.add(param.getAd_client_id());
//
//                data = DB.executeUpdateEx(query.toString(), params.toArray(), trx.getTrxName());
//            }
//        } catch (Exception e) {
//
//            return ResponseData.errorResponse(e.getMessage());
//        }
//
//        return ResponseData.successResponse(data + " Updated!", null);
//
//    }
}
