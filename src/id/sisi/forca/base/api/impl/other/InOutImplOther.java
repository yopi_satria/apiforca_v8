package id.sisi.forca.base.api.impl.other;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.compiere.util.DB;
import org.compiere.util.Env;
import id.sisi.forca.base.api.util.ProcessUtil;

public class InOutImplOther {
    public Object getInOutLineByInOutID(Integer m_inout_id, String trxName) throws Exception {
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Object> params = new ArrayList<>();
        List<Object> listInOutLine = new ArrayList<>();

        //@formatter:off
        sql.append(""
                + " SELECT "
                + "     l.m_inoutline_id, "
                + "     l.line, "
                + "     l.description "
                + " FROM "
                + "     m_inoutline l "
                + " WHERE "
                + "     l.m_inout_id = ? "
                + "     AND l.ad_client_id = ? ");
        params.add(m_inout_id);
        params.add(Env.getAD_Client_ID(Env.getCtx()));
        //@formatter:on

        try {
            ps = DB.prepareStatement(sql.toString(), trxName);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("line_number", rs.getString("line"));
                map.put("m_inoutline_id", rs.getString("m_inoutline_id"));
                map.put("description", rs.getString("description"));
                listInOutLine.add(map);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return listInOutLine;
    }
}
