/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Fitriani Rohmah Hardiyanti                                                                                
   Email          : fitrianirohmahhardiyanti@gmail.com 
   Subject        : Master                              
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.db.Database;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MCity;
import org.compiere.model.MClient;
import org.compiere.model.MCountry;
import org.compiere.model.MDocType;
import org.compiere.model.MLocation;
import org.compiere.model.MOrg;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MProductPrice;
import org.compiere.model.MRegion;
import org.compiere.model.MTax;
import org.compiere.model.MTaxCategory;
import org.compiere.model.MUOM;
import org.compiere.model.MUOMConversion;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.model.X_C_Campaign;
import org.compiere.model.X_C_Channel;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.Util;
//import org.idempiere.model.ForcaObject;
import id.sisi.forca.base.api.impl.master.PricelistImplMaster;
import id.sisi.forca.base.api.impl.master.ProductImplMaster;
import id.sisi.forca.base.api.impl.master.UserImplMaster;
import id.sisi.forca.base.api.model.MFORCAUserMobile;
import id.sisi.forca.base.api.request.ParamRequest;
import id.sisi.forca.base.api.response.PaginationData;
import id.sisi.forca.base.api.response.ResponseData;
import id.sisi.forca.base.api.util.Constants;
import id.sisi.forca.base.api.util.ProcessUtil;

public class MasterImpl {
    private CLogger log = CLogger.getCLogger(getClass());
    PreparedStatement statement = null;
    ResultSet rs = null;
    AuthenticationImpl authLoginImpl = new AuthenticationImpl();
    private static final String NATIVE_MARKER = "NATIVE_" + Database.DB_POSTGRESQL + "_KEYWORK";
    private ProductImplMaster Product = null;
    private UserImplMaster User = null;
    private PricelistImplMaster Pricelist = null;

//    public ResponseData getBPGroup(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.AD_Client_ID, " + 
//                "    b.Name client_name, " + 
//                "    a.AD_Org_ID, " + 
//                "    a.Name, " +
//                "    a.Value, " +
//                "    a.C_BP_Group_ID, " +
//                "    a.Description " 
//                );
//        join.append(
//                "FROM C_BP_Group a " + 
//                "LEFT JOIN AD_Client b ON " + 
//                        "b.AD_Client_ID = a.AD_Client_ID " );
//        whereCond.append(
//                "WHERE a.C_BP_Group_ID IS NOT NULL "
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        Integer c_bp_group_id = param.getC_bp_group_id();
//        if (param.getC_bp_group_id() != null) {
//            whereCond.append(" AND a.C_BP_Group_ID = ? ");
//            params.add(c_bp_group_id);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("c_bp_group_id", rs.getString("c_bp_group_id"));
//                map.put("ad_org_id", rs.getString("ad_org_id"));
//                map.put("value", rs.getString("value"));
//                map.put("description", rs.getString("description"));
//                map.put("name", rs.getString("name"));
//                listdata.add(map);
//            }
//
//            resp = ResponseData.successResponse(listdata.size() + " Data Found", listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getCity(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        if (param.getC_country_id() == null || param.getC_region_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.AD_Client_ID, " + 
//                "    b.Name client_name, " + 
//                "    a.C_City_ID, " + 
//                "    a.Name, " +
//                "    a.C_Country_ID, " +
//                "    a.C_Region_ID, " +
//                "    a.Locode, " +
//                "    a.Coordinates, " +
//                "    a.Postal, " +
//                "    a.Areacode "
//                );
//        join.append(
//                "FROM C_City a " + 
//                "LEFT JOIN AD_Client b ON " + 
//                        "b.AD_Client_ID = a.AD_Client_ID ");
//        whereCond.append(
//                "WHERE a.C_City_ID IS NOT NULL  "
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        String name = param.getName();
//        Integer c_country_id = param.getC_country_id();
//        Integer c_region_id = param.getC_region_id();
//        Integer c_city_id = param.getC_city_id();
//        whereCond.append(" AND a.C_Country_ID = ? ");
//        whereCond.append(" AND a.C_Region_ID = ? ");
//        params.add(c_country_id);
//        params.add(c_region_id);
//        if (param.getAd_client_id() != null) {
//            whereCond.append(" AND a.AD_Client_ID = ? ");
//            params.add(param.getAd_client_id());
//        }
//        if (param.getC_city_id() != null) {
//            whereCond.append(" AND a.C_City_ID = ? ");
//            params.add(c_city_id);
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(a.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("c_city_id", rs.getString("c_city_id"));
//                map.put("c_country_id", rs.getString("c_country_id"));
//                map.put("c_region_id", rs.getString("c_region_id"));
//                map.put("locode", rs.getString("locode"));
//                map.put("coordinates", rs.getString("coordinates"));
//                map.put("postal", rs.getString("postal"));
//                map.put("areacode", rs.getString("areacode"));
//                map.put("name", rs.getString("name"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getCountry(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.AD_Client_ID, " + 
//                "    b.Name client_name, " + 
//                "    a.C_Country_ID, " + 
//                "    a.Name, " + 
//                "    a.Description, " + 
//                "    a.Countrycode ");
//        join.append(
//                "FROM C_Country a " + 
//                "LEFT JOIN AD_Client b ON " + 
//                        "b.AD_Client_ID = a.AD_Client_ID ");
//        whereCond.append(
//                "WHERE a.C_Country_ID IS NOT NULL " 
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        String name = param.getName();
//        Integer c_country_id = param.getC_country_id();
//        if (param.getAd_client_id() != null) {
//            whereCond.append(" AND a.AD_Client_ID = ? ");
//            params.add(param.getAd_client_id());
//        }
//        if (param.getC_country_id() != null) {
//            whereCond.append(" AND a.C_Country_ID = ? ");
//            params.add(c_country_id);
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(a.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("c_country_id", rs.getString("c_country_id"));
//                map.put("name", rs.getString("name"));
//                map.put("description", rs.getString("description"));
//                map.put("countrycode", rs.getString("countrycode"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getProject(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.AD_Client_ID, " + 
//                "    b.Name client_name, " + 
//                "    a.AD_Org_ID, " + 
//                "    a.Name, " +
//                "    a.Value, " +
//                "    a.C_Project_ID, " +
//                "    a.Description, " +
//                "    a.Plannedamt, " +
//                "    a.Plannedmarginamt, " +
//                "    a.Plannedqty, " +
//                "    a.Committedamt, " +
//                "    a.Committedqty "
//                );
//        join.append(
//                "FROM C_Project a " + 
//                "LEFT JOIN AD_Client b ON " + 
//                        "b.AD_Client_ID = a.AD_Client_ID ");
//        whereCond.append(
//                "WHERE a.C_Project_ID IS NOT NULL  "
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        Integer c_project_id = param.getC_project_id();
//        if (param.getC_project_id() != null) {
//            whereCond.append(" AND a.C_Project_ID = ? ");
//            params.add(c_project_id);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("c_project_id", rs.getString("c_project_id"));
//                map.put("ad_org_id", rs.getString("ad_org_id"));
//                map.put("value", rs.getString("value"));
//                map.put("description", rs.getString("description"));
//                map.put("name", rs.getString("name"));
//                map.put("plannedamt", rs.getString("plannedamt"));
//                map.put("plannedmarginamt", rs.getString("plannedmarginamt"));
//                map.put("plannedqty", rs.getString("plannedqty"));
//                map.put("committedamt", rs.getString("committedamt"));
//                map.put("committedqty", rs.getString("committedqty"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//
//    }
//
//
//    public ResponseData getRegion(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        if (param.getC_country_id() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.AD_Client_ID, " + 
//                "    b.Name client_name, " + 
//                "    a.C_Country_ID, " + 
//                "    a.C_Region_ID, " + 
//                "    a.Name, " +
//                "    a.Description ");
//        join.append(
//                "FROM C_Region a " + 
//                "LEFT JOIN AD_Client b ON " +
//                        "b.AD_Client_ID = a.AD_Client_ID ");
//        whereCond.append(
//                "WHERE a.C_Region_ID IS NOT NULL  "
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        String name = "";
//        Integer c_country_id = param.getC_country_id();
//        Integer c_region_id = param.getC_region_id();
//        whereCond.append(" AND a.C_Country_ID = ? ");
//        params.add(c_country_id);
//        if (param.getAd_client_id() != null) {
//            whereCond.append(" AND a.AD_Client_ID = ? ");
//            params.add(param.getAd_client_id());
//        }
//        if (param.getC_region_id() != null) {
//            whereCond.append(" AND a.C_Region_ID = ? ");
//            params.add(c_region_id);
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(a.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("c_country_id", rs.getString("c_country_id"));
//                map.put("c_region_id", rs.getString("c_region_id"));
//                map.put("name", rs.getString("name"));
//                map.put("description", rs.getString("description"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getTax(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new PaginationData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "WITH cte AS (" +
//                "SELECT " + 
//                "    a.C_Tax_ID, " + 
//                "    a.AD_Client_ID, " + 
//                "    b.Name client_name, " + 
//                "    a.AD_Org_ID, " + 
//                "    c.Name org_name, " + 
//                "    a.Name, " + 
//                "    a.Description, " + 
//                "    a.C_TaxCategory_ID, " + 
//                "    d.Name tax_category_name, " + 
//                "    a.ValidFrom, " + 
//                "    a.isActive, " + 
//                "    a.isSummary, " + 
//                "    a.isSalesTax, " + 
//                "    a.isDocumentLevel, " + 
//                "    a.SopoType, " + 
//                "    a.Rate, " + 
//                "    a.C_Country_ID, " + 
//                "    e.Name country_name, " + 
//                "    a.C_Region_ID, " + 
//                "    f.Name region_name, " + 
//                "    TaxIndicator " + 
//                "FROM C_Tax a " 
//                );
//        join.append(
//                "LEFT JOIN AD_Client b ON " + 
//                        "b.AD_Client_ID = a.AD_Client_ID " + 
//                "LEFT JOIN AD_Org c ON " + 
//                        "c.AD_Org_ID = a.AD_Org_ID " + 
//                "LEFT JOIN C_TaxCategory d ON " + 
//                        "d.C_TaxCategory_ID = a.C_TaxCategory_ID " + 
//                "LEFT JOIN C_Country e ON " + 
//                        "e.c_country_id = a.C_Country_ID " + 
//                "LEFT JOIN C_Region f ON " + 
//                        "f.C_Region_ID = a.C_Region_ID ");
//        whereCond.append(
//                "WHERE a.C_Tax_ID IS NOT NULL "
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        String name = "";
//        if (param.getAd_client_id() != null) {
//            whereCond.append(" AND a.AD_Client_ID = ? ");
//            params.add(param.getAd_client_id());
//        }
//        if (param.getAd_org_id() != null) {
//            whereCond.append(" AND a.AD_Org_ID = ? ");
//            params.add(param.getAd_org_id());
//        }
//        if (param.getC_taxcategory_id() != null) {
//            whereCond.append(" AND a.C_TaxCategory_ID = ? ");
//            params.add(param.getC_taxcategory_id());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(a.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//      //@formatter:off
//        sql.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE  cte "
//            + "   ORDER  BY Name "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//            boolean first = true;
//            int totaldata = 0;
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("c_tax_id", rs.getString("c_tax_id"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("ad_org_id", rs.getString("ad_org_id"));
//                map.put("org_name", rs.getString("org_name"));
//                map.put("tax_name", rs.getString("name"));
//                map.put("description", rs.getString("description"));
//                map.put("c_taxcategory_id", rs.getString("c_taxcategory_id"));
//                map.put("tax_category_name", rs.getString("tax_category_name"));
//                map.put("validfrom", rs.getString("validfrom"));
//                map.put("isactive", rs.getString("isactive"));
//                map.put("issummary", rs.getString("issummary"));
//                map.put("issalestax", rs.getString("issalestax"));
//                map.put("isdocumentlevel", rs.getString("isdocumentlevel"));
//                map.put("sopotype", rs.getString("sopotype"));
//                map.put("rate", rs.getString("rate"));
//                map.put("c_country_id", rs.getString("c_country_id"));
//                map.put("country_name", rs.getString("country_name"));
//                map.put("c_region_id", rs.getString("c_region_id"));
//                map.put("region_name", rs.getString("region_name"));
//                map.put("taxindicator", rs.getString("taxindicator"));
//                listdata.add(map);
//
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = PaginationData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getTaxCategory(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.AD_Client_ID, " + 
//                "    b.Name client_name, " + 
//                "    a.AD_Org_ID, " + 
//                "    a.Name, " +
//                "    a.C_TaxCategory_ID, " +
//                "    a.Description " 
//                );
//        join.append(
//                "FROM C_TaxCategory a " + 
//                "LEFT JOIN AD_Client b ON " + 
//                        "b.AD_Client_ID = a.AD_Client_ID ");
//        whereCond.append(
//                "WHERE a.C_TaxCategory_ID IS NOT NULL "
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        if (param.getC_taxcategory_id() != null) {
//            whereCond.append(" AND a.C_TaxCategory_ID = ? ");
//            params.add(param.getC_taxcategory_id());
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("c_taxcategory_id", rs.getString("c_taxcategory_id"));
//                map.put("ad_org_id", rs.getString("ad_org_id"));
//                map.put("description", rs.getString("description"));
//                map.put("name", rs.getString("name"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getUom(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.UomSymbol, " + 
//                "    b.Name client_name, " + 
//                "    a.C_Uom_ID, " + 
//                "    a.Name, " +
//                "    a.Description, " +
//                "    a.Stdprecision, " +
//                "    a.Costingprecision "
//                );
//        join.append(
//                "FROM C_Uom a " + 
//                "LEFT JOIN AD_Client b ON " + 
//                        "b.AD_Client_ID = a.AD_Client_ID ");
//        whereCond.append(
//                "WHERE a.C_Uom_ID IS NOT NULL  "
//                + " AND (a.AD_Client_ID = ? "
//                + " OR a.AD_Client_ID = 0 )");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        String name = "";
//        if (param.getC_uom_id() != null) {
//            whereCond.append(" AND a.C_Uom_ID = ? ");
//            params.add(param.getC_uom_id());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(a.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("uomsymbol", rs.getString("uomsymbol"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("c_uom_id", rs.getString("c_uom_id"));
//                map.put("name", rs.getString("name"));
//                map.put("description", rs.getString("description"));
//                map.put("stdprecision", rs.getString("stdprecision"));
//                map.put("costingprecision", rs.getString("costingprecision"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getProductCategory(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.M_Product_Category_ID, " + 
//                "    b.Name client_name, " + 
//                "    a.Value, " + 
//                "    a.Name, " +
//                "    a.Description "
//                );
//        join.append(
//                "FROM M_Product_Category a " + 
//                "LEFT JOIN AD_Client b ON " + 
//                        "b.AD_Client_ID = a.AD_Client_ID ");
//        whereCond.append(
//                "WHERE a.M_Product_Category_ID IS NOT NULL "
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        if (param.getM_product_category_id() != null) {
//            whereCond.append(" AND a.M_Product_Category_ID = ? ");
//            params.add(param.getM_product_category_id());
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("m_product_category_id", rs.getString("m_product_category_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("description", rs.getString("description"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getUser(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT "
//                    + " a.AD_User_ID, "
//                    + " b.Name client_name, "
//                    + " a.AD_Client_ID, "
//                    + " a.Name, "
//                    + " a.Password, "
//                    + " a.Email, "
//                    + " a.Phone, "
//                    + " a.isLocked, "
//                    + " a.Description, "
//                    + " a.C_BPartner_ID, "
//                    + " bp.Value, "
//                    + " a.C_BPartner_Location_ID, "
//                    + " a.C_Location_ID, "
//                    + " bp.isSalesRep "
//                );
//        join.append(
//                "FROM "
//                    + " AD_User a "
//                    + "LEFT JOIN AD_Client b ON "
//                    + " b.AD_Client_ID = a.AD_Client_ID "
//                    + "LEFT JOIN C_BPartner bp ON "
//                    + " a.C_BPartner_ID = bp.C_BPartner_ID ");
//        whereCond.append(
//                "WHERE a.AD_User_ID IS NOT NULL " 
//                + " AND a.Password IS NOT NULL "
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        if (param.getAd_user_id() != null) {
//            whereCond.append(" AND a.AD_User_ID = ? ");
//            params.add(param.getAd_user_id());
//        }
//        if (param.getAd_client_id() != null) {
//            whereCond.append(" AND a.AD_Client_ID = ? ");
//            params.add(param.getAd_client_id());
//        }
//        if (param.getIs_active() != null) {
//            whereCond.append(" AND a.isActive = ? ");
//            params.add(param.getIs_active());
//        }
//        if (param.getName() != null) {
//            whereCond.append(" AND a.Name = ? ");
//            params.add(param.getName());
//        }
//        if(!Util.isEmpty(param.getIssalesrep())) {
//            if(param.getIssalesrep().equals("Y")) {
//                whereCond.append(" AND bp.isSalesRep = 'Y' ");
//            }
//        }
//
//        whereCond.append("ORDER BY a.Name ");
//        if (param.getPerpage() != null) {
//            whereCond.append(NATIVE_MARKER + "LIMIT ?" + NATIVE_MARKER);
//            params.add(param.getPerpage());
//        } else if (param.getPerpage() == null) {
//            whereCond.append(NATIVE_MARKER + "LIMIT 25" + NATIVE_MARKER);
//        }
//
//        if (param.getPage() != null) {
//            if (param.getPerpage() != null) {
//                whereCond.append(" OFFSET (?-1)*? ");
//                params.add(param.getPage());
//                params.add(param.getPerpage());
//            } else if (param.getPerpage() == null) {
//                whereCond.append(" OFFSET (?-1)*25 ");
//                params.add(param.getPage());
//            }
//        } else if (param.getPage() == null) {
//            whereCond.append("OFFSET 0 ");
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("ad_user_id", rs.getString("ad_user_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("name", rs.getString("name"));
////                map.put("password", rs.getString("password"));
//                map.put("email", rs.getString("phone"));
//                map.put("islocked", rs.getString("islocked"));
//                map.put("c_bpartner_id", rs.getString("c_bpartner_id"));
//                map.put("search_key", rs.getString("value"));
//                map.put("c_bpartner_location_id", rs.getString("c_bpartner_location_id"));
//                map.put("c_location_id", rs.getString("c_location_id"));
//                map.put("issalesrep", rs.getString("issalesrep"));
//
//                User = new UserImplMaster();
//                map.put("role", User.getRoleByUserID(Integer.valueOf(rs.getString("ad_user_id"))));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getWarehouse(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.M_Warehouse_ID, " + 
//                "    b.Name client_name, " + 
//                "    a.Value, " +
//                "    a.Description, " +
//                "    a.Name "
//                );
//        join.append(
//                "FROM M_Warehouse a " + 
//                "LEFT JOIN AD_Client b ON " + 
//                        "b.AD_Client_ID = a.AD_Client_ID ");
//        whereCond.append(
//                "WHERE a.M_Warehouse_ID IS NOT NULL " 
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        String name = "";
//        if (param.getM_warehouse_id() != null) {
//            whereCond.append(" AND a.M_Warehouse_ID = ? ");
//            params.add(param.getM_warehouse_id());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(a.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("m_warehouse_id", rs.getString("m_warehouse_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("description", rs.getString("description"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getBPartner(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new PaginationData();
//        List<Object> listdata = new ArrayList<Object>();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//
//        //@formatter:off
//        select.append(""
//                + "WITH cte AS ("
//                + " SELECT "  
//                //+ "    a.sap_code, "
//                //+ "    a.id_customer, "
//                + "    a.name, "
//                + "    a.Name2, " 
//                + "    a.taxid, "
//                + "    a.referenceno, "
//                + "    a.C_BP_Group_ID, " 
//                + "    a.C_BPartner_ID, " 
//                + "    a.Value, " 
//                + "    a.Description, " 
//                + "    a.AD_Client_ID, " 
//                + "    a.AD_Org_ID, " 
//                + "    a.isCustomer, " 
//                + "    a.SO_Creditlimit, " 
//                + "    b.Name client_name, " 
//                + "    b.name AS distname, "
//                //+ "    b.kode_dist, " 
//                + "    c.C_BPartner_Location_ID, " 
//                + "    d.address1, "
//                + "    d.address2, "
//                + "    d.address3, "
//                + "    d.address4, "
//                + "    d.city, " 
//                + "    d.C_Location_ID, "
//                + "    co.Name as country_name, "
//                + "    re.Description as region_name ");
//        join.append(
//                "FROM C_BPartner a " + 
//                "LEFT JOIN AD_Client b ON " + 
//                        "b.AD_Client_ID = a.AD_Client_ID " +
//                "LEFT JOIN C_BPartner_Location c ON " + 
//                        "c.C_BPartner_ID = a.C_BPartner_ID " +
//                "LEFT JOIN C_Location d ON " + 
//                        "d.C_Location_ID = c.C_Location_ID " +
//                "LEFT JOIN C_BP_Group cbp ON " +
//                        "cbp.C_BP_Group_ID = a.C_BP_Group_ID " + 
//                "LEFT JOIN C_Country co ON " +
//                        "co.C_Country_ID = d.C_Country_ID " +
//                "LEFT JOIN C_Region re ON " + 
//                        "re.C_Region_ID = d.C_Region_ID ");
//        whereCond.append(
//                "WHERE a.AD_Client_ID = ? ");
//        //@formatter:on
//
//
//        if (param.getAd_client_id() != null) {
//            params.add(param.getAd_client_id());
//        } else {
//            params.add(Env.getAD_Client_ID(Env.getCtx()));
//        }
//        if (param.getC_bp_group_id() != null) {
//            whereCond.append("AND cbp.c_bp_group_id = ? ");
//            params.add(param.getC_bp_group_id());
//        }
//        if (param.getC_bpartner_id() != null) {
//            whereCond.append(" AND a.C_BPartner_ID = ? ");
//            params.add(param.getC_bpartner_id());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            String name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(a.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//        if (param.getDistributorcode() != null) {
//            whereCond.append("AND b.kode_dist = ? ");
//            params.add(param.getDistributorcode());
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        //@formatter:off
//        sql.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE  cte "
//            + "   ORDER  BY name "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//            boolean first = true;
//            int totaldata = 0;
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("c_bpartner_id", rs.getString("c_bpartner_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("name2", rs.getString("name2"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("ad_org_id", rs.getString("ad_org_id"));
//                map.put("iscustomer", rs.getString("iscustomer"));
//                map.put("so_creditlimit", rs.getString("so_creditlimit"));
//                map.put("c_bpartner_location_id", rs.getString("c_bpartner_location_id"));
//                map.put("c_location_id", rs.getString("c_location_id"));
//                map.put("description", rs.getString("description"));
//                // map.put("sap_code", rs.getString("sap_code"));
//                // map.put("id_customer", rs.getString("id_customer"));
//                map.put("taxid", rs.getString("taxid"));
//                map.put("referenceno", rs.getString("referenceno"));
//                map.put("address1", rs.getString("address1"));
//                map.put("address2", rs.getString("address2"));
//                map.put("address3", rs.getString("address3"));
//                map.put("address4", rs.getString("address4"));
//                map.put("city", rs.getString("city"));
//                map.put("country_name", rs.getString("country_name"));
//                map.put("region_name", rs.getString("region_name"));
//                map.put("distributor_name", rs.getString("distname"));
//                // map.put("distributor_kode", rs.getString("kode_dist"));
//                if (rs.getString("c_bpartner_id") != null) {
//                    listdata.add(map);
//                }
//
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(totaldata + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = PaginationData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getOrg(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.Description, " +
//                "    a.AD_Org_ID, " +
//                "    a.Name "
//                );
//        join.append(
//                "FROM AD_Org a ");
//        whereCond.append(
//                "WHERE a.AD_Org_ID IS NOT NULL " 
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        if (param.getAd_org_id() != null) {
//            whereCond.append(" AND a.AD_Org_ID = ? ");
//            params.add(param.getAd_org_id());
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("name", rs.getString("name"));
//                map.put("ad_org_id", rs.getString("ad_org_id"));
//                map.put("description", rs.getString("description"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getClient(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    a.Description, " +
//                "    a.AD_Client_ID, " +
//                "    a.Name "
//                );
//        join.append(
//                "FROM AD_Client a ");
//        whereCond.append(
//                "WHERE a.AD_CLient_ID IS NOT NULL " 
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        String name = "";
//        if (param.getAd_client_id() != null) {
//            whereCond.append(" AND a.AD_Client_ID = ? ");
//            params.add(param.getAd_client_id());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(a.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("description", rs.getString("description"));
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getProduct(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new PaginationData();
//        List<Object> listdata = new ArrayList<Object>();
//        Map<String, Object> map = new LinkedHashMap<String, Object>();
//
//        //@formatter:off
//        select.append(""+
//                "WITH cte AS (" +
//                "  SELECT DISTINCT " + 
//                "    a.M_Product_ID, " +
//                "    a.AD_Client_ID, " +
//                "    a.M_Product_Category_ID, " +
//                "    a.Description, " +
//                "    a.Value, " +
//                "    a.C_Uom_ID, " +
//                "    a.C_TaxCategory_ID, " +
//                "    a.M_Locator_ID, " +
//                "    b.Uomsymbol, " +
//                "    a.Volume, " +
//                "    a.Issold, " +
//                "    a.Name "
//                );
//        join.append(
//                "FROM M_Product a " + 
//                "LEFT JOIN C_Uom b "+
//                        " ON a.C_Uom_ID = b.C_Uom_ID " +
//                "LEFT JOIN AD_Client c " + 
//                        "ON a.AD_Client_ID = c.AD_Client_ID " + 
//                "LEFT JOIN M_ProductPrice pp " + 
//                        "ON pp.M_Product_ID = a.M_Product_ID " +
//                "LEFT JOIN M_Pricelist_Version pv " +
//                        "ON pv.M_Pricelist_Version_ID = pp.M_Pricelist_Version_ID " +
//                "LEFT JOIN M_Pricelist pl " +
//                        "ON pl.M_Pricelist_ID = pv.M_Pricelist_ID ");
//        whereCond.append(
//                "WHERE a.M_Product_ID IS NOT NULL " 
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        String name = "";
//        if (param.getM_product_id() != null) {
//            whereCond.append(" AND a.M_Product_ID = ? ");
//            params.add(param.getM_product_id());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(a.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//        if (param.getM_pricelist_id() != null) {
//            whereCond.append(" AND pl.M_Pricelist_ID = ? ");
//            params.add(param.getM_pricelist_id());
//        }
//        if (param.getIssold() != null) {
//            whereCond.append(" AND a.Issold = ? ");
//            params.add(param.getIssold());
//        }
//
//        Date datefrom = null;
//        if (param.getDatefrom() != null) {
//            try {
//                datefrom = Constants.df.parse(param.getDatefrom());
//            } catch (ParseException e) {
//                resp = PaginationData.errorResponse("Parameter invalid date format");
//                e.printStackTrace();
//            }
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        //@formatter:off
//        sql.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE  cte "
//            + "   ORDER  BY Name "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//            boolean first = true;
//            int totaldata = 0;
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("m_product_id", rs.getString("m_product_id"));
//                map.put("m_product_category_id", rs.getString("m_product_category_id"));
//                map.put("value", rs.getString("value"));
//                map.put("c_taxcategory_id", rs.getString("c_taxcategory_id"));
//                map.put("m_locator_id", rs.getString("m_locator_id"));
//                map.put("uomsymbol", rs.getString("uomsymbol"));
//                map.put("volume", rs.getString("volume"));
//                map.put("issold", rs.getString("issold"));
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("description", rs.getString("description"));
//
//                Product = new ProductImplMaster();
//                if (rs.getString("m_product_id") != null) {
//                    map.put("uom", Product
//                            .getUOMByProductID(Integer.valueOf(rs.getString("m_product_id"))));
//                    map.put("uom_conversion", Product.getUOMConversionByProductID(
//                            Integer.valueOf(rs.getString("m_product_id"))));
//                    if (!Util.isEmpty(param.getShowpricelist())) {
//                        if (param.getShowpricelist().equals("Y")) {
//                            map.put("pricelist",
//                                    Product.getPriceListByProductID(
//                                            Integer.valueOf(rs.getString("m_product_id")), datefrom,
//                                            param.getC_uom_to_id(), param.getM_pricelist_id()));
//
//                        }
//                    }
//                    map.put("m_pricelist_version", Product.getPriceListVersionByProductID(
//                            Integer.valueOf(rs.getString("m_product_id"))));
//                    listdata.add(map);
//                }
//
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(totaldata + " Data Found");
//            resp.setResultdata(listdata);
//
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = PaginationData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getPricelist(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new PaginationData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "WITH cte AS (" +
//                " SELECT " + 
//                "    a.M_Pricelist_ID, " +
//                "    a.AD_Client_ID, " +
//                "    a.isActive, " +
//                "    a.Name, " +
//                "    a.Description, " +
//                "    a.isSoPricelist, " +
//                "    b.M_Pricelist_Version_ID "
//                );
//        join.append(
//                "FROM M_Pricelist a " + 
//                "LEFT JOIN M_Pricelist_Version b " +
//                        "ON a.M_Pricelist_ID = b.M_Pricelist_ID " +
//                "LEFT JOIN AD_Client c " + 
//                        "ON a.AD_Client_ID = c.AD_Client_ID ");
//        whereCond.append(
//                "WHERE a.M_Pricelist_ID IS NOT NULL "
//                + " AND a.AD_Client_ID = ? ");
//        //@formatter:on
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        String name = "";
//        if (param.getM_pricelist_id() != null) {
//            whereCond.append(" AND a.M_Pricelist_ID = ? ");
//            params.add(param.getM_pricelist_id());
//        }
//        if (param.getIssopricelist() != null) {
//            whereCond.append(" AND a.IsSOPriceList = ? ");
//            params.add(param.getIssopricelist());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(a.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//      //@formatter:off
//        sql.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE  cte "
//            + "   ORDER  BY m_pricelist_id "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//            boolean first = true;
//            int totaldata = 0;
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("m_pricelist_id", rs.getString("m_pricelist_id"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("isactive", rs.getString("isactive"));
//                map.put("name", rs.getString("name"));
//                map.put("description", rs.getString("description"));
//                map.put("issopricelist", rs.getString("issopricelist"));
//
//                Pricelist = new PricelistImplMaster();
//                map.put("m_pricelist_version", Pricelist.getPriceListVersionByPricelistID(
//                        Integer.valueOf(rs.getString("m_pricelist_id"))));
//
//                listdata.add(map);
//
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = PaginationData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = PaginationData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getPaymentRule(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    b.Value, " +
//                "    b.Name, " +
//                "    b.AD_Client_ID, " +
//                "    b.isActive, " +
//                "    a.Description, " +
//                "    b.AD_Ref_List_ID "
//                );
//        join.append(
//                "FROM AD_Reference a " + 
//                "LEFT JOIN AD_Ref_List b " + 
//                        "ON b.AD_Reference_ID = a.AD_Reference_ID ");
//        whereCond.append(
//                "WHERE LOWER(a.Name) like LOWER('%_Payment Rule%') " 
//                + " AND a.AD_Client_ID = 0 ");
//        //@formatter:on
//
//        String name = "";
//        if (param.getValue() != null) {
//            whereCond.append(" AND b.Value = ? ");
//            params.add(param.getValue());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(b.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("isactive", rs.getString("isactive"));
//                map.put("description", rs.getString("description"));
//                map.put("ad_ref_list_id", rs.getString("ad_ref_list_id"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//
//    public ResponseData getLastLogin(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//        String tahun = param.getPeriode().substring(0, 4);
//        String bulan = param.getPeriode().substring(4, 6);
//        Calendar cal = Calendar.getInstance();
//        cal.set(Integer.valueOf(tahun), Integer.valueOf(bulan) - 1, 1);
//        int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        if (param.getPeriode() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        whereCond.append(" ");
//        whereCond.append(" AND a.AD_Client_ID = ? ");
//
//        params.add(param.getPeriode());
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//
//        if (param.getAd_client_id() != null) {
//            whereCond.append(" AND a.AD_Client_ID = ? ");
//            params.add(param.getAd_client_id());
//        }
//        if (param.getIs_active() != null) {
//            whereCond.append(" AND a.IsActive = ? ");
//            params.add(param.getIs_active());
//        }
//        for (int a = 1; a <= daysInMonth; a++) {
//            params.add(param.getPeriode());
//        }
//
//        //@formatter:off
//        sql.append(
//            "   with ct as(         " + 
//                    "        select " + 
//                    "            name " + 
//                    "            ,ad_client_id " + 
//                    "            ,ad_user_id " + 
//                    "            ,day " + 
//                    "            ,tgl " + 
//                    "            ,updated " + 
//                    "         from ( " + 
//                    "             select " + 
//                    "                row_number()over(partition by day,name,ad_user_id,tgl order by a.updated desc) as row, a.* " + 
//                    "            from ( " + 
//                    "                 select " + 
//                    "                    date_part('day',b.updated) as day, " + 
//                    "                    a.name, " + 
//                    "                    a.ad_client_id, " + 
//                    "                    a.ad_user_id, " + 
//                    "                    to_char(b.updated,'YYYYMM') tgl, " + 
//                    "                    b.updated " + 
//                    "                 from ad_user a " + 
//                    "                 inner join ad_session b on b.createdby = a.ad_user_id " + 
//                    "                 inner join ad_user_roles c on c.ad_user_id = a.ad_user_id " + 
//                    "                 where  to_char(b.updated,'YYYYMM') = ?  " + whereCond+
//                    "                 and a.password is not null  " + 
//                    "                 and a.forca_isuserclient = 'Y'  " + 
//                    "             ) a " + 
//                    "         ) as sub " + 
//                    "         where row = 1 " + 
//                    "    ) ");
//        
//        select.append(" select distinct e.name,e.ad_user_id,ad_client_id ");
//        join.append(" FROM (SELECT DISTINCT name,ad_user_id,ad_client_id, tgl,updated FROM ct) e ");
//        for(int a = 1; a<=daysInMonth;a++) {
//            select.append(" , d"+a+".updated AS day"+a);
//            join.append(" LEFT JOIN (SELECT name,ad_client_id,ad_user_id, updated,tgl FROM ct WHERE day = "+a+" and tgl = ?) d"+a+" USING(name,ad_client_id,ad_user_id,tgl) ");
//        }
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(" ORDER BY e.name asc; ");     
//        //@formatter:on 
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("name", rs.getString(1));
//                map.put("ad_user_id", rs.getString(2));
//                map.put("ad_client_id", rs.getString(3));
//                for (int a = 1; a <= daysInMonth; a++) {
//                    map.put("day" + a, rs.getString(a + 3));
//                }
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData getMaxLogin(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//
//        StringBuilder whereClause = new StringBuilder();
//        StringBuilder whereClauseSession = new StringBuilder();
//
//        if (param.getDate_log() == null) {
//            return ResponseData.parameterRequired();
//        }
//
//        if (param.getAd_client_id() != null) {
//            whereClause.append(" AND a.AD_Client_ID = ? ");
//            whereClauseSession.append(" AND a.AD_Client_ID = ? ");
//        }
//
//        params.add(Integer.valueOf(param.getDate_log()));
//        params.add(Integer.valueOf(param.getDate_log()));
//        params.add(Integer.valueOf(param.getDate_log()));
//        if (whereClauseSession.length() > 0) {
//            params.add(param.getAd_client_id());
//        }
//        params.add(Integer.valueOf(param.getDate_log()));
//        params.add(Integer.valueOf(param.getDate_log()));
//        params.add(Integer.valueOf(param.getDate_log()));
//        params.add(Integer.valueOf(param.getDate_log()));
//        if (whereClause.length() > 0) {
//            params.add(param.getAd_client_id());
//        }
//
//        //@formatter:off
//        String sql = 
//                "with a as(  " + 
//                "select  " + 
//                "    u.ad_user_id, " + 
//                "    u.ad_client_id, " + 
//                "    c.name as client_name, " + 
//                "    u.name as user_name,   " + 
//                "    u.email, " + 
//                "    u.description, " + 
//                "    u.phone, " + 
//                "    u.phone2, " + 
//                "    u.isactive, " + 
//                "    u.islocked, " + 
//                "    u.created, " + 
//                "    u.createdby, " + 
//                "    ( " + 
//                "        select  " + 
//                "            ub.name " + 
//                "        from ad_user ub " + 
//                "        where ub.ad_user_id = u.createdby " + 
//                "    ) created_name, " + 
//                "    ( " + 
//                "            select  " + 
//                "                max(a.updated) " + 
//                "            from ad_changelog a " + 
//                "            where to_char(a.updated,'YYYYMM')::int <= ? " + 
//                "            and a.newvalue = 'true' " +
//                "            and a.record_id = u.ad_user_id " +
//                "            and a.ad_table_id = (select ad_table_id from ad_table where tablename = 'AD_User') " +
//                "            and a.ad_column_id = (select ad_column_id from ad_column where columnname = 'IsActive' and ad_table_id = a.ad_table_id) " +
//                "    ) date_actived, " + 
//                "    ( " + 
//                "            select  " + 
//                "                max(a.updated) " + 
//                "            from ad_changelog a " + 
//                "            where to_char(a.updated,'YYYYMM')::int <= ? " + 
//                "            and a.newvalue = 'false' " +
//                "            and a.record_id = u.ad_user_id " +
//                "            and a.ad_table_id = (select ad_table_id from ad_table where tablename = 'AD_User') " +
//                "            and a.ad_column_id = (select ad_column_id from ad_column where columnname = 'IsActive' and ad_table_id = a.ad_table_id) " +
//                "    ) date_deactived, " + 
//                "    d.updated lastlogin_date, " + 
//                "    d.remote_addr lastlogin_remote_addr, " + 
//                "    d.remote_host lastlogin_remote_host " + 
//                "from ad_user u " + 
//                "inner join ad_client c using(ad_client_id) " + 
//                "left join ( " + 
//                "    with a as( " + 
//                "        select " + 
//                "            row_number()over(partition by a.createdby order by a.updated desc) id, " + 
//                "            a.createdby ad_user_id, " + 
//                "            a.updated, " + 
//                "            a.remote_addr, " + 
//                "            a.remote_host " + 
//                "        from ad_session a " + 
//                "        where to_char(a.updated,'YYYYMM')::int <= ? " + whereClauseSession.toString()+
//                "    ) select * from a where a.id = 1 " + 
//                ") d using(ad_user_id) " + 
//                "where u.forca_isuserclient = 'Y' " + 
//                "and u.password is not null " +
//                "and (to_char(u.created,'YYYYMM')::int <= ? ) " +
//                "AND c.isactive = 'Y' " + 
//                "AND u.name not like 's-%' " + 
//                "AND u.name not like 'f-%' " + 
//                "AND u.name not like 'Migrasi%' " + 
//                "AND u.name not like '%migrasi%' " + 
//                "AND u.name not like '%api_user%' " + 
//                "AND u.name not like '%WebService%' " + 
//                "AND u.name not like '%GardenUser%' " + 
//                "AND u.name not like '%GardenAdmin%' " + 
//                "AND u.name not like 'support%' " + 
//                "AND u.name not like '%WsAdmin%' " + 
//                "AND u.name not like '%sisi%' " + 
//                "AND u.name not like 'Yogi' " + 
//                "AND u.name not like '%User%' " + 
//                "AND u.name not like '%Admin%' " + 
//                "AND c.name not like 'GardenWorld' " + 
//                ") " +
//                "select  " + 
//                "    *  " + 
//                "from a  " + 
//                "where ( " + 
//                "    to_char(a.lastlogin_date,'YYYYMM')::int = ? " + 
//                "    or ( " + 
//                "        to_char(a.date_deactived,'YYYYMM')::int = ?  " + 
//                "    ) " + 
//                "    or ( " + 
//                "        a.date_deactived is null " + 
//                "        and a.date_actived is not null " + 
//                "    ) " + 
//                "    or ( " + 
//                "        a.date_deactived is null " + 
//                "        and a.date_actived is null " + 
//                "        and a.isactive = 'Y' " + 
//                "    ) " +
//                "    or ( " + 
//                "        a.date_actived > a.date_deactived " + 
//                "        and to_char(a.date_actived,'YYYYMM')::int <= ?  " +
//                "    )"+
//                ") "
//                +whereClause.toString()+
//                "order by a.date_actived desc "
//                ;
//        //@formatter:on
//
//        try {
//            ps = DB.prepareStatement(sql, null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                Map<String, String> map = new LinkedHashMap<String, String>();
//                map.put("ad_user_id", rs.getString("ad_user_id"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("client_name", rs.getString("client_name"));
//                map.put("user_name", rs.getString("user_name"));
//                map.put("email", rs.getString("email"));
//                map.put("description", rs.getString("description"));
//                map.put("phone", rs.getString("phone"));
//                map.put("phone2", rs.getString("phone2"));
//                map.put("isactive", rs.getString("isactive"));
//                map.put("islocked", rs.getString("islocked"));
//                map.put("created", rs.getString("created"));
//                map.put("createdby", rs.getString("createdby"));
//                map.put("created_name", rs.getString("created_name"));
//                map.put("date_actived", rs.getString("date_actived"));
//                map.put("date_deactived", rs.getString("date_deactived"));
//                map.put("lastlogin_date", rs.getString("lastlogin_date"));
//                map.put("lastlogin_remote_addr", rs.getString("lastlogin_remote_addr"));
//                map.put("lastlogin_remote_host", rs.getString("lastlogin_remote_host"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getLocator(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    M_Locator_ID, " + 
//                "    Value AS locator_name "  
//                );
//        join.append(
//                "FROM M_Locator ");
//        whereCond.append(
//                "WHERE M_Locator_ID IS NOT NULL "
//                + " AND AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        if (param.getM_locator_id() != null) {
//            whereCond.append(" AND M_Locator_ID = ? ");
//            params.add(param.getM_locator_id());
//        }
//
//        if (param.getM_warehouse_id() != null) {
//            whereCond.append(" AND m_warehouse_id = ? ");
//            params.add(param.getM_warehouse_id());
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("m_locator_id", rs.getString("m_locator_id"));
//                map.put("locator_name", rs.getString("locator_name"));
//                listdata.add(map);
//            }
//
//            resp = ResponseData.successResponse(listdata.size() + " Data Found", listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//
//    public ResponseData setBPartner(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//        boolean isInsert = false;
//
//        if (param.getName() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        try {
//            String whereClause = " lower(Name) LIKE lower(?) ";
//            MBPartner bp = null;
//            String country_name = "";
//
//            if (!Util.isEmpty(param.getCountry_name())) {
//                country_name = param.getCountry_name();
//            } else {
//                country_name = "Indonesia";
//            }
//
//            MCountry country = new Query(Env.getCtx(), MCountry.Table_Name, whereClause, null)
//                    .setParameters(country_name).first();
//
//            MRegion region = new Query(Env.getCtx(), MRegion.Table_Name, whereClause, null)
//                    .setParameters(param.getRegion_name() + "%").first();
//
//            MCity city = new Query(Env.getCtx(), MCity.Table_Name, whereClause, null)
//                    .setParameters(param.getCity_name()).first();
//
//            if (param.getC_bpartner_id() != null) {
//                bp = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name,
//                        param.getC_bpartner_id(), trxName);
//                if (bp == null) {
//                    bp = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name,
//                            getIdFromSearchkey(param.getSearchkey(), param.getDistributorcode(),
//                                    trxName),
//                            trxName);
//                    if (bp == null) {
//                        bp = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name, 0, trxName);
//                        isInsert = true;
//                    }
//                }
//            } else {
//                bp = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name, 0, trxName);
//                isInsert = true;
//            }
//
//            if (param.getName() != null) {
//                bp.setName(param.getName());
//            } else if (isInsert) {
//                ResponseData.errorResponse("Parameter Required for Insert : Name");
//            }
//            if (param.getName2() != null) {
//                bp.setName2(param.getName2());
//            }
//            if (param.getReferenceno() != null) {
//                bp.setReferenceNo(param.getReferenceno());
//            }
//            if (param.getSo_credit_limit() != null) {
//                bp.setSO_CreditLimit(new BigDecimal(param.getSo_credit_limit()));
//            }
//            if (param.getForca_crm_bpartner_id() != null) {
//                bp.set_ValueOfColumn("forca_crm_bpartner_id", param.getForca_crm_bpartner_id());
//            }
//            if (param.getId_customer() != null) {
//                bp.set_CustomColumn("id_customer", param.getId_customer());
//            }
//            if (param.getSap_code() != null) {
//                bp.set_CustomColumn("sap_code", param.getSap_code());
//            }
//
//            if (param.getSearchkey() != null) {
//                bp.setValue(param.getSearchkey());
//            }
//
//            if (bp.save()) {
//                String prmtr = "AD_Client_ID = " + bp.getAD_Client_ID() + " AND C_BPartner_ID="
//                        + bp.getC_BPartner_ID();
//                Query q = new Query(Env.getCtx(), "C_BPartner_Location", prmtr, null);
//                q.setOnlyActiveRecords(true);
//                MBPartnerLocation bploc = (MBPartnerLocation) q.first();
//
//                MLocation loc;
//
//                if (bploc == null) {
//                    loc = ForcaObject.getObject(Env.getCtx(), MLocation.Table_Name, 0, trxName);
//                } else {
//                    loc = ForcaObject.getObject(Env.getCtx(), MLocation.Table_Name,
//                            bploc.getC_Location_ID(), trxName);
//                }
//
//                loc.setAddress1(param.getAddress1());
//                loc.setAddress2(param.getAddress2());
//                loc.setAddress3(param.getAddress3());
//                loc.setAddress4(param.getAddress4());
//
//                if (country != null) {
//                    loc.setC_Country_ID(country.get_ID());
//                }
//
//                if (region != null) {
//                    loc.setC_Region_ID(region.get_ID());
//                }
//
//                if (city != null) {
//                    loc.setC_City_ID(city.get_ID());
//                }
//
//                if (loc.save()) {
//                    MBPartnerLocation bplocation = null;
//                    if (bploc == null) {
//                        bplocation = ForcaObject.getObject(Env.getCtx(),
//                                MBPartnerLocation.Table_Name, 0, trxName);
//                    } else {
//                        bplocation =
//                                ForcaObject.getObject(Env.getCtx(), MBPartnerLocation.Table_Name,
//                                        bp.getPrimaryC_BPartner_Location_ID(), trxName);
//                    }
//                    bplocation.setName(loc.getAddress1());
//                    bplocation.setC_BPartner_ID(bp.getC_BPartner_ID());
//                    bplocation.setC_Location_ID(loc.getC_Location_ID());
//                    bplocation.saveEx();
//                }
//
//                Map<String, Object> map = new LinkedHashMap<String, Object>();
//                map.put("c_bpartner_id", Integer.valueOf(bp.getC_BPartner_ID()));
//                map.put("forca_crm_bpartner_id", bp.get_Value("forca_crm_bpartner_id"));
//                map.put("searchkey", bp.getValue());
//                map.put("name", bp.getName());
//                map.put("sap_code", bp.get_Value("sap_code"));
//                map.put("id_customer", bp.get_Value("id_customer"));
//                map.put("taxid", bp.getTaxID());
//                map.put("c_bpartner_location_id", bp.getPrimaryC_BPartner_Location_ID());
//                map.put("c_location_id", loc.get_ID());
//
//                MClient client = ForcaObject.getObject(Env.getCtx(), MClient.Table_Name,
//                        bp.getAD_Client_ID(), trxName);
//                map.put("distributor_name", client.getName());
//                map.put("distributor_code", client.get_Value("kode_dist"));
//                listdata.add(map);
//            }
//
//            if (isInsert)
//                resp = ResponseData.successResponse("Insert Business Partner Success", listdata);
//            else
//                resp = ResponseData.successResponse("Update Business Partner Success", listdata);
//
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setBPartnerLocation(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//        boolean isInsert = false;
//
//        try {
//            String whereClause = " lower(Name) LIKE lower(?) ";
//            MBPartner bp = null;
//            MBPartnerLocation bploc = null;
//            MLocation loc = null;
//            String country_name = "";
//
//            if (!Util.isEmpty(param.getCountry_name())) {
//                country_name = param.getCountry_name();
//            } else {
//                country_name = "Indonesia";
//            }
//
//            MCountry country = new Query(Env.getCtx(), MCountry.Table_Name, whereClause, null)
//                    .setParameters(country_name).first();
//            MRegion region = new Query(Env.getCtx(), MRegion.Table_Name, whereClause, null)
//                    .setParameters(param.getRegion_name() + "%").first();
//            MCity city = new Query(Env.getCtx(), MCity.Table_Name, whereClause, null)
//                    .setParameters(param.getCity_name()).first();
//
//            bp = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name, param.getC_bpartner_id(),
//                    trxName);
//            if (bp == null) {
//                throw new Exception("Business Partner Not Found, Please Check Your Parameter");
//            }
//
//            if (param.getC_bpartner_location_id() != null) {
//                bploc = ForcaObject.getObject(Env.getCtx(), MBPartnerLocation.Table_Name,
//                        param.getC_bpartner_location_id(), trxName);
//                if (bploc == null) {
//                    bploc = ForcaObject.getObject(Env.getCtx(), MBPartnerLocation.Table_Name, 0,
//                            trxName);
//                    isInsert = true;
//                }
//            } else {
//                bploc = ForcaObject.getObject(Env.getCtx(), MBPartnerLocation.Table_Name, 0,
//                        trxName);
//                isInsert = true;
//            }
//
//            if (isInsert) {
//                loc = ForcaObject.getObject(Env.getCtx(), MLocation.Table_Name, 0, trxName);
//            } else {
//                loc = ForcaObject.getObject(Env.getCtx(), MLocation.Table_Name,
//                        bploc.getC_Location_ID(), trxName);
//            }
//
//            loc.setAddress1(param.getAddress1());
//            loc.setAddress2(param.getAddress2());
//            loc.setAddress3(param.getAddress3());
//            loc.setAddress4(param.getAddress4());
//
//            if (country != null) {
//                loc.setC_Country_ID(country.get_ID());
//            }
//            if (region != null) {
//                loc.setC_Region_ID(region.get_ID());
//            }
//            if (city != null) {
//                loc.setC_City_ID(city.get_ID());
//            }
//
//            if (loc.save()) {
//                bploc.setName(loc.getAddress1());
//                bploc.setC_BPartner_ID(bp.getC_BPartner_ID());
//                bploc.setC_Location_ID(loc.getC_Location_ID());
//                if (!Util.isEmpty(param.getPhone())) {
//                    bploc.setPhone(param.getPhone());
//                }
//                bploc.saveEx();
//            }
//
//            Map<String, Object> map = new LinkedHashMap<String, Object>();
//            map.put("c_bpartner_id", bp.get_ID());
//            map.put("c_bpartner_location_id", bploc.getC_BPartner_Location_ID());
//            map.put("c_location_id", loc.get_ID());
//
//            listdata.add(map);
//
//            if (isInsert)
//                resp = ResponseData.successResponse("Insert Business Partner Location Success",
//                        listdata);
//            else
//                resp = ResponseData.successResponse("Update Business Partner Location Success",
//                        listdata);
//
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    private Integer getIdFromSearchkey(String searchKey, String kodeDistributor, String trxName)
//            throws Exception {
//        Boolean isNotNull = searchKey != null && kodeDistributor != null;
//        Boolean isNotNol = searchKey != "0" && kodeDistributor != "0";
//        int id = 0;
//        if (isNotNull && isNotNol) {
//            try {
//                // @formatter:off
//                id = DB.getSQLValue(trxName, " "
//                        + "SELECT "
//                        + "    cbp.c_bpartner_id "
//                        + "FROM "
//                        + "    c_bpartner cbp "
//                        + "JOIN ad_client ac ON "
//                        + "    ac.ad_client_id = cbp.ad_client_id "
//                        + "WHERE "
//                        + "    cbp.value = ? "
//                        + "    AND ac.kode_dist = ? ",
//                        searchKey, kodeDistributor);
//                // @formatter:on
//            } catch (AdempiereException e) {
//                throw new AdempiereException(e);
//            } catch (Exception e) {
//                throw new Exception(e);
//            }
//        }
//        return id;
//    }
//
//    public ResponseData setUser(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        if (param.getAd_user_id() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        MUser u = null;
//        try {
//            MUser user = ForcaObject.getObject(Env.getCtx(), MUser.Table_Name,
//                    param.getAd_user_id(), trxName);
//            if (user == null) {
//                u = ForcaObject.getObject(Env.getCtx(), MUser.Table_Name, 0, trxName);
//            } else {
//                u = user;
//            }
//
//            if (param.getC_bpartner_id() != null) {
//                u.setC_BPartner_ID(param.getC_bpartner_id());
//            }
//            if (param.getName() != null) {
//                u.setName(param.getName());
//            }
//            if (param.getPhone() != null) {
//                u.setPhone(param.getPhone());
//            }
//            if (param.getDescription() != null) {
//                u.setDescription(param.getDescription());
//            }
//            if (param.getPhone2() != null) {
//                u.setPhone2(param.getPhone2());
//            }
//            if (param.getFax() != null) {
//                u.setFax(param.getFax());
//            }
//            if (param.getTitle() != null) {
//                u.setTitle(param.getTitle());
//            }
//            if (param.getEmail() != null) {
//                u.setEMail(param.getEmail());
//            }
//            if (param.getForca_crm_contact_id() != null) {
//                u.set_ValueOfColumn("forca_crm_contact_id", param.getForca_crm_contact_id());
//            }
//            if (u.save()) {
//                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//                xmap.put("ad_user_id", Integer.valueOf(u.getAD_User_ID()));
//                xmap.put("c_bpartner_id", Integer.valueOf(u.getC_BPartner_ID()));
//                xmap.put("forca_crm_contact_id", u.get_Value("forca_crm_contact_id"));
//                listdata.add(xmap);
//                resp.setCodestatus("S");
//                resp.setMessage("Success update data User/Contact");
//                resp.setResultdata(listdata);
//            } else {
//                resp.setMessage("Failed to save data");
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData setUom(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        if (param.getName() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        MUOM uom = null;
//        MUOMConversion uomc = null;
//        boolean isInsert = false;
//        try {
//            if (param.getC_uom_id() != null) {
//                uom = ForcaObject.getObject(Env.getCtx(), MUOM.Table_Name, param.getC_uom_id(),
//                        trxName);
//                if (uom == null) {
//                    uom = ForcaObject.getObject(Env.getCtx(), MUOM.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            } else {
//                uom = ForcaObject.getObject(Env.getCtx(), MUOM.Table_Name, 0, trxName);
//                isInsert = true;
//            }
//
//            if (isInsert) {
//                if (param.getName() != null) {
//                    uom.setName(param.getName());
//                    uom.setX12DE355(param.getName());
//                }
//                if (param.getDescription() != null) {
//                    uom.setDescription(param.getDescription());
//                }
//                if (param.getUomsymbol() != null) {
//                    uom.setUOMSymbol(param.getUomsymbol());
//                } else {
//                    uom.setUOMSymbol(param.getName());
//                }
//                if (param.getCrm_id() != null) {
//                    uom.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//
//                uom.setUOMType(MUOM.UOMTYPE_Other);
//
//                if (param.getStdprecision() != null) {
//                    uom.setStdPrecision(param.getStdprecision());
//                } else {
//                    uom.setStdPrecision(2);
//                }
//
//                if (param.getCostingprecision() != null) {
//                    uom.setCostingPrecision(param.getCostingprecision());
//                } else {
//                    uom.setCostingPrecision(2);
//                }
//            } else {
//                if (param.getName() != null) {
//                    uom.setName(param.getName());
//                }
//                if (param.getDescription() != null) {
//                    uom.setDescription(param.getDescription());
//                }
//                if (param.getUomsymbol() != null) {
//                    uom.setUOMSymbol(param.getUomsymbol());
//                }
//                if (param.getCrm_id() != null) {
//                    uom.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//
//                if (param.getStdprecision() != null) {
//                    uom.setStdPrecision(param.getStdprecision());
//                }
//
//                if (param.getCostingprecision() != null) {
//                    uom.setCostingPrecision(param.getCostingprecision());
//                }
//            }
//
//            if (uom.save()) {
//                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//                xmap.put("c_uom_id", Integer.valueOf(uom.getC_UOM_ID()));
//                if (param.getCrm_id() != null) {
//                    xmap.put("crm_id", Integer.valueOf(param.getCrm_id()));
//                }
//
//                if (param.getC_uom_to_id() != null) {
//                    if (param.getC_uom_conversion_id() != null) {
//                        uomc = ForcaObject.getObject(Env.getCtx(), MUOMConversion.Table_Name,
//                                param.getC_uom_conversion_id(), trxName);
//                        if (uomc == null) {
//                            uomc = ForcaObject.getObject(Env.getCtx(), MUOMConversion.Table_Name, 0,
//                                    trxName);
//                        }
//                    } else {
//                        uomc = ForcaObject.getObject(Env.getCtx(), MUOMConversion.Table_Name, 0,
//                                trxName);
//                    }
//
//                    uomc.setC_UOM_ID(uom.get_ID());
//                    if (param.getM_product_id() != null) {
//                        uomc.setM_Product_ID(param.getM_product_id());
//                    }
//                    if (param.getMultiplyrate() != null) {
//                        uomc.setMultiplyRate(param.getMultiplyrate());
//                    }
//                    if (param.getDividerate() != null) {
//                        uomc.setDivideRate(param.getDividerate());
//                    }
//                    if (param.getC_uom_to_id() != null) {
//                        uomc.setC_UOM_To_ID(param.getC_uom_to_id());
//                    }
//                    uomc.saveEx();
//
//                    if (uomc.getC_UOM_Conversion_ID() > 0) {
//                        xmap.put("c_uom_conversion_id",
//                                Integer.valueOf(uomc.getC_UOM_Conversion_ID()));
//                        xmap.put("c_uom_to_id", Integer.valueOf(uomc.getC_UOM_To_ID()));
//                    }
//                }
//
//                if (isInsert) {
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success insert data Unit of Measure");
//                    resp.setResultdata(listdata);
//                } else {
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success update data Unit of Measure");
//                    resp.setResultdata(listdata);
//                }
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData setOrg(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        if (param.getAd_org_id() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        MOrg o = null;
//        try {
//            MOrg org = ForcaObject.getObject(Env.getCtx(), MOrg.Table_Name, param.getAd_org_id(),
//                    trxName);
//            if (org == null) {
//                o = ForcaObject.getObject(Env.getCtx(), MOrg.Table_Name, 0, trxName);
//            } else {
//                o = org;
//            }
//
//            if (param.getName() != null) {
//                o.setName(param.getName());
//            }
//            if (param.getDescription() != null) {
//                o.setDescription(param.getDescription());
//            }
//            if (param.getValue() != null) {
//                o.setValue(param.getValue());
//            }
//            if (param.getCrm_id() != null) {
//                o.set_ValueOfColumn("crm_id", param.getCrm_id());
//            }
//            if (o.save()) {
//                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//                xmap.put("ad_org_id", Integer.valueOf(o.getAD_Org_ID()));
//                if (param.getCrm_id() != null) {
//                    xmap.put("crm_id", Integer.valueOf(param.getCrm_id()));
//                }
//                listdata.add(xmap);
//                resp.setCodestatus("S");
//                resp.setMessage("Success update data Organization");
//                resp.setResultdata(listdata);
//            } else {
//                resp.setMessage("Failed to save data");
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData setProduct(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//        MProductPrice prdprice = null;
//
//        if (param.getName() == null || param.getM_product_category_id() == null
//                || param.getC_taxcategory_id() == null || param.getC_uom_id() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        MProduct p = null;
//        boolean isInsert = false;
//        try {
//            if (param.getM_product_id() != null) {
//                p = ForcaObject.getObject(Env.getCtx(), MProduct.Table_Name,
//                        param.getM_product_id(), trxName);
//                if (p == null) {
//                    p = ForcaObject.getObject(Env.getCtx(), MProduct.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            } else {
//                p = ForcaObject.getObject(Env.getCtx(), MProduct.Table_Name, 0, trxName);
//                isInsert = true;
//            }
//
//            if (isInsert) {
//                p.setValue(param.getValue() == null ? param.getName() : param.getValue());
//                if (param.getName() != null) {
//                    p.setName(param.getName());
//                }
//                p.setM_Product_Category_ID(param.getM_product_category_id());
//                p.setC_TaxCategory_ID(param.getC_taxcategory_id());
//                p.setC_UOM_ID(param.getC_uom_id());
//                p.setProductType(MProduct.PRODUCTTYPE_Item);
//                if (param.getDescription() != null) {
//                    p.setDescription(param.getDescription());
//                }
//
//                if (param.getCrm_id() != null) {
//                    p.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//            } else {
//                if (param.getValue() != null) {
//                    p.setValue(param.getValue());
//                }
//                if (param.getName() != null) {
//                    p.setName(param.getName());
//                }
//                if (param.getM_product_category_id() != null) {
//                    p.setM_Product_Category_ID(param.getM_product_category_id());
//                }
//                if (param.getC_taxcategory_id() != null) {
//                    p.setC_TaxCategory_ID(param.getC_taxcategory_id());
//                }
//                if (param.getC_uom_id() != null) {
//                    p.setC_UOM_ID(param.getC_uom_id());
//                }
//                if (param.getDescription() != null) {
//                    p.setDescription(param.getDescription());
//                }
//                if (param.getCrm_id() != null) {
//                    p.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//            }
//
//            if (p.save()) {
//                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//                xmap.put("m_prduct_id", Integer.valueOf(p.getM_Product_ID()));
//                if (param.getCrm_id() != null) {
//                    xmap.put("crm_id", Integer.valueOf(param.getCrm_id()));
//                }
//
//                if (param.getM_pricelist_version_id() != null) {
//                    if (param.getM_productprice_id() != null) {
//                        prdprice = ForcaObject.getObject(Env.getCtx(), MProductPrice.Table_Name,
//                                param.getM_productprice_id(), trxName);
//                        if (prdprice == null) {
//                            prdprice = ForcaObject.getObject(Env.getCtx(), MProductPrice.Table_Name,
//                                    0, trxName);
//                        }
//                    } else {
//                        prdprice = ForcaObject.getObject(Env.getCtx(), MProductPrice.Table_Name, 0,
//                                trxName);
//                    }
//
//                    prdprice.setM_Product_ID(p.get_ID());
//                    if (param.getM_pricelist_version_id() != null) {
//                        prdprice.setM_PriceList_Version_ID(param.getM_pricelist_version_id());
//                    }
//                    if (param.getPricelist() != null) {
//                        prdprice.setPriceList(new BigDecimal(param.getPricelist()));
//                    }
//                    if (param.getPricestd() != null) {
//                        prdprice.setPriceStd(new BigDecimal(param.getPricestd()));
//                    }
//                    if (param.getPricelimit() != null) {
//                        prdprice.setPriceLimit(new BigDecimal(param.getPricelimit()));
//                    }
//
//                    prdprice.saveEx();
//
//                    String prmtr = "M_Pricelist_Version_ID=" + prdprice.getM_PriceList_Version_ID();
//                    Query q = new Query(Env.getCtx(), "M_Pricelist_Version", prmtr, null);
//                    q.setOnlyActiveRecords(true);
//                    MPriceListVersion pricelist = (MPriceListVersion) q.first();
//
//                    if (prdprice.get_ID() > 0) {
//                        xmap.put("m_productprice_id", Integer.valueOf(prdprice.get_ID()));
//                        xmap.put("m_pricelist_version_id",
//                                Integer.valueOf(prdprice.getM_PriceList_Version_ID()));
//                        xmap.put("m_pricelist_id", Integer.valueOf(pricelist.getM_PriceList_ID()));
//                    }
//                }
//
//                if (isInsert) {
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success insert data Product");
//                    resp.setResultdata(listdata);
//                } else {
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success update data Product");
//                    resp.setResultdata(listdata);
//                }
//            } else {
//                resp.setMessage("Failed to save data");
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData setProductCategory(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        if (param.getName() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        MProductCategory pc = null;
//        boolean isInsert = false;
//        try {
//            if (param.getM_product_category_id() != null) {
//                pc = ForcaObject.getObject(Env.getCtx(), MProductCategory.Table_Name,
//                        param.getM_product_category_id(), trxName);
//                if (pc == null) {
//                    pc = ForcaObject.getObject(Env.getCtx(), MProductCategory.Table_Name, 0,
//                            trxName);
//                    isInsert = true;
//                }
//            } else {
//                pc = ForcaObject.getObject(Env.getCtx(), MProductCategory.Table_Name, 0, trxName);
//                isInsert = true;
//            }
//
//            if (isInsert) {
//                pc.setMMPolicy(MProductCategory.MMPOLICY_FiFo);
//                pc.setPlannedMargin(Env.ZERO);
//                pc.setName(param.getName());
//
//                if (param.getDescription() != null) {
//                    pc.setDescription(param.getDescription());
//                }
//                if (param.getValue() != null) {
//                    pc.setValue(param.getValue());
//                } else {
//                    pc.setValue(param.getName());
//                }
//                if (param.getCrm_id() != null) {
//                    pc.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//            } else {
//                if (param.getName() != null) {
//                    pc.setName(param.getName());
//                }
//
//                if (param.getDescription() != null) {
//                    pc.setDescription(param.getDescription());
//                }
//                if (param.getValue() != null) {
//                    pc.setValue(param.getValue());
//                }
//                if (param.getCrm_id() != null) {
//                    pc.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//            }
//
//            if (pc.save()) {
//                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//                xmap.put("m_product_category_id", Integer.valueOf(pc.getM_Product_Category_ID()));
//                if (param.getCrm_id() != null) {
//                    xmap.put("crm_id", Integer.valueOf(param.getCrm_id()));
//                }
//                if (isInsert) {
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success insert data Product Category");
//                    resp.setResultdata(listdata);
//                } else {
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success update data Product Category");
//                    resp.setResultdata(listdata);
//                }
//            } else {
//                resp.setMessage("Failed to save data");
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData setTaxCategory(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        if (param.getC_taxcategory_id() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        boolean isInsert = false;
//        MTaxCategory tc = null;
//        try {
//            if (param.getC_taxcategory_id() != null) {
//                tc = ForcaObject.getObject(Env.getCtx(), MTaxCategory.Table_Name,
//                        param.getC_taxcategory_id(), trxName);
//                if (tc == null) {
//                    tc = ForcaObject.getObject(Env.getCtx(), MTaxCategory.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            } else {
//                tc = ForcaObject.getObject(Env.getCtx(), MTaxCategory.Table_Name, 0, trxName);
//                isInsert = true;
//
//            }
//
//            if (param.getName() != null) {
//                tc.setName(param.getName());
//            }
//            if (param.getDescription() != null) {
//                tc.setDescription(param.getDescription());
//            }
//            if (param.getCrm_id() != null) {
//                tc.set_ValueOfColumn("crm_id", param.getCrm_id());
//            }
//
//            tc.saveEx();
//
//            Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//            xmap.put("c_taxcategory_id", Integer.valueOf(tc.getC_TaxCategory_ID()));
//            if (param.getCrm_id() != null) {
//                xmap.put("crm_id", Integer.valueOf(param.getCrm_id()));
//            }
//            if (isInsert) {
//                listdata.add(xmap);
//                resp.setCodestatus("S");
//                resp.setMessage("Success insert data Tsetcampadeleteax Category");
//                resp.setResultdata(listdata);
//            } else {
//                listdata.add(xmap);
//                resp.setCodestatus("S");
//                resp.setMessage("Success update data Tax Category");
//                resp.setResultdata(listdata);
//            }
//
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData setTax(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        if (param.getName() == null || param.getC_taxcategory_id() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        MTax tax = null;
//        boolean isInsert = false;
//        try {
//            if (param.getC_tax_id() != null) {
//                tax = ForcaObject.getObject(Env.getCtx(), MTax.Table_Name, param.getC_tax_id(),
//                        trxName);
//                if (tax == null) {
//                    tax = ForcaObject.getObject(Env.getCtx(), MTax.Table_Name, 0, trxName);
//                    isInsert = true;
//                }
//            } else {
//                tax = ForcaObject.getObject(Env.getCtx(), MTax.Table_Name, 0, trxName);
//                isInsert = true;
//
//            }
//
//            if (isInsert) {
//                tax.setName(param.getName());
//                tax.setValidFrom(Timestamp.valueOf(LocalDateTime.now().minusMonths(1)));
//                if (param.getDescription() != null) {
//                    tax.setDescription(param.getDescription());
//                }
//                if (param.getCrm_id() != null) {
//                    tax.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//                tax.setC_TaxCategory_ID(param.getC_taxcategory_id());
//                if (param.getSopotype() != null) {
//                    tax.setSOPOType(param.getSopotype());
//                } else if (param.getSopotype() == null) {
//                    tax.setSOPOType(MTax.SOPOTYPE_Both);
//                }
//                tax.setRate(BigDecimal.TEN);
//            } else {
//                tax.setName(param.getName());
//                if (param.getDescription() != null) {
//                    tax.setDescription(param.getDescription());
//                }
//                if (param.getCrm_id() != null) {
//                    tax.set_ValueOfColumn("crm_id", param.getCrm_id());
//                }
//                tax.setC_TaxCategory_ID(
//                        param.getC_taxcategory_id() == null ? 0 : param.getC_taxcategory_id());
//            }
//
//            if (tax.save()) {
//                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//                xmap.put("c_tax_id", Integer.valueOf(tax.getC_Tax_ID()));
//                if (param.getCrm_id() != null) {
//                    xmap.put("crm_id", Integer.valueOf(param.getCrm_id()));
//                }
//
//                if (isInsert) {
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success insert data Tax");
//                    resp.setResultdata(listdata);
//                } else {
//                    listdata.add(xmap);
//                    resp.setCodestatus("S");
//                    resp.setMessage("Success update data Tax");
//                    resp.setResultdata(listdata);
//                }
//            } else {
//                resp.setMessage("Failed to save data");
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData setChannel(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        if (param.getC_channel_id() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        X_C_Channel c = null;
//        try {
//            X_C_Channel ch = ForcaObject.getObject(Env.getCtx(), X_C_Channel.Table_Name,
//                    param.getC_channel_id(), trxName);
//            if (ch == null) {
//                c = ForcaObject.getObject(Env.getCtx(), X_C_Channel.Table_Name, 0, trxName);
//            } else {
//                c = ch;
//            }
//
//            if (param.getName() != null) {
//                c.setName(param.getName());
//            }
//            if (param.getDescription() != null) {
//                c.setDescription(param.getDescription());
//            }
//            if (c.save()) {
//                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//                xmap.put("c_channel_id", Integer.valueOf(c.getC_Channel_ID()));
//                listdata.add(xmap);
//                resp.setCodestatus("S");
//                resp.setMessage("Success update data Channel");
//                resp.setResultdata(listdata);
//            } else {
//                resp.setMessage("Failed to save data");
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData setCampaign(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//        List<Object> listdata = new ArrayList<Object>();
//
//        if (param.getC_channel_id() == null || param.getC_campaign_id() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        X_C_Campaign c = null;
//        try {
//            X_C_Campaign cp = ForcaObject.getObject(Env.getCtx(), X_C_Campaign.Table_Name,
//                    param.getC_campaign_id(), trxName);
//            if (cp == null) {
//                c = ForcaObject.getObject(Env.getCtx(), X_C_Campaign.Table_Name, 0, trxName);
//            } else {
//                c = cp;
//            }
//
//            c.setC_Channel_ID(param.getC_channel_id());
//            if (param.getName() != null) {
//                c.setName(param.getName());
//            }
//            if (param.getDescription() != null) {
//                c.setDescription(param.getDescription());
//            }
//            if (c.save()) {
//                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//                xmap.put("c_campaign_id", Integer.valueOf(c.getC_Campaign_ID()));
//                listdata.add(xmap);
//                resp.setCodestatus("S");
//                resp.setMessage("Success update data Campaign");
//                resp.setResultdata(listdata);
//            } else {
//                resp.setMessage("Failed to save data");
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//
//    public ResponseData insertUserMobile(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        Timestamp exptime = new Timestamp(System.currentTimeMillis());
//        Calendar cal = Calendar.getInstance();
//        cal.setTimeInMillis(exptime.getTime());
//        cal.add(Calendar.MONTH, 1);// expired 1 bulan
//        exptime = new Timestamp(cal.getTime().getTime());
//
//        if (param.getForca_device_id() == null || param.getForca_manufaktur() == null
//                || param.getForca_model() == null || param.getForca_version() == null
//                || param.getForca_platform() == null) {
//            ResponseData.parameterRequired();
//        }
//
//        MFORCAUserMobile um = null;
//        try {
//            boolean isInsert = false;
//
//            String whereClause = " Forca_Device_ID = ? ";
//            um = new Query(Env.getCtx(), MFORCAUserMobile.Table_Name, whereClause, null)
//                    .setParameters(param.getForca_device_id()).first();
//            if (um == null) {
//                um = ForcaObject.getObject(Env.getCtx(), MFORCAUserMobile.Table_Name, 0, trxName);
//                isInsert = true;
//            }
//
//            um.setFORCA_Platform(param.getForca_platform());
//            um.setFORCA_Model(param.getForca_model());
//            um.setFORCA_Manufaktur(param.getForca_manufaktur());
//            um.setFORCA_Device_ID(param.getForca_device_id());
//            um.setFORCA_Version(param.getForca_version());
//
//            um.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
//            um.setAD_User_ID(Env.getAD_User_ID(Env.getCtx()));
//            um.setExpiredDate(exptime);
//
//            if (param.getForca_versionappcode() != null) {
//                um.setFORCA_VersionAppCode(param.getForca_versionappcode());
//            }
//            if (param.getForca_versionappname() != null) {
//                um.setFORCA_VersionAppName(param.getForca_versionappname());
//            }
//
//            um.saveEx();
//            Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//            xmap.put("forca_user_mobile_id", Integer.valueOf(um.get_ID()));
//            xmap.put("forca_device_id", um.getFORCA_Device_ID());
//            resp.setCodestatus("S");
//            if (isInsert == true)
//                resp.setMessage("Success insert data User Mobile");
//            else
//                resp.setMessage("Success update data User Mobile");
//            resp.setResultdata(xmap);
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//
//    public ResponseData getProfitCenter(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        sql.append(
//                "SELECT "
//                + "    cev.c_elementvalue_id, "
//                + "    cev.ad_client_id, "
//                + "    cev.ad_org_id, "
//                + "    cev.value, "
//                + "    cev.name, "
//                + "    cev.accounttype, "
//                + "    cev.description "
//                + "FROM "
//                + "    C_ElementValue cev "
//                + "WHERE "
//                + "    cev.IsActive = 'Y' "
//                + "    AND cev.IsSummary = 'N' "
//                + "    AND cev.C_Element_ID IN ( "
//                + "    SELECT "
//                + "        C_Element_ID "
//                + "    FROM "
//                + "        C_AcctSchema_Element ase "
//                + "    WHERE "
//                + "        ase.ElementType = 'U2' "
//                + "        AND ase.AD_Client_ID = ?)"
//                );
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        String name = "";
//        if (param.getValue() != null) {
//            sql.append(" AND cev.Value = ? ");
//            params.add(param.getValue());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            sql.append(" AND LOWER(cev.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("c_elementvalue_id", rs.getString("c_elementvalue_id"));
//                map.put("ad_org_id", rs.getString("ad_org_id"));
//                map.put("accounttype", rs.getString("accounttype"));
//                map.put("description", rs.getString("description"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getPaymentTerm(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        sql.append(
//                "SELECT "
//                    + "    pt.name, "
//                    + "    pt.value, "
//                    + "    pt.ad_client_id, "
//                    + "    pt.ad_org_id, "
//                    + "    pt.documentnote, "
//                    + "    pt.c_paymentterm_id, "
//                    + "    pt.description "
//                    + "FROM "
//                    + "    c_paymentterm pt "
//                    + "WHERE "
//                    + "    pt.ad_client_id = ?"
//                );
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        String name = "";
//        if (param.getValue() != null) {
//            sql.append(" AND pt.Value = ? ");
//            params.add(param.getValue());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            sql.append(" AND LOWER(pt.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("c_paymentterm_id", rs.getString("c_paymentterm_id"));
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("ad_org_id", rs.getString("ad_org_id"));
//                map.put("documentnote", rs.getString("documentnote"));
//                map.put("description", rs.getString("description"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getCostCenter(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        sql.append(
//                "SELECT "
//                + "    cev.c_elementvalue_id, "
//                + "    cev.ad_client_id, "
//                + "    cev.ad_org_id, "
//                + "    cev.value, "
//                + "    cev.name, "
//                + "    cev.accounttype, "
//                + "    cev.description "
//                + "FROM "
//                + "    C_ElementValue cev "
//                + "WHERE "
//                + "    cev.IsActive = 'Y' "
//                + "    AND cev.IsSummary = 'N' "
//                + "    AND cev.C_Element_ID IN ( "
//                + "    SELECT "
//                + "        C_Element_ID "
//                + "    FROM "
//                + "        C_AcctSchema_Element ase "
//                + "    WHERE "
//                + "        ase.ElementType = 'U1' "
//                + "        AND ase.AD_Client_ID = ?)"
//                );
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        String name = "";
//        if (param.getValue() != null) {
//            sql.append(" AND cev.Value = ? ");
//            params.add(param.getValue());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            sql.append(" AND LOWER(cev.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("c_elementvalue_id", rs.getString("c_elementvalue_id"));
//                map.put("ad_org_id", rs.getString("ad_org_id"));
//                map.put("accounttype", rs.getString("accounttype"));
//                map.put("description", rs.getString("description"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getPriorityRule(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    b.Value, " +
//                "    b.Name, " +
//                "    b.AD_Client_ID, " +
//                "    b.isActive, " +
//                "    a.Description, " +
//                "    b.AD_Ref_List_ID "
//                );
//        join.append(
//                "FROM AD_Reference a " + 
//                "LEFT JOIN AD_Ref_List b " + 
//                        "ON b.AD_Reference_ID = a.AD_Reference_ID ");
//        whereCond.append(
//                "WHERE LOWER(a.Name) like LOWER('%_PriorityRule%') " 
//                + " AND a.AD_Client_ID = 0 ");
//        //@formatter:on
//
//        String name = "";
//        if (param.getValue() != null) {
//            whereCond.append(" AND b.Value = ? ");
//            params.add(param.getValue());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(b.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("isactive", rs.getString("isactive"));
//                map.put("description", rs.getString("description"));
//                map.put("ad_ref_list_id", rs.getString("ad_ref_list_id"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getInvoiceRule(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    b.Value, " +
//                "    b.Name, " +
//                "    b.AD_Client_ID, " +
//                "    b.isActive, " +
//                "    a.Description, " +
//                "    b.AD_Ref_List_ID "
//                );
//        join.append(
//                "FROM AD_Reference a " + 
//                "LEFT JOIN AD_Ref_List b " + 
//                        "ON b.AD_Reference_ID = a.AD_Reference_ID ");
//        whereCond.append(
//                "WHERE LOWER(a.Name) like LOWER('%Order InvoiceRule%') " 
//                + " AND a.AD_Client_ID = 0 ");
//        //@formatter:on
//
//        String name = "";
//        if (param.getValue() != null) {
//            whereCond.append(" AND b.Value = ? ");
//            params.add(param.getValue());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(b.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("isactive", rs.getString("isactive"));
//                map.put("description", rs.getString("description"));
//                map.put("ad_ref_list_id", rs.getString("ad_ref_list_id"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getDeliveryRule(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    b.Value, " +
//                "    b.Name, " +
//                "    b.AD_Client_ID, " +
//                "    b.isActive, " +
//                "    a.Description, " +
//                "    b.AD_Ref_List_ID "
//                );
//        join.append(
//                "FROM AD_Reference a " + 
//                "LEFT JOIN AD_Ref_List b " + 
//                        "ON b.AD_Reference_ID = a.AD_Reference_ID ");
//        whereCond.append(
//                "WHERE LOWER(a.Name) like LOWER('%DeliveryRule%') " 
//                + " AND a.AD_Client_ID = 0 ");
//        //@formatter:on
//
//        String name = "";
//        if (param.getValue() != null) {
//            whereCond.append(" AND b.Value = ? ");
//            params.add(param.getValue());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(b.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("isactive", rs.getString("isactive"));
//                map.put("description", rs.getString("description"));
//                map.put("ad_ref_list_id", rs.getString("ad_ref_list_id"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getDeliveryViaRule(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    b.Value, " +
//                "    b.Name, " +
//                "    b.AD_Client_ID, " +
//                "    b.isActive, " +
//                "    a.Description, " +
//                "    b.AD_Ref_List_ID "
//                );
//        join.append(
//                "FROM AD_Reference a " + 
//                "LEFT JOIN AD_Ref_List b " + 
//                        "ON b.AD_Reference_ID = a.AD_Reference_ID ");
//        whereCond.append(
//                "WHERE LOWER(a.Name) like LOWER('%DeliveryViaRule%') " 
//                + " AND a.AD_Client_ID = 0 ");
//        //@formatter:on
//
//        String name = "";
//        if (param.getValue() != null) {
//            whereCond.append(" AND b.Value = ? ");
//            params.add(param.getValue());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(b.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("isactive", rs.getString("isactive"));
//                map.put("description", rs.getString("description"));
//                map.put("ad_ref_list_id", rs.getString("ad_ref_list_id"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getCategory(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "SELECT " + 
//                "    b.Value, " +
//                "    b.Name, " +
//                "    b.AD_Client_ID, " +
//                "    b.isActive, " +
//                "    a.Description, " +
//                "    b.AD_Ref_List_ID "
//                );
//        join.append(
//                "FROM AD_Reference a " + 
//                "LEFT JOIN AD_Ref_List b " + 
//                        "ON b.AD_Reference_ID = a.AD_Reference_ID ");
//        whereCond.append(
//                "WHERE a.Name like '%_CategorySO%' " 
//                + " AND a.AD_Client_ID = 0 ");
//        //@formatter:on
//
//        String name = "";
//        if (param.getValue() != null) {
//            whereCond.append(" AND b.Value = ? ");
//            params.add(param.getValue());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            whereCond.append(" AND LOWER(b.Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("isactive", rs.getString("isactive"));
//                map.put("description", rs.getString("description"));
//                map.put("ad_ref_list_id", rs.getString("ad_ref_list_id"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getDocBaseType(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        sql.append(
//                " SELECT " + 
//                "    Value, " +
//                "    Name, " +
//                "    isActive, " +
//                "    Description " +
//                " FROM AD_Ref_List " + 
//                " WHERE AD_Reference_ID = 183 " + 
//                "    AND AD_Client_ID = 0 ");
//        //@formatter:on
//
//        String name = "";
//        if (param.getValue() != null) {
//            sql.append(" AND Value = ? ");
//            params.add(param.getValue());
//        }
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            sql.append(" AND LOWER(Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("value", rs.getString("value"));
//                map.put("name", rs.getString("name"));
//                map.put("isactive", rs.getString("isactive"));
//                map.put("description", rs.getString("description"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData getDocType(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new ResponseData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        sql.append(
//                " SELECT "
//                + "  C_DocType_ID, "
//                + "  AD_Client_ID, " +
//                "    Name, "
//                + "  PrintName, " +
//                "    DocBaseType, " +
//                "    Description, "
//                + "  IsSOTrx, "
//                + "  DocSubTypeSO " +
//                " FROM C_DocType " + 
//                " WHERE AD_Client_ID = ? ");
//        //@formatter:on
//
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        String name = "";
//        if (!Util.isEmpty(param.getName())) {
//            name = "%" + param.getName() + "%";
//            sql.append(" AND LOWER(Name) LIKE LOWER(?) ");
//            params.add(name);
//        }
//        if (param.getC_doctype_id() != null) {
//            sql.append(" AND C_DocType_ID = ? ");
//            params.add(param.getC_doctype_id());
//        }
//        if (!Util.isEmpty(param.getDocbasetype())) {
//            sql.append(" AND DocBaseType = ? ");
//            params.add(param.getDocbasetype());
//        }
//        if (!Util.isEmpty(param.getIssotrx())) {
//            sql.append(" AND IsSOTrx = ? ");
//            params.add(param.getIssotrx());
//        }
//        if (!Util.isEmpty(param.getDocsubtypeso())) {
//            if (param.getDocsubtypeso().equals("Y")) {
//                sql.append(" AND DocSubTypeSO = ? ");
//            } else {
//                sql.append(" AND (DocSubTypeSO <> ? OR DocSubTypeSO ISNULL)");
//            }
//            params.add(MDocType.DOCSUBTYPESO_ReturnMaterial);
//        }
//
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("c_doctype_id", rs.getString("c_doctype_id"));
//                map.put("ad_client_id", rs.getString("ad_client_id"));
//                map.put("name", rs.getString("name"));
//                map.put("printname", rs.getString("printname"));
//                map.put("issotrx", rs.getString("issotrx"));
//                map.put("docsubtypeso", rs.getString("docsubtypeso"));
//                map.put("docbasetype", rs.getString("docbasetype"));
//                map.put("description", rs.getString("description"));
//
//                listdata.add(map);
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
//
//    public ResponseData deleteUserMobile(ParamRequest param, Trx trx) throws Exception {
//        ResponseData resp = new ResponseData();
//        String trxName = trx.getTrxName();
//
//        MFORCAUserMobile um = null;
//
//        try {
//            String whereClause = " Forca_Device_ID = ? ";
//            um = new Query(Env.getCtx(), MFORCAUserMobile.Table_Name, whereClause, null)
//                    .setParameters(param.getForca_device_id()).first();
//
//            if (um == null) {
//                resp = ResponseData.errorResponse("No Data User Mobile Found");
//            } else {
//                Map<String, Object> xmap = new LinkedHashMap<>();
//                xmap.put("forca_user_mobile_id", um.get_ID());
//                xmap.put("forca_platform", um.getFORCA_Platform());
//                xmap.put("forca_device_id", um.getFORCA_Device_ID());
//                xmap.put("forca_model", um.getFORCA_Model());
//                xmap.put("forca_manufaktur", um.getFORCA_Manufaktur());
//                xmap.put("forca_version", um.getFORCA_Version());
//
//                um.deleteEx(true, trxName);
//
//                resp = ResponseData.successResponse("Delete User Mobile Succeeded", xmap);
//            }
//        } catch (AdempiereException e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new AdempiereException(e);
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//            throw new Exception(e);
//        }
//        return resp;
//    }
//    
//    public ResponseData getCharge(ParamRequest param) {
//        List<Object> params = new ArrayList<>();
//
//        StringBuilder sql = new StringBuilder();
//        StringBuilder whereCond = new StringBuilder();
//        StringBuilder select = new StringBuilder();
//        StringBuilder join = new StringBuilder();
//
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ResponseData resp = new PaginationData();
//        List<Object> listdata = new ArrayList<Object>();
//        LinkedHashMap<String, Object> map;
//
//        //@formatter:off
//        select.append(
//                "WITH cte AS (" +
//                "SELECT " + 
//                "    a.c_charge_id, " + 
//                "    a.name, " + 
//                "    a.description "
//                );
//        join.append(
//                "FROM c_charge a ");
//        whereCond.append(
//                "WHERE ( " + 
//                "a.C_Charge_ID IN ( " + 
//                "    SELECT  " + 
//                "        c.C_Charge_ID  " + 
//                "    FROM C_Charge c " + 
//                "    JOIN C_ChargeType ct ON (ct.C_ChargeType_ID = c.C_ChargeType_ID) " + 
//                "    JOIN C_ChargeType_Doctype ctd ON (ctd.C_ChargeType_ID = ct.C_ChargeType_ID) " + 
//                "    JOIN  C_DocType dt ON (dt.C_DocType_ID =ctd.C_DocType_ID) " + 
//                "    WHERE  ctd.C_DocType_ID = ? " + 
//                "    ) OR " + 
//                "    (SELECT  " + 
//                "        COUNT(*)  " + 
//                "    FROM C_ChargeType_DocType  " + 
//                "    WHERE AD_Client_ID=?) = 0) " + 
//                "AND a.ad_client_id = ? " +
//                "AND a.isactive = 'Y' ");
//        //@formatter:on
//        
//        params.add(param.getC_doctype_id());
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        params.add(Env.getAD_Client_ID(Env.getCtx()));
//        
//        whereCond.append("ORDER BY a.Name ");
//
//        sql.append(select.toString());
//        sql.append(join.toString());
//        sql.append(whereCond.toString());
//
//      //@formatter:off
//        sql.append(""
//            + ") "
//            + "SELECT * "
//            + "FROM  ( "
//            + "   TABLE  cte "
//            + "   ORDER  BY Name "
//            + NATIVE_MARKER 
//            + "LIMIT ?"
//            + NATIVE_MARKER
//            + "   OFFSET ? "
//            + "   ) sub "
//            + "RIGHT  JOIN (SELECT count(*) FROM cte) c(full_count) ON true "
//            );
//        //@formatter:on
//
//        Integer perPage = 25;
//        Integer page = 1;
//
//        if (param.getPerpage() != null) {
//            perPage = param.getPerpage();
//        }
//
//        if (param.getPage() != null) {
//            page = param.getPage();
//        }
//
//        params.add(perPage);
//        params.add((page - 1) * perPage);
//        
//        try {
//            ps = DB.prepareStatement(sql.toString(), null);
//            ps = ProcessUtil.setMultiParam(ps, params);
//            if (ps.getConnection().getAutoCommit()) {
//                ps.getConnection().setAutoCommit(false);
//            }
//            ps.setFetchSize(100);
//            rs = ps.executeQuery();
//            boolean first = true;
//            int totaldata = 0;
//
//            while (rs.next()) {
//                map = new LinkedHashMap<String, Object>();
//                map.put("c_charge_id", rs.getString("c_charge_id"));
//                map.put("name", rs.getString("name"));
//                map.put("description", rs.getString("description"));
//                
//                listdata.add(map);
//                
//                if (first) {
//                    totaldata = rs.getInt("full_count");
//                    map = new LinkedHashMap<String, Object>();
//                    map.put("perpage", perPage);
//                    map.put("page", page);
//                    map.put("totaldata", totaldata);
//                    ((PaginationData) resp).setPagination(map);
//                    first = false;
//                }
//            }
//            resp.setCodestatus("S");
//            resp.setMessage(listdata.size() + " Data Found");
//            resp.setResultdata(listdata);
//
//        } catch (SQLException e) {
//            log.log(Level.SEVERE, sql.toString(), e);
//            resp = ResponseData.errorResponse(e.getMessage());
//        } catch (Exception e) {
//            resp = ResponseData.errorResponse(e.getMessage());
//        } finally {
//            DB.close(rs, ps);
//            ps = null;
//            rs = null;
//        }
//        return resp;
//    }
    
}
