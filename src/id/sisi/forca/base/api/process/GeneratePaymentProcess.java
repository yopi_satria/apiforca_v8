/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                               
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                  
   Subject        : Generate Payment                                 
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MOrder;
import org.compiere.model.MPayment;
import org.compiere.model.X_C_Payment;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.wf.MWorkflow;
//import org.idempiere.model.ForcaObject;
import id.sisi.forca.base.api.model.MFORCAPOSOrderRequest;
import id.sisi.forca.base.api.model.MFORCAPOSPayment;

public class GeneratePaymentProcess extends SvrProcess {
    private int AD_Org_ID = 0, C_BankAccount_ID = 0, C_Currency_ID = 0;
    private Trx localTrx = null;
    private StringBuilder logSucceed =
            new StringBuilder("Generated Payment (Search Key) : ");
    private StringBuilder logFailed =
            new StringBuilder("Not Generated Payment (ref pos payment id) : ");
    private StringBuilder sql = new StringBuilder();
    
    @Override
    protected void prepare() {
        // @formatter:off
        sql.append(""
                + "SELECT "
                + "    FORCA_POS_Payment_ID, "
                + "    FORCA_POS_Order_Request_ID "
                + "FROM "
                + "    FORCA_POS_Payment, "
                + "    FORCA_POS_Order_Request, "
                + "    T_Selection "
                + "WHERE "
                + "    FORCA_POS_Payment.FORCA_POS_Payment_ID = T_Selection.T_Selection_ID "
                + "    AND FORCA_POS_Payment.ref_pos_order_id = FORCA_POS_Order_Request.ref_pos_order_id"
                + "    AND T_Selection.AD_PInstance_ID =?");
        // @formatter:on

        ProcessInfoParameter[] para = getParameter();
        for (int i = 0; i < para.length; i++) {
            String name = para[i].getParameterName();
            if (para[i].getParameter() == null)
                ;
            else if (name.equals("AD_Org_ID"))
                AD_Org_ID = para[i].getParameterAsInt();
            else if (name.equals("C_BankAccount_ID"))
                C_BankAccount_ID = para[i].getParameterAsInt();
            else if (name.equals("C_Currency_ID"))
                C_Currency_ID = para[i].getParameterAsInt();
            else
                log.log(Level.SEVERE, "Unknown Parameter: " + name);
        }
    }

    @Override
    protected String doIt() throws Exception {
        PreparedStatement statement = null;
        localTrx = Trx.get(Trx.createTrxName(getName()), false);
        try {
            statement = DB.prepareStatement(sql.toString(), localTrx.getTrxName());
            statement.setInt(1, getAD_PInstance_ID());
        } catch (Exception e) {
            throw new AdempiereException(e.getMessage());
        }
        return generate(statement);
    }

    private String generate(PreparedStatement pstmt) {
        ResultSet rs = null;
//        try {
//            rs = pstmt.executeQuery();
//            int counter = 0;
//            while (rs.next()) {
//                counter++;
//                String ref_pos_payment_id = "";
//                try {
//                    MFORCAPOSPayment paid =
//                            ForcaObject.getObject(Env.getCtx(), MFORCAPOSPayment.Table_Name,
//                                    rs.getInt(MFORCAPOSPayment.COLUMNNAME_FORCA_POS_Payment_ID),
//                                    localTrx.getTrxName());
//                    ref_pos_payment_id = paid.getRef_POS_Payment_ID();
//                    MFORCAPOSOrderRequest order = ForcaObject.getObject(Env.getCtx(),
//                            MFORCAPOSOrderRequest.Table_Name,
//                            rs.getInt(MFORCAPOSOrderRequest.COLUMNNAME_FORCA_POS_Order_Request_ID),
//                            localTrx.getTrxName());
//    
//                    MOrder sOrder = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name,
//                            order.getC_Order_ID(), localTrx.getTrxName());
//                    int C_Invoice_ID = sOrder.getC_Invoice_ID();
//    
//                    MPayment sPayment =
//                            ForcaObject.getObject(Env.getCtx(), MPayment.Table_Name, 0, localTrx.getTrxName());
//                    sPayment.setAD_Org_ID(AD_Org_ID);
//                    sPayment.setC_BPartner_ID(sOrder.getC_BPartner_ID());
//                    sPayment.setBankAccountDetails(C_BankAccount_ID);
//    
//                    if (C_Invoice_ID != 0) {
//                        sPayment.setC_Invoice_ID(C_Invoice_ID);
//                        BigDecimal payAmt = paid.getPayAmt();
//                        sPayment.setPayAmt(payAmt);
//                        sPayment.setIsOverUnderPayment(true);
//                        // hitung C_AllocationLine
//                        // @formatter:off
//                        sql = new StringBuilder();
//                        sql.append(""
//                           + "SELECT "
//                           + "    SUM( amount ) AS totalamount "
//                           + "FROM "
//                           + "    c_allocationline "
//                           + "WHERE "
//                           + "    c_invoice_id = ?");
//                        // @formatter:on
//                        BigDecimal totalamount =
//                                DB.getSQLValueBD(localTrx.getTrxName(), sql.toString(), C_Invoice_ID);
//                        if (totalamount != null)
//                            sPayment.setOverUnderAmt(
//                                    sOrder.getTotalLines().subtract(totalamount).subtract(payAmt));
//                        else
//                            sPayment.setOverUnderAmt(sOrder.getTotalLines().subtract(payAmt));
//                    } else {
//                        sPayment.setC_Order_ID(order.getC_Order_ID());
//                        // pada sales order, PayAmt harus sesuai nilai grand total. tidak dapat on
//                        // Credit
//                        sPayment.setPayAmt(sOrder.getGrandTotal());
//                    }
//                    sPayment.setC_Currency_ID(C_Currency_ID);
//                    sPayment.setDocumentNo("POS" + paid.getRef_POS_Payment_ID());
//                    sPayment.setTenderType(X_C_Payment.TENDERTYPE_Account);
//                    sPayment.saveEx();
//                    MWorkflow.runDocumentActionWorkflow(sPayment, DocAction.ACTION_Complete);
//                    
//                    paid.setC_Payment_ID(sPayment.getC_Payment_ID());
//                    paid.setIsCreated("Y");
//                    paid.saveEx();
//                    
//                    logSucceed.append(sPayment.getDocumentNo() + " ");
//                } catch (Exception e) {
//                    logFailed.append(ref_pos_payment_id + " ");
//                    localTrx.rollback();
//                    continue;
//                }
//                if (counter % 1 == 0) {
//                    localTrx.commit();
//                }
//            }
//            return logSucceed.toString() + " | " + logFailed.toString();
//        } catch (Exception e) {
//            localTrx.rollback();
//            throw new AdempiereException(e);
//        } finally {
//            if (localTrx.isActive())
//                localTrx.commit();
//            localTrx.close();
//            DB.close(rs, pstmt);
//            rs = null;
//            pstmt = null;
//        }
        return null;
    }


}
