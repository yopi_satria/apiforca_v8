/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                               
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                  
   Subject        : Forca Intgration Pos                                 
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MLocation;
import org.compiere.model.X_C_BPartner;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
//import org.idempiere.model.ForcaObject;
import id.sisi.forca.base.api.model.MFORCACustomerPOS;

public class GenerateBusinessPartner extends SvrProcess {
    private int BP_Group_ID = 0;
    private Trx localTrx = null;
    private StringBuilder logSucceed =
            new StringBuilder("Generated Business Partner (Search Key) : ");
    private StringBuilder logFailed =
            new StringBuilder("Not Generated Business Partner (pos customer id) : ");
    private StringBuilder sql = new StringBuilder();

    @Override
    protected void prepare() {
        // @formatter:off
        sql.append(""
                + "SELECT "
                + "    FORCA_CustomerPOS_ID "
                + "FROM "
                + "    FORCA_CustomerPOS, "
                + "    T_Selection "
                + "WHERE "
                + "    FORCA_CustomerPOS.FORCA_CustomerPOS_ID = T_Selection.T_Selection_ID "
                + "    AND T_Selection.AD_PInstance_ID =?");
        // @formatter:on

        ProcessInfoParameter[] para = getParameter();
        String name = para[0].getParameterName();
        if (para[0].getParameter() == null)
            ;
        else if (name.equals("C_BP_Group_ID"))
            BP_Group_ID = para[0].getParameterAsInt();
        else
            log.log(Level.SEVERE, "Unknown Parameter: " + name);
    }

    @Override
    protected String doIt() throws Exception {
        PreparedStatement statement = null;
        localTrx = Trx.get(Trx.createTrxName(getName()), false);
        try {
            statement = DB.prepareStatement(sql.toString(), localTrx.getTrxName());
            statement.setInt(1, getAD_PInstance_ID());
        } catch (Exception e) {
            throw new AdempiereException(e.getMessage());
        }
        return generate(statement);
    }

    private String generate(PreparedStatement pstmt) {
        ResultSet rs = null;
//        try {
//            rs = pstmt.executeQuery();
//            int counter = 0;
//            while (rs.next()) {
//                counter++;
//                String pos_customer_id = "";
//                try {
//                    MFORCACustomerPOS cust =
//                            ForcaObject.getObject(Env.getCtx(), MFORCACustomerPOS.Table_Name,
//                                    rs.getInt(MFORCACustomerPOS.COLUMNNAME_FORCA_CustomerPOS_ID),
//                                    localTrx.getTrxName());
//                    pos_customer_id = cust.getpos_customer_id();
//                    MBPartner bpartner = ForcaObject.getObject(Env.getCtx(), MBPartner.Table_Name,
//                            0, localTrx.getTrxName());
//                    bpartner.setName(cust.getName());
//                    bpartner.setAD_Org_ID(Env.getAD_Org_ID(getCtx()));
//                    bpartner.setSalesRep_ID(Env.getAD_User_ID(getCtx()));
//                    if (cust.getSO_CreditLimit() != Env.ZERO) {
//                        bpartner.setSOCreditStatus(X_C_BPartner.SOCREDITSTATUS_CreditOK);
//                        bpartner.setSO_CreditLimit(cust.getSO_CreditLimit());
//                    } else {
//                        bpartner.setSOCreditStatus(X_C_BPartner.SOCREDITSTATUS_NoCreditCheck);
//                    }
//
//                    bpartner.setC_BP_Group_ID(BP_Group_ID);
//                    bpartner.set_ValueOfColumn("SAP_Code", cust.getSAP_Code());
//                    bpartner.set_ValueOfColumn("ID_Customer", cust.getID_Customer());
//                    bpartner.set_ValueOfColumn("pos_customer_id", cust.getpos_customer_id());
//                    bpartner.setValue("POS" + cust.getpos_customer_id());
//                    bpartner.saveEx();
//
//                    cust.setIsCreated("Y");
//                    cust.setC_BPartner_ID(bpartner.get_ID());
//                    cust.saveEx();
//
//                    MLocation loc = ForcaObject.getObject(Env.getCtx(), MLocation.Table_Name, 0,
//                            localTrx.getTrxName());
//                    loc.setAddress1(cust.getAddress_Customer());
//                    loc.setAD_Org_ID(Env.getAD_Org_ID(getCtx()));
//                    loc.saveEx();
//
//                    MBPartnerLocation bploc = ForcaObject.getObject(Env.getCtx(),
//                            MBPartnerLocation.Table_Name, 0, localTrx.getTrxName());
//                    bploc.setAD_Org_ID(Env.getAD_Org_ID(getCtx()));
//                    bploc.setC_BPartner_ID(bpartner.getC_BPartner_ID());
//                    bploc.setName(cust.getAddress_Customer());
//                    bploc.setC_Location_ID(loc.getC_Location_ID());
//                    bploc.setPhone(cust.getPhone());
//                    bploc.saveEx();
//
//                    logSucceed.append(bpartner.getValue() + " ");
//                } catch (Exception e) {
//                    logFailed.append(pos_customer_id + " ");
//                    localTrx.rollback();
//                    continue;
//                }
//                if (counter % 1 == 0) {
//                    localTrx.commit();
//                }
//            }
//            return logSucceed.toString() + " | " + logFailed.toString();
//        } catch (Exception e) {
//            localTrx.rollback();
//            throw new AdempiereException(e);
//        } finally {
//            if (localTrx.isActive())
//                localTrx.commit();
//            localTrx.close();
//            DB.close(rs, pstmt);
//            rs = null;
//            pstmt = null;
//        }
        return null;
    }
}
