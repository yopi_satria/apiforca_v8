/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                               
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                  
   Subject        : Forca Intgration Pos                                 
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MConversionType;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.wf.MWorkflow;
//import org.idempiere.model.ForcaObject;
import id.sisi.forca.base.api.model.MFORCAPOSOrderRequest;

public class GenerateSalesOrder extends SvrProcess {
    private int AD_ORG_ID = 0, M_PriceList_ID = 0, C_DocType_ID = 0, M_Warehouse_ID = 0;
    private String Multiple_Price;
    private Trx localTrx = null;
    private StringBuilder logSucceed = new StringBuilder("Generated Sales Order (Search Key) : ");
    private StringBuilder logFailed =
            new StringBuilder("Not Generated Sales Order (pos customer id) : ");
    private StringBuilder sql = new StringBuilder();

    @Override
    protected void prepare() {
        // @formatter:off
        sql.append(""
                + "SELECT "
                + "    FORCA_POS_Order_Request_ID "
                + "FROM "
                + "    FORCA_POS_Order_Request, "
                + "    T_Selection "
                + "WHERE "
                + "    FORCA_POS_Order_Request.FORCA_POS_Order_Request_ID = T_Selection.T_Selection_ID "
                + "    AND T_Selection.AD_PInstance_ID =?");
        // @formatter:on

        ProcessInfoParameter[] para = getParameter();
        localTrx = Trx.get(Trx.createTrxName(getName()), false);
        for (int i = 0; i < para.length; i++) {
            String name = para[i].getParameterName();
            if (para[i].getParameter() == null)
                ;
            else if (name.equals("AD_Org_ID"))
                AD_ORG_ID = para[i].getParameterAsInt();
            else if (name.equals("M_Warehouse_ID"))
                M_Warehouse_ID = para[i].getParameterAsInt();
            else if (name.equals("C_DocType_ID"))
                C_DocType_ID = para[i].getParameterAsInt();
            else if (name.equals("M_PriceList_ID"))
                M_PriceList_ID = para[i].getParameterAsInt();
            else if (name.equals("Multiple_Price"))
                Multiple_Price = para[i].getParameterAsString();
            else
                log.log(Level.SEVERE, "Unknown Parameter: " + name);
        }
    }

    @Override
    protected String doIt() throws Exception {
        PreparedStatement statement = null;
        try {
            statement = DB.prepareStatement(sql.toString(), localTrx.getTrxName());
            statement.setInt(1, getAD_PInstance_ID());
        } catch (Exception e) {
            throw new AdempiereException(e.getMessage());
        }
        return generate(statement);
    }


    private String generate(PreparedStatement pstmt) {
        ResultSet rs = null;
//        try {
//            rs = pstmt.executeQuery();
//            int counter = 0;
//            while (rs.next()) {
//                counter++;
//                String ref_pos_order_id = "";
//                try {
//                    MFORCAPOSOrderRequest cust = ForcaObject.getObject(Env.getCtx(),
//                            MFORCAPOSOrderRequest.Table_Name,
//                            rs.getInt(MFORCAPOSOrderRequest.COLUMNNAME_FORCA_POS_Order_Request_ID),
//                            localTrx.getTrxName());
//                    ref_pos_order_id = cust.getRef_POS_Order_ID();
//                    MOrder sOrder = ForcaObject.getObject(Env.getCtx(), MOrder.Table_Name, 0,
//                            localTrx.getTrxName());
//                    sOrder.setAD_Org_ID(AD_ORG_ID);
//                    sOrder.setIsSOTrx(cust.isSOTrx());
//                    sOrder.setC_DocTypeTarget_ID(C_DocType_ID);
//                    sOrder.setC_ConversionType_ID(MConversionType.TYPE_SPOT);
//                    sOrder.setBPartner(MBPartner.get(Env.getCtx(), cust.getC_BPartner_ID()));
//                    sOrder.setM_Warehouse_ID(M_Warehouse_ID);
//                    sOrder.setM_PriceList_ID(M_PriceList_ID);
//                    sOrder.setSalesRep_ID(Env.getAD_User_ID(getCtx()));
//                    sOrder.setDateAcct(cust.getDateOrdered());
//                    sOrder.setDateOrdered(cust.getDateOrdered());
//                    sOrder.setDatePromised(cust.getDateOrdered());
//                    sOrder.saveEx();
//
//                    String[] M_Product_ID = cust.getMultiple_M_Product_ID().split("/");
//                    String[] Price = Multiple_Price.split("/");
//                    String[] Qty = cust.getMultiple_Qty().split("/");
//
//                    if (M_Product_ID.length != Price.length) {
//                        logFailed.append(ref_pos_order_id + "(PriceDifferentProduct) ");
//                        localTrx.rollback();
//                        continue;
//                    }
//                    if (M_Product_ID.length != Qty.length) {
//                        logFailed.append(ref_pos_order_id + "(QtyDifferentProduct) ");
//                        localTrx.rollback();
//                        continue;
//                    }
//
//                    for (int i = 0; i < M_Product_ID.length; i++) {
//                        generateOrderLine(sOrder, M_Product_ID[i], Price[i], Qty[i]);
//                    }
//
//                    MWorkflow.runDocumentActionWorkflow(sOrder, DocAction.ACTION_Complete);
//                    cust.setC_Order_ID(sOrder.getC_Order_ID());
//                    cust.setIsCreated("Y");
//                    cust.saveEx();
//
//                    logSucceed.append(sOrder.getDocumentNo() + " ");
//                } catch (Exception e) {
//                    logFailed
//                            .append(ref_pos_order_id + " failed, because " + e.getMessage() + ". ");
//                    localTrx.rollback();
//                    continue;
//                }
//                if (counter % 1 == 0) {
//                    localTrx.commit();
//                }
//            }
//            return logSucceed.toString() + " | " + logFailed.toString();
//        } catch (Exception e) {
//            localTrx.rollback();
//            throw new AdempiereException(e);
//        } finally {
//            if (localTrx.isActive())
//                localTrx.commit();
//            localTrx.close();
//            DB.close(rs, pstmt);
//            rs = null;
//            pstmt = null;
//            sql = null;
//        }
        return null;
    }

    private void generateOrderLine(MOrder sOrder, String mProductID, String price, String qty) {
//        MProduct sProduct = ForcaObject.getObject(Env.getCtx(), MProduct.Table_Name,
//                Integer.parseInt(mProductID), localTrx.getTrxName());
//        MOrderLine sOrderLine = ForcaObject.getObject(Env.getCtx(), MOrderLine.Table_Name, 0,
//                localTrx.getTrxName());
//        // sOrderLine.setForcaOrder(sOrder);
//        if (sOrder.get_ID() == 0)
//            throw new IllegalArgumentException("Header not saved");
//        sOrderLine.setC_Order_ID(sOrder.getC_Order_ID()); // parent
//        sOrderLine.setOrder(sOrder);
//        sOrderLine.setProduct(sProduct);
//        StringBuilder sql2 = new StringBuilder();
//        if (price.equals("-")) {
//            try {
//                // ambil dari price list
//                // @formatter:off
//                sql2.append(""
//                    + "SELECT "
//                    + "    mpp.pricestd "
//                    + "FROM "
//                    + "    m_productprice mpp "
//                    + "LEFT JOIN m_pricelist_version mplv ON "
//                    + "    mpp.m_pricelist_version_id = mplv.m_pricelist_version_id "
//                    + "WHERE "
//                    + "    mpp.m_product_id =? "
//                    + "    AND mplv.m_pricelist_id =?");
//                // @formatter:on
//
//                BigDecimal pricestd = DB.getSQLValueBD(localTrx.getTrxName(), sql2.toString(),
//                        Integer.parseInt(mProductID), M_PriceList_ID);
//                sOrderLine.setPrice(pricestd);
//            } catch (Exception e) {
//                throw new AdempiereException(e);
//            } finally {
//                sql2 = null;
//            }
//        } else {
//            sOrderLine.setPriceActual(new BigDecimal(price));
//            sOrderLine.setPriceEntered(new BigDecimal(price));
//        }
//        sOrderLine.setQty(new BigDecimal(qty));
//        sOrderLine.saveEx();
    }

}
