/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Nizar Adian                                                                                
   Email          : (1) nizar.adian.n@gmail.com                                  
                    (2) nizar.noviandy@sisi.id                                  
   Subject        :                             
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.interfacez;

public interface ForcaPerformExecution {
    
    public void execute();

}
