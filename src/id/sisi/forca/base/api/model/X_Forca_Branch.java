/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package id.sisi.forca.base.api.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for Forca_Branch
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_Forca_Branch extends PO implements I_Forca_Branch, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191111L;

    /** Standard Constructor */
    public X_Forca_Branch (Properties ctx, int Forca_Branch_ID, String trxName)
    {
      super (ctx, Forca_Branch_ID, trxName);
      /** if (Forca_Branch_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_Forca_Branch (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_Forca_Branch[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Forca_Branch_ID.
		@param Forca_Branch_ID 
		Forca_Branch_ID
	  */
	public void setForca_Branch_ID (int Forca_Branch_ID)
	{
		if (Forca_Branch_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Forca_Branch_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Forca_Branch_ID, Integer.valueOf(Forca_Branch_ID));
	}

	/** Get Forca_Branch_ID.
		@return Forca_Branch_ID
	  */
	public int getForca_Branch_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Forca_Branch_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Forca_Branch_UU.
		@param Forca_Branch_UU Forca_Branch_UU	  */
	public void setForca_Branch_UU (String Forca_Branch_UU)
	{
		set_Value (COLUMNNAME_Forca_Branch_UU, Forca_Branch_UU);
	}

	/** Get Forca_Branch_UU.
		@return Forca_Branch_UU	  */
	public String getForca_Branch_UU () 
	{
		return (String)get_Value(COLUMNNAME_Forca_Branch_UU);
	}

	/** Set Latitude.
		@param Latitude 
		Latitude
	  */
	public void setLatitude (BigDecimal Latitude)
	{
		set_Value (COLUMNNAME_Latitude, Latitude);
	}

	/** Get Latitude.
		@return Latitude
	  */
	public BigDecimal getLatitude () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Latitude);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Longitude.
		@param Longitude 
		Longitude
	  */
	public void setLongitude (BigDecimal Longitude)
	{
		set_Value (COLUMNNAME_Longitude, Longitude);
	}

	/** Get Longitude.
		@return Longitude
	  */
	public BigDecimal getLongitude () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Longitude);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}
}