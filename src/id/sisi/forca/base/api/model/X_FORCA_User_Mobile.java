/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package id.sisi.forca.base.api.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for FORCA_User_Mobile
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_FORCA_User_Mobile extends PO implements I_FORCA_User_Mobile, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190328L;

    /** Standard Constructor */
    public X_FORCA_User_Mobile (Properties ctx, int FORCA_User_Mobile_ID, String trxName)
    {
      super (ctx, FORCA_User_Mobile_ID, trxName);
      /** if (FORCA_User_Mobile_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_FORCA_User_Mobile (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_FORCA_User_Mobile[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Role getAD_Role() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Role)MTable.get(getCtx(), org.compiere.model.I_AD_Role.Table_Name)
			.getPO(getAD_Role_ID(), get_TrxName());	}

	/** Set Role.
		@param AD_Role_ID 
		Responsibility Role
	  */
	public void setAD_Role_ID (int AD_Role_ID)
	{
		if (AD_Role_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_AD_Role_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Role_ID, Integer.valueOf(AD_Role_ID));
	}

	/** Get Role.
		@return Responsibility Role
	  */
	public int getAD_Role_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Role_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Error Request.
		@param ErrorRequest 
		The Calculation Error of Request API
	  */
	public void setErrorRequest (BigDecimal ErrorRequest)
	{
		set_Value (COLUMNNAME_ErrorRequest, ErrorRequest);
	}

	/** Get Error Request.
		@return The Calculation Error of Request API
	  */
	public BigDecimal getErrorRequest () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ErrorRequest);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Expired Date.
		@param ExpiredDate Expired Date	  */
	public void setExpiredDate (Timestamp ExpiredDate)
	{
		set_Value (COLUMNNAME_ExpiredDate, ExpiredDate);
	}

	/** Get Expired Date.
		@return Expired Date	  */
	public Timestamp getExpiredDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ExpiredDate);
	}

	/** Set Device ID.
		@param FORCA_Device_ID Device ID	  */
	public void setFORCA_Device_ID (String FORCA_Device_ID)
	{
		set_Value (COLUMNNAME_FORCA_Device_ID, FORCA_Device_ID);
	}

	/** Get Device ID.
		@return Device ID	  */
	public String getFORCA_Device_ID () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_Device_ID);
	}

	/** Set Manufaktur.
		@param FORCA_Manufaktur Manufaktur	  */
	public void setFORCA_Manufaktur (String FORCA_Manufaktur)
	{
		set_Value (COLUMNNAME_FORCA_Manufaktur, FORCA_Manufaktur);
	}

	/** Get Manufaktur.
		@return Manufaktur	  */
	public String getFORCA_Manufaktur () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_Manufaktur);
	}

	/** Set Model.
		@param FORCA_Model Model	  */
	public void setFORCA_Model (String FORCA_Model)
	{
		set_Value (COLUMNNAME_FORCA_Model, FORCA_Model);
	}

	/** Get Model.
		@return Model	  */
	public String getFORCA_Model () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_Model);
	}

	/** Set Platform.
		@param FORCA_Platform Platform	  */
	public void setFORCA_Platform (String FORCA_Platform)
	{
		set_Value (COLUMNNAME_FORCA_Platform, FORCA_Platform);
	}

	/** Get Platform.
		@return Platform	  */
	public String getFORCA_Platform () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_Platform);
	}

	/** Set Forca Token.
		@param FORCA_Token Forca Token	  */
	public void setFORCA_Token (String FORCA_Token)
	{
		set_Value (COLUMNNAME_FORCA_Token, FORCA_Token);
	}

	/** Get Forca Token.
		@return Forca Token	  */
	public String getFORCA_Token () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_Token);
	}

	/** Set Topic Firebase.
		@param FORCA_Topic_Firebase Topic Firebase	  */
	public void setFORCA_Topic_Firebase (String FORCA_Topic_Firebase)
	{
		set_Value (COLUMNNAME_FORCA_Topic_Firebase, FORCA_Topic_Firebase);
	}

	/** Get Topic Firebase.
		@return Topic Firebase	  */
	public String getFORCA_Topic_Firebase () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_Topic_Firebase);
	}

	/** Set User Mobile.
		@param FORCA_User_Mobile_ID User Mobile	  */
	public void setFORCA_User_Mobile_ID (int FORCA_User_Mobile_ID)
	{
		if (FORCA_User_Mobile_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_FORCA_User_Mobile_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_FORCA_User_Mobile_ID, Integer.valueOf(FORCA_User_Mobile_ID));
	}

	/** Get User Mobile.
		@return User Mobile	  */
	public int getFORCA_User_Mobile_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_FORCA_User_Mobile_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set FORCA_User_Mobile_UU.
		@param FORCA_User_Mobile_UU FORCA_User_Mobile_UU	  */
	public void setFORCA_User_Mobile_UU (String FORCA_User_Mobile_UU)
	{
		set_Value (COLUMNNAME_FORCA_User_Mobile_UU, FORCA_User_Mobile_UU);
	}

	/** Get FORCA_User_Mobile_UU.
		@return FORCA_User_Mobile_UU	  */
	public String getFORCA_User_Mobile_UU () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_User_Mobile_UU);
	}

	/** Set Version.
		@param FORCA_Version Version	  */
	public void setFORCA_Version (String FORCA_Version)
	{
		set_Value (COLUMNNAME_FORCA_Version, FORCA_Version);
	}

	/** Get Version.
		@return Version	  */
	public String getFORCA_Version () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_Version);
	}

	/** Set Version App Code.
		@param FORCA_VersionAppCode Version App Code	  */
	public void setFORCA_VersionAppCode (String FORCA_VersionAppCode)
	{
		set_Value (COLUMNNAME_FORCA_VersionAppCode, FORCA_VersionAppCode);
	}

	/** Get Version App Code.
		@return Version App Code	  */
	public String getFORCA_VersionAppCode () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_VersionAppCode);
	}

	/** Set Version App Name.
		@param FORCA_VersionAppName Version App Name	  */
	public void setFORCA_VersionAppName (String FORCA_VersionAppName)
	{
		set_Value (COLUMNNAME_FORCA_VersionAppName, FORCA_VersionAppName);
	}

	/** Get Version App Name.
		@return Version App Name	  */
	public String getFORCA_VersionAppName () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_VersionAppName);
	}

	/** Set Hit.
		@param Hit 
		The Calculation Record of Request API 
	  */
	public void setHit (BigDecimal Hit)
	{
		set_Value (COLUMNNAME_Hit, Hit);
	}

	/** Get Hit.
		@return The Calculation Record of Request API 
	  */
	public BigDecimal getHit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Hit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (org.compiere.model.I_M_Warehouse)MTable.get(getCtx(), org.compiere.model.I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Success.
		@param Success Success	  */
	public void setSuccess (BigDecimal Success)
	{
		set_Value (COLUMNNAME_Success, Success);
	}

	/** Get Success.
		@return Success	  */
	public BigDecimal getSuccess () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Success);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}