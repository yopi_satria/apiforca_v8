/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package id.sisi.forca.base.api.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for FORCA_CustomerPOS
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_FORCA_CustomerPOS extends PO implements I_FORCA_CustomerPOS, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190328L;

    /** Standard Constructor */
    public X_FORCA_CustomerPOS (Properties ctx, int FORCA_CustomerPOS_ID, String trxName)
    {
      super (ctx, FORCA_CustomerPOS_ID, trxName);
      /** if (FORCA_CustomerPOS_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_FORCA_CustomerPOS (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 4 - System 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_FORCA_CustomerPOS[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Address_Customer.
		@param Address_Customer Address_Customer	  */
	public void setAddress_Customer (String Address_Customer)
	{
		set_Value (COLUMNNAME_Address_Customer, Address_Customer);
	}

	/** Get Address_Customer.
		@return Address_Customer	  */
	public String getAddress_Customer () 
	{
		return (String)get_Value(COLUMNNAME_Address_Customer);
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set FORCA_CustomerPOS_ID.
		@param FORCA_CustomerPOS_ID 
		ID for auto increament number of table
	  */
	public void setFORCA_CustomerPOS_ID (int FORCA_CustomerPOS_ID)
	{
		if (FORCA_CustomerPOS_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_FORCA_CustomerPOS_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_FORCA_CustomerPOS_ID, Integer.valueOf(FORCA_CustomerPOS_ID));
	}

	/** Get FORCA_CustomerPOS_ID.
		@return ID for auto increament number of table
	  */
	public int getFORCA_CustomerPOS_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_FORCA_CustomerPOS_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set FORCA_CustomerPOS_UU.
		@param FORCA_CustomerPOS_UU FORCA_CustomerPOS_UU	  */
	public void setFORCA_CustomerPOS_UU (String FORCA_CustomerPOS_UU)
	{
		set_Value (COLUMNNAME_FORCA_CustomerPOS_UU, FORCA_CustomerPOS_UU);
	}

	/** Get FORCA_CustomerPOS_UU.
		@return FORCA_CustomerPOS_UU	  */
	public String getFORCA_CustomerPOS_UU () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_CustomerPOS_UU);
	}

	/** Set ID Customer.
		@param ID_Customer ID Customer	  */
	public void setID_Customer (String ID_Customer)
	{
		set_ValueNoCheck (COLUMNNAME_ID_Customer, ID_Customer);
	}

	/** Get ID Customer.
		@return ID Customer	  */
	public String getID_Customer () 
	{
		return (String)get_Value(COLUMNNAME_ID_Customer);
	}

	/** IsCreated AD_Reference_ID=319 */
	public static final int ISCREATED_AD_Reference_ID=319;
	/** Yes = Y */
	public static final String ISCREATED_Yes = "Y";
	/** No = N */
	public static final String ISCREATED_No = "N";
	/** Set Records created.
		@param IsCreated Records created	  */
	public void setIsCreated (String IsCreated)
	{

		set_Value (COLUMNNAME_IsCreated, IsCreated);
	}

	/** Get Records created.
		@return Records created	  */
	public String getIsCreated () 
	{
		return (String)get_Value(COLUMNNAME_IsCreated);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Phone.
		@param Phone 
		Identifies a telephone number
	  */
	public void setPhone (String Phone)
	{
		set_ValueNoCheck (COLUMNNAME_Phone, Phone);
	}

	/** Get Phone.
		@return Identifies a telephone number
	  */
	public String getPhone () 
	{
		return (String)get_Value(COLUMNNAME_Phone);
	}

	/** Set pos_customer_id.
		@param pos_customer_id pos_customer_id	  */
	public void setpos_customer_id (String pos_customer_id)
	{
		set_Value (COLUMNNAME_pos_customer_id, pos_customer_id);
	}

	/** Get pos_customer_id.
		@return pos_customer_id	  */
	public String getpos_customer_id () 
	{
		return (String)get_Value(COLUMNNAME_pos_customer_id);
	}

	/** Set SAP Code.
		@param SAP_Code SAP Code	  */
	public void setSAP_Code (String SAP_Code)
	{
		set_ValueNoCheck (COLUMNNAME_SAP_Code, SAP_Code);
	}

	/** Get SAP Code.
		@return SAP Code	  */
	public String getSAP_Code () 
	{
		return (String)get_Value(COLUMNNAME_SAP_Code);
	}

	/** Set Credit Limit.
		@param SO_CreditLimit 
		Total outstanding invoice amounts allowed
	  */
	public void setSO_CreditLimit (BigDecimal SO_CreditLimit)
	{
		set_ValueNoCheck (COLUMNNAME_SO_CreditLimit, SO_CreditLimit);
	}

	/** Get Credit Limit.
		@return Total outstanding invoice amounts allowed
	  */
	public BigDecimal getSO_CreditLimit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SO_CreditLimit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}