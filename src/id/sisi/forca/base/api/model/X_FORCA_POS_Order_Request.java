/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package id.sisi.forca.base.api.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for FORCA_POS_Order_Request
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_FORCA_POS_Order_Request extends PO implements I_FORCA_POS_Order_Request, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190131L;

    /** Standard Constructor */
    public X_FORCA_POS_Order_Request (Properties ctx, int FORCA_POS_Order_Request_ID, String trxName)
    {
      super (ctx, FORCA_POS_Order_Request_ID, trxName);
      /** if (FORCA_POS_Order_Request_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_FORCA_POS_Order_Request (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 4 - System 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_FORCA_POS_Order_Request[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException
    {
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Order_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Payment getC_Payment() throws RuntimeException
    {
		return (org.compiere.model.I_C_Payment)MTable.get(getCtx(), org.compiere.model.I_C_Payment.Table_Name)
			.getPO(getC_Payment_ID(), get_TrxName());	}

	/** Set Payment.
		@param C_Payment_ID 
		Payment identifier
	  */
	public void setC_Payment_ID (int C_Payment_ID)
	{
		if (C_Payment_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Payment_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Payment_ID, Integer.valueOf(C_Payment_ID));
	}

	/** Get Payment.
		@return Payment identifier
	  */
	public int getC_Payment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Payment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date Ordered.
		@param DateOrdered 
		Date of Order
	  */
	public void setDateOrdered (Timestamp DateOrdered)
	{
		set_ValueNoCheck (COLUMNNAME_DateOrdered, DateOrdered);
	}

	/** Get Date Ordered.
		@return Date of Order
	  */
	public Timestamp getDateOrdered () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateOrdered);
	}

	/** Set FORCA_POS_Order_Request_ID.
		@param FORCA_POS_Order_Request_ID 
		ID for auto increament number of table
	  */
	public void setFORCA_POS_Order_Request_ID (int FORCA_POS_Order_Request_ID)
	{
		if (FORCA_POS_Order_Request_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_FORCA_POS_Order_Request_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_FORCA_POS_Order_Request_ID, Integer.valueOf(FORCA_POS_Order_Request_ID));
	}

	/** Get FORCA_POS_Order_Request_ID.
		@return ID for auto increament number of table
	  */
	public int getFORCA_POS_Order_Request_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_FORCA_POS_Order_Request_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set FORCA_POS_Order_Request_UU.
		@param FORCA_POS_Order_Request_UU FORCA_POS_Order_Request_UU	  */
	public void setFORCA_POS_Order_Request_UU (String FORCA_POS_Order_Request_UU)
	{
		set_Value (COLUMNNAME_FORCA_POS_Order_Request_UU, FORCA_POS_Order_Request_UU);
	}

	/** Get FORCA_POS_Order_Request_UU.
		@return FORCA_POS_Order_Request_UU	  */
	public String getFORCA_POS_Order_Request_UU () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_POS_Order_Request_UU);
	}

	/** IsCreated AD_Reference_ID=319 */
	public static final int ISCREATED_AD_Reference_ID=319;
	/** Yes = Y */
	public static final String ISCREATED_Yes = "Y";
	/** No = N */
	public static final String ISCREATED_No = "N";
	/** Set Records created.
		@param IsCreated Records created	  */
	public void setIsCreated (String IsCreated)
	{

		set_Value (COLUMNNAME_IsCreated, IsCreated);
	}

	/** Get Records created.
		@return Records created	  */
	public String getIsCreated () 
	{
		return (String)get_Value(COLUMNNAME_IsCreated);
	}

	/** IsGoodsReceived AD_Reference_ID=319 */
	public static final int ISGOODSRECEIVED_AD_Reference_ID=319;
	/** Yes = Y */
	public static final String ISGOODSRECEIVED_Yes = "Y";
	/** No = N */
	public static final String ISGOODSRECEIVED_No = "N";
	/** Set Goods received.
		@param IsGoodsReceived 
		The goods is received by customer
	  */
	public void setIsGoodsReceived (String IsGoodsReceived)
	{

		set_Value (COLUMNNAME_IsGoodsReceived, IsGoodsReceived);
	}

	/** Get Goods received.
		@return The goods is received by customer
	  */
	public String getIsGoodsReceived () 
	{
		return (String)get_Value(COLUMNNAME_IsGoodsReceived);
	}

	/** Set Paid.
		@param IsPaid 
		The document is paid
	  */
	public void setIsPaid (boolean IsPaid)
	{
		set_ValueNoCheck (COLUMNNAME_IsPaid, Boolean.valueOf(IsPaid));
	}

	/** Get Paid.
		@return The document is paid
	  */
	public boolean isPaid () 
	{
		Object oo = get_Value(COLUMNNAME_IsPaid);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Sales Transaction.
		@param IsSOTrx 
		This is a Sales Transaction
	  */
	public void setIsSOTrx (boolean IsSOTrx)
	{
		set_ValueNoCheck (COLUMNNAME_IsSOTrx, Boolean.valueOf(IsSOTrx));
	}

	/** Get Sales Transaction.
		@return This is a Sales Transaction
	  */
	public boolean isSOTrx () 
	{
		Object oo = get_Value(COLUMNNAME_IsSOTrx);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Multiple Product.
		@param Multiple_M_Product_ID 
		Multiple Product, Service, Item
	  */
	public void setMultiple_M_Product_ID (String Multiple_M_Product_ID)
	{
		set_Value (COLUMNNAME_Multiple_M_Product_ID, Multiple_M_Product_ID);
	}

	/** Get Multiple Product.
		@return Multiple Product, Service, Item
	  */
	public String getMultiple_M_Product_ID () 
	{
		return (String)get_Value(COLUMNNAME_Multiple_M_Product_ID);
	}

	/** Set Multiple Quantity.
		@param Multiple_Qty 
		Multiple Quantity
	  */
	public void setMultiple_Qty (String Multiple_Qty)
	{
		set_Value (COLUMNNAME_Multiple_Qty, Multiple_Qty);
	}

	/** Get Multiple Quantity.
		@return Multiple Quantity
	  */
	public String getMultiple_Qty () 
	{
		return (String)get_Value(COLUMNNAME_Multiple_Qty);
	}

	/** Set Ref_POS_Order_ID.
		@param Ref_POS_Order_ID Ref_POS_Order_ID	  */
	public void setRef_POS_Order_ID (String Ref_POS_Order_ID)
	{
		set_Value (COLUMNNAME_Ref_POS_Order_ID, Ref_POS_Order_ID);
	}

	/** Get Ref_POS_Order_ID.
		@return Ref_POS_Order_ID	  */
	public String getRef_POS_Order_ID () 
	{
		return (String)get_Value(COLUMNNAME_Ref_POS_Order_ID);
	}

	/** Set Ref_POS_Payment_ID.
		@param Ref_POS_Payment_ID Ref_POS_Payment_ID	  */
	public void setRef_POS_Payment_ID (String Ref_POS_Payment_ID)
	{
		set_Value (COLUMNNAME_Ref_POS_Payment_ID, Ref_POS_Payment_ID);
	}

	/** Get Ref_POS_Payment_ID.
		@return Ref_POS_Payment_ID	  */
	public String getRef_POS_Payment_ID () 
	{
		return (String)get_Value(COLUMNNAME_Ref_POS_Payment_ID);
	}

	/** Set Retail_ID.
		@param Retail_ID Retail_ID	  */
	public void setRetail_ID (String Retail_ID)
	{
		set_Value (COLUMNNAME_Retail_ID, Retail_ID);
	}

	/** Get Retail_ID.
		@return Retail_ID	  */
	public String getRetail_ID () 
	{
		return (String)get_Value(COLUMNNAME_Retail_ID);
	}
}