package id.sisi.forca.base.api.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MFORCAPOSPayment extends X_FORCA_POS_Payment {
    
    private static final long serialVersionUID = 1L;

    public MFORCAPOSPayment(Properties ctx, int FORCA_POS_Payment_ID, String trxName) {
        super(ctx, FORCA_POS_Payment_ID, trxName);
    }

    public MFORCAPOSPayment(Properties ctx, ResultSet rs, String trxName) {
        super(ctx, rs, trxName);
    }

}
