/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package id.sisi.forca.base.api.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for FORCA_User_Mobile
 *  @author iDempiere (generated) 
 *  @version Release 5.1
 */
@SuppressWarnings("all")
public interface I_FORCA_User_Mobile 
{

    /** TableName=FORCA_User_Mobile */
    public static final String Table_Name = "FORCA_User_Mobile";

    /** AD_Table_ID=1000024 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_Role_ID */
    public static final String COLUMNNAME_AD_Role_ID = "AD_Role_ID";

	/** Set Role.
	  * Responsibility Role
	  */
	public void setAD_Role_ID (int AD_Role_ID);

	/** Get Role.
	  * Responsibility Role
	  */
	public int getAD_Role_ID();

	public org.compiere.model.I_AD_Role getAD_Role() throws RuntimeException;

    /** Column name AD_User_ID */
    public static final String COLUMNNAME_AD_User_ID = "AD_User_ID";

	/** Set User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID);

	/** Get User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID();

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException;

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name ErrorRequest */
    public static final String COLUMNNAME_ErrorRequest = "ErrorRequest";

	/** Set Error Request.
	  * The Calculation Error of Request API
	  */
	public void setErrorRequest (BigDecimal ErrorRequest);

	/** Get Error Request.
	  * The Calculation Error of Request API
	  */
	public BigDecimal getErrorRequest();

    /** Column name ExpiredDate */
    public static final String COLUMNNAME_ExpiredDate = "ExpiredDate";

	/** Set Expired Date	  */
	public void setExpiredDate (Timestamp ExpiredDate);

	/** Get Expired Date	  */
	public Timestamp getExpiredDate();

    /** Column name FORCA_Device_ID */
    public static final String COLUMNNAME_FORCA_Device_ID = "FORCA_Device_ID";

	/** Set Device ID	  */
	public void setFORCA_Device_ID (String FORCA_Device_ID);

	/** Get Device ID	  */
	public String getFORCA_Device_ID();

    /** Column name FORCA_Manufaktur */
    public static final String COLUMNNAME_FORCA_Manufaktur = "FORCA_Manufaktur";

	/** Set Manufaktur	  */
	public void setFORCA_Manufaktur (String FORCA_Manufaktur);

	/** Get Manufaktur	  */
	public String getFORCA_Manufaktur();

    /** Column name FORCA_Model */
    public static final String COLUMNNAME_FORCA_Model = "FORCA_Model";

	/** Set Model	  */
	public void setFORCA_Model (String FORCA_Model);

	/** Get Model	  */
	public String getFORCA_Model();

    /** Column name FORCA_Platform */
    public static final String COLUMNNAME_FORCA_Platform = "FORCA_Platform";

	/** Set Platform	  */
	public void setFORCA_Platform (String FORCA_Platform);

	/** Get Platform	  */
	public String getFORCA_Platform();

    /** Column name FORCA_Token */
    public static final String COLUMNNAME_FORCA_Token = "FORCA_Token";

	/** Set Forca Token	  */
	public void setFORCA_Token (String FORCA_Token);

	/** Get Forca Token	  */
	public String getFORCA_Token();

    /** Column name FORCA_Topic_Firebase */
    public static final String COLUMNNAME_FORCA_Topic_Firebase = "FORCA_Topic_Firebase";

	/** Set Topic Firebase	  */
	public void setFORCA_Topic_Firebase (String FORCA_Topic_Firebase);

	/** Get Topic Firebase	  */
	public String getFORCA_Topic_Firebase();

    /** Column name FORCA_User_Mobile_ID */
    public static final String COLUMNNAME_FORCA_User_Mobile_ID = "FORCA_User_Mobile_ID";

	/** Set User Mobile	  */
	public void setFORCA_User_Mobile_ID (int FORCA_User_Mobile_ID);

	/** Get User Mobile	  */
	public int getFORCA_User_Mobile_ID();

    /** Column name FORCA_User_Mobile_UU */
    public static final String COLUMNNAME_FORCA_User_Mobile_UU = "FORCA_User_Mobile_UU";

	/** Set FORCA_User_Mobile_UU	  */
	public void setFORCA_User_Mobile_UU (String FORCA_User_Mobile_UU);

	/** Get FORCA_User_Mobile_UU	  */
	public String getFORCA_User_Mobile_UU();

    /** Column name FORCA_Version */
    public static final String COLUMNNAME_FORCA_Version = "FORCA_Version";

	/** Set Version	  */
	public void setFORCA_Version (String FORCA_Version);

	/** Get Version	  */
	public String getFORCA_Version();

    /** Column name FORCA_VersionAppCode */
    public static final String COLUMNNAME_FORCA_VersionAppCode = "FORCA_VersionAppCode";

	/** Set Version App Code	  */
	public void setFORCA_VersionAppCode (String FORCA_VersionAppCode);

	/** Get Version App Code	  */
	public String getFORCA_VersionAppCode();

    /** Column name FORCA_VersionAppName */
    public static final String COLUMNNAME_FORCA_VersionAppName = "FORCA_VersionAppName";

	/** Set Version App Name	  */
	public void setFORCA_VersionAppName (String FORCA_VersionAppName);

	/** Get Version App Name	  */
	public String getFORCA_VersionAppName();

    /** Column name Hit */
    public static final String COLUMNNAME_Hit = "Hit";

	/** Set Hit.
	  * The Calculation Record of Request API 
	  */
	public void setHit (BigDecimal Hit);

	/** Get Hit.
	  * The Calculation Record of Request API 
	  */
	public BigDecimal getHit();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name M_Warehouse_ID */
    public static final String COLUMNNAME_M_Warehouse_ID = "M_Warehouse_ID";

	/** Set Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID);

	/** Get Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID();

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException;

    /** Column name Success */
    public static final String COLUMNNAME_Success = "Success";

	/** Set Success	  */
	public void setSuccess (BigDecimal Success);

	/** Get Success	  */
	public BigDecimal getSuccess();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
