package id.sisi.forca.base.api.model;

public interface I_AD_User extends org.compiere.model.I_AD_User {
    /** Column name FORCA_Person_ID */
    public static final String COLUMNNAME_FORCA_Person_ID = "FORCA_Person_ID";

    /** Column name Forca_DeviceID */
    public static final String COLUMNNAME_Forca_DeviceID = "Forca_DeviceID";

    /** Set FORCA_Person_ID   */
    public void setFORCA_Person_ID (String FORCA_Person_ID);

    /** Get FORCA_Person_ID   */
    public String getFORCA_Person_ID();
    
    public void setForca_DeviceID (String Forca_DeviceID);

    /** Get Device ID */
    public String getForca_DeviceID();


}
