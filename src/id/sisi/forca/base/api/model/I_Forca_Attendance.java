/******************************************************************************
#20588
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package id.sisi.forca.base.api.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for Forca_Attendance
 *  @author iDempiere (generated) 
 *  @version Release 5.1
 */
@SuppressWarnings("all")
public interface I_Forca_Attendance 
{

    /** TableName=Forca_Attendance */
    public static final String Table_Name = "Forca_Attendance";

    /** AD_Table_ID=1000114 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name Forca_Attendance_ID */
    public static final String COLUMNNAME_Forca_Attendance_ID = "Forca_Attendance_ID";

	/** Set Forca_Attendance_ID.
	  * Forca_Attendance_ID
	  */
	public void setForca_Attendance_ID (int Forca_Attendance_ID);

	/** Get Forca_Attendance_ID.
	  * Forca_Attendance_ID
	  */
	public int getForca_Attendance_ID();

    /** Column name Forca_Attendance_UU */
    public static final String COLUMNNAME_Forca_Attendance_UU = "Forca_Attendance_UU";

	/** Set Forca_Attendance_UU	  */
	public void setForca_Attendance_UU (String Forca_Attendance_UU);

	/** Get Forca_Attendance_UU	  */
	public String getForca_Attendance_UU();

    /** Column name Forca_Branch_ID */
    public static final String COLUMNNAME_Forca_Branch_ID = "Forca_Branch_ID";

	/** Set Forca_Branch_ID.
	  * Forca_Branch_ID
	  */
	public void setForca_Branch_ID (int Forca_Branch_ID);

	/** Get Forca_Branch_ID.
	  * Forca_Branch_ID
	  */
	public int getForca_Branch_ID();

	public I_Forca_Branch getForca_Branch() throws RuntimeException;

    /** Column name Forca_DeviceID */
    public static final String COLUMNNAME_Forca_DeviceID = "Forca_DeviceID";

	/** Set Device ID.
	  * Device ID
	  */
	public void setForca_DeviceID (String Forca_DeviceID);

	/** Get Device ID.
	  * Device ID
	  */
	public String getForca_DeviceID();

    /** Column name Forca_DeviceModel */
    public static final String COLUMNNAME_Forca_DeviceModel = "Forca_DeviceModel";

	/** Set Device Model.
	  * Device Model
	  */
	public void setForca_DeviceModel (String Forca_DeviceModel);

	/** Get Device Model.
	  * Device Model
	  */
	public String getForca_DeviceModel();

    /** Column name FORCA_Distance */
    public static final String COLUMNNAME_FORCA_Distance = "FORCA_Distance";

	/** Set Distance.
	  * Distance
	  */
	public void setFORCA_Distance (BigDecimal FORCA_Distance);

	/** Get Distance.
	  * Distance
	  */
	public BigDecimal getFORCA_Distance();

    /** Column name Forca_Location */
    public static final String COLUMNNAME_Forca_Location = "Forca_Location";

	/** Set Location.
	  * Location
	  */
	public void setForca_Location (String Forca_Location);

	/** Get Location.
	  * Location
	  */
	public String getForca_Location();

    /** Column name ImageURL */
    public static final String COLUMNNAME_ImageURL = "ImageURL";

	/** Set Image URL.
	  * URL of  image
	  */
	public void setImageURL (String ImageURL);

	/** Get Image URL.
	  * URL of  image
	  */
	public String getImageURL();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Latitude */
    public static final String COLUMNNAME_Latitude = "Latitude";

	/** Set Latitude.
	  * Latitude
	  */
	public void setLatitude (BigDecimal Latitude);

	/** Get Latitude.
	  * Latitude
	  */
	public BigDecimal getLatitude();

    /** Column name Longitude */
    public static final String COLUMNNAME_Longitude = "Longitude";

	/** Set Longitude.
	  * Longitude
	  */
	public void setLongitude (BigDecimal Longitude);

	/** Get Longitude.
	  * Longitude
	  */
	public BigDecimal getLongitude();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Status */
    public static final String COLUMNNAME_Status = "Status";

	/** Set Status.
	  * Status of the currently running check
	  */
	public void setStatus (String Status);

	/** Get Status.
	  * Status of the currently running check
	  */
	public String getStatus();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();
}
