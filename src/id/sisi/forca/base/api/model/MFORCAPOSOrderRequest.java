package id.sisi.forca.base.api.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MFORCAPOSOrderRequest extends X_FORCA_POS_Order_Request{

    private static final long serialVersionUID = 1L;

    public MFORCAPOSOrderRequest(Properties ctx, int FORCA_POS_Order_Request_ID, String trxName) {
		super(ctx, FORCA_POS_Order_Request_ID, trxName);
	}
	
	public MFORCAPOSOrderRequest(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}



}
