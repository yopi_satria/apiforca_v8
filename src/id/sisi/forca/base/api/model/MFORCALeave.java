package id.sisi.forca.base.api.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;

public class MFORCALeave extends X_Forca_Leave implements DocAction {
    private static final long serialVersionUID = -5960379002382911168L;

    public MFORCALeave(Properties ctx, ResultSet rs, String trxName) {
        super(ctx, rs, trxName);
    }

    public MFORCALeave(Properties ctx, int Forca_Leave_ID, String trxName) {
        super(ctx, Forca_Leave_ID, trxName);
    }

    @Override
    public boolean processIt(String action) throws Exception {
        log.warning(
                "Processing Action=" + action + " - DocStatus=" + getDocStatus() + " - DocAction=" + getDocAction());
        DocumentEngine engine = new DocumentEngine(this, getDocStatus());
        return engine.processIt(action, getDocAction());
    }

    @Override
    public boolean unlockIt() {
        return false;
    }

    @Override
    public boolean invalidateIt() {
        setDocAction(DOCACTION_Prepare);
        return true;
    }

    @Override
    public String prepareIt() {
        return DocAction.STATUS_InProgress;
    }

    @Override
    public boolean approveIt() {
        setIsApproved(true);
        return true;
    }

    @Override
    public boolean rejectIt() {
        setIsApproved(false);
        return true;
    }

    @Override
    public String completeIt() {
        setProcessed(true);
        return DocAction.STATUS_Completed;
    }

    @Override
    public boolean voidIt() {
        setProcessed(true);
        setDocAction(DOCACTION_None);
        return true;
    }

    @Override
    public boolean closeIt() {
        return true;
    }

    @Override
    public boolean reverseCorrectIt() {
        return false;
    }

    @Override
    public boolean reverseAccrualIt() {
        return false;
    }

    @Override
    public boolean reActivateIt() {
        return false;
    }

    @Override
    public String getSummary() {
        return null;
    }

    @Override
    public String getDocumentInfo() {
        return null;
    }

    @Override
    public File createPDF() {
        return null;
    }

    @Override
    public String getProcessMsg() {
        return null;
    }

    @Override
    public int getDoc_User_ID() {
        return 0;
    }

    @Override
    public int getC_Currency_ID() {
        return 0;
    }

    @Override
    public BigDecimal getApprovalAmt() {
        return null;
    }

}
