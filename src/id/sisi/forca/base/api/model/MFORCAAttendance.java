package id.sisi.forca.base.api.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MFORCAAttendance extends X_Forca_Attendance {
    private static final long serialVersionUID = -6584142846424958874L;

    public MFORCAAttendance(Properties ctx, int Forca_Attendance_ID, String trxName) {
        super(ctx, Forca_Attendance_ID, trxName);
    }

    public MFORCAAttendance(Properties ctx, ResultSet rs, String trxName) {
        super(ctx, rs, trxName);
    }

    public void setClient(int AD_Client_ID) {
        setAD_Client_ID(AD_Client_ID);
    }
    
    public void setOrg(int AD_Org_ID) {
        setAD_Org_ID(AD_Org_ID);
    }
    
}
