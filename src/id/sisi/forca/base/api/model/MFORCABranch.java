package id.sisi.forca.base.api.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MFORCABranch extends X_Forca_Branch {
    private static final long serialVersionUID = -933406353028926441L;

    public MFORCABranch(Properties ctx, int Forca_Branch_ID, String trxName) {
        super(ctx, Forca_Branch_ID, trxName);
    }

    public MFORCABranch(Properties ctx, ResultSet rs, String trxName) {
        super(ctx, rs, trxName);
    }

}
