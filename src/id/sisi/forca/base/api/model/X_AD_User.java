package id.sisi.forca.base.api.model;

import java.sql.ResultSet;
import java.util.Properties;

public class X_AD_User extends org.compiere.model.X_AD_User implements I_AD_User{
    private static final long serialVersionUID = -3661172174580911010L;
    
    public X_AD_User(Properties ctx, int AD_User_ID, String trxName) {
        super(ctx, AD_User_ID, trxName);
    }

    public X_AD_User(Properties ctx, ResultSet rs, String trxName) {
        super(ctx, rs, trxName);
    }
    
    /** Set FORCA_Person_ID.
    @param FORCA_Person_ID FORCA_Person_ID    */
    public void setFORCA_Person_ID (String FORCA_Person_ID)
    {
        set_Value (COLUMNNAME_FORCA_Person_ID, FORCA_Person_ID);
    }

    @Override
    public String getFORCA_Person_ID() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setForca_DeviceID(String Forca_DeviceID) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getForca_DeviceID() {
        // TODO Auto-generated method stub
        return null;
    }


}
