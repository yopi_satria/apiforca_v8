/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package id.sisi.forca.base.api.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for Forca_Attendance
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_Forca_Attendance extends PO implements I_Forca_Attendance, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191204L;

    /** Standard Constructor */
    public X_Forca_Attendance (Properties ctx, int Forca_Attendance_ID, String trxName)
    {
      super (ctx, Forca_Attendance_ID, trxName);
      /** if (Forca_Attendance_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_Forca_Attendance (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_Forca_Attendance[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Forca_Attendance_ID.
		@param Forca_Attendance_ID 
		Forca_Attendance_ID
	  */
	public void setForca_Attendance_ID (int Forca_Attendance_ID)
	{
		if (Forca_Attendance_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Forca_Attendance_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Forca_Attendance_ID, Integer.valueOf(Forca_Attendance_ID));
	}

	/** Get Forca_Attendance_ID.
		@return Forca_Attendance_ID
	  */
	public int getForca_Attendance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Forca_Attendance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Forca_Attendance_UU.
		@param Forca_Attendance_UU Forca_Attendance_UU	  */
	public void setForca_Attendance_UU (String Forca_Attendance_UU)
	{
		set_Value (COLUMNNAME_Forca_Attendance_UU, Forca_Attendance_UU);
	}

	/** Get Forca_Attendance_UU.
		@return Forca_Attendance_UU	  */
	public String getForca_Attendance_UU () 
	{
		return (String)get_Value(COLUMNNAME_Forca_Attendance_UU);
	}

	public I_Forca_Branch getForca_Branch() throws RuntimeException
    {
		return (I_Forca_Branch)MTable.get(getCtx(), I_Forca_Branch.Table_Name)
			.getPO(getForca_Branch_ID(), get_TrxName());	}

	/** Set Forca_Branch_ID.
		@param Forca_Branch_ID 
		Forca_Branch_ID
	  */
	public void setForca_Branch_ID (int Forca_Branch_ID)
	{
		if (Forca_Branch_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Forca_Branch_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Forca_Branch_ID, Integer.valueOf(Forca_Branch_ID));
	}

	/** Get Forca_Branch_ID.
		@return Forca_Branch_ID
	  */
	public int getForca_Branch_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Forca_Branch_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Device ID.
		@param Forca_DeviceID 
		Device ID
	  */
	public void setForca_DeviceID (String Forca_DeviceID)
	{
		set_Value (COLUMNNAME_Forca_DeviceID, Forca_DeviceID);
	}

	/** Get Device ID.
		@return Device ID
	  */
	public String getForca_DeviceID () 
	{
		return (String)get_Value(COLUMNNAME_Forca_DeviceID);
	}

	/** Set Device Model.
		@param Forca_DeviceModel 
		Device Model
	  */
	public void setForca_DeviceModel (String Forca_DeviceModel)
	{
		set_Value (COLUMNNAME_Forca_DeviceModel, Forca_DeviceModel);
	}

	/** Get Device Model.
		@return Device Model
	  */
	public String getForca_DeviceModel () 
	{
		return (String)get_Value(COLUMNNAME_Forca_DeviceModel);
	}

	/** Set Distance.
		@param FORCA_Distance 
		Distance
	  */
	public void setFORCA_Distance (BigDecimal FORCA_Distance)
	{
		set_Value (COLUMNNAME_FORCA_Distance, FORCA_Distance);
	}

	/** Get Distance.
		@return Distanced
	  */
	public BigDecimal getFORCA_Distance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FORCA_Distance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Location.
		@param Forca_Location 
		Location
	  */
	//#20588
	public void setForca_Location (String Forca_Location)
	{
		set_Value (COLUMNNAME_Forca_Location, Forca_Location);
	}

	/** Get Location.
		@return Location
	  */
	public String getForca_Location () 
	{
		return (String)get_Value(COLUMNNAME_Forca_Location);
	}

	/** Set Image URL.
		@param ImageURL 
		URL of  image
	  */
	public void setImageURL (String ImageURL)
	{
		set_ValueNoCheck (COLUMNNAME_ImageURL, ImageURL);
	}

	/** Get Image URL.
		@return URL of  image
	  */
	public String getImageURL () 
	{
		return (String)get_Value(COLUMNNAME_ImageURL);
	}

	/** Set Latitude.
		@param Latitude 
		Latitude
	  */
	public void setLatitude (BigDecimal Latitude)
	{
		set_Value (COLUMNNAME_Latitude, Latitude);
	}

	/** Get Latitude.
		@return Latitude
	  */
	public BigDecimal getLatitude () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Latitude);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Longitude.
		@param Longitude 
		Longitude
	  */
	public void setLongitude (BigDecimal Longitude)
	{
		set_Value (COLUMNNAME_Longitude, Longitude);
	}

	/** Get Longitude.
		@return Longitude
	  */
	public BigDecimal getLongitude () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Longitude);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Check In = in */
	public static final String STATUS_CheckIn = "in";
	/** Check Out = out */
	public static final String STATUS_CheckOut = "out";
	/** Set Status.
		@param Status 
		Status of the currently running check
	  */
	public void setStatus (String Status)
	{

		set_Value (COLUMNNAME_Status, Status);
	}

	/** Get Status.
		@return Status of the currently running check
	  */
	public String getStatus () 
	{
		return (String)get_Value(COLUMNNAME_Status);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}