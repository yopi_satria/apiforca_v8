/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package id.sisi.forca.base.api.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for FORCA_POS_Payment
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_FORCA_POS_Payment extends PO implements I_FORCA_POS_Payment, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181126L;

    /** Standard Constructor */
    public X_FORCA_POS_Payment (Properties ctx, int FORCA_POS_Payment_ID, String trxName)
    {
      super (ctx, FORCA_POS_Payment_ID, trxName);
      /** if (FORCA_POS_Payment_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_FORCA_POS_Payment (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 4 - System 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_FORCA_POS_Payment[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Payment getC_Payment() throws RuntimeException
    {
		return (org.compiere.model.I_C_Payment)MTable.get(getCtx(), org.compiere.model.I_C_Payment.Table_Name)
			.getPO(getC_Payment_ID(), get_TrxName());	}

	/** Set Payment.
		@param C_Payment_ID 
		Payment identifier
	  */
	public void setC_Payment_ID (int C_Payment_ID)
	{
		if (C_Payment_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Payment_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Payment_ID, Integer.valueOf(C_Payment_ID));
	}

	/** Get Payment.
		@return Payment identifier
	  */
	public int getC_Payment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Payment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set FORCA_POS_Payment_ID.
		@param FORCA_POS_Payment_ID 
		ID for auto increament number of table
	  */
	public void setFORCA_POS_Payment_ID (int FORCA_POS_Payment_ID)
	{
		if (FORCA_POS_Payment_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_FORCA_POS_Payment_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_FORCA_POS_Payment_ID, Integer.valueOf(FORCA_POS_Payment_ID));
	}

	/** Get FORCA_POS_Payment_ID.
		@return ID for auto increament number of table
	  */
	public int getFORCA_POS_Payment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_FORCA_POS_Payment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set FORCA_POS_Payment_UU.
		@param FORCA_POS_Payment_UU FORCA_POS_Payment_UU	  */
	public void setFORCA_POS_Payment_UU (String FORCA_POS_Payment_UU)
	{
		set_Value (COLUMNNAME_FORCA_POS_Payment_UU, FORCA_POS_Payment_UU);
	}

	/** Get FORCA_POS_Payment_UU.
		@return FORCA_POS_Payment_UU	  */
	public String getFORCA_POS_Payment_UU () 
	{
		return (String)get_Value(COLUMNNAME_FORCA_POS_Payment_UU);
	}

	/** IsCreated AD_Reference_ID=319 */
	public static final int ISCREATED_AD_Reference_ID=319;
	/** Yes = Y */
	public static final String ISCREATED_Yes = "Y";
	/** No = N */
	public static final String ISCREATED_No = "N";
	/** Set Records created.
		@param IsCreated Records created	  */
	public void setIsCreated (String IsCreated)
	{

		set_Value (COLUMNNAME_IsCreated, IsCreated);
	}

	/** Get Records created.
		@return Records created	  */
	public String getIsCreated () 
	{
		return (String)get_Value(COLUMNNAME_IsCreated);
	}

	/** Set Payment amount.
		@param PayAmt 
		Amount being paid
	  */
	public void setPayAmt (BigDecimal PayAmt)
	{
		set_ValueNoCheck (COLUMNNAME_PayAmt, PayAmt);
	}

	/** Get Payment amount.
		@return Amount being paid
	  */
	public BigDecimal getPayAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Ref_POS_Order_ID.
		@param Ref_POS_Order_ID Ref_POS_Order_ID	  */
	public void setRef_POS_Order_ID (String Ref_POS_Order_ID)
	{
		set_Value (COLUMNNAME_Ref_POS_Order_ID, Ref_POS_Order_ID);
	}

	/** Get Ref_POS_Order_ID.
		@return Ref_POS_Order_ID	  */
	public String getRef_POS_Order_ID () 
	{
		return (String)get_Value(COLUMNNAME_Ref_POS_Order_ID);
	}

	/** Set Ref_POS_Payment_ID.
		@param Ref_POS_Payment_ID Ref_POS_Payment_ID	  */
	public void setRef_POS_Payment_ID (String Ref_POS_Payment_ID)
	{
		set_Value (COLUMNNAME_Ref_POS_Payment_ID, Ref_POS_Payment_ID);
	}

	/** Get Ref_POS_Payment_ID.
		@return Ref_POS_Payment_ID	  */
	public String getRef_POS_Payment_ID () 
	{
		return (String)get_Value(COLUMNNAME_Ref_POS_Payment_ID);
	}
}