package id.sisi.forca.base.api.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MFORCACustomerPOS extends X_FORCA_CustomerPOS{
	
	private static final long serialVersionUID = 1L;
	
	public MFORCACustomerPOS(Properties ctx, int FORCA_CustomerPOS_ID, String trxName) {
		super(ctx, FORCA_CustomerPOS_ID, trxName);
	}
	
	public MFORCACustomerPOS(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
