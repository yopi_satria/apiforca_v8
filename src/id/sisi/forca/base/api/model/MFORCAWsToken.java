package id.sisi.forca.base.api.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MFORCAWsToken extends X_FORCA_WS_Token {

    private static final long serialVersionUID = 1L;

    public MFORCAWsToken(Properties ctx, ResultSet rs, String trxName) {
        super(ctx, rs, trxName);
    }

    public MFORCAWsToken(Properties ctx, int FORCA_WS_Token_ID, String trxName) {
        super(ctx, FORCA_WS_Token_ID, trxName);
    }

}
