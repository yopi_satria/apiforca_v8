/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : Response Data                                
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.response;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Properties;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.compiere.util.Util;
import id.sisi.forca.base.api.filter.AuthenticationApi;
import id.sisi.forca.base.api.util.Constants;

@XmlRootElement(name = "responsedata")
public class ResponseData {
	Object resultdata = new ArrayList<Object>();
	        
	//E=Error or S=Sukses
	String codestatus = "E";

	String message = "Failed Error Found !!";
	
	@XmlElement(name = "codestatus")
	public String getCodestatus() {
		return codestatus;
	}

	public void setCodestatus(String codestatus) {
		this.codestatus = codestatus;
	}
		
	@XmlElement(name = "message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@XmlElement(name = "resultdata")
	public Object getResultdata() {
		return resultdata;
	}

	public void setResultdata(Object resultdata) {
		this.resultdata = resultdata;
	}
	
	public static ResponseData successResponse(String msg, Object maplist) {
        ResponseData respon = new ResponseData();

        respon.setCodestatus("S");
        respon.setMessage("Succeed, " + msg);
        respon.setResultdata(maplist);
        return respon;
    }

    public static ResponseData errorResponse(String msg, Object maplist) {
        ResponseData respon = new ResponseData();

        respon.setCodestatus("E");
        respon.setMessage("Error caused by : " + msg);
        respon.setResultdata(maplist);
        return respon;
    }
    
    public static ResponseData errorResponse(String msg) {
        ResponseData respon = new ResponseData();

        respon.setCodestatus("E");
        respon.setMessage("Error caused by : " + msg);
        return respon;
    }

    public static ResponseData parameterRequired() {
        return errorResponse(Constants.ERROR_ParameterRequired);
    }
    
    public static ResponseData cekToken(String token) {
        ResponseData result = new ResponseData();
        AuthenticationApi authAPI = new AuthenticationApi();
        
        if (Util.isEmpty(token)) {
            result = errorResponse("Please input header " + Constants.AUTH_FORCA_TOKEN);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = errorResponse("Wrong Token! "); 
            } else {
                result = successResponse("Valid Token", null);
            }
        }
        return result;
    }
    
    public static ResponseData inconsistent() {
        return errorResponse("Username or Password inconsistent");
    }
    
    public static ResponseData noConnection() {
        return errorResponse("No database connection");
    }
    
    public static Response finalResponse(ResponseData result) {
        AuthenticationApi.countErrorSukses(result);
        return Response.status(Response.Status.OK).entity(result).build();
    }
    
    public static String toRupiahFormat(String nominal) {
        String rupiah = "";
        if (!nominal.equals("")) {
            Locale locale = new Locale("ca", "CA");
            NumberFormat rupiahFormat = NumberFormat.getCurrencyInstance(locale);

            rupiah = rupiahFormat.format(Double.parseDouble(nominal)).substring(4);
        }
        return rupiah;
    }

    public static Properties getDefaultCtx() {
        Properties wsenv = new Properties();
        wsenv.setProperty("#AD_Language", "en_US");
        wsenv.setProperty("context", "deverp.semenindonesia.com/ws");
        return wsenv;
    }
}
