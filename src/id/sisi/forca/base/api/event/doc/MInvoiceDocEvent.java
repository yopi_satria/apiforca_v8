package id.sisi.forca.base.api.event.doc;

import org.adempiere.base.event.IEventTopics;
import org.compiere.model.PO;
import org.osgi.service.event.Event;
import id.sisi.forca.base.api.bispro.ForcaInvoiceAfterReverseUpdateStatusOdoo;

public class MInvoiceDocEvent {
    public static void validate(Event event, PO po) {
        switch (event.getTopic()) {
            case IEventTopics.DOC_AFTER_CLOSE:
                
                break;
            case IEventTopics.DOC_AFTER_COMPLETE:
                
                break;
            case IEventTopics.DOC_AFTER_POST:
                
                break;
            case IEventTopics.DOC_AFTER_PREPARE:
                
                break;
            case IEventTopics.DOC_AFTER_REACTIVATE:

                break;
            case IEventTopics.DOC_AFTER_REVERSEACCRUAL:
                forcaInvoiceAfterReverseUpdateStatusOdoo(po);
                
                break;
            case IEventTopics.DOC_AFTER_REVERSECORRECT:
                forcaInvoiceAfterReverseUpdateStatusOdoo(po);
                
                break;
            case IEventTopics.DOC_AFTER_VOID:

                break;
            case IEventTopics.DOC_ALL:

                break;
            case IEventTopics.DOC_BEFORE_CLOSE:

                break;
            case IEventTopics.DOC_BEFORE_COMPLETE:

                break;
            case IEventTopics.DOC_BEFORE_POST:

                break;
            case IEventTopics.DOC_BEFORE_PREPARE:

                break;
            case IEventTopics.DOC_BEFORE_REACTIVATE:

                break;
            case IEventTopics.DOC_BEFORE_REVERSEACCRUAL:

                break;
            case IEventTopics.DOC_BEFORE_REVERSECORRECT:

                break;
            case IEventTopics.DOC_BEFORE_VOID:

                break;

            default:
                break;
        }
    }
    
    private static void forcaInvoiceAfterReverseUpdateStatusOdoo(PO po) {
        ForcaInvoiceAfterReverseUpdateStatusOdoo forcaInvoiceAfterReverseUpdateStatusOdoo =
                new ForcaInvoiceAfterReverseUpdateStatusOdoo(po.getCtx(), po, po.get_TrxName());
        forcaInvoiceAfterReverseUpdateStatusOdoo.execute();
    }
}
