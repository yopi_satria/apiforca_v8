/*----------------------------------------------------------------------------
   Product        : Forca ERP Foundation                                      
   Author         : Budi Darmanto                                  
   Email          : budiskom93@gmail.com                                      
   Subject        : Process Util
   Issue          : 10416                                                       
  ---------------------------------------------------------------------------*/

package id.sisi.forca.base.api.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.DBException;
import org.compiere.model.MAttachment;
import org.compiere.model.MPInstance;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

public class ProcessUtil {

    public ProcessUtil() {

    }

    public static final PreparedStatement setMultiParam(PreparedStatement ps, List<Object> params)
            throws SQLException {
        int index = 0;
        for (Object param : params) {
            index++;
            if (param == null)
                ps.setObject(index, null);
            else if (param instanceof String)
                ps.setString(index, (String) param);
            else if (param instanceof Integer)
                ps.setInt(index, ((Integer) param).intValue());
            else if (param instanceof BigDecimal)
                ps.setBigDecimal(index, (BigDecimal) param);
            else if (param instanceof Timestamp)
                ps.setTimestamp(index, (Timestamp) param);
            else if (param instanceof Boolean)
                ps.setString(index, ((Boolean) param).booleanValue() ? "Y" : "N");
            else if (param instanceof byte[])
                ps.setBytes(index, (byte[]) param);
            else
                throw new DBException("Unknown parameter type " + index + " - " + param);
        }
        return ps;
    }

    public void generateReportList(List<LinkedHashMap<String, Object>> resultList, String trxName,
            String fileName, int ad_p_instance_id) throws IOException {
        int a = 0;
        File file = new File(fileName+".csv");
        FileWriter fileW = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fileW);
        List<String> listHeader = new ArrayList<>();
        int countFlush = 0;
        for (LinkedHashMap<String, Object> map : resultList) {
            countFlush++;
            a++;
            if (a == 1) {
                for (String key : map.keySet()) {
                    listHeader.add(key);
                }
                createHeaderMap(listHeader, bw);
            }

            for (int j = 0; j < listHeader.size(); ++j) {
                if (map.get(listHeader.get(j)) instanceof String) {
                    bw.write(map.get(listHeader.get(j)) != null ? "\""+map.get(listHeader.get(j)).toString()+"\"" : "");
                } else {
                    bw.write(map.get(listHeader.get(j)) != null ? map.get(listHeader.get(j)).toString() : "");
                }
                
                if (j != listHeader.size() - 1)
                    bw.write(",");
            }
            bw.newLine();
            if (countFlush % 100 == 0) {
                bw.flush();
            }
        }
        bw.flush();
        compressAndAttachIt(file, trxName, ad_p_instance_id);
    }

    public void generateReportSQL(String sql, List<Object> params, String fileName,
            int ad_p_instance_id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String trxLocalName = Trx.createTrxName("PROCUT");
        Trx localTrx = Trx.get(trxLocalName, true);
        try {
            ps = DB.prepareStatement(sql, trxLocalName);
            ps = ProcessUtil.setMultiParam(ps, params);
            ps.setFetchSize(100);
            rs = ps.executeQuery();
            ResultSetMetaData metadata = rs.getMetaData();
            int numColumns = metadata.getColumnCount();
            File file = new File(fileName+".csv");
            FileWriter fileW = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fileW);

            try {
                createHeaderDesc(metadata, numColumns, bw);

                int i = 0;
                while (rs.next()) {
                    ++i;
                    for (int j = 1; j <= numColumns; ++j) {
                        bw.write(rs.getObject(j) != null ? rs.getObject(j).toString() : "");
                        if (j != numColumns)
                            bw.write(",");
                    }
                    bw.newLine();

                    if (i % 100 == 0)
                        bw.flush();
                }
            } catch (Exception ex) {
                System.out.println(ex.getLocalizedMessage());

            } finally {
                bw.flush();
                bw.close();
                fileW.close();

                compressAndAttachIt(file, trxLocalName, ad_p_instance_id);
            }
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());

        } finally {
            localTrx.close();
            DB.close(rs, ps);
        }
    }

    private void compressAndAttachIt(File file, String trxName, int ad_p_instance_id)
            throws IOException {

        FileOutputStream fos = new FileOutputStream(trxName + ".zip");
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        FileInputStream fis = new FileInputStream(file);
        ZipEntry zipEntry = new ZipEntry(file.getName());
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024 * 10];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        zipOut.close();
        fis.close();
        fos.close();

        MAttachment att =
                new MAttachment(Env.getCtx(), MPInstance.Table_ID, ad_p_instance_id, null);
        att.addEntry(new File(trxName + ".zip"));
        att.save();
        
        file.delete();
        File zipDel = new File(trxName + ".zip");
        zipDel.delete();
    }

    private void createHeaderDesc(ResultSetMetaData metadata, int numColumns, BufferedWriter bw)
            throws SQLException, IOException {
        for (int j = 1; j <= numColumns; ++j) {
            String column_name = metadata.getColumnName(j);
            bw.write(column_name);
            if (j != numColumns)
                bw.write(",");
        }
        bw.newLine();
    }

    private void createHeaderMap(List<String> listHeader, BufferedWriter bw) throws IOException {
        for (int j = 0; j < listHeader.size(); ++j) {
            String column_name = listHeader.get(j);
            bw.write(column_name);
            if (j != listHeader.size() - 1)
                bw.write(",");
        }
        bw.newLine();
    }
    
    public List<Object> getListSingleObjectFromSQL(StringBuilder sql, List<Object> params){
        List<Object> listObject = new ArrayList<>(); 
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps = setMultiParam(ps, params);
            ps.setFetchSize(100);
            rs = ps.executeQuery();
            while (rs.next()) {
                listObject.add(rs.getObject(1));
            }
        } catch (Exception e) {
            throw new AdempiereException(e.getMessage());
        } finally {
            DB.close(rs, ps);
            ps = null;
            rs = null;
        }
        return listObject;
    }
}
